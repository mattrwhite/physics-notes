% ----------------------------------------------------------------
% Vector Calculus ***********************************************
% ----------------------------------------------------------------
\documentclass{article}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{booktabs}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
% MATH -----------------------------------------------------------
\input{D:/Physics/Notes/Include/Essentials.tex}
\input{D:/Physics/Notes/Include/Vectors.tex}
% ----------------------------------------------------------------
\begin{document}
% \maketitle
% ----------------------------------------------------------------
\begin{enumerate}[I.]
 \item \textbf{Coordinate Systems in $\mathbb{R}^3$.}

 \begin{enumerate}[A.]
 \item \emph{Spherical Coordinates.} In spherical coordinates,
 $r$ is the length of the position vector $\vect{r}$, $\theta$
 is the angle between $\vect{r}$ and the $z$ axis, and $\phi$ is
 the angle between the $x$ axis and the the projection of
 $\vect{r}$ onto the $x-y$ plane.  The corresponding unit vectors
 $\unitvect{r}$, $\unitvect{\theta}$, and $\unitvect{\phi}$ form a
 right-handed orthonormal basis of $\Real^3$, i.e.,
 \( \unitvect{r} \times \unitvect{\theta} = \unitvect{\phi}, \eqnsep
  \unitvect{\phi} \times \unitvect{r} = \unitvect{\theta}, \eqnsep
  \unitvect{\theta} \times \unitvect{\phi} = \unitvect{r} . \)
 \begin{enumerate}[1.]
  \item \emph{Coordinate Relations.}
  \begin{equation}
   \begin{split}
   x & = r \sin \theta \cos \phi \\
   y & = r \sin \theta \sin \phi \\
   z & = r \cos \theta
   \end{split}
  \end{equation}

  \begin{equation}
   \begin{split}
   \ihat & = (\sin \theta \cos \phi) \unitvect{r}
    + (\cos \theta \cos \phi) \unitvect{\theta} - (\sin \phi) \unitvect{\phi} \\
   \jhat & = (\sin \theta \sin \phi) \unitvect{r}
    + (\cos \theta \sin \phi) \unitvect{\theta} + (\cos \phi) \unitvect{\phi} \\
   \khat & = (\cos \theta) \unitvect{r} - (\cos \phi) \unitvect{\theta}
   \end{split}
  \end{equation}

  \begin{equation}
   \begin{split}
   r & = \sqrt{x^2 + y^2 + z^2} \\
   \theta & = \arctan \parenth{\sqrt{x^2 + y^2}/z} \\
   \phi & = \arctan ( y/x )
   \end{split}
  \end{equation}

  \begin{equation}
   \begin{split}
   \unitvect{r} & = (\sin \theta \cos \phi) \ihat
    + (\sin \theta \sin \phi) \jhat + (\cos \theta) \khat \\
   \unitvect{\theta} & = (\cos \theta \cos \phi) \ihat
    + (\cos \theta \sin \phi) \jhat - (\sin \theta) \khat \\
   \unitvect{\phi} & = (- \sin \phi) \ihat + (\cos \phi) \jhat
   \end{split}
  \end{equation}
  The relations between the unit vectors allow us to calculate
  quantities such as $\ihat \times \unitvect{r}$ and $\khat \cdot
  \unitvect{\theta}$ which frequently appear, especially in
  electrodynamics.  The quantities involving $\khat$ are used so
  often, they are worth noting here:
  \begin{equation}
   \begin{aligned}
   \khat \cdot \unitvect{r} &= \cos \theta
   &\khat \times \unitvect{r} &= \sin \theta \unitvect{\phi}  \\
   \khat \cdot \unitvect{\theta} &= \sin \theta
   &\khat \times \unitvect{\theta} &= \cos \theta \unitvect{\phi} \\
   \khat \cdot \unitvect{\phi} &= 0
   &\khat \times \unitvect{\phi} &= -\cos \phi \ihat -  \sin \phi \jhat
   \end{aligned}
  \end{equation}

  \item \emph{Infinitesimal Quantities.}
  Displacements, in $\unitvect{r}$, $\unitvect{\theta}$, and
  $\unitvect{\phi}$ directions, respectively:
  \begin{equation}
   \begin{split}
   dl_r & = dr \\
   dl_\theta & = r d\theta \\
   dl_\phi & = r \sin \theta d\phi
   \end{split}
  \end{equation}
  The general displacement is
  \begin{equation}
   \vect{dl} = dr \unitvect{r} + r d\theta \unitvect{\theta}
    + r \sin \theta d\phi \unitvect{\phi} .
  \end{equation}
  An infinitesimal area on the surface of a sphere is
  \begin{equation}
   \vect{da}_r = dl_\theta dl_\phi \unitvect{r} = r^2 \sin \theta
   d\theta d\phi \unitvect{r} .
  \end{equation}
  An infinitesimal area in the $x-y$ plane is
  \begin{equation}
   \vect{da}_\theta = dl_r dl_\phi \unitvect{\theta} = r dr
   d\phi \unitvect{r} .
  \end{equation}
  The infinitesimal volume is
  \begin{equation}
   d\Vol = dl_r dl_\theta dl_\phi = r^2 \sin \theta
   dr d\theta d\phi .
  \end{equation}
 \end{enumerate}

 \item \emph{Cylindrical Coordinates.} In cylindrical coordinates,
 $s$ is the distance from $\vect{r}$ to the $z$ axis, $\phi$ is
 the angle between the $x$ axis and the the projection of
 $\vect{r}$ onto the $x-y$ plane, and $z$ is the $z$ coordinate of
 $\vect{r}$. The corresponding unit vectors
 $\unitvect{s}$, $\unitvect{\phi}$, and $\unitvect{z}$ form a
 right-handed orthonormal basis of $\Real^3$, i.e.,
 \( \unitvect{s} \times \unitvect{\phi} = \unitvect{z}, \eqnsep
  \unitvect{\phi} \times \unitvect{z} = \unitvect{s}, \eqnsep
  \unitvect{z} \times \unitvect{s} = \unitvect{\phi} . \)
 \begin{enumerate}[1.]
  \item \emph{Coordinate Relations.}
  \begin{equation}
   \begin{split}
   x & = s \cos \phi \\
   y & = s \sin \phi \\
   z & = z
   \end{split}
  \end{equation}

  \begin{equation}
   \begin{split}
   \ihat & = (\cos \phi) \unitvect{s} - (\sin \phi) \unitvect{\phi} \\
   \jhat & = (\sin \phi) \unitvect{s} + (\cos \phi) \unitvect{\phi} \\
   \khat & = (\cos \theta) \unitvect{r} - (\cos \phi) \unitvect{\theta}
   \end{split}
  \end{equation}

  \begin{equation}
   \begin{split}
   s & = \sqrt{x^2 + y^2} \\
   \phi & = \arctan ( y/x ) \\
   z & = z
   \end{split}
  \end{equation}

  \begin{equation}
   \begin{split}
   \unitvect{s} & = (\cos \phi) \ihat + (\sin \phi) \jhat \\
   \unitvect{\phi} & = (- \sin \phi) \ihat + (\cos \phi) \jhat \\
   \unitvect{z} & = \khat
   \end{split}
  \end{equation}

  \item \emph{Infinitesimal Quantities.}
  Displacements, in $\unitvect{s}$, $\unitvect{\phi}$, and
  $\unitvect{z}$ directions, respectively:
  \begin{equation}
   \begin{split}
   dl_s & = ds \\
   dl_\phi & = s d\phi \\
   dl_z & = dz
   \end{split}
  \end{equation}
  The general displacement is
  \begin{equation}
   \vect{dl} = ds \unitvect{s} + s d\phi \unitvect{\phi}
    + dz \unitvect{z} .
  \end{equation}
  The infinitesimal volume is
  \begin{equation}
   d\Vol = dl_s dl_\phi dl_z = s ds d\phi dz .
  \end{equation}
 \end{enumerate}

\end{enumerate}

 \item \textbf{Vector Operations.}
 \begin{enumerate}[A.]
  \item \emph{Dot Product.} The dot, or scalar, product of two vectors
  $\vect{A} = a_1 e_1 + \ldots + a_n e_n$ and $\vect{B} =
  b_1 e_1 + \ldots + b_n e_n$ (where $e_1, \ldots, e_n$ is an
  orthonormal basis) is defined as
  \begin{equation}
   \dotpd{A}{B} =
    a_1 b_1 + \ldots + a_n b_n .
  \end{equation}
  The dot product is commutative, associative, and distributive,
  just like scalar multiplication.

  \item \emph{Geometric Meaning of the Dot Product.} The dot
  product $\dotpd{A}{B}$ is the magnitude of the component
  of $\vect{A}$ parallel to $\vect{B}$, which is equal to the
  magnitude of the component of $\vect{B}$ parallel to
  $\vect{A}$.  It is easy to show that an equivalent definition
  of the dot product is
  \begin{equation}
   \dotpd{A}{B} = A B \cos \theta ,
  \end{equation}
  where $\theta$ is the angle between $\vect{A}$ and $\vect{B}$
  (in their common plane).

  Clearly, $\dotpd{A}{A} = A^2$.  Now, let $\vect{C} = \vect{A} +
  \vect{B}$.  Then the magnitude of $\vect{C}$ is given by
  \begin{equation}
   C^2 = A^2 + B^2 + 2AB \cos \theta .
  \end{equation}
  Again, $\theta$ is the angle between $\vect{A}$ and $\vect{B}$.
  Note that this is just the law of cosines.

  The dot product of a vector with its derivative (\wrt some
  scalar) can be written as $\vect{A} \cdot \vect{A}' = \frac{1}{2}
  (A^2)'$.

  \item \emph{Cross Product.} The cross product of two vectors
  $\vect{A}$ and $\vect{B}$ is
  defined only on $\mathbb{R}^3$.  It produces a third vector
  which is orthogonal to both $\vect{A}$ and $\vect{B}$.  In
  Cartesian coordinates,
  \begin{equation}
   \crosspd{A}{B} = \left| \begin{array}{ccc}
   \ihat & \jhat & \khat \\
   a_x & a_y & a_z \\ b_x & b_y & b_z \end{array} \right|
  \end{equation}
  The cross product is associative and distributive, but is
  anti-commutative, i.e., $\crosspd{A}{B} = -\crosspd{B}{A}$

  \item \emph{Geometric Meaning of the Cross Product.} The direction
  of the vector produced by the cross product of $\vect{A}$
  and $\vect{B}$ is given by the right hand rule (rotate $\vect{A}$
  into $\vect{B}$ with your right hand).  Its magnitude is given
  by
  \begin{equation}
   \norm{\crosspd{A}{B}} = A B \sin \theta ,
  \end{equation}
  where $\theta$ is the angle between $\vect{A}$ and $\vect{B}$
  (in their common plane).

  \item \emph{Triple Products.} The standard vector triple
  product is
  \begin{equation}
   \vect{A} \cdot (\crosspd{B}{C}) = \vect{B} \cdot
   (\crosspd{C}{A}) = \vect{C} \cdot (\crosspd{A}{B}) .
  \end{equation}
  The following relation is known as the BAC-CAB rule:
  \begin{equation}
   \vect{A} \times (\crosspd{B}{C}) = \vect{B}
   (\dotpd{A}{C}) - \vect{C} (\dotpd{A}{B}) .
  \end{equation}

  \item \emph{The Levi-Civita Symbol.} The Levi-Civita symbol is
  defined as follows:
  \begin{equation}
   \epsilon_{ijk} = \left\{ \begin{array}{ll} 1 & \textrm{if $ijk
   = 123, 231, 312$} \\ -1 & \textrm{if $ijk = 132, 213, 321$} \\ 0 &
   \textrm{otherwise} \end{array} \right.
  \end{equation}
  In words, $\epsilon_{ijk}$ is $1$ if $ijk$ can be obtained from
  $123$ by swapping adjacent indexes an even number of times, $-1$
  for an odd number of times, and $0$ if any two indexes are the
  same.  From this, it follows that
  \begin{equation}
   \epsilon_{ijk} = \epsilon_{jki} = \epsilon_{kij} =
   -\epsilon_{ikj} = -\epsilon_{jik} = -\epsilon_{kji} .
  \end{equation}
  The Levi-Civita symbol is very useful for proving vector
  identities because we can write the $i\textrm{th}$ component of
  the cross product as:
  \begin{equation}
   (\crosspd{A}{B})_i = \sum_{j,k=1}^3 \epsilon_{ijk} A_j B_k
    = \epsilon_{ijk} A_j B_k.
  \end{equation}
  The second equality introduces the Einstein summation notation, in
  which summation over repeated indices is implied.  For latin
  indices, the summation is taken from 1 to 3.  The vector triple
  product can be written
  \( \vect{A} \cdot (\crosspd{B}{C}) = \epsilon_{ijk} A_i B_j C_k. \)

  The Levi-Civita symbol has the following properties:
  \begin{subequations} \begin{align}
   \epsilon_{ijk} \epsilon_{lmn} &= \begin{vmatrix}
    \delta_{il} & \delta_{im} & \delta_{in} \\
    \delta_{jl} & \delta_{jm} & \delta_{jn} \\
    \delta_{kl} & \delta_{km} & \delta_{kn} \end{vmatrix}, \\
   \epsilon_{ijk} \epsilon_{mnk}
    &= \delta_{im} \delta_{jn} - \delta_{jm} \delta_{in}, \\
   \epsilon_{ijk} \epsilon_{mjk} &= 2\delta_{im}, \\
   \epsilon_{ijk} \epsilon_{ijk} &= 6.
  \end{align} \end{subequations}
  The determinant of a $3 \times 3$ matrix $(a_{ij})$ is
  \( \det(a_{ij})
   = \frac{1}{3!}(\epsilon_{ijk} \epsilon_{lmn} a_{il} a_{jm} a_{kn}) . \)
  Defining the $\binom{0}{4}$ totally antisymmetric tensor
  $\epsilon_{\alpha \beta \gamma \delta}$, the determinant of a $4
  \times 4$ matrix $(a_{\mu \nu})$ is
  \( \det(a_{\mu \nu}) = \frac{1}{4!}(\epsilon_{\alpha \beta \gamma \delta}
   \epsilon_{\mu \nu \rho \sigma} a_{\alpha \mu} a_{\beta \nu}
   a_{\gamma \rho} a_{\delta \sigma}). \)
  The extension to higher dimensions (and two dimensions) is
  immediate.
 \end{enumerate}

 \item \textbf{Analytic Geometry.}
 \begin{enumerate}[A.]
  \item \emph{Distances.}
   Let us consider a point $\vect{P} = (x,y,z)$, a line
   $\vect{L}(t)$, and a plane $\alpha$. If the line passes
   through a point $\vect{A}$ and is parallel to a unit vector
   $\unitvect{v}$,  we can parameterize it as
   \( \vect{L}(t) = \vect{A} + t\unitvect{v}, \)
   where $t$ is a real number.  If the plane, call it $\alpha$, is normal to the
   unit vector $\unitvect{n}$ and contains a point $\vect{S}$, it can
   be described by the equation
   \( \unitvect{n} \cdot (\vect{r} - \vect{S}) = 0. \)
  \begin{enumerate}[1.]
   \item \emph{Distance from a point to a line.} The distance from
   $\vect{P}$ to $\vect{L}(t)$ can be found by considering the component of
   the vector from  $\vect{A} = \vect{L}(0)$ (or any other point on
   the line) to $\vect{P}$ perpendicular to the line, i.e.,
   \( \vect{d} = (\vect{P} - \vect{A})
    - [(\vect{P} - \vect{A}) \cdot \unitvect{v}]\unitvect{v}. \)
   The magnitude of this vector is the desired distance.

  \item \emph{Distance from a point to a plane.} To find the distance
  from a point to a plane, we project the vector from $\vect{S}$ (or
  any other point on the plane) to $\vect{P}$ onto $\vect{n}$:
  \( d = (\vect{P} - \vect{S}) \cdot \unitvect{n} . \)

  \item \emph{Distance between a line and a plane.} If a line and a
  plane are not parallel, then they intersect at some point and thus
  the distance between them is zero.  If the line and plane are
  parallel, i.e., if $\unitvect{v} \cdot \unitvect{n} = 0$, the
  distance between them is just the distance between the plane and
  any point on the line, say $\vect{A}$.  This distance can be found
  as described in the above section.

  \item \emph{Distance between two lines.}  Consider two lines,
  $\vect{L}(t)$ and $\vect{L}'(t') = \vect{A}' + t'\unitvect{v}'$.
  If the lines are not
  parallel, the distance between them will be the length of the
  unique vector which is perpendicular to both lines and joins them.
  This length is
  \( d = (\vect{A} - \vect{A}') \cdot (\unitvect{v} \times
  \unitvect{v}'). \)
  Of course, $\vect{A}$ and $\vect{A}'$ can be replaced by any pair
  of points on the lines.  If the lines are parallel, the distance
  between them is just the distance from any point on one line to the
  other line, which can be found as described above.

  \item \emph{Distance between two planes.} Consider two planes,
  $\alpha$ and $\alpha'$, described by $\vect{S}'$ and
  $\unitvect{n}'$. If the planes are not
  parallel, then they intersect along some line and thus
  the distance between them is zero.  If the planes are
  parallel, i.e., if $\unitvect{v} \parallel \unitvect{n}' = 0$, then
  the distance between them is just the distance between any point in
  one plane and the other plane, which can be found as described above.

  \end{enumerate}

 \end{enumerate}


 \item \textbf{Vector Derivatives.}
 \begin{enumerate}[A.]
  \item \emph{The Vector Operator ``del''.} The vector operator
  ``del'' is defined as
  \begin{equation}
   \del = \ihat\frac{\partial}{\partial x}
    + \jhat\frac{\partial}{\partial y}
    + \khat\frac{\partial}{\partial z} .
  \end{equation}
  We will see shortly how it acts on vectors and scalars.

  \item \emph{The Gradient of a Scalar Field.}  The gradient of a
  scalar function $f$ is a vector
  \begin{equation}
   \del f = \PD{f}{x} \ihat + \PD{f}{y} \jhat +
    \PD{f}{z} \khat .
  \end{equation}
  The gradient produces a vector which points in the direction of
  maximum increase of $f$.  The magnitude of this vector is the
  rate of increase in the maximal direction.

  \item \emph{The Divergence of a Vector Field.} The divergence of
  a vector field $\vect{A}$ is a scalar produced by taking the
  dot product of $\del$ with the field:
  \begin{equation}
   \diverge{A} = \textrm{div} \vect{A} =
   \PD{A_x}{x} + \PD{A_y}{y} + \PD{A_z}{z} .
  \end{equation}
  The divergence is a measure of how much the vector field spreads
  out from the point in question.

  \item \emph{The Circulation (Curl) of a Vector Field.} The curl
  of a vector field $\vect{A}$ is a vector produced by taking the
  cross product of $\del$ with the field:
  \begin{equation}
   \curl{A} = \textrm{curl} \vect{A} = \left| \begin{array}{ccc}
   \ihat & \jhat & \khat \\
   \frac{\partial}{\partial x} & \frac{\partial}{\partial y}
   & \frac{\partial}{\partial z} \\ A_x & A_y & A_z \end{array} \right| .
  \end{equation}

  \item \emph{The Operation $(\dotpd{A}{\nabla})\vect{B}$.} This
  operation produces a vector as follows:
  \begin{multline}
   (\dotpd{A}{\nabla})\vect{B} =
   \left(A_x \PD{B_x}{x} + A_y \PD{B_x}{y} + A_z \PD{B_x}{z}\right) \ihat
   \\ + \left(A_x \PD{B_y}{x} + A_y \PD{B_y}{y} + A_z \PD{B_y}{z}\right) \jhat
   \\ + \left(A_x \PD{B_z}{x} + A_y \PD{B_z}{y} + A_z \PD{B_z}{z}\right) \khat .
  \end{multline}

  \item \emph{The Laplacian.} The divergence of the gradient of a
  scalar field is (a scalar) called the Laplacian:
  \begin{equation}
   \nabla^2 f = \diverge{(\del f)} = \frac{\partial^2 f}{\partial x^2}
    + \frac{\partial^2 f}{\partial y^2} + \frac{\partial^2 f}{\partial z^2} .
  \end{equation}
  The Laplacian of a vector produces another vector and is defined
  as follows:
  \begin{equation}
   \del^2 \vect{A} = \nabla^2 A_x \ihat +
    \nabla^2 A_y \jhat +  \nabla^2 A_z \khat .
  \end{equation}

  \item \emph{Product Rules.} Let $f$ and $g$ be scalar functions,
  and let $\vect{A}$ and $\vect{B}$ be vectors.  Then:
  \begin{enumerate}[1.]
   \item \emph{Gradient:}
   \begin{equation}
    \del (f g) = f \del g + d \del f .
   \end{equation}
   \begin{equation}
    \del (\dotpd{A}{B}) = \crosspd{A}{(\curl{B})}
     + \crosspd{B}{(\curl{A})} + (\dotpd{A}{\nabla})\vect{B}
     + (\dotpd{B}{\nabla})\vect{A} .
   \end{equation}

   \item \emph{Divergence:}
   \begin{equation}
    \del \cdot (f \vect{A}) = f (\diverge{A}) + \vect{A} \cdot (\del f) .
   \end{equation}
   \begin{equation}
    \del \cdot \crosspd{A}{B} = \vect{B} \cdot (\curl{A})
     - \vect{A} \cdot (\curl{B}) .
   \end{equation}

   \item \emph{Curl:}
   \begin{equation}
    \del \times (f \vect{A}) = f (\curl{A}) - \vect{A} \times (\del f) .
   \end{equation}
   \begin{equation}
    \del \times (\crosspd{A}{B}) = (\dotpd{B}{\nabla})\vect{A}
     - (\dotpd{A}{\nabla})\vect{B} + \vect{A} (\diverge{B})
     - \vect{B} (\diverge{A}) .
   \end{equation}
  \end{enumerate}

  \item \emph{Second Derivatives.}
  \begin{equation}
   \del \cdot (\curl{A}) = 0
  \end{equation}
  for any vector field.  Also,
  \begin{equation}
   \del \times (\del f) = 0
  \end{equation}
  for any scalar field.
  The following relation is often used to define the Laplacian of
  a vector:
  \begin{equation}
   \del \times (\curl{A}) = \del (\diverge{A}) - \del^2 \vect{A} .
  \end{equation}

 \end{enumerate}

 \item \textbf{Vector Derivatives in Other Coordinate Systems.}
 \begin{enumerate}[A.]
  \item \emph{Spherical Coordinates.} In spherical coordinates,
  the gradient is
  \begin{equation}
   \del f = \PD{f}{r} \unitvect{r} + \frac{1}{r} \PD{f}{\theta}
   \unitvect{\theta} + \frac{1}{r \sin \theta} \PD{f}{\phi}
   \unitvect{\phi} ,
  \end{equation}
  the divergence is
  \begin{equation}
   \diverge{A} = \frac{1}{r^2} \PD{}{r}(r^2 A_r) +
   \frac{1}{r \sin \theta} \PD{}{\theta}(\sin \theta A_\theta)
   + \frac{1}{r \sin \theta} \PD{A_\phi}{\phi} ,
  \end{equation}
  the curl is
  \begin{multline}
   \curl{A} = \frac{1}{r \sin \theta} \left[
   \PD{}{\theta}(\sin \theta A_\phi) - \PD{A_\theta}{\phi} \right]
   \unitvect{r} \\ + \frac{1}{r} \left[ \frac{1}{\sin \theta}
   \PD{A_r}{\phi} - \PD{}{r}(r A_\phi) \right] \unitvect{\theta}
   + \frac{1}{r} \left[ \PD{}{r}(r A_\theta) - \PD{A_r}{\theta}
   \right] \unitvect{\phi} ,
  \end{multline}
  and the Laplacian is
  \begin{equation}
   \nabla^2 f = \frac{1}{r^2} \PD{}{r}\parenth{r^2 \PD{f}{r}} +
   \frac{1}{r^2 \sin \theta} \PD{}{\theta}\parenth{\sin \theta
   \PD{f}{\theta}}
   + \frac{1}{r^2 \sin^2 \theta} \frac{\partial^2 f}{\partial \phi^2} .
  \end{equation}

 \item \emph{Cylindrical Coordinates.} In cylindrical coordinates,
  the gradient is
  \begin{equation}
   \del f = \PD{f}{s} \unitvect{s} + \frac{1}{s} \PD{f}{\phi}
   \unitvect{\phi} + \PD{f}{z} \unitvect{z} ,
  \end{equation}
  the divergence is
  \begin{equation}
   \diverge{A} = \frac{1}{s} \PD{}{r}(s A_s) +
   \frac{1}{s} \PD{A_\phi}{\phi} + \PD{A_z}{z} ,
  \end{equation}
  the curl is
  \begin{multline}
   \curl{A} = \left[ \frac{1}{s} \PD{A_z}{\phi} - \PD{A_\phi}{z}
   \right] \unitvect{s} \\ + \left[ \PD{A_s}{z} - \PD{A_z}{s}
   \right] \unitvect{\phi} + \frac{1}{s} \left[ \PD{}{s}(r A_\phi)
   - \PD{A_s}{\phi} \right] \unitvect{z} ,
  \end{multline}
  and the Laplacian is
  \begin{equation}
   \nabla^2 f = \frac{1}{s} \PD{}{s} \parenth{s \PD{f}{s}} +
   \frac{1}{s^2} \frac{\partial^2 f}{\partial \phi^2}
   + \frac{\partial^2 f}{\partial z^2} .
  \end{equation}

 \end{enumerate}

 \item \textbf{The Fundamental Theorems of Vector Analysis.}
 \begin{enumerate}[A.]
  \item \emph{Gradient Theorem.} This theorem is the vector
  version of the fundamental theorem of calculus:
  \begin{equation}
   \int_{\vect{a}}^{\vect{b}} (\del f) \cdot \vect{dl} =
   f(\vect{b}) - f(\vect{a}) .
  \end{equation}

  \item \emph{Gauss's Theorem.} Also known as the Divergence
  Theorem, it states that the divergence of a vector field over a
  volume \textsc{V} is equal to the flux of the field across the closed
  surface \textsc{S} bounding the volume.
  \begin{equation}
   \int_\Vol (\diverge{A}) d\Vol = \oint_\textsc{S} \dotpd{A}{da} .
  \end{equation}

  \item \emph{Stokes' Theorem.} Also known as the Curl Theorem, it
  states that flux of the curl of a vector field through an open
  surface \textsc{S}
  is equal to the line integral of the field along the closed
  curve \textsc{C} bounding the surface.
  \begin{equation}
   \int_\textsc{S} (\curl{A}) \cdot \vect{da} = \oint_\textsc{C}
   \dotpd{A}{dl} .
  \end{equation}
  Note that there is are an infinite number of surfaces bounded by a
  given curve \textsc{C}. Stokes' Theorem says we will get
  the same result no matter which one of these surfaces we choose.

  \item \emph{Additional Integral Theorems.}
  \begin{enumerate}[1.]
   \item
   \begin{equation}
    \int_\Vol (\del f) d\Vol = \oint_\textsc{S} f \vect{da}
   \end{equation}

   \item
   \begin{equation}
    \int_\Vol (\crosspd{\nabla}{A}) d\Vol = -\oint_\textsc{S}
    \crosspd{A}{da}
   \end{equation}

   \item
   \begin{equation}
    \int_\textsc{S} \del f \times \vect{da} = - \oint_\textsc{C} f
    \vect{dl}
   \end{equation}

   \item \emph{Green's Identity.}
   \begin{equation}
    \oint_\textsc{S} g \del f \cdot \vect{da} = \int_\Vol [g
    \nabla^2 f + (\del g ) \cdot (\del f)] d\Vol
   \end{equation}

   \item \emph{Green's Theorem.}
   \begin{equation}
    \oint_\textsc{S} (g \del f - f \del g) \cdot \vect{da} = \int_\Vol (g
    \nabla^2 f + f \nabla^2 g) d\Vol
   \end{equation}
  \end{enumerate}
 \end{enumerate}

 \item \textbf{Useful Relations.}
 \begin{enumerate}[A.]
  \item \emph{Relations Involving the Position Vector $\vect{r} = (x,y,z)$.} Let $\vect{a}$ and
  $\vect{B}$ be constant vectors.
  % Consider the following scalar quantities involving $\vect{r}$:
  \begin{center}
  \begin{tabular}{ccc} \toprule
   $f$ & $\del f$ & $\nabla^2 f$ \\
   \midrule
   $\dotpd{a}{r}$ & $\vect{a}$ & $0$ \\
   $r^n$ & $n r^{n-1} \unitvect{r}$ & $n (n+1) r^{n-2}$ \\
   $\ln r$ & $\unitvect{r}/r$ & $1/r^2$ \\ \bottomrule
  \end{tabular} \end{center}
  % \\ Vector quantities involving $\vect{r}$:
  \begin{center}
  \begin{tabular}{cccc} \toprule
   $\vect{A}$ & $\diverge{A}$ & $\curl{A}$ &
    $(\dotpd{B}{\nabla})\vect{A}$ \\ \midrule
   $\vect{r}$ & $3$ & $0$ & $\vect{B}$ \\
   $\crosspd{a}{r}$ & $0$ & $2\vect{a}$ & $\crosspd{a}{B}$ \\
   $\vect{a} r^n$ & $n r^{n-1} (\unitvect{r} \cdot \vect{a})$
   & $n r^{n-1} (\unitvect{r} \times \vect{a})$ & $n r^{n-1}
   (\unitvect{r} \cdot \vect{B})\vect{a}$ \\
   $\unitvect{r} r^n$ & $(n+2) r^{n-1}$ & $0$ & $r^{n-1} \vect{B}
   + (n-1) r^{n-1} (\unitvect{r} \cdot \vect{B}) \unitvect{r}$ \\
   $\vect{a} \ln r$ & $\unitvect{r} \cdot \vect{a}/r$
   & $\unitvect{r} \times \vect{a}/r$ & $(\vect{B} \cdot
   \unitvect{r}) \vect{a}/r$ \\ \bottomrule
  \end{tabular}
  \end{center}
  The function $\unitvect{r}/r^2$ is special.  It has zero
  divergence everywhere, except at the origin, where it
  essentially has infinite divergence:
  \begin{equation}
   \del \cdot \left( \frac{\unitvect{r}}{r^2} \right) = -\nabla^2
   \frac{1}{r} = 4 \pi \delta^3 (\vect{r}) ,
  \end{equation}
  where $\delta^3 (\vect{r}) = \delta (x) \delta(y) \delta(z)$ is
  the three dimensional Dirac delta function.

  It is important to note that we can replace $\vect{r}$ in these
  relations (everywhere) with
  $\vect{r} + \vect{r}'$, where $\vect{r}'$ is any vector which is
  constant with respect to $\del$ here.  In particular, we can
  replace $\vect{r}$ with a displacement vector.

  \item \emph{Vector Area.} The vector area is defined as
  \begin{equation}
   \vect{a} = \int_\textsc{S} \vect{da} .
  \end{equation}
  It should be clear that $\vect{a} = 0$ for any closed surface.
  For any constant vector $\vect{B}$,
  \begin{equation}
   \oint_\textsc{C} (\dotpd{B}{r}) \vect{dl} = \crosspd{a}{B} .
  \end{equation}
  Another useful result is
  \begin{equation}
   \vect{a} = \frac{1}{2} \oint_\textsc{C} \crosspd{r}{dl} .
  \end{equation}

 \end{enumerate}
\end{enumerate}

\end{document}
