% ----------------------------------------------------------------
% Real Analysis **************************************************
% ----------------------------------------------------------------
\documentclass{article}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{booktabs}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
\reversemarginpar
% MATH -----------------------------------------------------------
\input{D:/Physics/Notes/Include/Layout.tex}
\input{D:/Physics/Notes/Include/Math.tex}

\newcommand{\st}{such that }

\newtheorem{dfn}{Definition}
\newtheorem{thm}{Theorem}

\makeatletter
\renewcommand{\thedfn}{\arabic{dfn}}
\@addtoreset{dfn}{subsection}

\renewcommand{\thethm}{\arabic{thm}}
\@addtoreset{thm}{subsection}
\makeatother

% ----------------------------------------------------------------
%\draftmode
\begin{document}
% \maketitle
% ----------------------------------------------------------------

\section{Methods of Proof}

\section{The Real Numbers}

\subsection{Absolute Value}

\begin{dfn}
The \emph{absolute value} of a real number $x$ is $\abs{x} = x$ if $x
\geq 0$ and $\abs{x} = -x$ if $x < 0$.
\end{dfn}

\begin{thm} The absolute value satisfies the following properties:
\begin{enumerate}[1.]
 \item $\abs{a} \leq b$ implies $-b \leq a \leq b$

 \item $\abs{ab} = \abs{a} \abs{b}$

 \item $\abs{a+b} \leq \abs{a} + \abs{b}$

 \item $\abs{\abs{a} - \abs{b}} \leq \abs{a-b}$
\end{enumerate}
\end{thm}

\subsection{Completeness}

\begin{dfn} A number $M$ is an \emph{upper bound} for a set $S$ if
$s \leq M$ for all $s \in S$.

A number $m$ is a \emph{lower bound} for a set $S$ if $s \geq m$ for
all $s \in S$.
\end{dfn}

\begin{dfn} If $M$ is an upper bound for $S$ and any $M'<M$ is not an
upper bound of $S$, $M$ is called the least upper bound or
\emph{supremum} of $S$

If $m$ is a lower bound for $S$ and any $m'>m$ is not a lower bound
of $S$, $m$ is called the greatest lower bound or \emph{infimum} of
$S$
\end{dfn}

\begin{thm} The following properties hold for $\inf$ and $\sup$:
\begin{enumerate}[1.]
 \item $\inf S \leq \sup S$
 \item If $S \subseteq T$, $\inf T \leq \inf S \leq \sup S \leq \sup T$
\end{enumerate}
\end{thm}

\begin{thm}[The Completeness Axiom] Every nonempty subset of $\Real$
that is bounded above has a least upper bound.  Similarly, every
nonempty subset of $\Real$ that is bounded below has a greatest lower
bound.
\end{thm}

\begin{dfn} If a set $S$ of reals is not bounded above, then we write
$\sup S = +\infty$.  If $S$ is not bounded below, we write $\inf S =
-\infty$.
\end{dfn}

\begin{thm}
Given any $a,b \in \Real$, $a < b$, there exist infinitely many $r
\in \Real$ \st $a < r <b$.
\end{thm}

\section{Point Set Topology in $\Real$}

\subsection{Neighborhoods}

\begin{dfn} A set $Q$ of real numbers is a \emph{neighborhood} of a
real number $x$ if there is an $\eps > 0$ \st $(x-\eps,x+\eps)
\subset Q$. \end{dfn}

\begin{dfn} A real number $x$ is called an \emph{accumulation point}
of a set $S$ of reals if every neighborhood of $x$ contains at least
one member of $S$ distinct from $x$.
\end{dfn}

\begin{thm}
  A real number $x$ is an accumulation point of a set $S$ if and only
  if every neighborhood of $x$ contains infinitely many points of
  $S$.
\end{thm}

\subsection{Open and Closed Sets}

\begin{dfn} A set $A$ is open if, for each $x \in A$, $x$ is interior
to $A$, that is, there is a neighborhood $Q$ of $x$ \st $Q \subseteq
A$.
\end{dfn}

\begin{dfn} A set $E$ is closed if every accumulation point of $E$
belongs to $E$.
\end{dfn}

Some alternate definitions for closed sets:

\begin{thm} A set $E$ is closed if and only if
\begin{enumerate}[1.]
 \item $E$ contains the limit of every convergent sequence of points
 in $E$.
 \item its complement $\Real \backslash E$ is open.
\end{enumerate}
\end{thm}

\begin{dfn} The set of points interior to some set $E$ is called the
interior of $E$, denoted $E^\circ$.  The closure of $E$, denoted
$\overline{E}$ is the intersection of all closed sets containing $E$.
The boundary of $E$ is $\partial E = \overline{E} \backslash
E^\circ$.
\end{dfn}

\begin{thm} Some miscellaneous results:
\begin{enumerate}[1.]
 \item A set $E$ is closed if and only if $E = \overline{E}$.
 \item $E$ is open if and only if $E = E^\circ$.
 \item $\partial E = \overline{E} \cap \overline{\Real \backslash E}$.
 \item If $E$ is open, $(\partial E)^\circ = \phi$
 \item If $x \in \overline{E}$, then for any neighborhood $Q$ of $x$,
 $Q \cap E \neq \phi$.
\end{enumerate}
\end{thm}

\begin{thm} Some properties of open and closed sets:
\begin{enumerate}[1.]
 \item $\Real$ and the empty set are open.
 \item Any finite set is closed.
 \item The union of any collection of open sets is open.
 \item The intersection of finitely many open sets is open.
 \item The union of finitely many closed sets is closed.
 \item The intersection of any collection of closed sets is closed.
\end{enumerate}
\end{thm}

\subsection{Compactness}

\begin{dfn} A collection of open sets $\set{G_1, G_2, \ldots}$ is
called an open cover of a set $E$ if $E \subseteq \cup G_n$.  We say
$\cup G_n$ covers $E$.  A subcover is a subset of $\set{G_1, G_2,
\ldots}$ which also covers $E$.  A cover is finite if it contains
only finitely many sets $G_i$.

A set $E$ is compact if every open cover of $E$ has a finite
subcover.
\end{dfn}

The following theorem is \emph{not} accepted as an alternate
definition of compactness.

\begin{thm}[Heine--Borel Theorem] A set $E$ is compact if and only if
it is closed and bounded.
\end{thm}

\section{Sequences}

\subsection{Convergence}

\begin{dfn} A sequence $(s_n)$ is said to \emph{converge} to $s$ if
for every $\eps > 0$, there exists an $N$ \st $\abs{s_n - s} < \eps$
for all $n > N$.
\end{dfn}

\begin{thm} If a sequence is convergent, it is bounded. \end{thm}

\begin{thm} Consider two sequences $(a_n) \To a$ and $(b_n) \To b$.
The following properties hold:
\begin{enumerate}[1.]
 \item $\lim (a_n + b_n) = a + b$
 \item $\lim (a_n b_n) = ab$
 \item if $b \neq 0$ and $b_n \neq 0$ for all $n$, then $\lim
 (a_n/b_n) = a/b$
\end{enumerate}
\end{thm}

\begin{dfn} A sequence $(s_n)$ is
\begin{itemize}
 \item \emph{(strictly) increasing} if $s_n < s_{n+1}$
 \item \emph{nondecreasing} if $s_n \leq s_{n+1}$
 \item \emph{nonincreasing} if $s_n \geq s_{n+1}$
 \item \emph{(strictly) decreasing} if $s_n > s_{n+1}$
\end{itemize}
for all $n$.  In any of these cases, the sequence is called
\emph{monotonic}.
\end{dfn}

\begin{thm} All bounded monotone sequences converge. \end{thm}

\subsection{Infinite Limits}

\subsection{Lim Sup and Lim Inf}

\begin{dfn} Given a sequence $(s_n)$, define the set $S_N = \set{s_n
| n \geq N}$. Let $u_N = \sup S_N$ and $v_N = \inf S_N$.  We define
\begin{itemize}
 \item $\limsup s_n = \lim_{N \To \infty} u_N$ and
 \item $\liminf s_n = \lim_{N \To \infty} v_N$.
\end{itemize}
\end{dfn}

The following is an alternate definition.

\begin{thm} Given a real number $u$, $u = \limsup s_n$ if and only
if, for all $\eps > 0$,
\begin{enumerate}[i.]
 \item there exist only finitely many terms in the sequence \st
 $s_n > u+\eps$.

 \item there exist infinitely many terms in the sequence \st $s_n >
 u-\eps$.
\end{enumerate}
Similarly for $\liminf s_n$.
\end{thm}

\begin{thm} Given a sequence $(s_n)$, if $\lim s_n$ is defined,
then $\liminf s_n = \lim s_n = \limsup s_n$. Conversely, if $\liminf
s_n = \limsup s_n$, then $\lim s_n$ is defined.
\end{thm}

\subsection{Cauchy Sequences}

\begin{dfn} A sequence $(s_n)$ is said to be \emph{Cauchy} if
for every $\eps > 0$, there exists an $N$ \st $\abs{s_n - s_m} <
\eps$ for all $n,m > N$.
\end{dfn}

\begin{thm} A sequence is convergent if and only if it is Cauchy.
\end{thm}

\subsection{Subsequences}

\begin{dfn} A subsequence of a sequence $(s_n)$ is a sequence
$(s_{n_k})$ where $n_k < n_{k+1}$ for all $k$. \end{dfn}

\begin{thm} If $(s_n)$ converges, then every subsequence of $(s_n)$
converges to the same limit. \end{thm}

\begin{thm} If all the convergent subsequences of a sequence have the
same limit, then the sequence itself will converge to that limit.
\end{thm}

\begin{thm} Every sequence $(s_n)$ has a monotonic
subsequence whose limit is $\limsup s_n$ and a monotonic subsequence
whose limit is $\liminf s_n$. \end{thm}

\begin{thm}[Bolzano-Weierstrass Theorem] Every bounded sequence has a
convergent subsequence.
\end{thm}

The following is an alternate statement of the Bolzano-Weierstrass
Theorem.

\begin{thm}
Every bounded, infinite set of real numbers has at least one
accumulation point.
\end{thm}

\begin{thm} Let $S$ denote the set of subsequential limits of
$(s_n)$, that is, $S = \set{t | \textrm{$t$ is the limit of a
subsequence of $(s_n)$}}$.  Then the following properties hold.
\begin{enumerate}
 \item $\sup S = \limsup s_n$ and $\inf S = \liminf s_n$
 \item $\lim s_n$ exists if and only if $S = \set{\lim s_n}$
 \item if $(t_n)$ is a sequence in $S \cap \Real$ and $t = \lim t_n$,
 then $t \in S$.
\end{enumerate}
\end{thm}


\end{document}
