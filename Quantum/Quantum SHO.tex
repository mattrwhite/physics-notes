% ----------------------------------------------------------------
% Quantum SHO ****************************************************
% ----------------------------------------------------------------
\documentclass{article}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{booktabs}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
\reversemarginpar
% MATH -----------------------------------------------------------
\input{../Include/Math.tex}
\input{../Include/Layout.tex}
\input{../Include/Quantum.tex}
% ----------------------------------------------------------------
% \draftmode
\begin{document}
% \maketitle
% ----------------------------------------------------------------
\begin{enumerate}[I.]
 \item \textbf{Coordinate Basis.}
 \begin{enumerate}[A.]
  \item \emph{The Classical Simple Harmonic Oscillator.}  Classically, the problem of the
  simple harmonic oscillator (without damping or forcing) is that of a
  mass $m$ moving under the
  influence of a force $F = -kx$. The resulting simple harmonic
  motion is sinusoidal vibration about the equilibrium point $x =
  0$ with angular frequency $\omega = \sqrt{k/m}$.

  The classical Hamiltonian for the simple harmonic oscillator is just
  \( \mathcal{H} = \frac{p^2}{2m} + \frac{1}{2} m \omega^2 x^2 . \)
  The energy of the system is
  \[ E = K + U = \frac{1}{2} m \dot{x} + \frac{1}{2} m \omega^2 x^2
  = \const . \]

  \item \emph{The Quantum Simple Harmonic Oscillator.}  To study the SHO quantum
  mechanically, we consider the promoted Hamiltonian
  \( \op{H} = \frac{\op{P}^2}{2m} + \frac{1}{2} m \omega^2 \op{X}^2 . \)
  As usual, our first task is to solve the eigenvalue problem of
  $\op{H}$.  In the $x$ basis, this is
  \( -\frac{\hbar^2}{2m} \DD{\psi}{x} + \frac{1}{2} m
  \omega^2 x^2 \psi = E \psi .\)
  Before solving the differential equation, it is traditional to cast
  it into dimensionless form by making the following definitions:
  \begin{subequations}
  \( b \defn \sqrt{\frac{\hbar}{m \omega}} , \)
  \( \eps \defn \frac{E}{\hbar \omega} , \)
  \( y \defn \frac{x}{b} . \)
  \end{subequations}
  This gives
  \( \label{eq:SHO1} \psi'' + (2 \eps - y^2) \psi = 0 , \)
  where a prime indicated differentiation \wrt $y$.

  \item \emph{Limiting Cases.}  Before attempting a power series
  solution to (\ref{eq:SHO1}), it is helpful to consider the limiting
  cases.

  In the limit as $y \To \infty$, (\ref{eq:SHO1}) becomes
  \[ \psi'' - y^2 \psi = 0 . \]
  The solution to this equation \emph{in the same limit} is
  \[ \psi = A y^m e^{\pm y^2 / 2} , \]
  where $A$ is an arbitrary constant and $m$ is an arbitrary integer.
  The $e^{+ y^2 / 2}$ solution
  is clearly nonphysical and we discard it.

  In the limit $y \To 0$, we have $\psi'' + 2 \eps \psi = 0$, which
  has sinusoidal solutions.  However, since we have discarded the
  $y^2$ term in (\ref{eq:SHO1}), we ought to discard the $y^2$ and
  higher terms in the (power series expansion of) the solution.  This
  suggests a solution which approaches $a + by$ as $y \To 0$

  Combining these results suggests we try a solution of form
  \( \label{eq:SHO2} \psi(y) = u(y) e^{-y^2/2} \)
  for (\ref{eq:SHO1}).

  \item \emph{Solution of the Quantum SHO.}  Inserting
  (\ref{eq:SHO2}) into (\ref{eq:SHO1}) and attempting a power series
  solution for $u(y)$ yields the following recursion relation:
  \[ C_{n+2} = C_n \frac{(2n + 1 - 2 \eps)}{(n+2) (n+1)} .\]
  In order to have a physical solution, the power series for $u(y)$
  must converge; this is only possible if it terminates.  This can
  only happen if
  \[ \eps = \frac{2 n + 1}{2} , \eqnsep n = 0,1,2,\ldots .\]

  The restriction on the value of $\eps$ leads to quantization of
  energy for the SHO.  The allowed energies are
  \begin{namedeqn}{\textbf{SHO Energy Levels}}
   E_n = (n + \tfrac{1}{2}) \hbar \omega,  \eqnsep n = 0,1,2,\ldots .
  \end{namedeqn}
  For a given $n$, $u(y)$ is (to within a normalization factor) the
  $n$th Hermite polynomial, denoted $H_n(y)$.

  The first few Hermite polynomials are
  \begin{namedalign}{\textbf{Hermite Polynomials}}
   H_0(y) &= 1 \notag \\
   H_1(y) &= 2y \notag \\
   H_2(y) &= 4y^2 - 2 \notag \\
   H_3(y) &= 8y^3 - 12y  \notag \\
   H_4(y) &= 16y^4 -48 y^2 + 12 \notag
  \end{namedalign}
  Note that the coefficient of the highest term is always $2^n$.
  The orthogonality condition for the Hermite polynomial is
  \( \infint H_n(y) H_m(y) e^{-y^2} dy = (2^n n! \sqrt{\pi})
  \delta_{nm} . \)
  The Rodrigues' formula for the Hermite polynomials is
  \( H_n(y) = (-1)^n e^{y^2} \frac{d^n}{dy^n} e^{-y^2} .\)
  Two usual recursion relations for the Hermite polynomials are
  \begin{subequations}
  \begin{gather}
   H_{n+1}(y) = 2y H_n(y) - 2n H_{n-1}(y) , \\
   H'_n(y) = 2n H_{n-1}(y) .
  \end{gather}
  \end{subequations}

  Finally, we arrive at the solutions to the SHO eigenvalue problem.
  In terms of $y$, we have
  \( \psi_n (y) = A_n H_n(y) e^{-y^2/2} , \)
  where the normalization constant $A_n$ is
  \( A_n = \sqrt{\frac{1}{2^n n!} \sqrt{\frac{m \omega}{\pi \hbar}}} . \)
  Putting $x$ back in, we get
  \begin{namedeqn}{\textbf{SHO Eigenstates}}
   \psi_n(x) = A_n H_n \parenth{\sqrt{\frac{m \omega}{\hbar}} x}
   \exp \parenth{-\frac{m \omega}{2 \hbar} x^2} .
  \end{namedeqn}

  To denote the eigenstates in ket form, we will write $\ket{n}$, since $n$
  uniquely identifies the eigenstate.  Then we have
  \( \op{H} \ket{n} = (n + \tfrac{1}{2}) \hbar \omega \ket{n} . \)
 \end{enumerate}

 \item \textbf{Energy Basis.}
 \begin{enumerate}[A.]
  \item \emph{The Rasing and Lowering Operators.}  To begin our
  consideration of the quantum SHO in the energy basis, we define
  the operator
  \begin{subequations} \label{eq:Adef}
  \( \Adown \defn \sqrt{\frac{m \omega}{2 \hbar}}\parenth{\op{X} + i
  \frac{\op{P}}{m\omega}} \)
  and its adjoint
  \( \Aup = \sqrt{\frac{m \omega}{2 \hbar}}\parenth{\op{X} - i
  \frac{\op{P}}{m\omega}}. \)
  \end{subequations}
  Note that $\Aup \Adown$ is Hermitian:
  \( \label{eq:Hupdown} \Aup \Adown = \frac{\op{H}}{\hbar \omega} - \frac{1}{2} . \)
  It follows that
  \( \op{H} = (\Aup \Adown + \tfrac{1}{2})\hbar \omega .\)
  The following commutators will prove to be quite useful:
  \begin{subequations} \label{eq:CommAH}
  \begin{gather}
  [\Adown, \Aup] = 1, \\
  [\Adown, \op{H}] = \hbar \omega \Adown, \\
  [\Aup, \op{H}] = - \hbar \omega \Aup.
  \end{gather}
  \end{subequations}

  \item \emph{The Action of $\Adown$ and $\Aup$ on the SHO Eigenstates.}  Let
  us consider
  \begin{align}
   \op{H} \Adown \ket{n} &= (\Adown \op{H}
    - [\Adown, \op{H}])\ket{n} \notag \\
   &= (\Adown \op{H} - \hbar \omega \Adown)\ket{n} \notag \\
   &= ((n + \tfrac{1}{2}) \hbar \omega \Adown
    - \hbar \omega \Adown)\ket{n} \notag \\
   &= [(n-1)+\tfrac{1}{2}] \hbar \omega \Adown \ket{n} .
  \end{align}
  Because there is no degeneracy in one dimension, it follows that
  \( \label{eq:Lower} \Adown \ket{n} = \alpha \ket{n-1} , \)
  where $\alpha$ is some constant.
  Similarly, we can show that
  \( \Aup \ket{n} = \beta \ket{n+1} . \)
  Given an eigenstate of $\op{H}$, $\Adown$ gives us the next lowest
  eigenstate and $\Aup$ gives us the next highest eigenstate.  In
  light of these results, we call $\Adown$ a lowering (or destruction)
  operator and $\Aup$ a raising (or creation) operator.

  Now, we know that the eigenvalues of $\op{H}$ are nonnegative, so
  the downward chain created by $\Adown$ must break at some point.
  That is, there must be a state $\ket{n_0}$ such that $\Adown
  \ket{n_0} = 0$.  Multiplying by $\Aup$ and using (\ref{eq:Hupdown})
  gives us
  \( \op{H} \ket{n_0} = \frac{\hbar \omega}{2} \ket{n_0}, \)
  the lowest energy state for the SHO, with $E_0 = \frac{1}{2} \hbar
  \omega$.  That is, $n_0 = 0$.

  By combining (\ref{eq:Lower}) with its adjoint, we find that
  $\abs{\alpha}^2 = n$.  Taking $\alpha$ to be real, we have
  \( \Adown \ket{n} = \sqrt{n} \ket{n-1} . \)
  Similarly, we find that for $\Aup$,
  \( \Aup \ket{n} = \sqrt{n+1} \ket{n+1} . \)
  Notice that $\Aup \Adown \ket{n} = n \ket{n}$.  For this reason, $\Aup
  \Adown$ is called the number operator - it counts the number of
  quanta of energy of state above the ground state, in units of
  $\hbar \omega$.  In fact, we can write any eigenstate $\ket{n}$
  in terms of the ground state:
  \( \label{eq:UpChain} \ket{n} = \frac{1}{\sqrt{n!}} (\Aup)^n \ket{0} . \)

  \item \emph{Matrix Elements.}  The matrix elements of $\Adown$ and
  $\Aup$ are simply
  \begin{subequations}
  \( \bra{n'}\Adown\ket{n} = \sqrt{n} \delta_{n',n-1} , \)
  \( \bra{n'}\Aup\ket{n} = \sqrt{n+1} \delta_{n',n+1} . \)
  \end{subequations}
  We can obtain the matrix elements of $\op{X}$ and $\op{P}$ (and
  any function of them) by writing
  \begin{subequations} \label{eq:XPa}
  \( \op{X} = \sqrt{\frac{\hbar}{2 m \omega}} (\Aup + \Adown) , \)
  \( \op{P} = i \sqrt{\frac{\hbar m \omega }{2}} (\Aup - \Adown) , \)
  \end{subequations}

  \item \emph{Passage to the $x$ Basis.}  In order to go to the $x$
  basis from the energy basis, we need only write $\Adown$ in the $x$
  basis and use the relation $\Adown \ket{0} = 0$.  This gives us a
  simple differential equation, the solution to which is (after
  normalizing),
  \( \inner{x}{0} = \parenth{\frac{m \omega}{\pi \hbar}}^{1/4} \exp
  \parenth{-\frac{m \omega x^2}{2 \hbar}} . \)
  This is just the SHO ground state that we found before!  We can
  obtain the rest of the eigenstates by writing (\ref{eq:UpChain}) in
  the $x$ basis.  This is one way to obtain the Rodriques'
  formula for the Hermitian polynomials.

  \item \emph{Heisenberg Picture.} We can find the following simple
  relations using the Heisenberg equation of motion and (\ref{eq:CommAH}):
  \begin{subequations} \begin{gather}
   \Adown_H (t) = \Adown_H (0) e^{-i\omega t}
    = \Adown e^{-i\omega t}, \\ \label{eq:AupHeisen}
   \Aup_H (t) = \Aup_H (0) e^{i\omega t} = \Aup e^{i\omega t},
  \end{gather} \end{subequations}
  where $\Adown$ and $\Aup$ are the Schrodinger picture operators.
  Plugging these results into (\ref{eq:XPa}) yields
  \begin{subequations} \begin{gather}
   \op{X}_H (t)
   = \op{X} \cos \omega t + (\op{P}/m \omega) \sin \omega t, \\
   \op{P}_H (t)
   = \op{P} \cos \omega t - m \omega \op{X} \sin \omega t.
  \end{gather} \end{subequations}
  Here, $\op{X}$ and $\op{P}$ are the Schrodinger picture operators.

  \item \emph{Expectation Values and Uncertainties.} Using the
  results of the previous section, it is easy to show that the time
  evolution of $\expval{X}$
  and $\expval{P}$ obeys the classical SHO equations of motion for
  \emph{all} states (of the quantum SHO).  Explicitly,
  \begin{subequations} \begin{gather}
   \expval{\op{X}}(t) = \expval{\op{X}}(0) \cos \omega t
    + (\expval{\op{P}}(0)/m \omega) \sin \omega t, \\
   \expval{\op{P}}(t) = \expval{\op{P}}(0) \cos \omega t
    - m \omega \expval{\op{X}}(0) \sin \omega t.
  \end{gather} \end{subequations}

  Using (\ref{eq:XPa}), it is easy to show that for the SHO energy
  eigenstates,
  \begin{subequations} \begin{gather}
   \bra{n}\op{X}\ket{n} = 0, \eqnsep \bra{n}\op{P}\ket{n} = 0, \\
   \bra{n}\op{X}^2\ket{n} = E_n/m\omega^2, \eqnsep
   \bra{n}\op{P}^2\ket{n} = m E_n.
  \end{gather} \end{subequations}
  It follows that
  \( \Delta \op{X} \Delta \op{P} = E_n/\omega
   = (n+\tfrac{1}{2})\hbar. \)
 \end{enumerate}

 \item \textbf{Coherent States of the Harmonic Oscillator.}
 \begin{enumerate}[A.]
  \item \emph{General Properties.}  While the raising operator
  $\Aup$ has no eigenstates, we can show that the lowering operator
  $\Adown$ does have nontrivial eigenstates, called coherent states,
  which will will denote by $\ket{z}$.  Note that the eigenvalues $z$
  need not be real since $\Adown$ is not Hermitian.  By writing $\ket{z}
  = \sum_n \ket{n}\inner{n}{z}$ and using (\ref{eq:UpChain}) together
  with the defining property of a coherent state (i.e., $\Adown\ket{z}
  = z\ket{z}$) to compute $\inner{n}{z}$, we find
   \[ \ket{z} = \sum_{n=0}^\infty \frac{z^n}{\sqrt{n!}} \ket{n}
    = \exp(z\Aup) \ket{0}. \]
  Here $\ket{0}$ is, of course, the SHO ground state.
  Using (\ref{eq:UpChain}) to replace $\ket{n}$ in the first
  equality yields the second. To complete the cycle, we can use the
  Baker-Campbell-Hausdorff theorem to rederive the defining property
  of $\ket{z}$ from the second equality. In a similar manner, one can
  show that $\inner{z'}{z} = \exp(z'z^*)$.  Thus, properly normalized,
  \begin{namedeqn}{\textbf{Coherent State}} \label{eq:CStateDef}
   \ket{z} = e^{-\abs{z}^2/2} \exp(z\Aup) \ket{0} = e^{-\abs{z}^2/2}
    \sum_{n=0}^\infty \frac{z^n}{\sqrt{n!}} \ket{n}.
  \end{namedeqn}
  Unnormalized coherent states are sometimes called Bargmann states.
  From the second equality, we see that the probability of a coherent
  state $\ket{z}$ being found (upon measurement) to have $n$ quanta
  (of energy) is $e^{-\abs{z}^2} (\abs{z}^2)^n/n!$, which is a
  Poisson distribution in $n$.  The expectation value of the number
  operator $\Aup \Adown$, i.e., the average number of quanta in the
  coherent state, is $\abs{z}^2$.  The expectation value of energy is
  thus $\hbar \omega(\abs{z}^2 + 1/2)$.

  For normalized coherent states, $\inner{z'}{z} = e^{-\abs{z'}^2/2}
  e^{-\abs{z}^2/2} e^{z'z^*}$ so that
  \( \abs{\inner{z'}{z}}^2 = e^{-\abs{z-z'}^2}. \)
  We see that for large $\abs{z-z'}$, the two coherent states are
  nearly orthogonal.

  By working backwards, it is easy to verify that
  \( \label{eq:CStateComplete} \op{I}
   = \frac{1}{\pi}\int \ket{z} \bra{z} d^2 z
   = \frac{1}{2 \pi i}\int \ket{z} \bra{z} dz dz^*, \)
  where $d^2 z = d(\Re z) d(\Im z)$; the integration is over the
  entire complex plane.  This
  completeness relation together with the fact that the $\ket{z}$s
  are not orthogonal implies that they form an overcomplete basis.

  \item \emph{Coherent States in the Coordinate Basis.}  Using only
  the defining property of coherent states and (\ref{eq:XPa}), one
  can show that for any coherent state $\ket{z}$
  \( \Delta \op{X}^2 = \hbar/2 m \omega , \eqnsep
   \Delta \op{P}^2 = \hbar m \omega/2, \)
  from which we can see that $\Delta \op{X} \Delta \op{P} = \hbar/2$,
  i.e., that coherent states are minimum uncertainty states (or wave
  packets).  In the process, the following results are also obtained:
  \( \innerop{z}{X}{z} = \sqrt{2\hbar/(m\omega)}\Re z, \eqnsep
   \innerop{z}{P}{z} = \sqrt{2 \hbar m \omega}\Im z .\)
  We can then write (note the similarity to the definition of
  $\Adown$)
  \( \label{eq:zdef} z = \sqrt{m\omega/(2\hbar)} x_0
    + i \sqrt{1/(2 \hbar m \omega)} p_0, \)
  where $x_0 \defn \innerop{z}{X}{z}$ and $p_0 \defn
  \innerop{z}{P}{z}$. Let us now show that the coherent state
  $\ket{z}$ can be obtained from the SHO ground state $\ket{0}$ via a
  translation by $x_0$ in space and $p_0$ in momentum space, with
  $x_0$, $p_0$, and $z$ related by the above expression.  Beginning
  with the expression for a normalized coherent state, we write
  \begin{align} \label{eq:CStateForms}
   \ket{z} &= e^{-\abs{z}^2/2} \exp(z\Aup)\ket{0} \notag \\
    &= e^{-\abs{z}^2/2} \exp(z\Aup) \exp(-z^*\Adown) \ket{0} \notag \\
    &= \exp(z\Aup - z^*\Adown) \ket{0} \notag \\
    &= \exp[i(p_0 \op{X} - x_0 \op{P})/\hbar] \ket{0}.
  \end{align}
  We have used, in order, the fact that $\Adown \ket{0} = 0$, a
  version of the Baker-Campbell-Hausdorff theorem, and, finally,
  (\ref{eq:Adef}) and (\ref{eq:zdef}).  We recognize in our final
  expression the operator for a translation by $x_0$ and $p_0$ in
  phase space, as promised.  In the coordinate basis, we have
  \( \inner{x}{z} = \parenth{\frac{m\omega}{\pi \hbar}}^{1/4}
   e^{i p_0 x/\hbar} e^{- m \omega (x - x_0)^2/2 \hbar}. \)

 %\item \emph{Adiabatic Changes.} An adiabatic change in $\omega$ from
 %$\omega_1$ to $\omega_2$ (i.e. adiabatic compression or relaxation
 %of the SHO) preserves the value of $z$ (see section on adiabatic
 %theorem).  Equating real and imaginary parts of (\ref{eq:zdef})
 %yields $x_2/x_1 = \sqrt{\omega_1

  \item \emph{Time Evolution of Coherent States.} We conclude by
  considering the time evolution of a coherent state:
  \begin{align} \op{U}(t)\ket{z} &= \op{U}(t)
   \exp(z \Aup) \op{U}^\dag(t) \op{U}(t) \ket{0} \notag  \\
   &= \exp(z \Aup(-t)) e^{-i\omega t/2} \ket{0} \notag \\
   &= e^{-i\omega t/2} \exp(z \Aup e^{-i\omega t}) \ket{0} \notag \\
   &= e^{-i\omega t/2} \ket{z e^{-i\omega t}}.
  \end{align}
  Here, we have used $\op{U}(-t) = \op{U}^\dag (t)$ after pulling the
  propagators inside the exponential and then employed (\ref{eq:AupHeisen}).
  A coherent state remains a coherent state and hence a minimum
  uncertainty state for all time.  This behavior
  is the origin of the name for coherent states.  Because,
  additionally, a coherent state (more precisely, its expectation
  values) moves along a nontrivial classical trajectory, it
  represents the best approximation to a classical SHO. This
  statement can be justified further by noting that the average
  energy of a coherent state $\innerop{z}{H}{z} = p_0^2/2m + m
  \omega^2 x^2/2 + \hbar \omega /2$ is just the classical value plus
  the zero point energy.

  The coherent state is also a solution of the driven SHO; consider
  adding a term $\hbar F(t) (\Adown +  \Aup) \propto \op{x} F(t)$.
  Solving in the interaction picture (see Gardiner 4.3.2) yields
  (back in the Schrodinger picture)
  \( \ket{\psi(t)} = \exp(\alpha(t)\Aup - \alpha^*(t)\Adown
   + i \phi(t)) \exp(-i \op{H}_0 t/\hbar) \ket{\psi(0)}, \)
  where
  \[ \alpha(t) = -i \int_0^t F(t') e^{i \omega (t - t')} dt' \]
  and
  \[ \phi(t) = \frac{i}{2} \int_0^t \int_0^t F(s) F(s')
   \R{sgn}(s - s') e^{i \omega (s - s')} ds ds' .\]
  Note that $\ket{\psi(t)}$ will be a coherent state for all times if
  $\ket{\psi(0)}$ is.

  \item \emph{Squeezed States.}  For the coherent state, the
  uncertainties in the unitless Hermitian operators $(\Aup+\Adown)$ and
  $i(\Aup-\Adown)$ are both equal to one.  A state of the harmonic
  oscillator for which these uncertainties are no longer equal but still
  multiply to one (equivalently, $\Delta \op{X} \Delta \op{P}
  = \hbar/2$) is called, fittingly, a squeezed state.  A squeezed
  state can be produced by suddenly changing the potential, i.e.,
  the characteristic frequency, of an oscillator in a
  coherent state, say from $\omega'$ to $\omega$.  Provided this
  change occurs instantaneously, the state vector will be preserved
  and thus the uncertainties in
  $\op{X}$ and $\op{P}$ will be unchanged numerically.  However, they
  will be no longer be balanced relative to the new potential:
  \begin{subequations} \label{eq:SqueezedUncert} \begin{gather}
   \Delta \op{X} = \sqrt{\frac{\hbar}{2 m \omega'}} =
    \sqrt{\frac{\hbar}{2 m \omega}} \sqrt{\frac{\omega}{\omega'}}, \\
   \Delta \op{P} = \sqrt{\frac{m\hbar \omega'}{2}}
    = \sqrt{\frac{m\hbar \omega}{2}} \sqrt{\frac{\omega'}{\omega}}.
  \end{gather} \end{subequations}
  For $\omega' > \omega$, the state is ``squeezed'' in position space
  (hence spread out in momentum space),
  while for $\omega' > \omega$, it is ``squeezed'' in momentum space
  (hence spread out in position space).

  More formally, a squeezed state (of a SHO with characteristic
  frequency $\omega$) is an eigenstate of an operator $\op{b}$:
  \( \op{b} = \sqrt{\frac{m \omega'}{2 \hbar}}\parenth{\op{X} + i
  \frac{\op{P}}{m\omega'}}, \eqnsep
  \op{b}^\dag = \sqrt{\frac{m \omega'}{2 \hbar}}\parenth{\op{X} - i
  \frac{\op{P}}{m\omega'}}. \)
  Note that these are just the ladder operators for a SHO with
  characteristic frequency $\omega'$.  We can reproduce
  (\ref{eq:SqueezedUncert}) by computing the
  uncertainties for an eigenstate of $\op{b}$, using the relations
  \( \op{b} = \cosh \eta \Adown + \sinh \eta \Aup, \eqnsep
   \op{b}^\dag = \cosh \eta \Aup + \sinh \eta \Adown, \)
  where
  \( \eta = \ln \sqrt{\omega'/\omega}. \)
  Notice that this is a Bogoliubov transformation.
  Alternately, $\op{b}$ and $\Adown$ (and $\op{b}^\dag$ and $\Aup$)
  can be related by a unitary transformation $\op{b} =
  \op{U}\Adown\op{U}^\dag$, where
  \( \op{U} = \exp[\eta({\Adown}^2 - (\Aup)^2)/2], \)
  sometimes called the squeeze operator.
  For an eigenstate $\ket{z}$ of $\Adown$ (a coherent state), the
  corresponding squeezed state is $\ket{z,\omega'} = \op{U}\ket{z}$.
  This notation is designed to emphasize that
  $\ket{z,\omega'}$ is the coherent state labelled by $z$ of an
  oscillator with frequency $\omega'$, i.e.,
  $\op{b}\ket{z,\omega'} = z\ket{z,\omega'}.$ More generally, for
  number states, $\ket{n,\omega'} = \op{U}\ket{n}$.

  The time evolution of a squeezed state can be determined in the same
  manner as for a coherent state; we find $\ket{(z,\omega')(t)}
  = \op{U}(\eta(t)) \ket{z}$ where
  \( \op{U}(\eta(t)) = \exp[(\eta^*(t)\Adown^2
   - \eta(t)(\Aup)^2)/2], \eqnsep \eta(t) = \eta e^{-2i\omega t} . \)
  Using this result, we can show that the uncertainties in $\op{X}$
  and $\op{P}$ oscillate according to
  \begin{subequations} \begin{gather}
   \Delta \op{X} = \sqrt{\frac{\hbar}{2 m \omega}}
    \bracket{\parenth{\frac{\omega}{\omega'}} \cos^2 \omega t
    + \parenth{\frac{\omega'}{\omega}}\sin^2 \omega t}^{1/2} \\
   \Delta \op{P} = \sqrt{\frac{m\hbar \omega}{2}}
    \bracket{\parenth{\frac{\omega'}{\omega}} \cos^2 \omega t
    + \parenth{\frac{\omega}{\omega'}}\sin^2 \omega t}^{1/2}
  \end{gather} \end{subequations}
  Observe that the state vector of a squeezed state oscillates in
  Hilbert space with angular frequency $2 \omega$.  Furthermore,
  note that the ``squeezing'' oscillates between position and momentum
  space so that, after half a cycle, $\op{U} \ket{z}$ is a squeezed
  state with reversed uncertainties in position and momentum.  At all
  other times, $\Delta \op{X} \Delta \op{P} > \hbar/2$.

  For completeness, we give the decomposition of a squeezed state in
  terms of the SHO energy eigenstates:
  \( \ket{z,\omega'} = \sum_{n=0}^\infty \frac{1}{\sqrt{n! \cosh \eta}}
   \parenth{\frac{\sinh \eta}{2 \cosh \eta}}^{n/2} \ket{n}. \)
  Further discussion can be found in Scully and Zubairy 2.7,
  including the case of complex $\eta$.

 \end{enumerate}

 \itemI{The Forced Harmonic Oscillator.}
 \begin{enumerate}[A.]
  \itemA{Heisenberg Picture.} Consider a simple harmonic oscillator
  subject to a spatially uniform, time varying force $Q(t)$.  The
  corresponding potential is $-\op{X}Q(t)$.  The Hamiltonian becomes,
  allowing for the inclusion of a velocity dependent term,
  \[ \op{H} = \frac{\op{P}^2}{2 m} + \frac{1}{2}m \omega^2 \op{X}^2 -
   Q(t) \op{X} - P(t) \op{P} .\]
  In the energy basis, we see  that the $-P(t) \op{P}$ term produces
  no complications:
  \( \label{eq:ForcedH} \op{H} = (\Aup \Adown
   + \tfrac{1}{2})\hbar \omega + f(t) \Adown + f^*(t) \Aup , \)
  where
  \( f(t) = -\sqrt{\frac{\hbar}{2 m \omega}}Q(t) + i
   \sqrt{\frac{\hbar m \omega}{2}}P(t). \)
  The Hamiltonian has the same form in the Heisenberg picture (we
  will denote Heisenberg picture operators via explicit time
  dependence rather than a subscript) and the evolution of
  $\Adown(t)$ is readily found from the Heisenberg equation and
  $[\Adown(t), \Aup(t)] = 1$:
  \( \D{}{t} \Adown(t) + i \omega \Adown(t)
   = - \frac{i}{\hbar}f^*(t). \)
  The solution to this equation is
  \( \Adown(t) = \Adown(t_0) e^{-i \omega (t - t_0)} -
   \frac{i}{\hbar} \int_{t_0}^t e^{-i \omega (t - t')} f^*(t) dt'. \)
  If we take $t_0 = 0$, $\Adown(t_0)$ will be the Schrodinger picture
  $\Adown$.

  Suppose that the force acts for a limited time and denote by
  $\Adown_{\R{in}}$ and $\Adown_{\R{out}}$ the solutions to the homogeneous
  equation $\D{}{t} \Adown(t) + i \omega \Adown(t) = 0$ before and
  after the force acts, respectively. Using a Green's function
  method (see Merzbacher 14.6), it can be shown that
  \( \Adown_{\R{out}} = \Adown_{\R{in}}
   - \frac{i}{\hbar}g^*(\omega). \)
  where
  \( g(\omega) = \int_{T_1}^{T_2} e^{i \omega t'} f(t') dt'. \)
  is the Fourier transform of the generalized force $f(t)$ evaluated
  at $\omega$, which is taken to act from $T_1$ to $T_2$.  Notice
  that the force has no effect if it has no Fourier component at the
  natural frequency of the oscillator.

  \itemA{Interaction Picture.} In the interaction picture with
  $\op{H}_0 = (\Aup \Adown + \frac{1}{2})\hbar \omega$, the
  Hamiltonian for the forced SHO is
  \( \op{H} = f(t)\Adown e^{-i \omega t}
   + f^*(t)\Aup e^{i \omega t} .\)
  Using (Approximations ???) we obtain the interaction picture
  propagator (compare (\ref{eq:CStateForms})):
  \( \label{eq:ForcedUIntPict}
  \op{U}_I(t,t_0) = e^{i \beta(t, t_0)} \exp[\zeta(t, t_0) \Aup -
   \zeta^*(t, t_0) \Adown]. \)
  where
  \begin{gather}
  \zeta(t, t_0) = -\frac{i}{\hbar} \int_{t_0}^t f^*(t') e^{i
   \omega t'} dt', \\
  \beta(t, t_0) = \frac{i}{2 \hbar^2} \int_{t_0}^t \int_{t_0}^{t'}
   f(t') f^*(t'') e^{-i \omega (t' - t'')}
   - f^*(t') f(t'') e^{i \omega (t' - t'')} dt'' dt'.
  \end{gather}

  \itemA{Application to Coherent States.} Let us examine the
  evolution of a forced harmonic oscillator initially in a coherent
  state $\ket{\psi(t_0 = 0)} = \ket{z}$ (recall that the interaction
  and Schrodinger pictures coincide at $t = 0$).  Applying
  (\ref{eq:ForcedUIntPict}) yields
  \( \ket{\psi_I(t)} = e^{i \gamma(t)} \ket{z + \zeta(t)}, \)
  where
  \( \gamma(t) = \beta(t)-\frac{i}{2}[\zeta(t)z^* - \zeta^*(t)z]. \)
  Back in the Schrodinger picture,
  \( \ket{\psi(t)} = e^{i (\gamma(t) - \omega t/2)} \ket{(z +
   \zeta(t))e^{-i \omega t}}. \)
  We see that a coherent state of a forced harmonic oscillator
  remains a coherent state and that the ground state can be excited
  to a nontrivial coherent state, which suggests a practical method
  for the preparation of coherent states.  It is very instructive to
  note that $\zeta$ is related to $g(\omega)$ defined above by
  \( g(\omega) = -i \hbar \zeta^*(T_2, T_1). \)
  We see that the coherent state index $z$, corresponding to an
  average number of quanta $\abs{z}^2$, is shifted according to the
  Fourier component of the driving force at $\omega$ (as calculated
  between the time the force was turned on and the time of interest).
  Note that the relative phase between $z$ and $\zeta$ must be taken
  into account!
  If we are interested in the net effect of a force which acts for a
  limited time, it is useful to consider the scattering operator:
  \( \op{S} \defn \op{T}(+\infty, -\infty). \)


 \end{enumerate}

 \itemI{Coupled Harmonic Oscillators.}
 \begin{enumerate}[A.]
  \itemA{Normal Modes.} For two identical SHOs centered at $\pm a$  with a harmonic interaction, the Hamiltonian is
  \( \op{H} = \frac{1}{2 m}(\op{P}_1^2 + \op{P}_2^2) + \frac{1}{2} m \omega^2 (\op{X}_1 - a)^2 + \frac{1}{2} m \omega^2 (\op{X}_2 + a)^2 +m \omega^2 \lambda (\op{X}_1 - \op{X}_2^2), \)
  where $\lambda$ parameterizes the strength of the interaction.  The potential energy can be made separable if we introduce new coordinates
  \( \op{X}_G = (\op{X}_1 + \op{X}_2)/2, \eqnsep \op{X}_R = \op{X}_1 - \op{X}_2, \)
  which are clearly just the center of mass and relative positions.  The conjugate momenta,
  \( \op{P}_G = \op{P}_1 + \op{P}_2, \eqnsep \op{P}_R = (\op{P}_1 - \op{P}_2)/2, \)
  satisfy the usual commutation relations,
  \( [\op{X}_G, \op{P}_G] = i\hbar, \eqnsep [\op{X}_R, \op{P}_R] = i\hbar, \)
  all others being zero.

  In these variables, the Hamiltonian becomes
  \( \op{H} = \frac{1}{2\mu_G}\op{P}_G^2 + \frac{1}{2\mu_R}\op{P}_R^2 + \frac{1}{2} \mu_G \omega_G^2 \op{X}_G^2 + \frac{1}{2} \mu_R \omega_R^2 \parenth{\op{X}_R - \frac{2a}{1+4\lambda}}^2, \)
  where we have dropped the constant term $m \omega^2 a^2 [4\lambda/(1+4\lambda)]$ and introduced
  \( \mu_G = 2m, \eqnsep \mu_R = m/2, \eqnsep \omega_G = \omega, \eqnsep \omega_R = \omega \sqrt{1 + 4\lambda}. \)
  The problem has now been reduced to a pair of noninteracting harmonic oscillators, referred to as normal modes.  The eigenstates of the system can be written
  \( \ket{n_R;m_G} = \ket{n_R} \ket{m_G}, \)
  with energies
  \( E_{n,m} = (n + \tfrac{1}{2})\hbar \omega_R + (n + \tfrac{1}{2})\hbar \omega_G. \)

  \itemA{Impulse to One Oscillator.}  We consider an impulse to the first oscillator:
  \[ \exp(i p_0 \op{X}_1 /\hbar) = \exp[i p_0 (\op{X}_G + \op{X}_R/2) /\hbar]. \]
  If we assume that both normal modes begin in the ground state, both are transformed to coherent states:
  \( \ket{z_R; z_G} = \exp(i p_0 \op{X}_R /2\hbar) \ket{0_R} \exp(i p_0 \op{X}_G /\hbar) \ket{0_G}, \)
  where we have made use of (\ref{eq:CStateForms}).
  Of particular interest is the probability of remaining in the ground state:
  \[ \inner{0;0}{z_R; z_G} = e^{-p_0^2/16 \hbar m_R \omega_R} e^{-p_0^2/4\hbar m_G \omega_G}. \]
  In general, the condition for significant excitation is that the energy corresponding to the impulse, $p_0^2/2m$, be larger than the spacing between levels, $\hbar \omega$.

 \end{enumerate}

 \itemI{Phase Space Representations.}
 \begin{enumerate}[A.]
  \itemA{P and Q Representations.} By applying
  (\ref{eq:CStateComplete}) twice, we can show that any operator can
  be written as
  \( \op{T} = \frac{1}{\pi^2} \int T(\alpha^*, \beta)
   e^{- \abs{\alpha}^2 /2 - \abs{\beta}^2 /2} \ket{\alpha}
   \bra{\beta} d^2\alpha d^2\beta, \)
  where
  \( T(\alpha^*, \beta) = e^{\abs{\alpha}^2 /2 + \abs{\beta}^2 /2}
   \innerop{\alpha}{T}{\beta} \)
  is an analytic function of $\alpha^*$ and $\beta$; note that the
  prefactor is essential for analyticity.

  Because of the overcompleteness of the coherent states, any
  operator can be completely specified by its diagonal coherent state
  matrix elements, i.e., its expectation values for all coherent
  states: $\matrixel{\alpha}{\op{\Omega}}{\alpha}$.  It is
  particulary useful to apply this approach to the density matrix
  $\DM$ to arrive at the so-called phase space representations.

  The $Q$-representation is defined via
  \( \label{eq:QRepDef} Q(\alpha, \alpha^*) = \frac{1}{\pi}
   \matrixel{\alpha}{\DM}{\alpha} . \)
  Using $\tr{\DM} = 1$ and (\ref{eq:CStateComplete}) yields
  \( \int Q(\alpha, \alpha^*) d^2 \alpha = 1. \)
  The expectation value of an antinormally ordered product of ladder
  operators can be expressed in terms of the $Q$-function as
  \( \expval{\Adown^m (\Aup)^n} = \tr[\Adown^m (\Aup)^n \DM]
   = \int \alpha^m (\alpha^*)^n Q(\alpha, \alpha^*) d^2 \alpha. \)

  Using (\ref{eq:CStateDef}) and (\ref{eq:QRepDef}), find
  \[ Q(\alpha, \alpha^*) = e^{-\alpha \alpha^*} \sum_{n,m}
   \frac{\bra{n}\DM\ket{m}}{\pi \sqrt{n! m!}} (\alpha^*)^n
   \alpha^m = \sum_{n,m} Q_{n,m} (\alpha^*)^n \alpha^m \]
  from which we obtain
  \[ \DM = \pi \sum_{n,m}  Q_{n,m} (\Aup)^n \Adown^m . \]

  The $P$-representation can be defined via the following:
  \( \DM = \int P(\alpha, \alpha^*) \ket{\alpha} \bra{\alpha} d^2
   \alpha. \)
  As for the $Q$-representation,
  $\int P(\alpha, \alpha^*) d^2 \alpha = 1$. The expectation value
  of a normally ordered product of ladder operators can be expressed
  in terms of the $P$-function as
  \( \expval{(\Aup)^m \Adown^n} =  \int (\alpha^*)^m \alpha^n
   P(\alpha, \alpha^*) d^2 \alpha. \)
  Starting from
  \[ \DM = \pi \sum_{n,m}  P_{n,m} \Adown^n (\Aup)^m \]
  and using (\ref{eq:CStateComplete}), we find
  \[ P(\alpha, \alpha^*) = \frac{1}{\pi} \sum_{n,m} P_{n,m}
   \alpha^n (\alpha^*)^m . \]

  \itemA{Quantum Characteristic Function.}  The normally, antinormally
  and symmetrically ordered
  quantum characteristic functions are, respectively,
  \begin{subequations} \label{eq:CharFns} \begin{gather}
   \chi_n(\lambda, \lambda^*) = \tr(\DM e^{\lambda \Aup}
    e^{-\lambda^* \Adown}), \\
   \chi_a(\lambda, \lambda^*) = \tr(\DM e^{-\lambda^* \Adown}
     e^{\lambda \Aup}), \\
   \chi_s(\lambda, \lambda^*)
    = \tr(\DM  e^{\lambda \Aup - \lambda^* \Adown}).
  \end{gather} \end{subequations}
  Some authors, e.g. Scully and Zubairy, make the substitution
  $\lambda = i \beta$. The characteristic functions can be related
  by making use of the Baker-Campbell-Hausdorff theorem:
  \( \label{eq:RelateCharFns}
   \chi_s(\lambda, \lambda^*) = e^{-\abs{\lambda^2}/2}
   \chi_n(\lambda, \lambda^*) = e^{\abs{\lambda^2}/2}
   \chi_a(\lambda, \lambda^*). \)
  In analogy to the classical characteristic function, the normally
  ordered moments (for example) are given by the derivatives of
  $\chi_n$ evaluated at $\lambda = 0$:
  \[ \expval{(\Aup)^m \Adown^n} = (-1)^n \frac{\partial^{m+n}}
   {\partial^m \lambda \partial^n \lambda^*} \At{\chi_n(\lambda,
   \lambda^*)}{\lambda = 0}. \]
  Inserting (\ref{eq:CStateComplete}) into (\ref{eq:CharFns}) and
  taking the inverse Fourier transform (in $\Re \alpha$ and $\Im
  \alpha$) yields
  \begin{subequations} \begin{gather}
   P(\alpha, \alpha^*) = \frac{1}{\pi^2} \int e^{-\lambda \alpha^*
    + \lambda^* \alpha} \chi_n(\lambda, \lambda^*) d^2 \lambda, \\
   Q(\alpha, \alpha^*) = \frac{1}{\pi^2} \int e^{-\lambda \alpha^*
    + \lambda^* \alpha} \chi_a(\lambda, \lambda^*) d^2 \lambda, \\
   W(\alpha, \alpha^*) = \frac{1}{\pi^2} \int e^{-\lambda \alpha^*
    + \lambda^* \alpha} \chi_s(\lambda, \lambda^*) d^2 \lambda.
  \end{gather} \end{subequations}
  We can make use of these results and (\ref{eq:RelateCharFns}) to
  relate $P$, $Q$, and $W$ directly:
  \( Q(\alpha, \alpha^*) = \frac{1}{\pi} \int
   e^{-\abs{\alpha - \beta}^2} P(\beta, \beta^*) d^2 \beta . \)
  \( W(\alpha, \alpha^*) = \frac{2}{\pi} \int
   e^{-2\abs{\alpha - \beta}^2} P(\beta, \beta^*) d^2 \beta . \)

   See Chapter 4 of Puri for further discussion.

  \itemA{Phase Space Representations for Common States.}
  \begin{enumerate}[1.]
    \itemi{Coherent State.} For a coherent state $\ket{\beta}$, $\DM =
    \ket{\beta}\bra{\beta}$, so
    \begin{subequations} \begin{gather}
     Q(\alpha, \alpha^*) = \frac{1}{\pi} \abs{\inner{\alpha}
     {\beta}}^2 = \frac{1}{\pi} e^{-\abs{\alpha - \beta}^2},
     \\
     P(\alpha, \alpha^*) = \delta^2(\alpha - \beta),
     \\
     W(\alpha, \alpha^*) = \frac{2}{\pi}
      e^{-2\abs{\alpha - \beta}^2},
     \\
     \chi_n(\lambda, \lambda^*) = e^{\lambda \alpha^*
     - \alpha \lambda^*}.
    \end{gather} \end{subequations}

    \itemi{Number State.} For a number state $\ket{n}$, $\DM =
    \ket{n}\bra{n}$, so
    \begin{subequations} \begin{gather}
     Q(\alpha, \alpha^*) = \frac{1}{\pi} e^{-\abs{\alpha}^2}
      \frac{\abs{\alpha}^{2n}}{n!},
     \\
     W(\alpha, \alpha^*) = (-1)^n \frac{2}{\pi}
      e^{-2 \abs{\alpha}^2} L_n(4 \abs{\alpha}^2),
     \\
     \chi_n(\lambda, \lambda^*) = \sum_{m=0}^n \frac{n!}{(m!)^2
      (n-m)!} (-\abs{\lambda}^2).
    \end{gather} \end{subequations}
    Here, $L_n$ is a Laguerre polynomial.
    No functional form of $P(\alpha, \alpha^*)$ exists for a number
    states, although an expression in terms of derivatives of delta
    functions can be found; see Gardiner 4.4.3.

    \itemi{Thermal State.}  For a harmonic oscillator, the canonical
    ensemble density matrix
    $\DM = e^{-\op{H}/k_B T}/tr[e^{-\op{H}/k_B T}]$ becomes
    \( \DM = \parenth{1 - e^{-\hbar \omega / k_B T}}
     \sum_n e^{-n \hbar \omega / k_B T} \ket{n}\bra{n} = \sum_n
     \frac{\expval{n}^n}{(1 + \expval{n})^{n+1}} \ket{n}\bra{n}. \)
    Here, $\expval{n} = \tr(\Aup \Adown \DM) = 1/(e^{\hbar \omega /
    k_B T} - 1)$ which is, of course, the Bose-Einstein distribution.
    For the phase space representations, we have
    \begin{subequations} \begin{gather}
     Q(\alpha, \alpha^*) = \frac{1}{\pi} \parenth{1
      - e^{-\hbar \omega / k_B T}} \exp\bracket{- \abs{\alpha}^2
      \parenth{1 - e^{-\hbar \omega / k_B T}}},
     \\
     P(\alpha, \alpha^*) = \frac{1}{\pi} \parenth{
      e^{\hbar \omega / k_B T} - 1} \exp\bracket{
      - \abs{\alpha}^2 \parenth{e^{\hbar \omega / k_B T} - 1}},
     \\
     W(\alpha, \alpha^*) = \frac{2}{\pi} \tanh \pfrac{\hbar \omega}{2
     k_B T} \exp\bracket{-2 \abs{\alpha}^2\tanh \pfrac{\hbar \omega}{2
     k_B T}},
     \\
     \chi_n(\lambda, \lambda^*) = \exp \parenth{
      -\frac{\abs{\lambda}^2}{e^{\hbar \omega / k_B T} - 1}}.
    \end{gather} \end{subequations}

   \end{enumerate}

  \end{enumerate}


\end{enumerate}

\end{document}
