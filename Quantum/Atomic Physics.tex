% ----------------------------------------------------------------
% Atomic Physics *************************************************
% ----------------------------------------------------------------
\documentclass{article}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{booktabs}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
\reversemarginpar
% MATH -----------------------------------------------------------
\input{../Include/Layout.tex}
\input{../Include/Math.tex}
\input{../Include/Quantum.tex}
\input{../Include/EM.tex}
\newcommand{\Delt}[1]{\Delta_\textrm{#1}}
\newcommand{\Std}[1]{#1^\ominus}
% ----------------------------------------------------------------
% \draftmode
\begin{document}
% \maketitle
% ----------------------------------------------------------------
\begin{enumerate}[I.]
 \item \textbf{The Hydrogen Atom.}
 \begin{enumerate}[A.]
  \item \emph{The Eigenvalue Problem.}  Written in the coordinate
  basis, the Hamiltonian for a system
  involving an electron, mass $m_e$, and a proton, mass $m_p$, is:
  \( \op{H} = - \frac{\hbar^2}{2 m} \nabla^2 - \kay \frac{e^2}{r} . \)
  where
  \( m = \frac{m_p m_e}{m_p + m_e} \)
  is the reduced mass of the system.  To a very good approximation,
  $m = m_e$.  Note that we have made no assumptions about the electron
  orbiting the proton---this is the most general Hamiltonian for this
  system.

  Here, we are using MKS units, but Gaussian units where the Coulomb interaction is simply $q_1 q_2/r$ are also common.  Expressions below that do not contain $e$ or $\epsilon_0$ are identical in either unit system.

  \item \emph{Solution.}
  Because this system is spherically symmetric, we expect the
  solution to involve spherical harmonics $Y_l^m(\theta, \phi)$ and
  write the energy eigenfunctions as
  \( \psi_{E,l,m} = R_{E,l}(r) Y_l^m(\theta, \phi) , \)
  where the function $R_{E,l}(r)$ has yet to be determined.  With
  foresight, we have not included $m$ in the subscript of
  $R_{E,l}(r)$; neither the energy nor the radial function depend on
  it.
  Plugging this guess into the eigenvalue equation gives us:
  \( \parenth{\DD{}{r} + \frac{2m}{\hbar^2}\bracket{E + \kay \frac{e^2}{r}
   - \frac{l(l+1)\hbar^2}{2mr^2}}} U_{E,l} = 0 ,\)
  where
  \( U_{E,l} = rR_{E,l} . \)
  Considering the asymptotic behavior of $U_{E,l}$ suggests we try
  \[ U_{E,l} = e^{-\rho} \rho^{l+1} \sum_{k=0}^\infty C_k \rho^k . \]
  Here, we have introduced the dimensionless variable
  \( \rho \defn \frac{2r}{a_0} , \)
  where
  \( a_0 \defn \frac{4 \pi \epsilon_0 \hbar^2}{m_e e^2}
  = 5.29177 \times 10^{-11} \textrm{ m}\)
  is the famous Bohr radius ($\hbar^2/m_e e^2$ in Gaussian units).
  Plugging this in yields the following recursion relation:
  \( C_{k+1} = \frac{-e^2 \lambda
   + 2(k+l+1)}{(k+l+2)(k+l+1) - l(l+1)} C_k , \)
  where
  \( \lambda \defn \kay \sqrt{-\frac{2m}{\hbar^2 E}} .\)
  Note that we are assuming $E < 0$, i.e., that we are dealing with
  bound states.
  In order to yield a physical solution, the series must terminate,
  so $e^2 \lambda = 2(k+l+1)$ for some $k$.  Defining the principle
  quantum number
  \( n \defn k+l+1 , \)
  we find that the allowed energies are
  \begin{namedeqn}{\textbf{Energy Levels for Hydrogen}}
   E_n = \frac{-me^4}{32 \pi^2 \epsilon_0^2 \hbar^2 n^2} = \frac{-\hbar^2}{2 m a_0^2 n^2}
   = \frac{-\textrm{ Ry}}{n^2}, \eqnsep n = 1,2,3,\ldots,
  \end{namedeqn}
  where $\textrm{Ry} \approx 13.6\textrm{ eV}$ is the Rydberg unit of energy.
  For a given $n$, the allowed values for $l$ are
  \( l = 0,1,\ldots,n-2,n-1 . \)
  For each $l$, the possible values of $m$ are
  \( m = -l, -l+1,\ldots,l-1,l .\)
  It follows that the degeneracy at a given value of $n$ is $n^2$.

  The normalized energy eigenstates for the hydrogen atom are given in the
  coordinate basis by
  \begin{namedeqn}{\textbf{Hydrogen EESs}}
   \begin{split}
   \psi_{n,l,m}(r, \theta,\phi) &= -\sqrt{\frac{(n-l-1)!}{2n (n+l)!} \parenth{\frac{2}{n a_0}}^3}
    \parenth{\frac{2 r}{n a_0}}^l e^{-r/n a_0} \\ & \qquad \cdot L_{n-l-1}^{2l+1}
    \parenth{\frac{2r}{n a_0}} Y_l^m (\theta, \phi) .
   \end{split}
  \end{namedeqn}
  Here, $L_{n-l-1}^{2l+1}$ denotes an associated Laguerre polynomial.

  The first few energy eigenstates for the hydrogen atom are listed
  below.
  \begin{align*}
   \psi_{1,0,0} &= \sqrt{\frac{1}{\pi a_0^3}} e^{-r/a_0} \\
   \psi_{2,0,0} &= \sqrt{\frac{1}{32 \pi a_0^3}}
    \parenth{2-\frac{r}{a_0}} e^{-r/2a_0} \\
   \psi_{2,1,0} &= \sqrt{\frac{1}{32 \pi a_0^3}}
    \frac{r}{a_0} e^{-r/2a_0} \cos \theta \\
   \psi_{2,1,\pm 1} &= \mp \sqrt{\frac{1}{64 \pi a_0^3}}
    \frac{r}{a_0} e^{-r/2a_0} \sin \theta e^{\pm i \phi}
  \end{align*}

  \item \emph{Shells and Orbitals.}  The term orbital refers to the
  wave function for an electron in an atom.  Each orbital can hold two
  electrons, one with spin up, the other with spin down. An orbital is
  specified by the three numbers $n$, which determines its energy,
  $l$, which determines the magnitude of its angular momentum, and
  $m$, which determines the $z$ component of its angular momentum.
  All of the orbitals with a given value of $n$ (there are
  $n^2$) form a shell.  Within each shell, all the orbitals with a
  given value of $l$ comprise a subshell.  Subshells are named
  according to the following convention:
  \begin{center} \begin{tabular}{lcccccccc}
  \toprule
  $l=$ &0 &1 &2 &3 &4 &5 &6 &\ldots \\
  Name &$s$ &$p$ &$d$ &$f$ &$g$ &$h$ &$i$ &\ldots \\
  Electrons &2 &6 &10 &14 &18 &22 &26 &\ldots \\
  \bottomrule
  \end{tabular} \end{center}
  The number of electrons a subshell can hold, $2(2l+1)$, is just twice
  the number of orbitals it has.

  The expectation value of $r$ for a given orbital, which can be
  interpreted as its mean radius, is
  \( \expval{r}_{n,l} = a_0 n^2\bracket{1+\frac{1}{2}\parenth{1
  - \frac{l(l+1)}{n^2}}} . \)

  For higher nuclear charges (atomic numbers) $Z > 1$, $e^2$ is replaced with $Ze^2$ so $a_0 can be replaced by $a_0/Z$---electrons are bound more strongly as expected.

 \end{enumerate}

 \item \textbf{Multi-Electron Atoms.}
 \begin{enumerate}[A.]
  \item \emph{The Orbital Approximation.} The exact solution for the
  energy eigenstates of multi-electron atoms cannot be written in
  closed form.  In order to deal with multi-electron atoms, we assume
  that each electron (pair) occupies its own orbital, with wave
  functions $\psi_1$, $\psi_2$, etc. and that the total wave function
  (for the ``electron cloud'') is
  \[ \Psi = \psi_1 \psi_2 \cdots . \]
  Called the orbital approximation, this amounts to ignoring the
  interaction between the electrons and thus rendering the Hamiltonian
  separable.

  \item \emph{Shielding and Penetration.} The effects of
  inter-electron interactions are accounted for in terms of shielding
  and penetration.  Shielding refers to the apparent reduction in the
  nuclear charge $eZ$ seen by a given electron due to the electrons
  closer to the nucleus.  This effect for a given orbital is quantified
  in terms of the
  shielding constant $\sigma$: $Z_\textrm{eff} = Z - \sigma$.
  Because the $s,p,d,f,\ldots$ subshells all have different radial
  distributions, i.e., varying levels of ``penetration'' through inner shells,
  each experiences a different level of shielding.  The effect of
  this in a splitting of the subshells: shells with higher $l$
  generally lie at higher energy.

  \item \emph{The Aufbau Principle.}  The Aufbau (or building-up)
  principle is the name given to the order in which the subshells of a
  multi-electron atom are filled.  The general order (lowest energy to
  highest energy) is:
  \( 1s,\, 2s,\, 2p,\, 3s,\, 3p,\, 4s,\, 3d,\, 4p,\, 5s,\, 4d,\, 5p,\, 6s, \ldots. \)
  Electrons occupy different orbitals of a given subshell before
  doubly occupying any one of them.  Furthermore, electrons in singly
  occupied orbitals tend to adopt the same spin.  This is the content
  of Hund's rule: An atom in its ground state adopts a configuration
  with the greatest number unpaired (i.e., aligned) spins.

  A filled shell or subshell is
  said to be closed.  The electrons in the outermost shell of an atom
  are called valence electrons and responsible for most of the atom's
  chemical interactions.

  \item \emph{Ionization Energies.}  The minimum energy needed to
  remove the outermost electron from a given atom is called its first
  ionization energy, $I_1$.  The energy needed to remove a second
  electron from the resulting ion is the second ionization energy,
  $I_2$, and so on.  The standard enthalpy of ionization is
  \( \Delt{ion}\Std{H} = I + \tfrac{5}{2} RT .\)
  The energy released when an electron is gained by an atom in the
  gas phase is called the electron affinity $E_\textrm{ea}$.  The
  standard enthalpy of electron gain is
  \( \Delt{eg}\Std{H} = -E_\textrm{ea} - \tfrac{5}{2} RT .\)
  As we would expect, the ionization energy for a species $X^-$ is
  related to the electron affinity of the species $X$ by
  \( \Delt{eg}\Std{H}(X) = -\Delt{ion}\Std{H}(X^-) . \)

 \end{enumerate}

 \item \textbf{Atomic Bonding.}
 \begin{enumerate}[A.]
  \item \emph{Valence Bond Theory.}

  \item \emph{Molecular Orbital Theory.}

 \end{enumerate}

 \item \textbf{Group Theory.}
 \begin{enumerate}[A.]
  \item \emph{}

 \end{enumerate}

\end{enumerate}

\end{document}
