% ----------------------------------------------------------------
% Simple QM Problems in 1D ***************************************
% ----------------------------------------------------------------
\documentclass{article}
\usepackage{enumerate}
\usepackage{amsmath}

% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
% MATH -----------------------------------------------------------
\input{../Include/Layout.tex}
\input{../Include/Math.tex}
\input{../Include/Vectors.tex}
\input{../Include/Quantum.tex}
% ----------------------------------------------------------------
\begin{document}
% \maketitle
% ----------------------------------------------------------------
\begin{enumerate}[I.]
 \item \textbf{Boundary Conditions on the Wave Function in Piecewise
 Potentials.}
 \begin{enumerate}[A.]
  \item \emph{Constant Potentials.} In a constant potential, the
  wave function must of course be continuous and smooth
  (continuous derivative).  If the potential energy is infinite,
  the wave function must be zero since there is no possibility for
  a particle to exist in such a region.

  \item \emph{Discontinuities in $V(x)$.} Consider a piecewise
  potential $V(x)$ with a finite discontinuity at $\tilde{x}$.
  The wave equation gives
  \begin{equation}
   \DD{\psi}{x} = \frac{2m}{\hbar^2} (V(x) - E)\psi(x) .
  \end{equation}
  Note that the discontinuity in $V(x)$ leads to a discontinuity
  in the second derivative of the wave function.
  Integrating this with respect to $x$ over a small region about
  the the discontinuity yields
  \begin{equation}
   \At{\D{\psi}{x}}{x = \tilde{x}+\eps} -
    \At{\D{\psi}{x}}{x = \tilde{x}-\eps}
    = \frac{2m}{\hbar^2} \int_{\tilde{x}-\eps}^{\tilde{x}+\eps}
    (V(x) - E)\psi(x) dx .
  \end{equation}
  Now, in the limit $\eps \To 0$, the term
  \[ \int_{\tilde{x}-\eps}^{\tilde{x}+\eps} E\psi(x) dx \]
  goes to zero since both $E$ and $\psi(x)$ are finite
  everywhere.  Furthermore, since the discontinuity in $V(x)$ is
  finite, the term
  \begin{equation}\label{PETerm}
   \int_{\tilde{x}-\eps}^{\tilde{x}+\eps} V(x) \psi(x) dx
  \end{equation}
  will also vanish, hence
 \begin{equation}
   \lim_{\eps \To 0} \At{\frac{d\psi}{dx}}{x = \tilde{x}+\eps}
   = \lim_{\eps \To 0} \At{\frac{d\psi}{dx}}{x = \tilde{x}-\eps} .
  \end{equation}
  In other words, the first derivative of the wave function and
  hence the wave function itself are continuous at any finite
  discontinuity in the potential.

  If instead the discontinuity in infinite, then in general the
  first derivative of the wave function will suffer a
  discontinuity, but the wave function itself will remain
  continuous.

  \item \emph{Delta Function Potentials.} For a delta function
  potential $V(x) = V_0 \delta(x-\tilde{x})$, the term (\ref{PETerm})
  does not go to zero, but becomes $V_0 \psi(\tilde{x})$.  This
  gives
  \begin{equation}\label{eq:DeltaFnBC}
   \At{\D{\psi}{x}}{x = \tilde{x}+\eps} -
    \At{\D{\psi}{x}}{x = \tilde{x}-\eps}
    = \frac{2m}{\hbar^2} V_0 \psi(\tilde{x}) .
  \end{equation}
  That is, the first derivative of the wave function suffers a
  discontinuity at $\tilde{x}$ proportional to its value there.
  Integrating again shows that the wave function itself is
  continuous, as always

 \end{enumerate}

 \item \textbf{Probability Current and Bound States.}
 \begin{enumerate}[A.]
  \item \emph{Bound States.} A single particle state $\psi(x,t)$ is
  called a bound state if $\psi \To 0$ when $x \To \pm \infty$ (for
  all $t$).  It is easy to show that the probability current for such
  a state is zero.

  \item \emph{The Wronskian.} The Wronskian of two functions
  $\psi_1(x)$ and $\psi_2(x)$ is defined as
  \( w(\psi_1,\psi_2)
   = \psi_1 \D{\psi_2}{x} - \psi_2 \D{\psi_1}{x} \)
  In general, $\psi_1$ and $\psi_2$ are linearly independent (i.e.,
  not proportional) if and only if $w(\psi_1,\psi_2) \neq 0$.

  If $\psi_1$ and $\psi_2$ are eigenstates with the same energy
  eigenvalue, $w(\psi_1,\psi_2)$ is independent of $x$, that is,
  constant.  Furthermore, if $\psi_1$ and $\psi_2$ are bound states,
  $w(\psi_1,\psi_2) \To 0$ as $x \To \pm \infty$ so $w(\psi_1,\psi_2)
  = 0$ for all $x$.  This implies that $\psi_1 \propto \psi_2$, i.e.,
  that there is no degeneracy in one dimensional bound states.

  \item \emph{Probability Current in 1D Unbound States.} Recall that
  the probability current in one dimension is
  \( J = \frac{-i \hbar}{2m}\parenth{\psi^*\D{\psi}{x}
   - \psi \D{\psi^*}{x} } . \)
  This is related to the probability density $P = \psi* \psi$ by
  \[ \D{P}{t} = -\D{J}{x} . \]
  Since the plane waves $\psi$ we consider in the following problems are
  eigenstates of energy, and since $\psi*$ and $\psi$ are both
  solutions with the same eigenvalue of energy, $w(\psi*,\psi)
  \propto J$ is independent of $x$.

  Since the plane waves we consider in the following problems have no
  time dependence, the above equation implies that $J$ is independent
  of $x$, i.e., $J$ is constant.  This is simply the statement that
  there are no sources of sinks for the particles represented by the
  plane waves (besides the ones at infinity).


 \end{enumerate}

 \item \textbf{Particle in a Box.}
 \begin{enumerate}[A.]
  \item \emph{Infinite Potential Well.} Let us solve the wave
  equation for the following potential:
  \begin{equation}
   V(x) = \left\{ \begin{array}{ll} \infty & x \leq 0, x \geq L
   \\ 0 & 0<x<L \end{array}  \right. .
  \end{equation}
  This is the asymmetric infinite well potential.  We apply the
  following general approach to solving problems with piecewise
  constant potentials:
  \begin{enumerate}[1.]
   \item \emph{Assign a wave function to each potential region.}
   Here, we have three regions: I ($x \leq 0$), $\psiI$;
   II ($0<x<L$), $\psiII$; III ($x\geq 0$), $\psiIII$.

   \item \emph{Find the general solution to the wave function for
   each region.} Here, $\psiI = \psiIII = 0$ since the potential
   is infinite in these regions.  In region II, we have
   \begin{equation}
    \frac{-\hbar^2}{2m} \DD{\psiII}{x} =  E\psiII(x) ,
   \end{equation}
   which has the simple solution
   \begin{equation}\label{eq:IISoln}
    \psiII(x) = A \sin kx + B \cos kx ,
   \end{equation}
   where
   \begin{equation}\label{eq:Wellk}
    k = \sqrt{\frac{2mE}{\hbar^2}}
   \end{equation}
   and $A$ and $B$ are undetermined coefficients.

   \item \emph{Apply boundary conditions to the wave functions.}
   In this case, there are two boundaries: I-II ($x=0$) and II-III
   ($x=L$).  At $x=0$, $\psiI = 0$.  Since the discontinuity in
   $V(x)$ is infinite here, the only boundary condition is $\psiI(0)
   = \psiII(0)$ which implies that $B=0$ in (\ref{eq:IISoln}).
   Similarly, at $x=L$ we have $\psiII(L) = \psiIII(L)$, so $A
   \sin kx = 0$ which implies that
   \[ k = \frac{n\pi}{L}, \qquad n = 1,2,3,\ldots . \]
   ($A=0$ gives the non-physical solution of no particle.) Of course,
   we must normalize the solutions; in this case, we get $A = \sqrt{2/L}$.
  \end{enumerate}
  We now have the complete solution for
  the infinite potential well:
  \begin{equation}
   \psi(x) = \sqrt{\frac{2}{L}} \sin \frac{n\pi x}{L} .
  \end{equation}
  The allowed energy levels are
  \begin{equation}
   E_n = \frac{\hbar^2 k_n^2}{2m} = \frac{\hbar^2 \pi^2
   n^2}{2mL^2} , \qquad n = 1,2,3,\ldots .
  \end{equation}
  Note that the energy of a particle above the bottom of the well
  is purely kinetic (since the potential within the well is constant).
  The simple case of the infinite well introduces two results
  which appear throughout quantum mechanics.  First, that
  imposing boundary conditions on the wave function leads to
  quantization of observables, in this case energy.  Second, that
  the lowest possible energy state ($n=1$) has $E>0$, called the
  zero point energy.

  A state for which $\abs{\psi(x)}^2 \To 0$ as $x \To \pm \infty$
  is called a bound state for obvious reasons. All of the EESs of the
  infinite potential well are bound states.

  \item \emph{Finite Potential Well.} For the finite potential
  well
  \begin{equation}
   V(x) = \left\{ \begin{array}{ll} V_0 & x \leq 0, x \geq L
   \\ 0 & 0<x<L \end{array}  \right. ,
  \end{equation}
  we have the same regions and the following solutions, assuming the
  particle has energy $E<V_0$:
  \begin{equation}
   \psi(x) = \left\{ \begin{array}{ll} Ce^{\kappa x} & x \leq 0
   \\ A \sin kx + B \cos kx & 0<x<L  \\ De^{-\kappa x} & x \geq L
   \end{array}  \right. ,
  \end{equation}
  where $k$ is defined as in (\ref{eq:Wellk}) and
  \begin{equation}
   \kappa = \sqrt{\frac{2m(V_0-E)}{\hbar^2}} .
  \end{equation}
  Now we apply the boundary conditions, noting that we have the
  additional requirement that the first derivative of the wave
  function be continuous since the discontinuity in $V(x)$ is
  finite.  This gives
  \begin{equation}\label{eq:FiniteWellSoln}
   \psi(x) = \left\{ \begin{array}{ll} Ce^{\kappa x} & x \leq 0
   \\ C (\kappa/k) \sin kx + C \cos kx & 0<x<L
   \\ De^{-\kappa x} & x \geq L \end{array}  \right.
  \end{equation}
  and the restriction
  \begin{equation}\label{eq:FiniteWellEnergy}
   2 \cot kL = \frac{k}{\kappa} - \frac{\kappa}{k} .
  \end{equation}
  In (\ref{eq:FiniteWellSoln}), $D$ is related to $C$ by requiring
  that the wave function be symmetric about the center of the well
  and $C$ is determined by normalization.  Inserting the
  definitions of $k$ and $\kappa$ into (\ref{eq:FiniteWellEnergy})
  gives an equation for the allowed energy levels of the the well,
  but this equation cannot be solved explicitly.  See Harris 4.7
  or Shankar Exercise 5.2.6 for more.

  Note that the particle has a nonzero probability of existing in
  the classically forbidden region outside of $0<x<L$.  To help
  quantify this, we define the penetration depth $\delta =
  1/\kappa$.

  \item \emph{Delta Function Potentials.} Consider the rather
  nonphysical delta function potential (centered at the origin
  \wolog ):
  \begin{equation}
   V(x) = -V_0 \delta(x).
  \end{equation}
  For $E > 0$, we will clearly have unbound states, so we only
  wish to consider $E < 0$. Now, we have two regions, $x<0$ and
  $x>0$, in which $V(x) = 0$.  In these regions, the general solution
  to the wave equation
  \begin{equation}
   \frac{-\hbar^2}{2m} \DD{\psi}{x} =  E\psi(x) ,
  \end{equation}
  is $\psi(x) = A e^{kx} + B e^{-kx}$ where
  \begin{equation}
   k = \sqrt{-\frac{2mE}{\hbar^2}} .
  \end{equation}
  Remember that $E <0$. Requiring that the solution be physical
  in each region, we obtain
  \begin{equation}
   \psi(x) =
   \begin{cases} Ae^{kx} & x < 0 \\ Ae^{-kx} & x > 0 \end{cases} .
  \end{equation}
  Note that we have already applied the condition that the wave
  function be continuous at $x=0$. In order to determine the
  possible values for $k$, we apply (\ref{eq:DeltaFnBC}), giving
  \begin{equation}
   k = \frac{m V_0}{\hbar^2}.
  \end{equation}
  This yields the energy of the only allowed bound state for the
  delta function potential:
  \begin{equation}
   E = -\frac{m V_0^2}{2\hbar^2} .
  \end{equation}
  Cases with multiple delta function potentials are handled in the
  same way as above and are quite
  interesting to consider, usually yielding more bound states.
  See Physics 115A HW Set 9, problem 5.

  \item \emph{Bloch's Theorem in 1D.}  Consider a Hamiltonian which
  is (infinitely) periodic with spatial period $a$, i.e. $\op{H}(x+a) =
  \op{H}(x)$.  Denote by $\op{T}$ the operator which produces a
  translation through $a$ such that $\bra{x}\op{T}\ket{\psi}
  = \inner{x+a}{\psi}$.  Then, $\op{T} \op{H}= \op{H} \op{T}$, i.e.,
  the Hamiltonian commutes with $\op{T}$ and therefore we can find a
  complete set of simultaneous eigenstates of $\op{H}$ and $\op{T}$.
  If $\ket{\psi}$ is such a state,
  \[ \bra{x}\op{T}\ket{\psi} = \lambda \inner{x}{\psi}, \]
  and we are free to write $\lambda = e^{ika}$ for some number $k$
  (which we will show below to be real).  Comparing
  $\bra{x}\op{T}\ket{\psi} = \inner{x+a}{\psi}$ immediately yields
  \( \label{eq:Bloch1D} \psi(x + a) = e^{ika} \psi(x). \)
  Equivalently, $\psi(x) = e^{ikx} u_k(x)$, where $u_k(x)$ is
  periodic: $u_k(x+a) = u_k(x)$.  It is now clear that $k$ must be
  real for the wavefunction to be everywhere physical.  Further
  notice that we can restrict $k$ to an interval of $2\pi/a$, since
  this gives all the unique eigenvalues of $\op{T}$. This does not
  mean than there is only one energy eigenstate for each of these
  $k$ values; we will see below that there can be infinitely many.

  \item \emph{Periodic Potentials.} Let us now apply the above
  result to following periodic potential:
  \( V(x) = \sum_{n = -\infty}^{\infty} V_0 \delta(x - na), \)
  known as the Kronig-Penney model.  Note that this potential has no
  bound states because we assume $V_0 > 0$. Indeed, symmetry requires
  that the wavefunctions extend to $x=\pm \infty$. A solution in the case
  where the delta function is replaced by an arbitrary square barrier
  can be found in Kittel Ch.~7.

  In the region $0 < x < a$, we have
  \[ \psi(x)= A e^{iqx} + B e^{-iqx}. \]
  The energy of this state is $E = \hbar^2 q^2/2m$, of course.
  Bloch's theorem (\ref{eq:Bloch1D}) gives the condition $A + B =
  e^{-ika}(A e^{iqa} + B e^{-iqa})$.  Applying (\ref{eq:DeltaFnBC}) at
  $x=0$ gives a second condition: $(\hbar^2/2m)iq(A - B - A
  e^{i(q-k)a} + B e^{-i(q+k)a}) = V_0 (A + B)$, where we have used
  (\ref{eq:Bloch1D}) to wrap around from $x = -\eps$ to $x = a-\eps$.
  These two conditions yield the following relation between $k$ and $q$:
  \( \cos ka = \cos qa
   + \frac{m a V_0}{\hbar^2} \frac{\sin qa}{qa}. \)
  Solving this equation graphically (see Baym figure 4-15), we see
  that for a given $k$, there are infinitely many values of $q$, but
  some ranges of $q$ values are disallowed altogether; the allowed
  values of $q$ are said to lie in energy bands, separated by gaps.

  Finally, let us consider the case of a finite periodic potential
  with $N = L/a$ wells. We stipulate a periodic boundary condition
  $\psi(L) = \psi(0)$ for mathematical convenience (and for no other
  reason).  Applying (\ref{eq:Bloch1D}) to this condition $N$ times
  yields $e^{ikNa} = 1$ or $e^{ikL} = 1$, from which we can see that
  for a periodic potential with $N$ wells, there are $N$
  independent values of $k$.  There are still infinitely many values
  of $q$, i.e., energy eigenstates, for each $k$ (one per energy
  band, as in the case of an infinite potential).
  In other words, there are $N$ states in each band.

 \end{enumerate}

 \item \textbf{Step Potentials: Scattering and Tunnelling.}
 \begin{enumerate}[A.]
  \item \emph{Single Step Potential: $E > V_0$.}
  \begin{enumerate}[1.]
   \item \emph{The Wave Function.} We wish to
   consider the fate of a wave packet incident (from the left)
   on a step potential
   \begin{equation}\label{eq:StepPot}
    V(x) = \begin{cases} 0 & x < 0 \\ V_0 & x > 0 \end{cases} .
   \end{equation}
   The full analysis for a Gaussian wave packet can be found in
   Shankar 5.4.  It turns out that we can neglect time and
   consider a plane wave with energy $E > V_0$ incident on the
   potential, obtaining the
   correct transmission and reflection probabilities.  We can
   think of this plane wave as representing a continuous stream
   of wave packets, i.e., particles, each with mass $m$. Plugging
   (\ref{eq:StepPot}) into the wave equation and solving, we get
   \begin{equation}
    \psi(x) = \begin{cases} Ae^{ik_1 x} + Be^{-ik_1 x} & x < 0 \\
     Ce^{ik_2 x} + De^{-ik_2 x} & x > 0 \end{cases} ,
   \end{equation}
   where
   \begin{equation}
    k_1 = \sqrt{\frac{2mE}{\hbar^2}}
   \end{equation}
   and
   \begin{equation}
    k_2 = \sqrt{\frac{2m(E-V_0)}{\hbar^2}} .
   \end{equation}
   The term involving $A$ represents the incident wave, that
   involving $B$ the wave reflected from the step, and
   that involving $C$ the wave transmitted past the step.
   The term involving $D$ represents a leftward travelling wave to
   the right of the step.  Since there is nothing to generate such
   a wave (no step or barrier to cause reflection and no source), we
   have $D=0$.

   Because these wave functions represent unbound states, the
   energy spectrum is continuous. The only further conditions on
   the wave function are continuity and smoothness at $x=0$.
   Applying these yields
   \begin{equation}
    B = \parenth{\frac{k_1 - k_2}{k_1 + k_2}}A
   \end{equation}
   and
   \begin{equation}
    C = \parenth{\frac{2k_1}{k_1 + k_2}}A .
   \end{equation}

   \item \emph{Reflection and Transmission Probabilities.}
   In order to find the reflection probability, we simply
   compute the probability current:
   \begin{equation}
    j = \frac{\hbar}{2mi}(\psi^* \PD{\psi}{x} - \psi
    \PD{\psi^*}{x})
   \end{equation}
   in one dimension.
   For the incident wave we obtain
   \begin{equation}
    j_I = \frac{\hbar k_1}{m} \abs{A}^2 ,
   \end{equation}
   for the reflected wave,
   \begin{equation}
    j_R = - \frac{\hbar k_1}{m} \abs{B}^2 ,
   \end{equation}
   and for the transmitted wave,
   \begin{equation}
    j_T = \frac{\hbar k_2}{m} \abs{C}^2 .
   \end{equation}
   Note the minus sign for $j_R$, representing the fact that the
   reflected wave is moving to the left. The reflection probability
   is just
   \begin{align}
    R & = \abs{\frac{j_R}{j_I}} = \parenth{\frac{k_1 - k_2}{k_1 +
     k_2}}^2 \\ & = \parenth{\frac{\sqrt{E} - \sqrt{E - V_0}}{\sqrt{E}
     + \sqrt{E - V_0}}}^2
   \end{align}
   and the transmission probability is
   \begin{align}
    T & = \abs{\frac{j_T}{j_I}} = \parenth{\frac{4 k_1 k_2}{k_1 +
     k_2}}^2 \\ & = \frac{4 \sqrt{E(E - V_0)}}{(\sqrt{E}
     + \sqrt{E - V_0})^2} .
   \end{align}
   Note that the total probability is unity as it must be:
   \begin{equation}
    R + T = 1 .
   \end{equation}
  \end{enumerate}

  \item \emph{Single Step Potential: $E < V_0$.} If instead the
  incident's wave energy is below of the barrier, it will be
  completely reflected (assuming the step extends to $x = +\infty$).
  However, unless the barrier is infinitely high, there will be an
  attenuated real exponential wave function extending into
  the barrier. The solution is:
  \begin{equation}
   \psi(x) = \begin{cases} Ae^{ikx} - A\parenth{\frac{\kappa + ik}
    {\kappa - ik}}e^{-ikx} & x < 0 \\
    A\parenth{1-\frac{\kappa + ik}{\kappa - ik}}e^{-\kappa x}
    & x > 0 \end{cases} ,
  \end{equation}
  where
  \begin{equation}
   k = \sqrt{\frac{2mE}{\hbar^2}}
  \end{equation}
  and
  \begin{equation}
   \kappa = \sqrt{\frac{2m(V_0-E)}{\hbar^2}} .
  \end{equation}
  As in the case of the finite potential well, the
  possibility of the particle existing inside the step is
  quantified by the penetration depth $\delta = 1/\kappa$.

  \item \emph{Potential Barrier: $E > V_0$.} Next we consider a
  barrier potential
  \begin{equation}\label{eq:BarrierPot}
   V(x) = \begin{cases} 0 & x < 0 \\ V_0 & 0 < x < L \\
    0 & x > L \end{cases}
  \end{equation}
  and an incident plane wave with $E > V_0$. Here, we have three
  regions but the solution proceeds as usual.  We find that the
  probability for reflection by the barrier is
  \begin{equation}
   R = \frac{\alpha}{\alpha + 4 \beta}
  \end{equation}
  where
  \[ \alpha = \sin^2 \bracket{ \frac{ \sqrt{2m(E-V_0)} }{\hbar}L } \]
  and
  \[ \beta = \frac{E}{V_0}\parenth{\frac{E}{V_0}-1} . \]
  The transmission probability is
  \begin{equation}
   T = \frac{4 \beta}{\alpha + 4 \beta}
  \end{equation}
  Because of the sine term in the numerator of the reflection
  probability, certain incident energies will yield $R=0$, i.e.,
  total transmission.  These resonant transmission energies can be
  found by requiring that the argument of the sine be multiple of
  pi, yielding
  \begin{equation}
   E = V_0 + \frac{n^2 \pi^2 \hbar^2}{2mL^2} , \quad n = 0,1,2,\ldots
  \end{equation}
  The details can be found in Harris, pp. 199 - 201.

  \item \emph{Potential Barrier: $E < V_0$.} If instead have a
  plane wave with $E < V_0$ incident on the barrier potential
  (\ref{eq:BarrierPot}), we find that the reflection probability is
  \begin{equation}
   R = \frac{\zeta}{\zeta + 4 \eta}
  \end{equation}
  where
  \[ \zeta = \sinh^2 \bracket{ \frac{ \sqrt{2m(V_0-E)} }{\hbar}L } \]
  and
  \[ \eta = \frac{E}{V_0}\parenth{1-\frac{E}{V_0}} . \]
  The transmission probability is
  \begin{equation}
   T = \frac{4 \eta}{\zeta + 4 \eta}
  \end{equation}
  The most important result to note is that a particle with
  $E < V_0$ has a finite probability of ``tunnelling'' through
  the barrier and emerging on the other side.  It somehow crosses
  the forbidden region where it has negative kinetic energy.
  For details, see Harris pp. 204 - 208.

  \item \emph{Delta Function Barrier.} For a delta function
  potential
  \begin{equation}
   V(x) = V_0 \delta(x) ,
  \end{equation}
  we find
  \begin{equation}
   R = \frac{(m V_0)^2}{(\hbar^2 k)^2 + (m V_0)^2}
  \end{equation}
  and
  \begin{equation}
   T = \frac{(\hbar^2 k)^2}{(\hbar^2 k)^2 + (m V_0)^2} .
  \end{equation}

 \end{enumerate}

 \itemI{Smooth Potentials.}
 \begin{enumerate}[A.]
  \itemA{Parabolic Potential.} The harmonic oscillator is discussed in Quantum SHO.
  \itemA{Morse Potential.}  The three parameter Morse potential (which looks qualitatively like the Lennard�Jones potential)
  \( V(r) = V_e (1 - e^{-a(r - r_e)})^2 \)
  is often used to model the vibrational behavior of a diatomic molecule, including the possibility of dissociation. Here, $r_e$ is position of the well minimum, $V_e$ is its depth (vs. the asymptoic value of $V$ at large $r$), and $a$ is related to the force constant at the well minimum by $k_e = 2V_e a^2$.  The Schrodinger equation with the Morse potential can be solved analytically using a ladder operator method (analogous to that for the harmonic oscillator, but obviously more complex).  The energy levels of the bound states are given by
  \( E_v = \hbar \omega_0 (v + \tfrac{1}{2}) - [\hbar \omega_0 (v + \tfrac{1}{2})]^2/4V_e, \)
  where $\omega_0 = \sqrt{k_e/m}$.  The energy level spacing $E_{v+1} - E_v = \hbar \omega_0 - (v+1)(\hbar \omega_0)^2/2V_e$ monotonically decreases with $v$, until it becomes $\leq 0$, indicating that we have reached the last bound state (beyond which the expression $E_v$ is not valid).

 \end{enumerate}

\end{enumerate}

\end{document}
