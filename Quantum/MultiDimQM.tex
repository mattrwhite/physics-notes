% ----------------------------------------------------------------
% Systems with Multiple Degrees of Freedom ***********************
% ----------------------------------------------------------------
\documentclass{article}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{booktabs}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
\reversemarginpar
% MATH -----------------------------------------------------------
\input{../Include/Layout.tex}
\input{../Include/Math.tex}
\input{../Include/Vectors.tex}
\input{../Include/Quantum.tex}
\newcommand{\ScrE}{\mathcal{E}}
\newcommand{\V}{\textrm{V}}
% ----------------------------------------------------------------
%\draftmode
\begin{document}
% \maketitle
% ----------------------------------------------------------------
\begin{enumerate}[I.]
 \item \textbf{Direct Product Spaces.}
 \begin{enumerate}[A.]
  \item \emph{Direct Product Spaces.} In our
  opening discussion of one dimensional quantum mechanics, we
  considered only one degree of freedom, $x$, and the infinite
  dimensional configuration space corresponding to $x$.  In the
  general case, for each degree of freedom that a system (or particle)
  has, there is a corresponding configuration space.  Note that
  the configuration space need not be infinite, such as in case where
  the observable can take only a finite number of discrete values.
  The state of the system is represented by a vector in a larger
  configuration space called a direct product space which is
  formed by combining the configuration spaces for each degree of
  freedom.

  Let us denote the configuration space for the $i$th degree of
  freedom by $\V_i$ and its (possibly infinite) dimensionality
  by $N_i$.  We will refer to the $V_i$ as component spaces. The
  direct product space (which is just an ordinary infinite dimensional
  vector space) is denoted by $\V_1 \otimes \V_2 \otimes \ldots$.

  \itemA{Bases of a Direct Product Space.} Let $\ket{\evA_i}$ denote
  some basis of $\V_i$.  Then the set of all vectors of the form
  \( \label{eq:DPBasis} \ket{\evA_1} \otimes \ket{\evA_2} \otimes \ldots
   \defn \ket{\evA_1;\evA_2;\ldots}\)
  is a basis of the direct product space.  It follows that the
  dimensionality of the direct product space is $N_1 N_2 \ldots$.  By
  no means is this the only basis; not
  only can we choose different bases of the individual configuration
  spaces, we can choose a different representation of the degrees of
  freedom, i.e., different
  component configuration spaces. For example, we could specify the
  position of a particle in three dimensions by $(x,y,z)$ or
  $(r,\theta,\phi)$, giving two different bases ($\ket{x;y;z}$
  or $\ket{r;\theta;\phi}$) of the \emph{same} direct product
  space.

  The direct product of vectors used in (\ref{eq:DPBasis}) is a linear
  operation and hence can be extended to arbitrary vectors from the
  component spaces, e.g.:
  \( a\ket{\evA_1} \otimes (b\ket{\evA_2} + c\ket{\evB_2})
   = ab\ket{\evA_1;\evA_2} + ac\ket{\evA_1;\evB_2} .\)
  A vector in the direct product space cannot, in general, be written
  as a direct product of vectors from the component spaces; we will
  call such a vector an entangled vector (see the aside below). We
  can form a complete basis of the space out entangled vectors.  One
  example of such a basis is the set of spherical
  harmonics, which form another basis of the space spanned by
  $\ket{\theta,\phi}$.

  From a mathematical standpoint, the configuration space for two (or
  $N$) particles in one ($M$) dimensions is same as the space for one
  ($M$) particles
  in two ($N$) dimensions. The difference is in interpretation. For
  example, in the two particle case, $\ket{\psi_1;\psi_2}$ represents a
  state where particle 1 is in state $\ket{\psi_1}$ and particle 2 is
  in state $\ket{\psi_2}$.  The interpretation of such a state for
  the one particle case is obvious if $\ket{\psi_1}$ and $\ket{\psi_2}$ are
  eigenstates of position or momentum, but far less obvious in other
  cases.

  \itemA{Aside: Entangled States.} Not every state vector in a direct
  product space can be
  written as a direct product of states from the individual
  configuration spaces.  This is the case for the state
  $\ket{x_1';x_2'} + \ket{x_1'';x_2''}$. Notice that
  for a system in this state, measurement of one observable will determine
  the value of the other as well!  Compare this to the state
  \[ (\ket{x_1'} + \ket{x_1''}) \otimes (\ket{x_2'} + \ket{x_2''}) =
  \ket{x_1';x_2'} + \ket{x_1';x_2''} + \ket{x_1'';x_2'}
  + \ket{x_1'';x_2''} \]
  for which measurement of one observable does not affect the other.

  \itemA{Operators and Inner Products on Direct Product Spaces.} We
  will consider the action of operators and inner products on a two
  component direct product space; the extension to larger spaces is
  immediate.  Also, by linearity, we need only consider their action
  on a basis, call it $\ket{\evA_1;\evA_2}$, of this space.  Except
  for discussion related to equations
  (\ref{eq:GenDPKet1})--(\ref{eq:GenDPKet2}), however, the two component
  spaces could just as well be direct product spaces themselves.

  The inner product of two basis vectors is simply
  \( \inner{\evB_1;\evB_2}{\evA_1;\evA_2} = \inner{\evB_1}{\evA_1}
   \inner{\evB_2}{\evA_2} . \)
  We never take the inner product of two vectors from different
  spaces.  Clearly, when we use the inner product to go to a basis,
  the direct product becomes ordinary multiplication.  Any state
  vector in the direct product space can be written as (assuming a
  discrete spectrum of eigenvalues):
  \( \label{eq:GenDPKet1}
  \ket{\psi} = \sum_{\evA_1} \sum_{\evA_2} C_{\evA_1, \evA_2}
   \ket{\evA_1;\evA_2}. \)
  In the coordinate basis,
  \( \psi(x_1,x_2) = \inner{x_1;x_2}{\psi} = \sum_{\evA_1}
  \sum_{\evA_2} C_{\evA_1, \evA_2} \evA_1(x_1) \evA_2(x_2). \)
  It is often useful perform one of the summations to yield
  \( \label{eq:GenDPKet2}
  \psi(x_1,x_2) = \sum_{\evA_2} f_{\evA_2}(x_1) \evA_2(x_2), \)
  where $f_{\evA_2}(x_1) = \sum_{\evA_1} C_{\evA_1, \evA_2}
  \evA_1(x_1)$ is some function.

  An operator from one space, e.g., $\opB_1$, acts only on the
  part of the basis vector from that space:
  \( \label{eq:DPOp}\opB_1 \ket{\evA_1;\evA_2}
   = (\opB_1 \ket{\evA_1}) \otimes \ket{\evA_2} . \)
  Note that if $\ket{\evA_1}$ is an eigenstate of $\opA_1$ with
  eigenvalue $\evA_1$, then $\opA_1 \ket{\evA_1;\evA_2}
  = \evA_1 \ket{\evA_1;\evA_2}$.
  We can combine operators, say $\opB_1$ and $\opC_2$ from the two
  spaces to form the direct product of two operators, $\opB_1
  \otimes \opC_2$, which acts as follows:
  \( \opB_1 \otimes \opC_2 \ket{\evA_1;\evA_2}
  = (\opB_1 \ket{\evA_1}) \otimes (\opC_2 \ket{\evA_2}) . \)
  To be precise, an operator from one component space cannot act on
  a vector in the direct product space directly, since its action on
  the other component is not defined.  Instead, we must combine it
  with the identity operator from the other component space.
  Rigorously, then, $\opB_1$ in (\ref{eq:DPOp}) should be written as
  $\opB_1 \otimes \op{I}_2$.

  Note that observables related to
  different degrees of freedom are always compatible:
  \( [\opB_1 \otimes \op{I}_2,\op{I}_1 \otimes \opC_2] = 0 .\)
  Also note that, in analogy to the situation with vectors, not every
  operator on a direct product space can be written as a direct
  product of operators from component spaces.
  Finally, note the connection between the direct product and the concept of
  a complete set of commuting operators, which has to do with
  degenerate operators.  Let $\opA_1$ be a nondegenerate
  operator on $\V_1$ and observe that it is however degenerate on $\V_1
  \otimes \V_2$.  Say $\opA_2$ is not degenerate on $\V_2$; then $\opA_1$
  and $\opA_2$ together form a complete set of commuting operators for
  $\V_1 \otimes \V_2$.  It is also important to note the differences
  between these concepts: one can have a complete set of commuting
  operators in a space corresponding to a single degree of freedom
  (i.e., one which is not a direct product of other spaces).

  \item \emph{Separable Hamiltonians; Undisturbed Degrees of
  Freedom.}  Consider a system with two degrees of freedom, represented
  by a two component direct product space. Suppose that we can write its
  Hamiltonian as $\op{H} = \op{H}_1 + \op{H}_2$ where $\op{H}_1$ acts
  only on the first space and $\op{H}_2$ acts only on the second.
  Such a Hamiltonian is said to be separable.  Note that a
  Hamiltonian which is not separable in one representation of the
  degrees of freedom may be in another, that is, in the normal
  coordinates of the system.
  Now, denote the eigenstates of $\op{H}_1$ and $\op{H}_2$ by $\ket{E_1}$
  and $\ket{E_2}$; if the energy eigenstates are degenerate, we
  introduce additional labels as necessary.  Consider the action of
  $\op{H}$ on the basis $\ket{E_1;E_2}$ of the direct product space:
  \( \begin{split}
  \op{H}\ket{E_1;E_2} &= \op{H}_1 \ket{E_1;E_2}
   + \op{H}_2 \ket{E_1;E_2} \\
  &= E_1\ket{E_1;E_2} + E_2\ket{E_1;E_2} \\
  &= (E_1+E_2)\ket{E_1;E_2} .
  \end{split} \)
  Since $\ket{E_1;E_2}$ spans the direct product space, it is a
  complete set of eigenstates of $\op{H}$ (\emph{the} complete set if
  there is no degeneracy).  Finding the eigenstates of a
  separable Hamiltonian, then, reduces to finding the eigenstates of the
  pieces and combining them.

  Say $\op{H}_2 = 0$, i.e., the Hamiltonian does not involve the
  second degree of freedom. Since every ket is an eigenstate of the
  null operator, the eigenstates of $\op{H}$ are $\ket{E_1;\psi_2}$
  where $\ket{\psi_2}$ is any state in the second configuration space.

  \item \emph{Two Independent Particles.}
  The general Hamiltonian for a system with two degrees of freedom,
  such a two particles in one dimension or one particle in two, is
  \begin{equation}
   \op{H} = \frac{\op{P}_1^2}{2m_1} + \frac{\op{P}_2^2}{2m_2} +
   \op{V}(\op{X}_1,\op{X}_2) .
  \end{equation}
  This Hamiltonian is separable if $\op{V}(\op{X}_1,\op{X}_2)
  = \op{V}_1(\op{X}_1) + \op{V}_2(\op{X}_2)$.  Let $\ket{E_1}$
  and $\ket{E_2}$ denote the eigenstates of the two pieces of the
  Hamiltonian.  The eigenstates of the total Hamiltonian are then
  just $\ket{E_1;E_2}$.  In the coordinate basis, we have
  \( \inner{x_1;x_2}{E_1;E_2} = \inner{x_1}{E_1} \inner{x_2}{E_2}
   = \psi_{E_1}(x_1) \psi_{E_2}(x_2). \)
  The time dependent Schrodinger equation separates similarly and its
  solution is
  \( \ket{\psi(t)} = \ket{E_1(t);E_2(t)} = \ket{E_1}
  e^{-iE_1 t/\hbar} \otimes \ket{E_2} e^{-iE_2 t/\hbar}. \)
  In the coordinate basis:
  \( \inner{x_1;x_2}{\psi(t)} = \psi_{E_1}(x_1)
   e^{-iE_1 t/\hbar} \psi_{E_2}(x_2) e^{-iE_2 t/\hbar}. \)
  The prescription for finding solutions to separable problems in
  multiple dimensions is clear: once we have the solution in one
  dimension, just multiply the wave functions and add the energies!

  \item \emph{Two Interacting Particles.} For a system
  consisting of two particles, $m_1$ and $m_2$, consider the special case
  of a nonseparable Hamiltonian with $V(x_1,x_2) = V(x_1 - x_2)
  \neq V_1(x_1) + V_2(x_2)$ in the $x$ basis.  This is just the case
  of two interacting but otherwise isolated particles; the quantum
  mechanical treatment of such a system parallels the classical
  treatment.  That is, the Hamiltonian for this system is separable
  if we go to normal coordinates:
  \begin{subequations} \begin{gather}
   x_\R{CM} = \frac{m_1 x_1 + m_2 x_2}{m_1 + m_2} , \\ x = x_1 - x_2 .
  \end{gather}
  \end{subequations}
  With the usual definition of the reduced mass
  \( \mu = \frac{m_1 m_2}{m_1 + m_2}, \)
  the classical Hamiltonian of the system is
  \( \label{eq:CMH}
   \ClassH = \frac{p^2_{\R{CM}}}{2M} + \frac{p^2}{2\mu} + V(x), \)
  where $M = m_1 + m_2$, $p_\R{CM} = M\dot{x}_\R{CM}$, and $p = \mu
  \dot{x}$.  Using the results of the previous section, the
  eigenstates of the promotion of (\ref{eq:CMH}) are
  \( \psi_E(x_\R{CM},x) = \frac{\exp (i p_\R{CM} x_\R{CM}/\hbar)}
   {\sqrt{2 \pi \hbar}} \psi_\ScrE(x) \)
  with eigenvalues
  \( E = \frac{p_\R{CM}^2}{2M} + \ScrE . \)
  Here, $\psi_\ScrE(x)$ and $\ScrE$ are given by
  \( \bracket{\frac{p^2}{2\mu} + V(x)}\psi_\ScrE(x)
  = \ScrE \psi_\ScrE(x) . \)
  By working in the CM frame (so $x_\R{CM} = 0$), we can completely
  ignore the center of mass motion of the system, i.e., $E = \ScrE$
  and $\psi_E(x_\R{CM},x) = \psi_\ScrE(x)$. See Shankar
  pp.\ 256--258 for more.
 \end{enumerate}

 \itemI{Three Dimensions}
 \begin{enumerate}[A.]
  \itemA{The 3D Coordinate Basis.} We will further elucidate the idea of
  direct product spaces by applying the above results to the coordinate basis of the
  configuration space for a particle in three dimensions.
  \begin{enumerate}[1.]
   \item Let $\ket{x}$, $\ket{y}$,
   and $\ket{z}$ be eigenvectors of the (position) operators
   $\op{X}$, $\op{Y}$, and $\op{Z}$, respectively.  The direct
   product of these
   \( \ket{x} \otimes \ket{y} \otimes \ket{z} \defn \ket{x;y;z} \)
   is a vector in the new direct product space.  The set of all vectors
   $\ket{x;y;z}$ forms a complete basis of this space.

   \item The inner product on this space is given by
   \begin{equation} \begin{split}
    \inner{x';y';z'}{x;y;z} &= \inner{x'}{x} \inner{y'}{y}
    \inner{z'}{z} \\ &= \delta(x-x') \delta(y-y') \delta(z-z') =
    \delta^3(\vect{r} - \vect{r}') .
   \end{split} \end{equation}
   The completeness relation is, in the coordinate basis,
   \( \op{I} = \op{I}_x \otimes \op{I}_y \otimes \op{I}_z
    = \int_{\textrm{all space}} \ket{x;y;z}\bra{x;y;z} dx dy dz . \)

   \item An operator from one space acts on the corresponding element
   of a basis vector, leaving the others alone:
   \[ \op{X}\ket{x;y;z} = (\op{X} \ket{x}) \otimes \ket{y} \otimes \ket{z}
    = (x \ket{x}) \otimes \ket{y} \otimes \ket{z} = x\ket{x;y;z} . \]
   \end{enumerate}

  \itemA{Position and Momentum.}  In three dimensions, the
  position operator $\op{X}$ is replaced by a vector of operators:
  \(   \vectop{R} = \op{X} \xhat + \op{Y} \yhat
   + \op{Z} \zhat . \)
  Note that $\vectop{R} \neq \op{X}\otimes\op{Y}\otimes\op{Z}$.
  Similarly, for momentum
  \( \vectop{P} = \op{P}_x \xhat + \op{P}_y \yhat
   + \op{P}_z \zhat . \)
  The commutation relations between the component operators are
  \( [\op{X}_i,\op{X}_j] = 0, \eqnsep [\op{P}_i,\op{P}_j] = 0,
   \eqnsep [\op{X}_i,\op{P}_j] = i\hbar \delta_{ij} . \)

  \itemA{The Schrodinger Equation in Three Dimensions.} The
  following summarizes what becomes of the position and momentum
  operators in the $x$ and $p$ bases:
  \begin{center} \begin{tabular}{lcccccc}
  \toprule
  Basis & $\op{X}$ & $\op{Y}$ & $\op{Z}$ & $\op{P}_x$ & $\op{P}_y$
  & $\op{P}_z$ \\ \midrule Coordinate & $x$ & $y$ & $z$ & $-i \hbar
   \PD{}{x}$ & $-i \hbar \PD{}{y}$ & $-i \hbar \PD{}{z}$ \\ Momentum & $i \hbar
   \PD{}{p_x}$ & $i \hbar \PD{}{p_y}$ & $i \hbar \PD{}{p_z}$ & $p_x$ & $p_y$
   & $p_z$ \\ \bottomrule
  \end{tabular} \end{center}
  Written more concisely, in the coordinate basis we have
  \begin{equation}
   \vectop{R} \To \vect{r}, \quad \vectop{P} \To -i \hbar \del .
  \end{equation}

  So the Schrodinger
  equation
  \[ \op{H}\ket{\psi} = \frac{\op{P}^2}{2m}\ket{\psi}
  + \op{V}(\vect{r})\ket{\psi} = i \hbar \D{}{t} \ket{\psi} \]
  becomes
  \begin{namedeqn}{\textbf{Schrodinger Equation in 3D}}
   -\frac{\hbar^2}{2m} \nabla^2 \psi(\vect{r},t) + V(\vect{r})
   \psi(\vect{r},t) = i \hbar \PD{}{t} \psi(\vect{r},t) .
  \end{namedeqn}

 \end{enumerate}


 \itemI{Bosons and Fermions.}
 \begin{enumerate}[A.]
  \itemA{Identical Particles.}  In classical mechanics, identical
  particles are a non-issue because the perfect classical observer
  can tell them apart by following their nonidentical trajectories.
  In quantum mechanics, the ideas of the perfect observation and
  classical trajectories no longer apply---there is no basis in
  quantum mechanics for distinguishing between identical particles.

  Consider two nonidentical particles, labeled 1 and 2, at positions
  $a$ and $b$, respectively.  Their state is just
  \[ \ket{\psi} = \ket{x_1 = a; x_2 = b} = \ket{a;b} . \]
  Now suppose the particles are identical, i.e., indistinguishable.
  After measurement, how can we say whether the state is $\ket{a;b}$
  or $\ket{b;a}$?  Clearly, we cannot.  Because these two vectors are
  not multiples of each other, as they must be if they are to
  correspond to the same physical state, neither is acceptable.  The
  only choices that yield the same physical state upon the
  interchange of the particles are the symmetric state vector:
  \( \ket{a;b}_S = \ket{a;b} + \ket{b;a} \)
  and the antisymmetric state vector:
  \( \ket{a;b}_A = \ket{a;b} - \ket{b;a} . \)
  Note that these are unnormalized as written; to normalize them, we
  need only multiply by $1/\sqrt{2}$.  In general, if
  $\omega_1$ and $\omega_2$ are obtained upon measurement of
  $\op{\Omega}$, the state immediately after measurement is
  $\ket{\omega_1;\omega_2}_S$ or $\ket{\omega_1;\omega_2}_A$.

  The set of symmetric state vectors forms a subspace $\V_S$ of the
  two particle direct product space $\V_1 \otimes \V_2$, as does the
  set of antisymmetric vectors $\V_A$.  Together, these two subspaces
  cover the entire space, i.e., $\V_1 \otimes \V_2 = \V_S \oplus
  \V_A$.

  \itemA{Fermions.}  Those particles which are always
  found in antisymmetric states, such as electrons, protons, and
  neutrons, are called fermions.  Fermions have
  half-integer spins.  The Pauli exclusion principle states that two
  identical fermions cannot be in the same quantum state.  If they
  were, we would have
  \[ \ket{\omega;\omega}_A = \ket{\omega;\omega} -
  \ket{\omega;\omega} = 0 ,\]
  which clearly cannot happen.

  \itemA{Bosons.}  Particles, such as the photon, which are
  always found in symmetric states are called bosons.  Bosons have
  integer spins.  In contrast with fermions, bosons prefer to be in
  the same quantum state.

  \itemA{The Pauli Principle.} When
  dealing with three or more identical particles, we need only
  remember the Pauli principle: the wave function must remain the
  same under the exchange of any two identical bosons and change sign
  under the exchange of any two identical fermions.  For example, the
  unnormalized symmetric and antisymmetric state vectors for three
  particles are
  \( \ket{a;b;c}_S = \ket{a;b;c} + \ket{b;a;c} + \ket{b;c;a} +
  \ket{a;c;b} + \ket{c;a;b} + \ket{c;b;a} \)
  and
  \( \ket{a;b;c}_A = \ket{a;b;c} - \ket{b;a;c} + \ket{b;c;a} -
   \ket{a;c;b} + \ket{c;a;b} - \ket{c;b;a}, \)
  respectively.  To normalize these, we multiply by $1/\sqrt{6}$.
 \end{enumerate}

 % Use \bhat for bosons, \ahat for fermions

\end{enumerate}
\end{document}
