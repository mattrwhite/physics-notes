% ----------------------------------------------------------------
% The Free Particle in Quantum Mechanics *************************
% ----------------------------------------------------------------
\documentclass{article}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{booktabs}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
\reversemarginpar
% MATH -----------------------------------------------------------
\input{../Include/Layout.tex}
\input{../Include/Math.tex}
\input{../Include/Vectors.tex}
\input{../Include/Quantum.tex}
\newcommand{\A}{\op{A}}  % Generic operators for various derivations
\newcommand{\B}{\op{B}}
\newcommand{\C}{\op{C}}
% ----------------------------------------------------------------
%\draftmode
\begin{document}
% \maketitle
% ----------------------------------------------------------------
\begin{enumerate}[I.]
 \itemI{The Free Particle in One Dimension.}
 \begin{enumerate}[A.]
  \itemA{Energy Eigenstates of the Free Particle.}  For a
  free particle with mass $m$, the classical Hamiltonian is
  $\mathcal{H} = p^2/2m$.  Promotion gives us
  \begin{equation}
   \op{H} = \frac{\op{P}^2}{2m} .
  \end{equation}
  The EES equation is then
  \begin{equation}\label{eq:FreePartWaveEq}
   \frac{\op{P}^2}{2m} \ket{\psi} = E \ket{\psi} .
  \end{equation}
  Let us plug in the trivial solution $\ket{\psi} = \ket{p}$ where
  $\ket{p}$ is an eigenstate of $\op{P}$, and hence of $\op{P}^2$.
  This gives us
  \begin{equation}
   (\frac{p^2}{2m} - E) \ket{p} = 0 ,
  \end{equation}
  so the allowed values of $p$ are
  \begin{equation}
   p = \pm \sqrt{2mE} .
  \end{equation}
  Nowhere have any restrictions appeared on the value of $E$,
  other than the usual $E \geq 0$, so the eigenspectrum is
  continuous. Now, for each energy eigenvalue $E$, there are two
  orthogonal eigenstates, corresponding to the plus and minus
  signs above. Physically these represent particles moving to
  the right and left with the same kinetic energy. In order to
  uniquely identify the eigenstates we must introduce an
  additional label to differentiate between these two cases.
  However, we could simply label the energy eigenstates with the
  non-degenerate value of $p$, which uniquely determines the
  state.  The propagator is then
  \begin{equation}
   \op{U}(t) = \infint \ket{p}\bra{p} e^{-iE(p)t/\hbar} dp
    = \infint \ket{p}\bra{p} e^{-ip^2t/2m\hbar} dp .
  \end{equation}
  This integral can actually be evaluated explicitly in the $x$
  basis; the matrix elements are:
  \begin{eqnarray}\label{UElements}
   \innerop{x}{U}{x'} & = & \infint \inner{x}{p} \inner{p}{x'}
    e^{-ip^2t/2m\hbar} dp \nonumber \\ & = & \frac{1}{2 \pi \hbar}
    \infint e^{ip(x-x')/\hbar} \cdot e^{-ip^2t/2m\hbar} dp \nonumber
    \\ & = & \sqrt{\frac{m}{2 \pi i \hbar t}} e^{im(x-x')^2 / 2 \hbar t}
  \end{eqnarray}
  The time evolution of a given initial state is
  \begin{equation}
   \ket{\psi(t)} = \op{U}(t) \ket{\psi(0)} = \int \op{U}(t) \ket{x'}
   \inner{x'}{\psi(0)} dx ,
  \end{equation}
  written in the $x$ basis,
  \begin{equation}\label{eq:TimeEv}
   \psi(x,t) = \inner{x}{\op{U}(t)|\psi(0)} = \int \inner{x}{\op{U}(t)|x'}
   \inner{x'}{\psi(0)} dx .
  \end{equation}
  We can easily evaluate this by plugging in the matrix elements
  for $\op{U}(t)$ found in (\ref{UElements}) and by noting that
  $\inner{x'}{\psi(0)} = \psi(x',0)$.

  \itemA{Free Particle Wave Function in the $x$ Basis.} The
  general solution to (\ref{eq:FreePartWaveEq}) in the $x$ basis
  is called a plane wave:
  \begin{equation}
   \psi_E(x) = A e^{ipx/\hbar} + B e^{-ipx/\hbar} ,
  \end{equation}
  where $\abs{A}^2 + \abs{B}^2 = 1/2\pi\hbar$.
  Recall that $p = \sqrt{2mE}$, which implies that we cannot have
  $E<0$ for the wave function to be physical (i.e., in our
  physical Hilbert space). The probability current for this wave
  function is $(\abs{A}^2 - \abs{B}^2)p/m$; observe that $p/m$ is
  just the classical velocity of the particle.

  We often write the free particle wave function as
  \( \psi_E(x) = A e^{ikx} + B e^{-ikx} , \)
  where
  \( k \defn \frac{p}{\hbar} = \sqrt{\frac{2mE}{\hbar^2}} \)
  is called the wavenumber.  The wavenumber is related to the de
  Broglie wavelength $\lambda$ by
  \( \lambda = \frac{2 \pi}{k} = \frac{h}{p} . \)
  The de Broglie wavelength determines the extent to which a particle
  behaves like a wave: if $\lambda$ is much smaller than the relevant
  dimensions in a given problem, the particle will behave like a
  particle while if $\lambda$ is of the same order as (or larger than)
  the relevant dimensions of the problem, the particle will behave
  like a wave.

  \itemA{Wave Packets.} A wave packet is a linear combination of free
  particle EESs; it represents a state which is localized, to some
  degree, in space and has a spread in momentum.  We will consider the
  prototypical wave packet: the Gaussian which, at $t = 0$, is given
  by
  \( \psi(x,0) = \frac{1}{(\pi \sigma^2)^{1/4}} e^{-x^2/2 \sigma^2}
  e^{i p_0 x/\hbar} .\)
  Note that we have centered the wave packet at the origin for
  simplicity.  The uncertainty in position is $\Delta \op{X} =
  \sigma/\sqrt{2}$, the mean momentum is $p_0$, and the uncertainty
  in momentum is $\Delta \op{P} = \hbar/(\sqrt{2}\sigma)$.  Using
  (\ref{eq:TimeEv}), we obtain
  \begin{multline} 
   \psi(x,t) = \bracket{\sqrt{\pi}\parenth{\sigma + \frac{i\hbar
    t}{m \sigma}}}^{-1/2} \exp \bracket{\frac{-x(t)^2}{2\sigma^2(1 + i\hbar t/m \sigma^2)}} \\ \times 
    \exp \bracket{\frac{i p_0}{\hbar}\parenth{x - \frac{p_0 t}{2m}}},
  \end{multline}
  where we have defined $x(t) = x - p_0 t/m$.
% Note: the phase factor (last term) above seems to be correct - appears in cohen-tannoudji too
  It can be useful to separate the phase factor more completely:
  \begin{multline} 
   \psi(x,t) = \bracket{\sqrt{\pi}\parenth{\sigma + \frac{i\hbar
    t}{m \sigma}}}^{-1/2} \exp \bracket{\frac{-x(t)^2}{2 \sigma^2(1+\hbar^2 t^2 / m^2 \sigma^4)}} \\ \times 
    \exp \bracket{\frac{i \hbar t x(t)^2}{2 m \sigma^4(1+\hbar^2 t^2 / m^2 \sigma^4)}} 
    \exp \bracket{\frac{i p_0}{\hbar}\parenth{x - \frac{p_0 t}{2m}}} .
  \end{multline}
  In the limit of long expansion time, the first phase factor becomes simply $\exp (imx(t)^2/2\hbar t)$.
  The probability density for this packet is
  \( \abs{\psi(x,t)}^2 = \bracket{\pi\parenth{\sigma^2 +
  \frac{\hbar^2 t^2}{m^2 \sigma^2}}}^{-1/2} \exp
  \bracket{\frac{-x(t)^2}{\sigma^2 (1 + \hbar^2 t^2/m^2
  \sigma^4)}} . \)
  The mean position of the packet is
  \( \expval{X} = \frac{p_0 t}{m} = \frac{\expval{P} t}{m} . \)
  The packet moves like a classical particle with velocity $p_0/m$
  (i.e., its group velocity is $p_0/m$).
  Note that the mean momentum of the packet does not vary (conservation
  of energy), nor does $\Delta \op{P}$.  Because of the initial
  uncertainty in the momentum of the packet, the uncertainty in
  position will grow with time:
  \( \Delta\op{X}(t) = \frac{\sigma}{\sqrt{2}}\sqrt{1+\frac{\hbar^2
  t^2}{m^2 \sigma^4}} . \)

  See Harris 5.3 for a discussion of the dispersion relation
  \( \omega(k) = \frac{mc^2}{\hbar} + \frac{\hbar k^2}{2m} \)
  for a free particle (recall that $\omega \defn E/\hbar$) from which
  we find the group velocity:
  \( v_g = \D{\omega(k)}{k} = \frac{\hbar k}{m} = \frac{p}{m} . \)
  Note that the phase velocity is $v_p = \omega(k)/k$.
 \end{enumerate}

 \itemI{The Free Particle in Three Dimensions.}
 \begin{enumerate}[A.]
  \itemA{The Free Particle in Cartesian Coordinates.}  The energy
  eigenstates for the free particle in three dimensions are clearly
  just $\ket{\vect{p}}$, corresponding to energy $E = (\dotpd{p}{p})/2m =
  p^2/2m$.  However, there are now infinitely many eigenstates
  for a given energy, each one corresponding to a point
  on the spherical shell of radius $\sqrt{2mE}$ in $\vect{p}$ space.

  In the coordinate basis, the wave function of a free particle
  with momentum $\vect{p}$ is
  \begin{equation*} \begin{split}
   \inner{\vect{r}}{\vect{p}} &= \inner{x;y;z}{p_x;p_y;p_z} \\
   &= \inner{x}{p_x}\inner{y}{p_y}\inner{z}{p_z} \\
   &= \parenth{\frac{1}{\sqrt{2\pi \hbar}}}^3 e^{ip_x x/\hbar}
    e^{ip_y y/\hbar} e^{ip_z z/\hbar} \\
   &= \parenth{\frac{1}{\sqrt{2\pi \hbar}}}^3 e^{i\dotpd{p}{r}/\hbar}
  \end{split} \end{equation*}
  This is nothing more than a plane wave:
  \( \psi(\vect{r}) = \parenth{\frac{1}{\sqrt{2\pi \hbar}}}^3
   e^{i\dotpd{k}{r}} \)
  where the wavevector $\vect{k}$ is given by
  \( \vect{k} \defn \frac{\vect{p}}{\hbar} . \)
  The magnitude of the wavevector is the usual wavenumber $k$ for
  which all the one dimensional relations hold, most notably
  \( E = \frac{\hbar^2 k^2}{2m} . \)

  If, as is often the case, a properly normalized state is required,
  we adopt the so-called box normalization:
  \begin{namedeqn}{\textbf{Box Normalization}}
   \psi(\vect{r}) = \frac{1}{\sqrt{V}} e^{i\dotpd{k}{r}},
  \end{namedeqn}
  where we assume that the particle is confined to a box of volume
  $V$.  At end of the problem, $V$ can be taken to infinity if necessary.
  Periodic boundary conditions are often imposed, yielding
  \( \vect{k} = \frac{2 \pi}{L}(n_x,n_y,n_x) ,\)
  where $n_x$, $n_y$, and $n_z$ are integers and we have assumed a
  cubic box of side $L$.  The density of states in $\vect{k}$ space is
  then $(L/2\pi)^3$.

  \itemA{The Free Particle in Spherical Coordinates.}  Since the
  potential felt by a free particle, namely $V = 0$, has no angular
  dependence, we can find solutions of the form
  \( \psi_{E,l,m} (r, \theta, \phi) = R_{E,l}(r) Y^m_l (\theta,
   \phi), \)
  where $Y^m_l$ is a spherical harmonic and $R_{E,l}$ has yet to be
  determined.  A straightforward but tedious analysis (see Shankar
  pp. 346--349) yields
  \( R_{E,l} (r) = Aj_l(kr) + Bn_l(kr) , \)
  where $A$ and $B$ are constants and, as usual, $k$ is given by
  $E = \hbar^2 k^2 / 2m$.  The $n_l$
  are spherical Neumann functions and are only included in cases
  where the origin is excluded, since they are singular there.  The
  $j_l$ are spherical Bessel functions and are regular over all
  space.  The orthogonality relation for the spherical Bessel
  functions is
  \( \int_0^\infty j_l(kr) j_{l'}(k'r) r^2 dr =
  \frac{\pi}{2k^2}\delta_{ll'} \delta(k-k'). \)

  \itemA{Spherical Wave Decomposition.}  The results of the two
  pervious sections can be related using the spherical wave
  decomposition:
  \begin{align}
   e^{i\dotpd{k}{r}} = e^{ikr\cos\theta} &= \sum_{l=0}^{\infty} i^l
   (2l + 1)j_l(kr)P_l(\cos \theta) \notag \\
   &= 4\pi \sum_{l=0}^{\infty} \sum_{m=-l}^{l}  i^l (2l + 1)
    j_l(kr) Y_l^m(\theta_{\vect{k}},\phi_{\vect{k}})
    Y_l^m(\theta_{\vect{r}},\phi_{\vect{r}}) .
  \end{align}
  Here, $\theta$ is the angle between $\vect{k}$ and $\vect{r}$.
 \end{enumerate}

\end{enumerate}

\end{document}
