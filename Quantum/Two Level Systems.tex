% ----------------------------------------------------------------
% Two Level Systems **********************************************
% ----------------------------------------------------------------
\documentclass{article}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{booktabs}
\usepackage{verbatim}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
\reversemarginpar
% MATH -----------------------------------------------------------
\input{../Include/Math.tex}
\input{../Include/Layout.tex}
\input{../Include/Quantum.tex}
\newcommand{\Hm}{\op{H}_0}
\newcommand{\EA}{E_-}
\newcommand{\EB}{E_+}
\newcommand{\Urot}{\op{U}_{\R{rot}}}
\newcommand{\Eavg}{\bar{E}}
% ----------------------------------------------------------------
% \draftmode
\begin{document}
% \maketitle
% Static stuff is from Cohen-Tannoudji Ch 4.C; except I use E2-E1 in
%  place of E1-E2, which suprisingly results in huge changes
% ----------------------------------------------------------------
\begin{enumerate}[I.]
 \itemI{Static Perturbation.}
 \begin{enumerate}[A.]
  \itemA{Static Effects.}
  Let the energy eigenstates of unperturbed system be denoted by
  $\ket{1}$ and $\ket{2}$.  The Hamiltonian in the basis is
  \( \label{eq:Hzero} \Hm = E_1 \ket{1} \bra{1} + E_2 \ket{2} \bra{2}
   = \begin{pmatrix} E_1 & 0 \\ 0 & E_2 \end{pmatrix}. \)
  Without loss of generality, we take $E_2 > E_1$.
  We now introduce a perturbation $\op{W}$ which in the same basis is
  \( \op{W} = \begin{pmatrix} W_{11} & W_{12} \\ W_{21} & W_{22}
   \end{pmatrix}. \)
  Note that $W_{11}$ and $W_{22}$ are real and
  \( W_{12} = W_{21}^* \defn W e^{i \phi}. \)

  The total Hamiltonian is now
  \( \op{H} = \Hm + \op{W} = \begin{pmatrix} E_1 & W e^{i \phi}
   \\ W e^{-i \phi} & E_2 \end{pmatrix}. \)
  Here, we have redefined $E_1 = E_1 + W_{11}$ and $E_2 = E_2 + W_{22}$.
  The eigenvalues of the total Hamiltonian are readily found to be
  \begin{subgather}
   \EA = \Eavg
    - \frac{1}{2}\hbar\Omega, \\
   \EB = \Eavg
    + \frac{1}{2}\hbar\Omega,
  \end{subgather}
  where we have introduced what will be later called the generalized Rabi frequency
  \( \Omega = \sqrt{\omega_{21}^2 + 4 W^2/\hbar^2}, \)
  and $\Eavg = (E_1 + E_2)/2$ and $\omega_{21} =
  (E_2 - E_1)/\hbar$. The corresponding eigenstates are
  \begin{subequations} \label{eq:CplTwoLvlEESs} \begin{gather} 
   \ket{-} = \cos(\theta/2) e^{i \phi/2}\ket{1}
    - \sin(\theta/2) e^{-i \phi/2}\ket{2}, \\
   \ket{+} = \sin(\theta/2) e^{i \phi/2}\ket{1}
    + \cos(\theta/2) e^{-i \phi/2}\ket{2}.
  \end{gather} \end{subequations}
  Here, the mixing angle $\theta$ is given by
  \( \tan \theta = \frac{2 W}{\hbar \omega_{21}},
    \eqnsep 0 \leq \theta < \pi. \)
  We can easily show that $\cos \theta = \omega_{21}/\Omega$ and
  $\sin \theta = 2 W/\hbar \Omega$.  As $W \To 0$, with $\omega_{21} > 0$,
  $\ket{-} \To \ket{1}$ and $\ket{+} \To \ket{2}$; if $\omega_{21} < 0$,
  this mapping is reversed.

  It is worth noting that $\abs{\EB - \EA} > \abs{E_2 - E_1}$, i.e.,
  the coupling results in repulsion of the levels.  For $\hbar
  \omega_{21} \gg W$, $\EA$ and $\EB$ can be expanded as
  \begin{subgather}
   \EA = \Eavg - \frac{\hbar}{2} \omega_{21} \parenth{ 1
    + 2 \abs{\frac{W}{\hbar \omega_{21}}}^2 + \ldots}, \\
   \EB = \Eavg + \frac{\hbar}{2} \omega_{21} \parenth{ 1
    + 2 \abs{\frac{W}{\hbar \omega_{21}}}^2 + \ldots},
  \end{subgather}
  The eigenstates in this limit are given by
  \begin{subgather}
   \ket{-} = \ket{1} - e^{-i \phi} \frac{\abs{W}}{\hbar \omega_{21}}
    \ket{2} + \ldots, \\
   \ket{+} = \ket{2} + e^{i \phi} \frac{\abs{W}}{\hbar \omega_{21}}
    \ket{1} + \ldots .
  \end{subgather}
  In the opposite extreme of degenerate levels, $\omega_{21} = 0$, the
  perturbation splits the levels by $2\abs{W}$. Thus, the closer (to
  degeneracy) the levels, the greater the effect of an off-diagonal
  perturbation.  In this case, $\ket{-}$ and $\ket{+}$ are equal
  superpositions of $\ket{1}$ and $\ket{2}$.

  \itemA{Evolution.}
  The evolution of the perturbed system is given, up to a global
  phase, by
  \( \ket{\psi(t)} = c_1(t) \ket{1} + c_2(t) \ket{2} 
   = c_{-} e^{i \Omega t/2} \ket{-} + c_{+} e^{-i \Omega t/2} \ket{+}. \)
  From (\ref{eq:CplTwoLvlEESs}), we can read off the unitary matrix $U$ which appears below:
  \begin{align} 
  \begin{pmatrix} c_1(t) \\ c_2(t) \end{pmatrix}
   &= U \begin{pmatrix} e^{i \Omega t/2} & 0 \\ 0 & e^{-i \Omega t/2} \end{pmatrix} U^{-1} 
   \begin{pmatrix} c_1(0) \\ c_2(0) \end{pmatrix} \notag
  \\ \label{eq:Ustatic}
  \begin{pmatrix} c_1(t) \\ c_2(t) \end{pmatrix}
   &= \begin{pmatrix} \cos(\frac{1}{2}\Omega t) + \frac{i \omega_{21}}{\Omega}
    \sin(\frac{1}{2}\Omega t) & -e^{i\phi}\frac{2iW}{\hbar \Omega} \sin(\frac{1}{2}\Omega t) \\
    -e^{-i\phi}\frac{2iW}{\hbar \Omega} \sin(\frac{1}{2}\Omega t) & \cos(\frac{1}{2}\Omega t)
     - \frac{i \omega_{21}}{\Omega} \sin(\frac{1}{2}\Omega t) \end{pmatrix}
   \begin{pmatrix} c_1(0) \\ c_2(0) \end{pmatrix}   
  \end{align}
  The usual example considers $\ket{\psi(0)} = \ket{1}$, in which case we can read off
  $\abs{\inner{2}{\psi(t)}}^2 = (4W^2/\hbar^2 \Omega^2) \sin^2 (\Omega t/2)$.
  This is sometimes known as Rabi's formula, especially in the case of an oscillatory 
  perturbation.
 \end{enumerate}

 \itemI{Oscillatory Perturbation.}
 \begin{enumerate}[A.]
 \itemA{Rotating (Frame) Picture.}
  We will consider an oscillatory perturbation
  \( \op{W}(t) = \begin{pmatrix} 0 & \hbar\Omega_R \cos \omega t \\
   \hbar\Omega_R \cos \omega t & 0 \end{pmatrix}, \)
  where the Rabi frequency $\Omega_R$ parameterizes the strength of the perturbation. 
  We will change to a rotating frame synchronous with the perturbation
  to remove the time dependence by introducing
  \( \Urot(t) = \begin{pmatrix} e^{i \omega t /2} & 0 \\
   0 & e^{-i \omega t /2}  \end{pmatrix}. \)
  This is equivalent to the interaction picture with $\op{H}_0 = (-\hbar\omega/2)\ket{1}\bra{1} + (\hbar\omega/2)\ket{2}\bra{2}$ and $\op{H}_1 = (E_1 + \hbar\omega/2)\ket{1}\bra{1} + (E_2-\hbar\omega/2)\ket{2}\bra{2} + \op{W}(t)$.
   
  We have $\ket{\psi_\R{rot}(t)} = \Urot^\dag \ket{\psi_S(t)}$ and
  $\op{H}_\R{rot} = \Urot^\dag \op{H}_S \Urot$; plugging these into the Schrodinger
  equation yields
  \( i\hbar \D{}{t}\ket{\psi_\R{rot}(t)} = (\op{H}_\R{rot} - i\hbar \Urot^\dag \D{}{t} \Urot)\ket{\psi_\R{rot}(t)}. \)
  The expression in parenthesis on the right is the effective Hamiltonian in the
  rotating frame, denoted $\op{H}_\R{rot,eff}$.  With $\op{H}_S = \Hm + \op{W}(t)$, 
  where $\Hm$ is given by (\ref{eq:Hzero}), we find
  \( \op{H}_\R{rot} = \begin{pmatrix} E_1 & \hbar \Omega_R e^{-i \omega t} \cos \omega t \\
   \hbar \Omega_R e^{i \omega t} \cos \omega t & E_2 \end{pmatrix}
   \To \begin{pmatrix} E_1 & \hbar \Omega_R/2 \\
   \hbar \Omega_R/2 & E_2 \end{pmatrix}. \)
  The final result is obtained by discarding the $2\omega$ terms (after expanding $\cos \omega t$) in what
  is known as the rotating wave approximation.
  The effective Hamiltonian in the rotating frame is
  \( \op{H}_\R{rot,eff} = \begin{pmatrix} E_1 + \hbar \omega/2 & \hbar \Omega_R/2 \\
   \hbar \Omega_R/2 & E_2 - \hbar \omega/2 \end{pmatrix}
  = \frac{\hbar}{2} \begin{pmatrix} -\delta & \Omega_R \\
   \Omega_R & \delta \end{pmatrix}. \)
  To arrive at the final expression, we have taken $E_2 = -E_1 = \hbar \omega_{21}/2$ and
  introduced the detuning
  \( \delta = \omega - \omega_{21}. \)
  We can now apply all the results of the static case to obtain $\ket{\psi_\R{rot}(t)}$,
  with $W \To \hbar \Omega_R/2$ and $\omega_{21} \To \delta$. The generalized Rabi frequency is
  \( \Omega = \sqrt{\delta^2 + \Omega_R^2}. \)
  In the end,
  \( \label{eq:TotalProp} \ket{\psi_S(t)} = \Urot(t) \op{U}_\R{static}(t-t_0) \Urot^\dag(t_0) \ket{\psi_S(t_0)}, \)
  where $\op{U}_\R{static}$ is the matrix in (\ref{eq:Ustatic}).
  
  \itemA{Interaction Picture.}
  The problem can also be treated in the interaction picture, although in this case we
  typically use less formal manipulations, writing
  \( \ket{\psi_S(t)} = \sum_{k=1,2} b_k(t) e^{-i E_k t/\hbar} \ket{k}, \)
  so that $\ket{\psi_I(t)} = \sum_k b_k(t) \ket{k}$.
  Plugging into the Schrodinger equations and making the rotating wave approximation yields
  \begin{subalign}
   \D{}{t} b_1(t) &= -(i\Omega_R/2\hbar) b_2(t) e^{i \delta t}, \\
    \D{}{t} b_2(t) &= -(i\Omega_R/2\hbar) b_1(t)  e^{-i \delta t}.
  \end{subalign}
  These yield the interaction picture propagator as 
  \( U_I(t) = \begin{pmatrix} e^{i \delta t /2} & 0 \\
   0 & e^{-i \delta t /2} \end{pmatrix} \op{U}_\R{static}(t). \)
  Using $\op{U}_S(t) = \op{U}_S^{(0)}(t) \op{U}_I(t)$, we can verify that
  this agrees with (\ref{eq:TotalProp}).
 \end{enumerate}

 \itemI{Density Matrix Approach with Phenomenological Decay.}
 Inserting the Hamiltonian $\Hm + \op{W}$ in the equation of motion
 for the density matrix, $\tD{\DM}{t} = -(i/\hbar)[\op{H}, \DM]$ and
 assuming that the diagonal elements of $\op{W}$ are zero (the other
 case can be handled as in the first section), we obtain
 \begin{subequations} \begin{gather}
  \D{}{t} (\rho_{11} - \rho_{22}) = -\frac{2 i}{\hbar} (W_{12}
   \rho_{21} - W_{21} \rho_{12}), \\
  \D{}{t} \rho_{21} = -i \omega_{21} \rho_{21} - \frac{i}{\hbar}
  W_{21} (\rho_{11} - \rho_{22}).
 \end{gather} \end{subequations}
 Recall that $tr \DM = \rho_{11} + \rho_{22} = 1$ so
 $\tD{\rho_{11}}{t} = -\tD{\rho_{22}}{t}$, which should be physically
 obvious.
 We now include phenomenological decay terms to yield
 \begin{subequations} \begin{gather}
  \D{}{t} (\rho_{11} - \rho_{22})
   = -\frac{2 i}{\hbar} (W_{12} \rho_{21} - W_{21} \rho_{12})
   - \Gamma [(\rho_{11} - \rho_{22}) - \Delta \rho_0], \\
  \D{}{t} \rho_{21} = -i \omega_{21} \rho_{21} - \frac{i}{\hbar}
  W_{21} (\rho_{11} - \rho_{22}) - \gamma \rho_{21}.
 \end{gather} \end{subequations}
 Here, we have introduced $\Delta \rho_0$, the equilibrium value of
 $(\rho_{11} - \rho_{22})$, sometimes denoted
 $(\rho_{11} - \rho_{22})_0$. The decay rates can be expressed in
 terms of decay times as $\Gamma = 1/\tau$ and $\gamma = 1/T_2$.

 We now consider an oscillatory perturbation of the form $W_{12}
 = W \cos \omega t$.  From the above equations and introducing
 \( \sigma_{21} = \rho_{21} e^{i \omega t}, \)
 we find
 \begin{subequations} \begin{gather}
  \D{}{t} (\rho_{11} - \rho_{22})
   = -\frac{i}{\hbar} W (\sigma_{21} - \sigma_{12})
   - \Gamma [(\rho_{11} - \rho_{22}) - \Delta \rho_0], \\
  \D{}{t} \sigma_{21} = i \sigma_{21} (\omega - \omega_{21}) -
   \frac{i}{2 \hbar} W (\rho_{11} - \rho_{22}) - \gamma \sigma_{21}.
 \end{gather} \end{subequations}
 Note that we have made the rotating wave approximation, discarding
 terms which vary as $e^{\pm 2 i \omega t}$. The contribution of
 these terms averages to zero over time scales long compared to
 $2 \pi/\omega$.

 The steady state solution, i.e., for $\D{}{t} (\rho_{11} -
 \rho_{22}) = 0$ and $\D{}{t} \sigma_{21} = 0$, can be shown to be
 (see Yariv 8.1)
 \begin{subequations} \begin{gather}
  \Re \sigma_{21} = \frac{\delta T_2^2 (W/2\hbar) \Delta \rho_0}
   {1 + \delta^2 T_2^2 + 4 (W/2\hbar)^2 T_2 \tau}, \\
  \Im \sigma_{21} = \frac{-(W/2\hbar) T_2 \Delta \rho_0}
   {1 + \delta^2 T_2^2 + 4 (W/2\hbar)^2 T_2 \tau}, \\
  (\rho_{11} - \rho_{22}) = \frac{1 + \delta^2 T_2^2}
   {1 + \delta^2 T_2^2 + 4 (W/2\hbar)^2 T_2 \tau} \Delta \rho_0 .
 \end{gather} \end{subequations}
 Recall that $\delta = \omega - \omega_{21}$ is the detuning.

\end{enumerate}

\begin{comment}
\itemA{Rabi Oscillations.}
  Consider the Hamiltonian $\op{H} = \Hm + \op{W}(t)$ and denote the
  eigenstates and eigenvalues of $\Hm$ by $\ket{k}$ and $E_k$,
  respectively. To determine the evolution due to $\op{W}(t)$, we can
  write
  \( \ket{\psi(t)} = \sum_k c_k(t) e^{-i E_k t/\hbar} \ket{k}. \)
  Plugging this into the Schrodinger equation and taking the inner
  product with $\ket{j}$ yields
  \( \D{}{t}c_j(t) = -\frac{i}{\hbar} \sum_k \bra{j}\op{W}(t)\ket{k}
   c_k(t) e^{i (E_j - E_k) t/\hbar}. \)

  For an oscillatory potential
  \( \op{W}(t) = \begin{pmatrix} 0 & W \cos \omega t \\
   W \cos \omega t & 0 \end{pmatrix}, \)
  we have
  \begin{subequations} \begin{align}
   \D{}{t} c_1(t) &= -(i/\hbar) c_2(t) W \cos \omega t
    e^{-i \omega_{21} t}, \\
    \D{}{t} c_2(t) &= -(i/\hbar) c_1(t) W \cos \omega t
    e^{i \omega_{21} t}.
  \end{align} \end{subequations}
  We now make the so-called rotating wave approximation by discarding
  terms which vary as $e^{\pm i (\omega + \omega_{21}) t}$.  This
  yields
  \begin{subequations} \begin{align}
   \D{}{t} c_1(t) &= -(iW/2\hbar) c_2(t) e^{i \delta t}, \\
    \D{}{t} c_2(t) &= -(iW/2\hbar) c_1(t)  e^{-i \delta t}.
  \end{align} \end{subequations}
  where we have defined the detuning $\delta \defn \omega -
  \omega_{21}$. This coupled system is easily solved (see, e.g.,
  Scully and Zubairy 5.2.1) to give
   \( \begin{pmatrix} c_1(t) e^{-i \delta t/2} \\
    c_2(t) e^{i \delta t/2} \end{pmatrix}
   = \begin{pmatrix} \cos(\frac{1}{2}\Omega t) - \frac{i \delta}{\Omega}
    \sin(\frac{1}{2}\Omega t) & \frac{i W}{\hbar \Omega} \sin(\frac{1}{2}\Omega t) \\
    \frac{i W}{\hbar \Omega} \sin(\frac{1}{2}\Omega t) & \cos(\frac{1}{2}\Omega t)
     + \frac{i \delta}{\Omega} \sin(\frac{1}{2}\Omega t) \end{pmatrix}
   \begin{pmatrix} c_1(0) \\ c_2(0) \end{pmatrix}. \)
  where
  \( \Omega \defn \sqrt{\delta^2 + (W/\hbar)^2}. \)
  In the case where the system is initially in $\ket{1}$, i.e.,
  $c_1(0) = 1$, we can read off the probability of finding it in
  $\ket{2}$ at a later time as $c_2^2(t) = (W/\hbar \Omega)^2 \sin^2
  \parenth{\Omega t/2}$.  This is sometimes referred to as
  Rabi's formula.

  \itemA{Dressed State Approach.} MAY BE WRONG
  The $e^{\pm i \delta t}$ factors in the above result suggest we
  consider
  \( \ket{\psi(t)} = b_1(t) e^{i \omega t/2} \ket{1}
   + b_2(t) e^{-i \omega t/2} \ket{2}. \)
  Plugging this into the Schrodinger equation with
  \( \op{H}(t) = \begin{pmatrix} E_1 & W \cos \omega t \\
   W \cos \omega t & E_2 \end{pmatrix}, \)
  yields, with $\bar{E} = (E_1 + E_2)/2$ and making the rotating wave
  approximation,
  \( \begin{pmatrix} \D{}{t} b_1(t) \\ \D{}{t} b_2(t) \end{pmatrix}
   = -\frac{i}{\hbar} \begin{pmatrix} \bar{E} + \hbar \Omega/2 & W/2 \\
    W/2 & \bar{E} - \hbar \Omega/2 \end{pmatrix}
   \begin{pmatrix} b_1(t) \\ b_2(t) \end{pmatrix}. \)
  The two by two matrix is the Hamiltonian in the picture which
  ``rotates'' with the perturbation.  Note especially that it is
  independent of time, in analogy to a forced oscillator which
  oscillates at the driving frequency regardless of its natural
  frequency.  This eigenstates of this Hamiltonian are found to be
  \begin{subequations} \begin{align}
   \ket{+} &= \cos \theta \ket{1} + \sin \theta \ket{2}, \\
   \ket{-} &= \sin \theta \ket{1} - \cos \theta \ket{2},
  \end{align} \end{subequations}
  where $\cos 2 \theta = \delta/\Omega$. These are referred to as the
  dressed states of the system (or atom). The corresponding energies
  are
  \( E_{\pm} = \bar{E} \pm \hbar \Omega /2. \)
  For $\abs{\delta} \gg W/\hbar$ (but still consistent with the
  rotating wave approximation), the energy shifts for $\ket{1}$ and
  $\ket{2}$ are $W^2/4 \hbar \delta$ and $-W^2/4 \hbar \delta$,
  respectively, while for $\abs{\delta} \ll W/\hbar$ the shifts are
  $\R{sgn}(\delta) \hbar \Omega /2$ for $\ket{1}$ and $-\R{sgn}(\delta)
  \hbar \Omega /2$ for $\ket{2}$. Consider the case where $W$, the
  strength of the perturbation, varies with position. A system in the
  $\ket{1}$ state will experience a force attracting it toward regions
  of greater strength if $\delta < 0$ or of lower strength if $\delta
  > 0$.  The most obvious example of this effect is the dipole force
  on atoms in an electromagnetic field (e.g. laser light). In this
  case, the energy shifts are known as AC Stark shifts or light
  shifts
\end{comment}

\end{document}
