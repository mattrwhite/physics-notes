% ----------------------------------------------------------------
% Infinite Dimensional Vector Spaces *****************************
% ----------------------------------------------------------------
\documentclass{article}
\usepackage{enumerate}
\usepackage{amsmath}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
\reversemarginpar
% MATH -----------------------------------------------------------
\input{../Include/Layout.tex}
\input{../Include/Math.tex}
\input{../Include/Vectors.tex}
\input{../Include/Quantum.tex}
% ----------------------------------------------------------------
\begin{document}
% \maketitle
% ----------------------------------------------------------------
\begin{enumerate}[I.]
 \item \textbf{The Transition to Infinite Dimension.}
 \begin{enumerate}[A.]
  \item \emph{Functions as Vectors.}  Consider some
  function $f$ defined on an interval, say $[a,b]$, from which we
  extract $n$ values, call them $x_1, x_2, \ldots , x_n$. We can
  use these to construct a vector space $\textrm{V}^n(\Real)$.  In this
  space, we \emph{define} the vector corresponding to $f$:
  \begin{equation}
   \ket{f_n} = \left[ \begin{array}{c} f(x_1) \\ f(x_2) \\ \vdots
   \\ f(x_n) \end{array} \right] .
  \end{equation}
  This matrix is written in the basis
  \begin{equation}
   \ket{x_i} = \left[ \begin{array}{c} 0 \\ \vdots \\
   1 \\ \vdots \\ 0 \end{array} \right] ,
  \end{equation}
  where all entries are zero save the one in the $i$th position.
  The basis vector $x_i$ then corresponds to a function that is
  one at $x = x_i$ and zero everywhere else. If we define the
  inner product as usual:
  \begin{equation}
   \inner{f_n}{g_n} = \sum_{i=1}^n f_n(x_i) g_n(x_i) ,
  \end{equation}
  we get orthonormality
  \begin{equation}
   \inner{x_i}{x_j} = \delta_{ij}
  \end{equation}
  and completeness
  \begin{equation}
   \sum_i \ket{x_i}\bra{x_i} = \op{I} .
  \end{equation}
  Note that $\inner{x_i}{f_n} = f_n(x_i)$, just the value of $f_n$
  at $x_i$ (examine the matrices).
  This result will be very important for the generalization to
  infinite dimensions.

  \item \emph{Vectors in Infinite Dimensions.} The logical
  extension of the above discussion to infinite dimensions is to
  take the limit $n \To \infty$. However, in this limit, the inner
  product becomes infinite for nearly any pair of functions.  To
  remedy this, we divide the inner product by something that goes
  as $(a-b)/n$.  Then in the limit, the sum becomes the usual Riemann
  integral and we have (now allowing $\textrm{V}$ to be complex)
  \begin{equation}
   \inner{f}{g} = \int_a^b f^*(x) g(x) dx .
  \end{equation}
  We will usual omit the limits from the integral.
  The completeness relation generalizes simply as
  \begin{equation}
   \int \ket{x}\bra{x} dx = \op{I} ,
  \end{equation}
  but what of the orthogonality relation? We know that
  $\inner{x}{x'} = 0$ for $x \neq x'$, but we can say nothing
  about the case $x = x'$.  Let us call $\inner{x}{x'} =
  \delta(x,x') = \delta(x-x')$.  The second equality comes from
  the fact that $\inner{x}{x'}$ depends only on the difference
  between $x$ and $x'$.  Now, let us compute
  \begin{equation}
   \inner{x}{f} = \inner{x}{\op{I}|f} = \int \inner{x}{x'}
   \inner{x'}{f} dx' .
  \end{equation}
  In analogy with the finite dimensional case, $\inner{x}{f} =
  f(x)$, so we have
  \begin{equation}\label{Dirac1}
   \inner{x}{f} = f(x) = \int \delta(x-x') f(x') dx' .
  \end{equation}
  Because $\delta(x-x')$ is zero everywhere except at $x' = x$, we
  can take the limits of the integral to cover an infinitesimally
  small region around $x$.  Over this region, any reasonably
  smooth function will be essentially constant, equal to its value
  at $x$.  This constant can be taken outside the integral to give
  \begin{equation}
   \inner{x}{f} = f(x) = f(x) \int_{x-\eps}^{x+\eps} \delta(x-x') dx' .
  \end{equation}
  It then follows that
  \begin{equation}
   \int_{x-\eps}^{x+\eps} \delta(x-x') dx' = 1.
  \end{equation}
  Since $\delta(x-x') = 0$ everywhere except at $x' = x$, these
  limits can be extended to any region which includes $x$.  We
  then have the definition of the \emph{Dirac delta function}:
  \begin{equation}
   \delta(x-x') = 0 , \qquad x \neq x' ,
  \end{equation}
  \begin{equation}
   \int_a^b \delta(x-x') dx' = 1 , \qquad a<x<b .
  \end{equation}
  Notice that the units of the delta function are the inverse of the
  units of its argument.
  This improper function gives us the normalization of the basis
  vectors for our infinite dimensional vector space:
  \begin{equation}
   \inner{x}{x'} = \delta(x-x') .
  \end{equation}

  \item \emph{Properties of the Dirac Delta Function.}
  \begin{enumerate}[1.]
   \item  As we saw above (\ref{Dirac1}), the delta function
   integrated with some $f$ simply gives the value of $f$
   at a single point.

   \item The Delta function can be expressed as the limit of some
   function with a single ``hump'' centered at $x = x'$ as the
   width of the hump goes to zero while the height goes to
   infinity in such as way that the area underneath remains one.
   For example, we can use a Gaussian:
   \begin{equation}
    \delta(x-x') = \lim_{\eps \To  0} g_\eps (x - x') =
    \frac{1}{\sqrt{\pi}\eps} \exp \left[ - \frac{(x-x')^2}{\eps^2}
    \right]
   \end{equation}
   We can see from this expression (or just using the fact that
   $\delta(x-x')$) that the delta function is even.

   \item \emph{The Derivative of the Delta Function.}  Let us consider
   the derivative of the Gaussian (with respect to the first
   argument):
   \begin{equation}
    \frac{d}{dx} \delta(x-x') = \delta'(x-x') = -\frac{d}{dx'} \delta(x-x').
   \end{equation}
   As usual, we always consider the delta function inside an
   integral:
   \begin{equation}\label{DiracD1}
    \int \frac{d}{dx} \delta(x-x') f(x) dx' = \frac{d}{dx} \int
    \delta(x-x') f(x) dx' = \left. \frac{df}{dx}\right|_{x}
   \end{equation}
   Inside an integral over $dx'$, the derivative of the delta
   function acts as
   \begin{equation}
    \delta'(x-x') = \delta(x-x') \frac{d}{dx'}
   \end{equation}
   where the derivative $d/dx'$ acts on everything else in the
   integrand. Note that $\delta'(x-x')$ is an odd function.

   In general,
   \begin{equation}
    \frac{d^n}{dx^n}\delta(x-x') = \delta(x-x') \frac{d^n}{dx'^n}
   \end{equation}

   \item \emph{The Delta Function and the Fourier Transform.}
   Applying the Fourier transform and the inverse Fourier
   transform to a function $f$, we obtain
   \begin{equation}
    f(x) = \infint \left[ \frac{1}{2\pi}
    \infint e^{ik(x-x')} dk \right] f(x') dx'
   \end{equation}
   Comparing this to (\ref{Dirac1}), we find that
   \begin{equation}\label{Dirac2}
    \delta(x-x') = \frac{1}{2\pi}
    \infint e^{ik(x-x')} dk ,
   \end{equation}
   a remarkable result.

   \item \emph{More Properties.}
   \begin{enumerate}[a.]
    \item
    \begin{equation}
     \delta(ax) = \frac{\delta(x)}{|a|} .
    \end{equation}

    \item For a function $f(x)$ which has zeros at $x_i$,
    \( \delta(f(x))
    = \sum_i \frac{\delta(x-x_i)}{\abs{df/dx}_{x_i}} \)
    This surprisingly useful result is obtained by expanding $f(x)$
    in a Taylor series about $x_i$.

    \item \emph{The Theta Function}
    \begin{equation}
     \delta(x-x') = \frac{d}{dx} \theta(x-x') ,
    \end{equation}
    where the theta function is defined as
    \begin{equation}
     \theta(x-x') = \left\{ \begin{array}{ll} 0 & \textrm{if $x -
     x' < 0$} \\ 1 & \textrm{if $x - x' > 0$} \end{array} \right. .
    \end{equation}

    \item \emph{Complex Plane Representations.}
    \( \delta(\omega) = \lim_{\epsilon \To 0} \frac{1}{\pi} \Im
    \frac{1}{\omega - i \epsilon} = \lim_{\epsilon \To 0} \frac{1}{\pi}
    \frac{\epsilon}{\omega^2 + \epsilon^2}\)
    For $\eta > 0$, 
    \( \theta(x - x') = -\int_{-\infty}^\infty \frac{e^{-i \omega (x
    - x')}}{\omega + i \eta} \frac{d\omega}{2 \pi i}. \)
  \end{enumerate}

 \end{enumerate}

 \end{enumerate}

 \item \textbf{Operators in Infinite Dimensions.}
 \begin{enumerate}[A.]
  \item \emph{The Physical Hilbert Space.} Since we will be
  applying these results to quantum mechanics, we restrict
  ourselves to the physical (infinite dimensional) Hilbert space
  $\HSpace$ of functions which are normalizable to either unity or the
  Dirac delta function. This means that any $f(x)$ in this space must
  go to zero as $x \To \infty$, among other things.


  \item \emph{The Derivative Operator in $\HSpace$}. We
  will generally be concerned only with two main operators in
  $\HSpace$: the derivative operator $\op{D}$ and the $\op{X}$
  operator.

  The derivative operator acts on a member of the space (which is
  just a function, recall) as follows:
  \begin{equation}
   \op{D} \ket{f} = \ket{\frac{df}{dx}} .
  \end{equation}
  The first thing that we would like to know is what are the
  matrix elements $D_{xx'} = \innerop{x}{D}{x'}$ of $\op{D}$. Let
  us consider
  \begin{equation}
   \innerop{x}{D}{f} = \left. \frac{df}{dx}\right|_{x} = \int
   \innerop{x}{D}{x'} \inner{x'}{f} dx' .
  \end{equation}
  Comparing this to (\ref{DiracD1}), we can conclude that the
  matrix elements of $\op{D}$ are
  \begin{equation}
   D_{xx'} = \innerop{x}{D}{x'} = \delta'(x-x') =
    \delta(x-x')\frac{d}{dx'} .
  \end{equation}

  \item \emph{The Operator $\op{K} = -i\op{D}$.} Because the
  derivative of the delta function is odd, we find
  that $D_{xx'} = -D^*_{x'x}$, so $\op{D}$ is not Hermitian.
  Now, the operator
  \begin{equation}
   \op{K} = -i\op{D}
  \end{equation}
  appears to be Hermitian.  However, if we calculate
  $\innerop{f}{K}{g}$ and
  $\innerop{g}{K}{f}^*$, we find that $\op{K}$ is only Hermitian
  if
  \begin{equation}
   \left. -ig^*(x)f(x) \right|_a^b = 0 .
  \end{equation}
  This additional condition is not present for finite dimensional
  vector spaces.  See Shankar p. 66 for more.

  \item \emph{The Eigensystem of $\op{K}$.}  Let us find $k$ and
  $\ket{k}$ such that
  \begin{equation}
   \op{K}\ket{k} = k\ket{k} .
  \end{equation}
  \begin{eqnarray}
   \innerop{x}{K}{k} & = & k\inner{x}{k} \nonumber \\
   \int \innerop{x}{K}{x'} \inner{x'}{k} dx' & = & k \psi_k(x) \nonumber \\
   -i \frac{d}{dx} \psi_k(x) & = & k \psi_k(x) ,
  \end{eqnarray}
  where we have defined $\inner{x}{k} = \psi_k(x)$.  This
  differential equation has a simple solution, namely
  \begin{equation}
   \psi_k(x) = Ae^{ikx} .
  \end{equation}
  We can assume that $k$ is real since $\psi_k \in \HSpace$
  If we choose $A=\sqrt{1/2\pi}$,
  \begin{equation}
   \inner{k}{k'} = \infint \inner{k}{x}
   \inner{x}{k'} dx = \frac{1}{2\pi}
    \int_{-\infty}^\infty e^{-i(k-k')x} dx = \delta(k-k') .
  \end{equation}
  Note that for the last equality, we have used (\ref{Dirac2}) and
  the fact that the delta function is even. Using this result, the
  matrix elements of $\op{K}$ (in the $\ket{k}$ basis) are just
  \begin{equation}
   K_{kk'} = \innerop{k}{K}{k'} = k' \delta(k-k') .
  \end{equation}

  Because $\op{K}$ is Hermitian, there is an ON basis of $\HSpace$
  consisting of eigenvectors of $\op{K}$.  Let us express some $f$
  in this basis:
  \begin{equation}
   f(k) = \inner{k}{f} = \infint \inner{k}{x} \inner{x}{f} dx
   = \frac{1}{\sqrt{2\pi}} \infint e^{-ikx} f(x) dx .
  \end{equation}
  This is just the familiar Fourier transform.  Going the other
  direction we have
  \begin{equation}
   f(x) = \inner{x}{f} = \infint \inner{x}{k} \inner{k}{f} dk
   = \frac{1}{\sqrt{2\pi}} \infint e^{ikx} f(k) dk ,
  \end{equation}
  which is just the inverse Fourier transform.

  \item \emph{The Eigensystem of $\op{X}$.} Let us define $\op{X}$
  as the Hermitian operator which produces the ON basis $\ket{x}$
  of $\HSpace$.  That is,
  \begin{equation}
   \op{X}\ket{x} = x\ket{x} .
  \end{equation}
  Then, in analogy the result for $\op{K}$, the matrix elements of
  $\op{X}$ in the $\ket{x}$ basis are
  \begin{equation}
   X_{xx'} = \innerop{x}{X}{x'} = x' \delta(x-x') .
  \end{equation}
  $\op{X}$ acts on a vector in our space as follows:
  \begin{equation}
   \innerop{x}{X}{f} = \int \innerop{x}{X}{x'} \inner{x'}{f} dx' =
   xf(x) .
  \end{equation}
  So in general,
  \begin{equation}
   \op{X}\ket{f(x)} = \ket{xf(x)} .
  \end{equation}

  Let us find the matrix elements of $\op{X}$ in the $\ket{k}$
  basis:
  \begin{eqnarray}
   X_{kk'} = \innerop{k}{X}{k'} & = & \frac{1}{2\pi} \infint e^{-ikx} x e^{ik'x}
   dx \nonumber \\ & = & i \frac{d}{dk} \left[\frac{1}{2\pi} \infint e^{i(k'-k)x}
   dx \right] \nonumber \\ & = & i\delta'(k-k'),
  \end{eqnarray}
  so its action in the $\ket{k}$ basis is
  \begin{equation}
   \op{X}\ket{f(k)} = \ket{i\frac{df(k)}{dk}} .
  \end{equation}

  As an aside, we can compute the commutator of $\op{X}$ and
  $\op{K}$:
  \begin{equation}
   [\op{X},\op{K}] = \op{X}\op{K} - \op{K}\op{X} = i\op{I} .
  \end{equation}
  Note that $\op{I}$ is just the identity operator.  This is an
  example of a very general result:
  \( [\D{}{x},f(x)] = \D{f}{x} .\)

 \item \emph{Function of an Operator.}  Given a function $f(x)$ of a
 real or complex variable with a power series expansion
 \[ f(x) = \sum_n a_n x^n, \]
 the corresponding function of an operator $\op{\Omega}$ is
 \( f(\op{\Omega}) \defn \sum_n a_n \op{\Omega}^n, \)
 provided the power series converges.
 If $\op{\Omega}$ is a normal operator, i.e., $\op{\Omega}^\dag
 \op{\Omega} = \op{\Omega} \op{\Omega}^\dag$ (including but not
 limited to Hermitian and unitary operators), we can write it in the
 basis consisting of its eigenstates as
 $\op{\Omega} = \sum \omega \ket{\omega}\bra{\omega}$.  Because
 diagonal matrices behave like scalars under multiplication, we can
 easily show that
 \( \label{eq:OpFn}
  f(\op{\Omega}) = \sum f(\omega) \ket{\omega}\bra{\omega}. \)

 Finally, note that if a $\opA$ commutes with $\opB$, then $f(\opA)$
 commutes with $g(\opB)$ for any functions $f$ and $g$ (assuming
 convergence of the power series, of course).

 \item \emph{Exponential of an Operator.}
 The operator function encountered most frequently is the exponential
 of an operator. For generality, the exponential of an operator
 $\op{\Omega}$ is defined by means of the power series:
 \begin{namedeqn}{\textbf{Exponential of an Operator}}
  e^{\op{\Omega}} \defn \op{I} + \op{\Omega} +
  \frac{1}{2!}{\op{\Omega}}^2 + \frac{1}{3!}{\op{\Omega}}^3 + \ldots .
 \end{namedeqn}
 This definition is equivalent to (\ref{eq:OpFn}) with $f(x) = e^x$
 for normal operators.  For other operators, it is only valid if the
 power series converges.  Note the following properties.
 \begin{enumerate}[1.]
  \item If $\ket{\omega}$ is an eigenstate of $\op{\Omega}$ with
  eigenvalue $\omega$,
  \( e^{\op{\Omega}} \ket{\omega} = e^\omega \ket{\omega} . \)

  \item \emph{The Baker-Campbell-Hausdorff Theorem.}  The most
  general form of the Baker-Campbell-Hausdorff theorem is
  \( e^{\opA} \opB e^{-\opA} = \opB + [\opA,\opB] +
  \frac{1}{2!}[\opA,[\opA,\opB]] + \ldots .\)
  If $[\opA,\opB]$ commutes with $\opA$ and $\opB$ so that only the
  first two terms in the series remain, we can show that
  \( e^{\opA} e^{\opB} = e^{\opA + \opB} e^{[\opA,\opB]/2}
   = e^{\opB} e^{\opA} e^{[\opA,\opB]} . \)
  Note that the exponentials of commutators in the last two
  equalities commute with other exponentials, by assumption.

  \item If $\op{\Omega}$ is a Hermitian operator, it can easily be
  shown that $e^{i\op{\Omega}}$ is a unitary operator.
 \end{enumerate}

 \end{enumerate}

\end{enumerate}

\end{document}
