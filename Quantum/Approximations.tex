% ----------------------------------------------------------------
% Approximation Methods ******************************************
% ----------------------------------------------------------------
\documentclass{article}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{booktabs}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
\reversemarginpar
% MATH -----------------------------------------------------------
\input{../Include/Layout.tex}
\input{../Include/Math.tex}
\input{../Include/Vectors.tex}
\input{../Include/Quantum.tex}
\newcommand{\Hm}{\op{H}_0}%{\op{H}^{(0)}}
\newcommand{\Hp}{\op{V}}%{\op{H}^{(1)}}
\newcommand{\Um}{\op{U}^{(0)}}
\newcommand{\Up}{\op{U}^{(1)}}
\newcommand{\En}[1]{E^{(#1)}}
\newcommand{\Em}{E^{(0)}}
\newcommand{\Ep}{E^{(1)}}
\newcommand{\Sn}[1]{\alpha^{(#1)}}
\newcommand{\Sm}{\Sn{0}}
%\newcommand{\Sp}{\Sn{1}}
\newcommand{\UmDag}{\op{U}^{(0)\dag}}
\newcommand{\UpDag}{\op{U}^{(1)\dag}}
% ----------------------------------------------------------------
%\draftmode
\begin{document}
% \maketitle
% ----------------------------------------------------------------
\begin{enumerate}[I.]
 \itemI{Time Independent Perturbation Theory.}
 \begin{enumerate}[A.]
  \itemA{Rayleigh-Schrodinger Perturbation Series}

  We start from the definitions of eigenvalue equations for the unperturbed and total Hamiltonians, respectively,
  \begin{subgather}
   \Hm\ket{\Sn{0}} = \Em_{\alpha} \ket{\Sm}, \\
   (\Hm+\Hp)\ket{\alpha} = \op{H} \ket{\alpha} = E_{\alpha} \ket{\alpha},
  \end{subgather} where
  \begin{subgather}
   E_{\alpha} = \sum_{i=0}^{\infty} \En{i}_{\alpha}, \\
   \ket{\alpha} = \sum_{i=0}^{\infty} \ket{\Sn{i}}.
  \end{subgather}
  The order of each term specifies the power of $\Hp$ involved.  We immediately find
  \( \label{eq:PTEnergy} E_{\alpha} - \Em_{\alpha} = \bra{\Sm}\Hp\ket{\alpha}. \)

  Choosing so-called intermediate normalization
  \( \inner{\alpha}{\Sm} = 1, \)
  we have
  \( \label{eq:RSIterative} \ket{\alpha} = \ket{\Sm} + (\Em_{\alpha} - \Hm)^{-1} (1-\ket{\alpha}\bra{\Sm}) \Hp \ket{\alpha}. \)
  This equation can be verified by operating on it from the left with $(\Em_{\alpha} - \Hm)$.  Here, $(\Em_{\alpha} - \Hm)^{-1}$ is the inverse of $(\Em_{\alpha} - \Hm)$ on its support (i.e., the subspace spanned by the eigenstates of $\Hm$ with eigenvalue $\Em_{\alpha}$ is excluded).  The projection operator $(1-\ket{\alpha}\bra{\Sm})$ ensures (iteratively) that $\inner{\Sm}{\Sn{i}} = 0$ for all $i$.

  Solving (\ref{eq:RSIterative}) iteratively yields the usual Rayleigh-Schrodinger perturbation series.  Because $\ket{\alpha}$ appears in two places on the right side, plugging $\sum_{i=0}^N \ket{\Sn{i}}$ into (\ref{eq:RSIterative}) will yield terms up to order $2N+1$ in $\Hp$; $\sum_{i=0}^{N+1} \ket{\Sn{i}}$ is obtained by keeping only terms up to order $N+1$.  Alternatively, the perturbation series can be obtained by writing the Schrodinger equation as
  \[ (\Hm + \lambda \Hp) \sum_{i=0}^{\infty} \lambda^i \ket{\Sn{i}} = \parenth{\sum_{i=0}^{\infty} \lambda^i \En{i}_{\alpha}} \sum_{i=0}^{\infty} \lambda^i \ket{\Sn{i}} \]
  and collecting terms with like powers of $\lambda$.

  The $(n+1)$th order contribution to the energy can determined from $\ket{\Sn{n}}$ using (\ref{eq:PTEnergy}): $\En{n+1}_{\alpha} = \bra{\Sm}\Hp\ket{\Sn{n}}$.  The $n$th order contribution to the state vector satisfies
  \[ (\Em_{\alpha} - \Hm) \ket{\Sn{n}} = \Hp \ket{\Sn{n-1}} - \sum_{i=1}^n \En{i}_{\alpha} \ket{\Sn{n-i}}. \]
  With the help of this expression, we can obtain Wigner's $(2n+1)$ rule,
  \( \En{2n+1}_{\alpha} = \bra{\Sn{n}}\Hp\ket{\Sn{n}} - \sum_{i,j=1}^n \En{2n+1-i-j}_{\alpha} \inner{\Sn{i}}{\Sn{j}}, \)
  which expresses the $(2n+1)$th order contribution to the energy using only state vectors up to the $n$th order.

  For reference, we give the first two orders of the Rayleigh-Schrodinger perturbation series:
  \begin{subgather}
   \En{1}_{\alpha} = \bra{\Sm}\Hp\ket{\Sm}, \\
   \ket{\Sn{1}} = \sum_{\beta \neq \alpha} \frac{\ket{\beta^{(0)}} \bra{\beta^{(0)}}\Hp\ket{\Sm}}{\Em_{\alpha} - \Em_{\beta}}, \\
   \En{2}_{\alpha} = \sum_{\beta \neq \alpha} \frac{\bra{\Sm}\Hp\ket{\beta^{(0)}} \bra{\beta^{(0)}}\Hp\ket{\Sm}}{\Em_{\alpha} - \Em_{\beta}}.
  \end{subgather}
  The key step in obtaining such expressions is the replacement of the projection operator $(1-\ket{\Sm}\bra{\Sm})$ with $\sum_{\beta \neq \alpha} \ket{\beta^{(0)}} \bra{\beta^{(0)}}$.  Note that the use of intermediate normalization requires that $\ket{\alpha}$ be divided by $\inner{\alpha}{\alpha}$ at the end of the calculation.

  %near degenerate perturbation theory?
  \itemA{Degenerate Case.} In the event that $\Hm$ has a degenerate subspace, we must choose from it basis vectors $\{\ket{\Sm}\}$ which diagonalize the perturbation $\Hp$ within the subspace.  Then the numerator $\bra{\beta^{(0)}}\Hp\ket{\Sm}$ will disappear whenever the denominator $\Em_{\alpha} - \Em_{\beta}$ does, so inifinties do not appear.

  \itemA{Brillouin-Wigner Perturbation Series.}
  In the same manner as for (\ref{eq:RSIterative}), we can show that $\ket{\alpha}$ also obeys
  \( \label{eq:BWIterative} \ket{\alpha} = \ket{\Sm}
   + (E_{\alpha} - \Hm)^{-1} (1 - \ket{\Sm}\bra{\Sm}) \Hp \ket{\alpha}, \)
  by operating from the left with $(E_{\alpha} - \Hm)$.  Iteration of this equation yields the Brillouin-Wigner
  perturbation series.  The value of $E_{\alpha}$ must be determined self-consistently \emph{a posteriori}.

  Because $\ket{\alpha}$ only appears once on the right hand side of (\ref{eq:BWIterative}), we can immediately write the perturbation series:
  \( \ket{\alpha} = \ket{\Sm} + \op{G}_{\alpha} \Hp \ket{\Sm} + \op{G}_{\alpha} \Hp \op{G}_{\alpha} \Hp \ket{\Sm} + \ldots, \)
  where $\op{G}_{\alpha} = (E_{\alpha} - \Hm)^{-1} (1 - \ket{\Sm}\bra{\Sm})$.  The series for the energy follows immediately using $E_{\alpha} - \Em_{\alpha} = \bra{\Sm}\Hp\ket{\alpha}$.
 \end{enumerate}

 \itemI{Time Dependent Perturbation Theory.}
 \begin{enumerate}[A.]
  \itemA{Preliminaries.} The goal of time dependent perturbation
  theory is to determine the time evolution of state under the
  Hamiltonian
  \( \op{H} = \Hm + \Hp(t), \)
  where time independent term $\Hm$ term is dominant.  We assume that
  the eigenvalue problem for $\Hm$ has been solved, or at least that
  we know the propagator $\Um$ for $\Hm$.  Furthermore, we assume
  that $\Hp$ does not alter the eigenstates of $\Hm$ (as is the case in
  time independent perturbation theory), but serves only to allow
  transitions between the eigenstates.

  \itemA{General Solution.} To determine the time evolution of a state
  under $\op{H}$, we need only find the propagator for $\op{H}$.
  Denoting this propagator by $\op{U}_S(t,t_0)$ in the Schrodinger
  picture, we have from our discussion of the interaction picture
  \( \label{eq:Prop1} \op{U}_S(t,t_0) = \Um_S(t,t_0) \op{U}_I(t,t_0) , \)
  where $\op{U}_I$, the propagator in the interaction picture is related
  to the perturbation Hamiltonian in the interaction picture
  \( \label{eq:Prop2} \Hp_I = \UmDag_S \Hp_S \Um_S \)
  by the usual differential equation:
  \( \label{eq:PropDE} i\hbar \D{\op{U}_I}{t} = \Hp_I \op{U}_I . \)
  Since $\Hp_I$ is time dependent, the solution to (\ref{eq:PropDE})
  is the integral equation
  \( \op{U}_I(t,t_0) = \op{I} - \frac{i}{\hbar}
  \int_{t_0}^t \Hp_I(t') \op{U}_I(t',t_0) dt' . \)
  We can use this equation to obtain a perturbation expansion for
  $\op{U}_I$ as follows.  To zeroth order in $\Hp_I$, we have
  $\op{U}_I = \op{I}$.  Feeding this back into the equation, we get
  to first order
  \( \label{eq:FirstOrd} \op{U}_I(t,t_0) = \op{I} - \frac{i}{\hbar}
  \int_{t_0}^t \Hp_I(t') dt' . \)
  Repeating this process, we can obtain $\op{U}_I$ to any order:
  \begin{multline}
  \op{U}_I(t,t_0) = \op{I} - \frac{i}{\hbar}
   \int_{t_0}^t \Hp_I(t') dt' + (-i/\hbar)^2 \int_{t_0}^t
   \int_{t_0}^{t'}
   \Hp_I(t') \Hp_I(t'') dt'' dt' \\ + (-i/\hbar)^3 \int_{t_0}^t
   \int_{t_0}^{t'}
   \int_{t_0}^{t''} \Hp_I(t') \Hp_I(t'') \Hp_I(t''') dt''' dt'' dt' +
   \ldots
  \end{multline}
  We can write this more formally by introducing the (Dyson)
  time-ordered product $T[]$, defined as follows:
  \( T[\op{A}(t_1) \op{B}(t_2)] = \begin{cases}
   \op{A}(t_1) \op{B}(t_2) & t_1 \ge t_2, \\
   \op{B}(t_2) \op{A}(t_1) & t_1 \le t_2. \end{cases} \)
  It is simple, save one trick, to verify that
  \( \op{U}_I(t,t_0)
   = T \exp \bracket{-\frac{i}{\hbar}\int_{t_0}^t \Hp_I(t')dt'}. \)
  Defining
  \[ V_n = -\frac{i}{\hbar}
   \int_{t_0 + (n-1)\eps}^{t_0 + n \eps} \Hp_I(t')dt', \]
  let us write
  \begin{align*}
   \op{U}_I(t,t_0) &= \lim_{\eps \To 0} \op{U}_I(t,t - \eps)
   \op{U}_I(t - \eps, t - 2 \eps) \ldots \op{U}_I(t_0 + 2 \eps,t_0 +
   \eps) \op{U}_I(t_0 + \eps,t_0) \\ &= \lim_{N \To \infty} e^{V_N}
   e^{V_{N-1}} \ldots e^{V_2} e^{V_1},
  \end{align*}
  Note that the time ordering is no longer needed in the limit $\eps
  \To 0$, i.e., $\lim_{\eps \To 0} [\Hp_I(t + \eps), \Hp_I(t)] =
  0$.  If we now assume that the commutator $[\Hp_I(t), \Hp_I(t')]$
  itself commutes with $\Hp_I(t'')$ for all $t$, $t'$, and $t''$
  (most likely, this means the commutator is a c-number), the
  Baker-Campbell-Hausdorff theorem can be applied in its simplest
  non-trival form to yield
  \begin{align}
   \op{U}_I(t,t_0) &= \lim_{N \To \infty} \exp \bracket{\sum_{n=1}^N
   \parenth{V_n + \frac{1}{2}\bracket{V_n, \sum_{k=1}^n V_k}}} \notag
   \\ &= \exp \bracket{-\frac{i}{\hbar} \int_{t_0}^t \Hp_I(t')dt' -
   \frac{1}{2 \hbar^2} \int_{t_0}^t \int_{t_0}^{t'}
   [\Hp_I(t'), \Hp_I(t'')]dt'' dt'}.
  \end{align}

  To go back to the Schrodinger picture, we apply (\ref{eq:Prop1})
  and (\ref{eq:Prop2}) to obtain
  \begin{namedeqn}{\textbf{Perturbation Propagator}}
  \begin{split}
  & \op{U}_S(t,t_0) = \Um_S(t,t_0) - \frac{i}{\hbar}
   \int_{t_0}^t \Um_S(t,t') \Hp_S(t') \Um_S(t',t_0) dt'
   \\ & + (-i/\hbar)^2 \int_{t_0}^t \int_{t_0}^{t'}
   \Um_S(t,t')\Hp_S(t')\Um_S(t',t'') \Hp_S(t'')\Um_S(t'',t_0) dt''
   dt' + \ldots .
  \end{split}
  \end{namedeqn}

  \itemA{Feynman Diagrams.} The series expansion for the propagator
  $\op{U}_S(t,t_0)$ has a helpful
  interpretation which forms the basis for Feynman diagrams.  The
  first term, of course, is just the unperturbed propagator $\Um_S$.
  In the second term, the system evolves under $\Um_S$  from $t_0$ to
  $t'$, where the perturbation acts.  The system then continues
  "normally" to $t$ under $\Um_S$.  In the second term, the perturbation
  acts twice, at $t''$ and $t'>t''$.  In between these times, the
  system evolves under $\Um_S$ alone.

  \itemA{Transition Probabilities.} We wish to consider the
  probability of a system transitioning from one state to another
  under the influence of a perturbation. To simplify notation, all
  states are assumed to be in the Schrodinger picture. Assume the system
  starts (at $t_0$) in the state $\ket{i^0}$. It evolves under the
  full propagator according to $\ket{i^0(t)} = \op{U}(t,t_0)\ket{i^0}$.
  The probability of the finding the system, at time $t$, in the state
  $\ket{f^0(t)} = \Um(t,t_0)\ket{f^0}$ is simply $\abs{d_f(t)}^2$,
  where
  \begin{align}
   d_f(t) &= \inner{f^0(t)}{i^0(t)}
    = \bra{f^0}\UmDag(t,t_0)\op{U}(t,t_0)\ket{i^0} \notag \\
    &= \bra{f^0}\op{U}_I(t,t_0)\ket{i^0} .
  \end{align}
  Using (\ref{eq:FirstOrd}), we obtain to first order
  \begin{align}
   d_f(t) &= \inner{f^0}{i^0} - \frac{i}{\hbar} \int_{t_0}^t
   \bra{f^0}\Hp_I(t')\ket{i^0} dt' \notag \\
    &= \inner{f^0}{i^0} - \frac{i}{\hbar} \int_{t_0}^t
   \bra{f^0}\UmDag(t',t_0)\Hp_S(t')\Um(t',t_0)\ket{i^0} dt' .
  \end{align}
  In the usual case where both $\ket{i^0}$ and $\ket{f^0}$ are
  energy eigenstates, this becomes (note the Kronecker delta):
  \( \label{eq:TransAmp}
  d_f(t) = \delta_{fi} - \frac{i}{\hbar} \int_{t_0}^t
   \bra{f^0}\Hp_S(t')\ket{i^0} e^{i\omega_{fi}(t - t_0)} dt' . \)
  Here, we have introduced the Bohr frequency $\omega_{fi} \defn (\Em_f
  - \Em_i)/\hbar$. See Shankar 18.2 for an alternate derivation
  of this expression.

  \itemA{Periodic Perturbations.} Consider a periodic perturbation of
  the form
  \( \Hp(t) = \Hp e^{-i\omega t} . \)
  Note that any periodic function can be expanded in terms of
  complex exponentials.  Using (\ref{eq:TransAmp}), we obtain
  \begin{align} \label{eq:PTPInt}
   d_f(t) &= -\frac{i}{\hbar} \int_{t_0}^t \bra{f^0}\Hp\ket{i^0}
   e^{i(\omega_{fi} - \omega)t'} dt' \\
   &= -\frac{1}{\hbar} \bra{f^0}\Hp\ket{i^0}
  \frac{e^{i(\omega_{fi} - \omega)t} - 1}{\omega_{fi} - \omega} ,
  \end{align}
  setting $t_0 = 0$. The transition probability is
  \( \abs{d_f(t)}^2 = \frac{1}{\hbar^2} \abs{\bra{f^0}\Hp\ket{i^0}}^2
  \parenth{\frac{\sin[\frac{1}{2}(\omega_{fi}
  - \omega)t]}{\frac{1}{2}(\omega_{fi} - \omega)t}}^2 t^2 . \)
  Because the function $(\sin^2 x)/x^2$ is peaked at the origin with
  a width $\Delta x \approx \pi$, the system is most likely to go
  states which satisfy $\abs{\frac{1}{2}(\omega_{fi} - \omega)t} \leq
  \pi$.  Inserting the definition of $\omega_{fi}$,
  \( \hbar \omega\parenth{1 - \frac{2\pi}{\omega t}}\leq \Em_f
  - \Em_i \leq \hbar \omega \parenth{1 + \frac{2\pi}{\omega t}} . \)
  The fact that $\Em_f - \Em_i$ approaches the expected value of $\pm
  \hbar \omega$ only for $\omega t \gg 2 \pi$ simply reflects the fact
  that the system cannot know the frequency of the perturbation until
  after several cycles have passed.  Noting that (\ref{eq:PTPInt}) is
  a Fourier transform, it is clear the probability amplitude
  for the system to transition to a state with energy $\Em_f = \Em_i
  + \hbar \omega_{fi}$ is proportional to the Fourier component of the
  perturbation at $\omega = \omega_{fi}$. As more cycles pass, this
  component becomes increasingly dominant.

  To investigate the response of the system over large time scales,
  we set $t_0 = -T/2$ and $t = T/2$ in (\ref{eq:PTPInt}), letting $T
  \To \infty$ (i.e., we apply the perturbation for all time). This
  yields
  \[ d_f(t) = -\frac{2\pi i}{\hbar} \bra{f^0}\Hp\ket{i^0}
  \delta(\omega_{fi} - \omega) . \]
  The transition probability is just $\abs{d_f(t)}^2$ (see Shankar p. 483 for
  evaluation of the $[\delta(\omega_{fi} - \omega)]^2$).  Dividing by
  $T$, we obtain the average transition rate:
  \begin{namedeqn}{\textbf{Fermi's Golden Rule}}
  R_{if} = \frac{\abs{d_f(t)}^2}{T} = \frac{2\pi}{\hbar}
  \abs{\bra{f^0}\Hp\ket{i^0}}^2 \delta(\Em_f - \Em_i - \hbar \omega).
  \end{namedeqn}

 \end{enumerate}

\itemI{Adiabatic and Sudden Perturbations.}
 \begin{enumerate}[A.]
  \itemA{Adiabatic Perturbations.} Suppose the Hamiltonian of a system
  changes continuously from $\op{H}_0$ at $t_0$ to $\op{H}_1$ at
  $t_1$.  In general, both the energy eigenvalues $E_n(t)$ and the
  corresponding eigenstates $\ket{n(t)}$ will change as the
  Hamiltonian changes. The change of the latter is referred to as
  the geometric evolution. Let us write
  \( \ket{\psi(t)}
   = \sum_n a_n(t) e^{-i\phi_n(t)} \ket{n(t)}, \)
  where
  \( \phi_n(t) = \frac{1}{\hbar}\int_{t_0}^t E_n(t') dt' \)
  is the dynamical phase.
  If $\op{H}(t)$ varies sufficiently slow, we
  expect there to be little chance of a transition between
  eigenstates and hence that the $a_n(t)$ will be slowly varying
  functions of time.  In this case, the perturbation is referred to
  as adiabatic.
  Note that if $\op{H}(t)$ commutes with itself for all times, the
  $a_n(t)$ are completely independent of time.

  Inserting our expression for $\ket{\psi(t)}$ into the time
  dependent Schrodinger equation and taking the inner product with
  $\ket{m(t)}$ yields
  \( \label{eq:dadt}
   \D{a_m}{t} = - \sum_n a_n(t) e^{i(\phi_m - \phi_n)}
   \bra{m(t)}\D{}{t}\ket{n(t)} . \)
  Taking $\ket{\psi(t_0)} = \ket{n(t)}$,
  let us first consider the probability of a transition to another
  state.  Formally integrating this expression gives
  \( a_m(t) = a_m(t_0) - \int_{t_0}^t \sum_n a_n(t')
   e^{i(\phi_m - \phi_n)} \bra{m(t')}\D{}{t'}\ket{n(t')} dt' \)
  Iterating and using $a_m(t_0) = \delta_{mn}$ gives, to first order,
  \( a_m(t) = - \int_{t_0}^t \bra{m(t')}\D{}{t'}\ket{n(t')}
   e^{i(\phi_m - \phi_n)}  dt' \)
  for $m \neq n$.  By differentiating $\op{H}(t)\ket{n(t)}
  = E_n(t) \ket{n(t)}$ and taking the inner product with
  $\ket{m(t)}$, we can prove the following result:
  \( \bra{m(t)}\D{}{t}\ket{n(t)} = \frac{-\bra{m(t)}
   \tD{\op{H}}{t}\ket{n(t)}}{E_m(t) - E_n(t)}. \)
  This allows us to write
  \( a_m(t) = \int_{t_0}^t \frac{\bra{m(t)}
   \tD{\op{H}}{t}\ket{n(t)}}{E_m(t) - E_n(t)}
   e^{i(\phi_m - \phi_n)} dt' \)
  The probability of a transition from $\ket{n(t)}$ to $\ket{m(t)}$
  near the adiabatic limit is given by $\abs{a_m(t)}^2$.

  \itemA{Berry's Phase.}
  Let us next consider the case where no transition occurs, so
  $a_m(t) = 0$, $m \neq n$. Then (\ref{eq:dadt}) becomes
  \[ \D{a_n}{t} = - a_n(t) \bra{n(t)}\D{}{t}\ket{n(t)}, \]
  with the solution $a_n(t) = e^{i \gamma_n}$ where
  \( \gamma_n = i \int_{t_0}^t \bra{n(t')}\D{}{t'}\ket{n(t')} dt'. \)
  We then have $\ket{\psi(t)} = e^{i(\gamma_n - \phi_n)} \ket{n(t)}$.
  Suppose that (with $t_0 = 0$) the Hamiltonian changes in a cyclic
  manner, returning to its original value after a time $T$, i.e.,
  $\op{H}(0) = \op{H}(T)$.  Let us introduce a rephased basis
  $\ket{n'(t)} = e^{i\chi(t)} \ket{n(t)}$.  In order for the new
  basis to be single valued, we must have $\chi(T) - \chi(0) = 2 \pi
  m$, where $m$ is an integer.  Using
  \[ i\bra{n'(t)}\D{}{t}\ket{n'(t)} = i\bra{n(t)}\D{}{t}\ket{n(t)}
   - \D{\chi}{t} \]
  we find $\gamma_n' = \gamma_n - (\chi(T) - \chi(0))$.  We see that
  the factor $e^{i \gamma_n}$ cannot be altered by a rephasing of the
  basis.  The phase $\gamma_n$ is called the geometric or Berry
  phase.

  \itemA{The Adiabatic Theorem.} We will now consider adiabatic
  perturbations in more detail, but in a manner which
  ignores the Berry phase.  To facilitate our discussion, we
  introduce the following variables:
  \( \tau = t_1 - t_0, \eqnsep s = (t-t_0)/\tau. \)
  Henceforth, $\op{H} = \op{H}(s)$ so $\op{H}(0) = \op{H}_0$ and
  $\op{H}(1) = \op{H}_1$.  We will write the Hamiltonian as
  \( \label{eq:AdiaH} \op{H}(s) = \sum_i E_i(s) \project{i}(s). \)
  where $\project{i}(s) = \ket{i(s)} \bra{i(s)}$ are projection
  operators.
  We require that $\D{}{s}\project{i}$ and $\DD{}{s}\project{i}$ exist
  and are piecewise continuous on $0 \leq s \leq 1$.
  We assume that the eigenvalues do not
  cross, i.e., $E_i(s) \neq E_j(s)$, $\forall i\neq j$, $0\leq s \leq
  1$.  This assumption, as well as the assumption of a discrete
  spectrum are not necessary: see Avron and Elgart, math-ph/9805022.

  The adiabatic theorem states that, for all $i$,
  \( \lim_{\tau \To \infty} \op{U}_{\tau}(s) \project{i}(0)
   = \project{i}(s) \lim_{\tau \To \infty} \op{U}_{\tau}(s), \)
  where $\op{U}_{\tau}(s) \defn \op{U}(t,t_0)$ is the propagator.
  This means, for example, that if the system starts out in the state
  $\ket{i(t_0)}$, at a later time $t$, it will be found in
  $\ket{i(t)}$, up to a phase factor. Although the proof of the
  adiabatic theorem is lengthy and provides
  only limited physical insight, we include it because it is
  nontrivial.

  \itemA{Proof of the Adiabatic Theorem.}
  We begin the proof (due to Kato) of the adiabatic theorem by introducing a
  picture in which the $\project{i}$ are stationary.  Denoting
  the unitary transformation from the Schrodinger representation to
  this so called ``rotating axis picture'' (at a given $s$)
  by $\op{A}(s)$, we have
  \( \label{eq:PEvol}
    \project{i}(s) = \op{A}(s) \project{i}(0) \op{A}^{\dag}(s). \)
  Of course, $\op{A}(0) = \op{I}$.  Now, we can think of $\op{A}(s)$
  as the propagator for a Hamiltonian $\op{K}(s)/\tau$ which gives
  only the geometric evolution. Differentiating (\ref{eq:PEvol}) and
  using $i \hbar \tD{\op{A}}{s} = \op{K}(s) \op{A}(s)$ yields
  \[ [\op{K}(s),\project{i}(s)] = i \hbar \tD{\project{i}}{s}. \]
  This commutator equation does not uniquely specify $\op{K}(s)$, so
  we impose the constraint $\project{i}\op{K}\project{i} = 0$,
  $\forall i$ and obtain
  \( \label{eq:DefK}
   \op{K}(s) = i \hbar \sum_i \D{\project{i}}{s} \project{i}(s). \)

  The
  propagator in this picture is $\op{U}_A(s) = \op{A}^{\dag}(s)
  \op{U}_{\tau}(s)$.  By analogy with (\ref{eq:PropDE}), the equation
  of motion for $\op{U}_A(s)$ is
  \[ i\hbar \D{}{s} \op{U}_A(s)
   = [\tau \op{H}_A(s) - \op{K}_A(s)] \op{U}_A(s), \]
  where $\op{K}_A(s) = \op{A}^{\dag}(s) \op{K}(s) \op{A}(s)$ and
  \[ \op{H}_A(s) = \op{A}^{\dag}(s) \op{H}(s) \op{A}(s) = \sum_i
   E_i(s) \project{i}(0). \]
  Here, the second equality follows from (\ref{eq:PEvol}).  We see
  that $\op{H}_A(s)$ can be interpreted as the Hamiltonian (in the
  Schrodinger picture) when the geometric evolution
  is ignored.  We expect that as $\tau \To \infty$, $\tau \op{H}_A$
  will dominate $\op{K}_A$, which represents the contribution to the
  propagator at intermediate times due to the geometric evolution (as
  opposed to $\op{A}$, which simply adds the geometric evolution at
  the end).
  Because $\op{H}_A(s)$ commutes with itself at all
  times, $i\hbar \tD{\op{V}_{\tau}}{s} = \tau \op{H}_A(s) \op{V}_{\tau}(s)$ can
  be readily integrated to yield the corresponding propagator:
  \( \label{eq:DefV} \op{V}_{\tau}(s)
   = \sum_i \exp(-i \tau \phi_i(s)/\hbar) \project{i}(0), \)
  where $\phi_i(s) = \int_0^s E_i(s') ds'$.

  We now define $\op{W} = \op{V}_{\tau}^{\dag}(s) \op{U}_A(s)
  = \op{V}_{\tau}^{\dag}(s) \op{A}^{\dag}(s) \op{U}_{\tau}(s)$.  We
  will soon see that the adiabatic theorem is equivalent to the
  statement $\op{U}_{\tau}(s) \To \op{A}(s) \op{V}_{\tau}(s)$ as
  $\tau \To infty$; thus, the
  extent to which $\op{W}$ differs from the identity is a measure of
  the difference between the true evolution of the system and the
  evolution given by the adiabatic theorem.
  Differentiating the definition of $\op{W}$ and using the equations
  of motion above for $\op{V}_{\tau}(s)$ and $\op{U}_A(s)$ yields
  \( \label{eq:Wop} \op{W}(s) = \op{I} + \frac{i}{\hbar} \int_0^s
  \op{V}_{\tau}^{\dag}(s') \op{K}_A(s') \op{V}_{\tau}(s') ds'. \)
  Integrating by parts and using $i \hbar \tD{\op{W}}{s}
  = -(\op{V}_{\tau}^{\dag} \op{K}_A \op{V}_{\tau})\op{W}$ yields
  \[ \op{W}(s) = \op{I} + \frac{i}{\hbar} \parenth{
   \op{F}(s) \op{W}(s) - \frac{i}{\hbar}  \int_0^s \op{F}(s')
   (\op{V}_{\tau}^{\dag} \op{K}_A \op{V}_{\tau}) \op{W}(s') ds'}, \]
  where
  \( \label{eq:DefF}
   \op{F}(s) = \int_0^s \op{V}_{\tau}^{\dag} \op{K}_A \op{V}_{\tau} ds'. \)

  Introducing the notation $\opA_{ij} = \project{i}(0 )\opA
  \project{j}(0)$ and noting that $\sum_{i,j} \opA_{ij} = \opA$,
  \[ (\op{V}_{\tau}^{\dag} \op{K}_A \op{V}_{\tau})_{ij} = e^{i\tau(\phi_i -
  \phi_j)/\hbar} (\op{K}_A)_{ij}, \]
  where, using (\ref{eq:PEvol}),
  \[ (\op{K}_A)_{ij} = \op{A}^{\dag}(s) \project{i}(s) \op{K}(s)
  \project{j}(s) \op{A}(s). \]
  The condition imposed above, $\project{i}\op{K}(s)\project{i} = 0$,
  gives $(\op{K}_A)_{ii} = 0$.
  Using these results and integrating by parts yields, for $i \neq
  j$,
  \[ \op{F}_{ij}(s)= \frac{\hbar}{i \tau} \bracket{\left.
   e^{i\tau(\phi_i - \phi_j)/\hbar} \frac{(\op{K}_A)_{ij}}{E_i - E_j}
   \right|_0^s - \int_0^s e^{i\tau(\phi_i - \phi_j)/\hbar} \D{}{s'}
   \parenth{\frac{(\op{K}_A)_{ij}}{E_i - E_j}} ds' }. \]
  Of course, $\op{F}_{ii} = 0$ for all $i$.
  We see immediately that as $\tau \To \infty$, $\op{F}(s) =
  \sum_{i,j} \op{F}_{ij}(s) = O(1/\tau)$.  We then have $\op{W}
  = \op{I} + O(1/\tau)$ and finally,
  \( \lim_{\tau \To \infty} \op{U}_{\tau}(s)
   = \op{A}(s) \op{V}_{\tau}(s) ,\)
  from which the above statement of the adiabatic theorem is readily
  verified (since both $\op{A}$ and $\op{V}_{\tau}$ commute with the
  projection operators).

  \itemA{Transition Probabilities Near the Adiabatic Limit.}
  We can now use the above development to calculate transition
  probabilities near the adiabatic limit.  Specifically, we wish to
  find the matrix element
  \[ \bra{j(1)}\op{U}_{\tau}(1)\ket{i(0)}
  = \bra{j(0)}\op{A}^{\dag}(1)\op{U}_{\tau}(1)\ket{i(0)}
  = \bra{j(0)}\op{V}_{\tau}(1) \op{W}(1)\ket{i(0)}, \]
  where $i \neq j$.
  This is equal, up to a phase, to $\bra{j(0)}\op{W}(1)\ket{i(0)}$.
  Solving (\ref{eq:Wop}) iteratively, we have $\op{W} = \op{I} +
  i\op{F}/\hbar$ to first order.  So, again to first order,
  \[ \bra{j(0)}\op{W}(1)\ket{i(0)} = i \bra{j(0)}\op{F}(1)\ket{i(0)}
  /\hbar .\]
  Proceeding,
  \begin{align*}
   \bra{j(0)}\op{F}(1)\ket{i(0)} &= \int_0^1 \bra{j(0)}\op{V}^{\dag}
    \op{A}^{\dag} \op{K} \op{A} \op{V} \ket{i(0)} ds \\
    &= \int_0^1 \sum_{k} \bra{j(0)} \op{A}^{\dag} \D{\project{k}}{s}
     \project{k} \op{A} \ket{i(0)} e^{i \tau (\phi_j - \phi_i)/\hbar}
     ds \\ &= \int_0^1 \bra{j(0)} \op{A}^{\dag} \D{\project{i}}{s}
     \op{A} \ket{i(0)} e^{i \tau (\phi_j - \phi_i)/\hbar} ds.
   \end{align*}
  Here, we have used, in order, (\ref{eq:DefF}), (\ref{eq:DefK}) and
  (\ref{eq:DefV}), and $[\project{i},\op{A}] = 0$.
  At this point, we will change variables from $s$ back to $t$ and
  define
  \( \alpha_{ji}(t) = \bra{j(t_0)} \op{A}^{\dag} \D{\project{i}}{t}
    \op{A} \ket{i(t_0)} = \bra{j(t)}\D{\project{i}}{t}\ket{i(t)}.\)
  Calculating $\tD{\project{i}}{t}$ using $\project{i} =
  \ket{i}\bra{i}$ or $(\op{H} - E_i)\project{i} = 0$ (obtained from
  multiplying (\ref{eq:AdiaH}) from the left by $\project{i}$) gives,
  respectively,
  \( \alpha_{ji} = \bra{j(t)}\parenth{\D{}{t}\ket{i(t)}}
   = -\frac{1}{E_j - E_i}\bra{j(t)}\D{\op{H}}{t}\ket{i(t)}. \)
  We can now write our main result:
  \( \bra{j(t_0)}\op{F}(t_1)\ket{i(t_0)} = i \hbar \int_{t_0}^{t_1}
   \alpha_{ji}(t) \exp \bracket{i\int_{t_0}^{t}
   \omega_{ji}(t') dt'} dt, \)
  where $\omega_{ji}(t) = (E_j(t) - E_i(t))/\hbar$.
  The transition probability near the adiabatic limit is
  \( P_{i \To j} = \abs{\bra{j(t_0)}\op{F}(t_1)\ket{i(t_0)}}^2/\hbar^2 .\)

  \itemA{Validity of the Adiabatic Approximation.}
  We expect the adiabatic approximation to hold if the probability
  of a transition out of the initial state to any other state is
  very small, i.e., $\sum_{j \neq i} P_{i \To j} \ll 1$.
  If $\alpha_{ji}$ and $\omega_{ji}$ do not depend on time,
  $P_{i \To j}= \abs{\alpha_{ji}/\omega_{ji}}^2 2(1-\cos \omega_{ji}
  \tau)$.  This motivates the following conservative statement of the
  requirement for the validity of the adiabatic approximation:
  \( \frac{\sum_{j \neq i}
  \abs{\alpha_{ji}}^2}{\abs{\omega_{i,\textrm{min}}}^2} \ll 1 .\)
  Here, $\omega_{i,\textrm{min}}$ is the Bohr frequency for a
  transition from $\ket{i}$ to its nearest neighbor and the numerator
  is simply the magnitude of $\tD{\ket{i}}{t}$.  A perhaps more
  useful statement is that the time scale on which the Hamiltonian
  changes should be much less that $1/\omega_{i,\textrm{min}}$.  It
  is then clear that there is little chance of a transition occurring.

  The expression for the propagator in the adiabatic limit deserves
  another look:
  \( \op{U}(t_1,t_0) = \op{A}(t_1,t_0) \op{V}(t_1,t_0) = \op{A}
   \sum_i \exp\parenth{-\frac{i}{\hbar} \int_{t_0}^{t_1} E_i(t') dt'}
   \project{i}(t_0). \)
  This propagator evolves the system with $\op{V}$, ignoring the effect
  of the geometric evolution at intermediate times, then adds it in at
  the end ($t = t_1$) with $\op{A}$.

  \itemA{Sudden Perturbations.} A sudden perturbation does not change
  the state vector a system.  In the notation of the previous
  sections,
  \( \lim_{\tau \To 0} = \op{I} .\)
  The proof follows trivially from the relation
  \[ \op{U}_{\tau}(s) = \op{I} - \frac{i}{\hbar} \int_0^s \op{H}(s')
   \op{U}_{\tau}(s') ds' .\]
  A simple calculation (Messiah XVII.8) yields the following
  condition for the validity of the sudden approximation: $\tau \ll
  \hbar/\Delta \op{\bar{H}}$ where $\op{\bar{H}} = \int_0^1 \op{H}(s)
  ds$ is essentially the average value of the Hamiltonian over the
  interval $\tau$.
 \end{enumerate}

 \itemI{The Variational Method.}
 \begin{enumerate}[A.]
  \itemA{Introduction.}
  For a system $\op{H}\ket{i} = E_i\ket{i}$, $i = 0,1,2,\ldots$, and an arbitrary state $\ket{\psi} = \sum_i \ket{i}\inner{i}{\psi}$, we have from the general expression for the expectation value of the energy of a state $\expval{\op{H}} = \bra{\psi}\op{H}\ket{\psi}/\inner{\psi}{\psi}$
  \begin{align}
    \expval{\op{H}} &= \frac{\sum_{i=0}^\infty \abs{\inner{i}{\psi}}^2 E_i}{\sum_{i=0}^\infty \abs{\inner{i}{\psi}}^2} \\
    &= E_0 + \frac{\sum_{i=1}^\infty \abs{\inner{i}{\psi}}^2 (E_i - E_0)}{\sum_{i=0}^\infty \abs{\inner{i}{\psi}}^2}
  \end{align}
  Since $E_i - E_0 \geq 0$ by definition, $\expval{H} \geq E_0$ for any $\ket{\psi}$, trivially.  Now, if $\inner{i}{\psi} \sim O(\eps)$ then $\expval{\op{H}} = E_0 + O(\eps^2)$.

  The variational method approximates the ground state of a system by minimizing $\expval{\op{H}}$ for a parameterized state $\ket{\psi(\lambda_1, \lambda_2, \ldots)}$, i.e., by finding the $\lambda_i$ that give the smallest $\expval{\op{H}}$.  This of course involves solving the set of equations $d\expval{\op{H}}/d\lambda_i = 0$.  In general, $\ket{\psi(\lambda_1, \lambda_2, \ldots)}$ will cover only a subset of the system's state space and there will be no choice of $\lambda_i$ values that make $\ket{\psi} \propto \ket{0}$, in which case $\expval{\op{H}} > E_0$.  However, if, for example, $\ket{\psi} = \ket{0} + 0.1\ket{1}$, then $\expval{\op{H}} \approx E_0 + 0.01E_1$, i.e., the approximated ground state energy is very close to the actual value even if $\ket{\psi}$ contains significant contributions from other states.

  \itemA{Example} Example goes here - see Shankar or Sakurai

  \end{enumerate}

\end{enumerate}

\end{document}
