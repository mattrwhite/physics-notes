% ----------------------------------------------------------------
% Periodic potentials, etc ***************************************
% ----------------------------------------------------------------
\documentclass{article}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{booktabs}
\usepackage{verbatim}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
\reversemarginpar
% MATH -----------------------------------------------------------
\input{../Include/Math.tex}
\input{../Include/Vectors.tex}
\input{../Include/Layout.tex}
\input{../Include/Quantum.tex}
%\newcommand{\Hm}{\op{H}_0}
% ----------------------------------------------------------------
% \draftmode
\begin{document}
% \maketitle
% ----------------------------------------------------------------
\begin{enumerate}[I.]
 \itemI{Electrons in a Periodic Lattice.}
 \begin{enumerate}[A.]  
  \itemA{Bloch's Theorem.}  Consider a potential which is
  periodic on a lattice and let $\vect{R}$ denote an arbitrary
  lattice vector:
  \( V(\vect{r} + \vect{R}) = V(\vect{r}). \)
  Denote by $\op{T}_{\vect{R}}$ the operator which produces a
  translation by $\vect{R}$.  Explicitly, $\bra{\vect{r}}\op{T}_{\vect{R}} \ket{\psi}
  = \inner{\vect{r} + \vect{R}}{\psi}$.  Then, assuming that Hamiltonian
  retains the periodicity of the potential, $\op{T}_{\vect{R}} \op{H}
  = \op{H} \op{T}_{\vect{R}}$, i.e., the Hamiltonian commutes with
  all the $\op{T}_{\vect{R}}$.  Because all the $\op{T}_{\vect{R}}$
  commute with each other as well,
  \( \label{eq:Bloch1} \op{T}_{\vect{R}} \op{T}_{\vect{R}'} =
   \op{T}_{\vect{R}'} \op{T}_{\vect{R}} = \op{T}_{\vect{R}+\vect{R}'}, \)
  we can find a complete set of simultaneous eigenstates of $\op{H}$
  and the $\op{T}_{\vect{R}}$.  For such an eigenstate,
  \[ \op{T}_{\vect{R}'} \op{T}_{\vect{R}} \ket{\psi}
   = \lambda(\vect{R}) \lambda(\vect{R}') \ket{\psi}, \eqnsep
  \op{T}_{\vect{R}+\vect{R}'} \ket{\psi}
  = \lambda(\vect{R}+\vect{R}') \ket{\psi}. \]
  It follows from (\ref{eq:Bloch1}) that the eigenvalues satisfy
  $\lambda(\vect{R}+\vect{R}') = \lambda(\vect{R})
  \lambda(\vect{R}')$.  Writing $\vect{R}$ in terms of the basis
  vectors of the lattice, $\vect{R} = n_1 \vect{a}_1 + n_2 \vect{a}_2
  + n_3 \vect{a}_3$, where the $n_i$ are integers, we obtain
  $\lambda(\vect{R}) = \lambda(\vect{a}_1)^{n_1}
  \lambda(\vect{a}_2)^{n_2} \lambda(\vect{a}_3)^{n_3}$. Finally,
  noting that we can always find an value $x_i$ such that
  $\lambda(\vect{a}_i) = e^{2 \pi i x_i}$, we find that
  \( \lambda(\vect{R}) = e^{i \vect{k} \cdot \vect{R}}, \)
  where $\vect{k} = x_1 \vect{b}_1 + x_2 \vect{b}_2 + x_3
  \vect{b}_3$, with $\vect{b}_i$ the reciprocal lattice vectors which
  satisfy $\vect{b}_i \cdot \vect{a}_j = 2 \pi \delta_{ij}$.
  
  We have thus arrived at Bloch's theorem:
  \( \op{T}_{\vect{R}} \ket{\psi} 
   = e^{i \vect{k} \cdot \vect{R}} \ket{\psi}. \)
  In the $\vect{r}$ basis,
  \( \psi(\vect{r} + \vect{R}) = e^{i \vect{k} \cdot \vect{R}} \psi(\vect{r}). \)

  % from PhD thesis - fix notation to match other sections
  \itemA{Central Equation} Using Bloch's theorem, the energy eigenstates can be written as the product of a plane wave $e^{i \vect{q} \cdot \vect{r}}$ and a function $u$ with the periodicity of the lattice as
  \( \psi(\vect{r}) = e^{i \vect{q} \cdot \vect{r}} u_{n, \vect{q}}(\vect{r}), \)
  where $\vect{q}$ is called the quasi-momentum or crystal momentum.  Expanding both $u$ and the lattice potential $U$ into plane waves with the periodicity of the lattice yields
  \[ u_{n, \vect{q}}(\vect{r}) = \sum_{\vect{G}} c_{\vect{G}}(\vect{q}) e^{i \vect{G} \cdot \vect{r}}, 
   \eqnsep U(\vect{r}) = \sum_{\vect{G}} U_{\vect{G}} e^{i {\vect{G}} \cdot \vect{r}}, \]
  where $\vect{G}$ are reciprocal lattice vectors.  Substituting into the Schrodinger equation yields an infinite set of equations:
  \( \label{eq:CentralEq}
   \parenth{\frac{\hbar^2 \abs{\vect{q} + \vect{G}}^2}{2m} - E(\vect{q})} c_{\vect{G}}(\vect{q}) 
    + \sum_{\vect{G}'} U_{\vect{G}'} c_{\vect{G} - \vect{G}'}(\vect{q}) = 0. \)

  Consider, for example, a 1D optical lattice with $U(x) = -V_0 \cos^2 kx$, only the $G = \pm 2k$ components of the potential are non-zero; we can set the $G = 0$ component to zero since this amounts to a constant energy offset.  So equation (\ref{eq:CentralEq}) becomes
  \( \parenth{\frac{\hbar^2 (q + G)^2}{2m} - E(q)} c_{G}(q) 
    + U_{2k} c_{G - 2k}(q) + U_{-2k} c_{G + 2k}(q) = 0, \)
  with $U_{\pm 2k} = -V_0/4$.  Here, $G = 2nk$, $n$ integer, can be truncated

  An alternate approach invokes periodic boundary conditions instead of Bloch's theorem to expand $u$ into discrete plane waves. This method provides another way to prove Bloch's theorem. (ref: Ashcroft)
    
  \itemA{Energies near a Bragg plane.} Let us investigate the effect of the lattice
  on the energies of plane waves with wavevectors near a zone boundary $\vect{K}/2$,
  where $\vect{K}$ is a reciprocal lattice vector. In this case, the free space energy 
  $\eps_{\vect{k}} = \eps_k = \hbar^2 k^2/2m$, of the wave with wavevector
  $\vect{k}$ is nearly degenerate with $\eps_{\vect{k} - \vect{K}}$.  Thus, we consider 
  \[ \psi(r) = c_{\vect{k}} e^{i {\vect{k}} \cdot \vect{r}} 
   + c_{\vect{k} - \vect{K}} e^{i {\vect{k} - \vect{K}} \cdot \vect{r}}, \] 
  for which eq. (\ref{eq:CentralEq}) yields
  \begin{subequations} \begin{gather}
   (\eps_{\vect{k}} - E)c_{\vect{k}} + U_{\vect{K}} c_{\vect{k} - \vect{K}} = 0, \\
   (\eps_{\vect{k} - \vect{K}} - E)c_{\vect{k} - \vect{K}} + U_{-\vect{K}} c_{\vect{k}} = 0.
  \end{gather} \end{subequations}   
  Note that we choosen $U_0 = 0$.  In order for this pair of equations to have
  a solution, the determinant must satisfy
  \[ \begin{vmatrix} 
   \eps_{\vect{k}} - E & U_{\vect{K}} \\
   U_{-\vect{K}} & \eps_{\vect{k} - \vect{K}} - E
  \end{vmatrix} = 0. \]
  This yields two roots for $E$: 
  \( E = \frac{1}{2}(\eps_{\vect{k}} + \eps_{\vect{k} - \vect{K}}) 
   \pm \sqrt{(\eps_{\vect{k}} + \eps_{\vect{k} - \vect{K}})^2/4 + \abs{U_{\vect{K}}}^2 }, \)
  where we have used $U_{-\vect{K}} = U_{\vect{K}}^*$, which follows
  from the fact that $U(\vect{r})$ is real-valued.  Notice that this
  is just the familiar level splitting of a two-level system.
  Defining $\vect{q} = \vect{k} - \vect{K}/2$, this becomes
  \( \label{eq:Splitting2} E = \frac{\hbar^2}{2m}[(K/2)^2 + q^2] 
   \pm \sqrt{4\frac{\hbar^2 (K/2)^2}{2m} \frac{\hbar^2 q^2}{2m} + \abs{U_{\vect{K}}}^2} \)
  See Ashcroft, Mermin Ch.~9 for the argument showing that only wavevectors near zone
  boundaries are affected by the lattice, to first order.
  
  % from PhD thesis
  \itemA{Tight Binding.} Bloch wavefunctions can be written in a basis of maximally localized functions as
\( \psi_{n,q}(x) = N^{-1/2} \sum_{\R{sites }i} e^{i q x_i} w_n(x - x_i), \)
where $N$ is a normalization constant and
\( w_n(x - x_i) = N^{-1/2} \sum_q e^{-i q x_i} \psi_{n,q}(x) = \sum_q e^{i q (x-x_i)} u_{n,q}(x) \)
is called a Wannier function and is localized around $x_i$.  The Wannier states become energy eigenstates in the limit of infinite lattice depth.  Subsequently, we will drop the band index $n$.

For a sufficiently deep lattice, $w(x-x_i)$ falls off quickly away from $x_i$ and we can calculate the energy of the Bloch states in the tight-binding approximation by retaining only on-site and nearest neighbor terms:
\begin{align}
E(q) &= \innerop{\psi_q}{H}{\psi_q} \notag \\
  &= N^{-1} \sum_{i,j} e^{iq(x_j - x_i)} \int_{-\infty}^{\infty} w^*(x-x_i) \op{H} w(x-x_j) dx \notag \\
  &\approx N^{-1} \sum_i \bracket{\int_{-\infty}^{\infty} w^*(x) \op{H} w(x) dx + (e^{iqa} + e^{-iqa}) \int_{-\infty}^{\infty} w^*(x-a) \op{H} w(x) dx } \notag \\
  &= \eps - 2J\cos qa, \label{eq:TightBind}
\end{align}
where $a = \pi/k$ is the lattice period and we have defined
\begin{subequations} \label{eq:FirstEJdef} \begin{gather}
 \eps = \int_{-\infty}^{\infty} w^*(x) \op{H} w(x) dx, \\
 J = \int_{-\infty}^{\infty} w^*(x-a) \op{H} w(x) dx.
\end{gather} \end{subequations}
The tight-binding approximation is valid when $J$ is much larger than the next-nearest neighbor term $J'$.  From (\ref{eq:TightBind}), we can see that $J$ is related to the width of the band by $J = (E_{\R{max}} - E_{\R{min}})/4 = [E(k) - E(0)]/4$ in the tight-binding approximation.

As the lattice is strengthened, the Wannier functions approach harmonic oscillator ground state functions (i.e., Gaussians), and the energy bands flatten and approach the harmonic oscillator energy levels for a single lattice site.  Linearizing around the minima of the lattice potential, we find for the on-site trapping frequency
\( \label{eq:TightBindW} \omega_{\R{site}} = \sqrt{\frac{2 k^2 V_0}{m}} = 2 \sqrt{\frac{V_0}{E_R}} \frac{E_R}{\hbar}. \)
  
  \itemA{Peierls Instability} From Kittel Ch.~10. A one-dimensional metal is unstable to a deformation
  of the lattice with wavevector $K = 2k_F$, which opens an energy gap at $k_F$, thus
  lowering the overall energy of the system.  Using $K/2 = k_F$ in
  (\ref{eq:Splitting2}), we have
  \( E_q = \frac{\hbar^2}{2m}(k_F^2 + q^2)
   \pm \sqrt{4\frac{\hbar^2 k_F^2}{2m} \frac{\hbar^2 q^2}{2m} + \abs{U_{\vect{K}}}^2} \)
  for the energy of an electron with wavevector $k_F + q$.
  
  For a strain $\Delta \cos 2 k_F x$, $U(x) = 2 A \Delta \cos 2 k_F x$, where $A$
  is a constant, so $\abs{U_{\vect{K}}}^2 = A^2 \Delta^2$.  We wish to find the
  value of $\Delta$ which minimizes the overall energy. The elastic energy per 
  unit length is just $E_{\R{elastic}} = Y \Delta^2/4$, where $Y$ is the
  Young's modulus of the material.
  
  The dependence of the electronic energy on the deformation is given by
  \[ \D{E_{\R{elec}}}{\Delta} = \int_{k = -k_F}^{k_F} \D{E_q}{\Delta} \frac{dq}{\pi}
   = 2 \int_{q = 0}^{k_F} \D{E_q}{\Delta} \frac{dq}{\pi}, \]
  where $1/\pi$ is the density of states (per unit length) in one dimension.
  We find
  \[ \D{E_{\R{elec}}}{\Delta} = -\frac{2 A^2 \Delta}{\pi} \frac{m}{\hbar^2 k_F} 
   \sinh^{-1} \parenth{\frac{1}{A \Delta} \frac{m}{\hbar^2 k_F^2} }. \]
  To find the equilibrium deformation, we set $d E_{\R{tot}}/d \Delta = 0$,
  which gives
  \[ Y \Delta/2 - \frac{2 A^2 \Delta}{\pi} \frac{m}{\hbar^2 k_F} 
   \sinh^{-1} \parenth{\frac{1}{A \Delta} \frac{\hbar^2 k_F^2}{m} } = 0. \]
  Solving,
  \[ \frac{\hbar^2 k_F^2}{m A \Delta} = \sinh \frac{-\pi \hbar^2 k_F C}{4 m A^2}. \]
  Assuming the argument of $\sinh$ above is small and defining $W = \hbar^2 k_F^2/2m$,
  the conduction band width, $N(0) = 2m/\pi \hbar^2 k_F$, the density of states at the
  Fermi level, and $V = 2 A^2/Y$, the effective electron-electron interaction energy, we
  have
  \( \abs{A} \Delta = 4 W \exp (-1/N(0) V). \)
  
  In higher dimensions, the elastic energy increases faster than the electronic energy
  decreases, and there is no instability toward distortion.
  
  \begin{enumerate}[i.]
    \itemi{Application to Conjugated Polymers.}  Assuming periodic (period $L$) 
  boundary conditions, the density of states in $k$-space is $L/2 \pi$ in 1D, so the
  number of states with $\abs{k} < k_F$ is $k_F L / \pi$. To accomodate $N$ electrons,
  $k_F = \pi N/2 L$, where the additional factor of two accounts for the two spin states.
  For a conjugated $sp^2$ polymer, each carbon atom contributes a single $\pi$ electron
  which can be considered to be delocalized over the length of the chain. Thus, $L/N = a$,
  where $a$ is the spatial period of the chain (i.e., the distance between carbon atoms).
  The result above indicates an instability
  with wavevector $2 k_F = \pi/a$, i.e., wavelength $2a$, corresponding to a dimerization
  of the atoms in the chain. This partially justifies the usual manner of drawing
  such a chain as consisting of alternating double and single bonds.
  \end{enumerate}
  
 \end{enumerate}
 
 \itemI{Lattice Vibrations.}
 \begin{enumerate}[A.]
  % Refs: Cohen-Tannoudji J_V, 3D lattice refs below
  \itemA{1D Harmonic Lattice.} We consider a linear chain of $N$ coupled harmonic oscillators, evenly spaced by distance $a$.  The Hamiltonian is
  \( \op{H} = \sum_{q=1}^N \bracket{ \frac{1}{2m} \op{P}_q^2 + \frac{1}{2} m \omega^2 \op{X}_q^2 + \frac{1}{2} m \sigma^2 (\op{X}_q - \op{X}_{q+1})^2 }. \)
  Here, $\sigma$ parameterizes the harmonic interaction between sites and $\omega$ parameterizes an additional on-site harmonic potential; for a typical crystal lattice, $\omega = 0$.  We begin by introducing normal mode coordinates
  \( \op{Q}_k = \frac{1}{\sqrt{N}} \sum_q \op{X}_q e^{-iqka}, \eqnsep \op{\Pi}_k = \frac{1}{\sqrt{N}} \sum_q \op{P}_q e^{-iqka}, \)
  where $k$ can be fixed by, e.g., periodic boundary conditions: from (\ref{eq:LattXq}) below, $\op{X}_q = \op{X}_{q+N}$ requires $e^{iNka} = 1$ so we can take $k = 2 \pi n/(Na)$ where $n = -N/2,-N/2 + 1,\ldots,N/2$ (the first Brillouin zone).
  Next, we define ladder operators
  \( \Adown_k = \frac{1}{\sqrt{2}} \bracket{ \beta(k) \op{Q}_k + \frac{i}{\hbar \beta(k)} \op{\Pi}_k }, \eqnsep \Aup_k = \frac{1}{\sqrt{2}} \bracket{ \beta(k) \op{Q}^{\dag}_k - \frac{i}{\hbar \beta(k)} \op{\Pi}^{\dag}_k }, \)
  with $\beta(k) = \sqrt{m \Omega(k)/\hbar}$ where (the dispersion relation)
  \( \Omega(k) = \sqrt{\omega^2 + 4 \sigma^2 \sin^2(ka/2)} \)  
  gives the normal mode frequencies.
  In terms of ladder operators,
  \[ \op{Q}_k = \frac{1}{\sqrt{2} \beta(k)} \bracket{ \Adown_k + \Aup_{-k} }, \eqnsep \op{\Pi}_k = \frac{-i \hbar \beta(k)}{\sqrt{2}} \bracket{ \Adown_k - \Aup_{-k} }, \]
  where we have used $\op{Q}_k^{\dag} = \op{Q}_{-k}$ and $\op{\Pi}_k^{\dag} = \op{\Pi}_{-k}$.
  Finally, returning to the site operators,
  \begin{subgather} \label{eq:LattXq}
    \op{X}_q = \frac{1}{\sqrt{N}} \sum_k \op{Q}_k e^{iqka} = \frac{1}{\sqrt{N}} \sum_k \frac{1}{\sqrt{2} \beta(k)} \bracket{ \Adown_k e^{iqka} + \Aup_{k} e^{-iqka} }, \\
    \op{P}_q = \frac{1}{\sqrt{N}} \sum_k \op{\Pi}_k e^{iqka} = \frac{1}{\sqrt{N}} \sum_k \frac{-i \hbar \beta(k)}{\sqrt{2}} \bracket{ \Adown_k e^{iqka} - \Aup_{k} e^{-iqka} }.    
  \end{subgather}
  Plugging these expressions into the Hamiltonian (it is helpful to start with the leading equalities) yields
  \( \op{H} = \sum_k \hbar \Omega(k) (\Aup_k \Adown_k + \tfrac{1}{2}). \)
  Since the ladder operators satisfy $[\Adown_k, \Aup_{k'}] = \delta_{k, k'}$ (all other commutators zero), we see that each of the normal modes is harmonic.  The time evolution of $\Adown_k$ is given by the usual harmonic oscillator result, $\Adown_k(t) = \Adown_k e^{-i\Omega(k)t}$, which may be substituted into the above expressions for $\op{X}_q$ and $\op{P}_q$ to describe to motion of individual sites.
  
  % Refs: Marder, Ashcroft and Mermin
  \itemA{3D Harmonic Lattice.}
 
 \end{enumerate} 
 
 
 \itemI{Finite Systems.}
 \begin{enumerate}[A.]
  \itemA{Finite Linear Chain}  Let use consider an $N$-site 1D lattice in the tight binding approximation, i.e., nearest neighbor interactions only, parameterized by $J = \bra{\phi_{n+1}}\op{H}\ket{\phi_n}$, where $\ket{\phi_n}$ is the maximally localized state at site $n$.  Writing an energy eigenstate (with energy $E_a$) as
  \[ \ket{\psi_a} = \sum_n c_a(n) \ket{\phi_n}, \]
  and plugging into the Schrodinger equation yields the relations
  \[ (E_a - \eps) c_a(n) = J[c_a(n+1) + c_a(n-1)], \]
  for $n = 1,\ldots,N$, taking $c_a(0) = c_a(N+1) = 0$ in the two equations for the ends of the chain.
  Using the ansatz $c_a(n) = c \sin n a$, we obtain the familiar looking result $E_a = \eps + 2 J \cos a$.  The equation for $c_a(N)$ requires $a = \pi m/(N+1)$, where $m = 1,\ldots,N$ (the spectrum simply repeats for values outside this range).  Finally, we can show that $c = \sqrt{2/(N+1)}$.
  
  \itemA{Finite Ring} If we couple the first and last sites, so $\bra{\phi_1}\op{H}\ket{\phi_N} = J$, we find $c_a(n) = c_a(n+\nu N)$, $\nu$ integer, and, finally, $c_a(n) = N^{-1/2} \exp(ian)$, with $a = 2\pi m/N$, $m = 0,\ldots,N-1$ and $E_a = \eps + 2 J \cos a$ as in the linear case.
 
 \end{enumerate} 
 
\end{enumerate}

\begin{comment}
  % removed this section - reference source (Ashcroft) instead
  \itemA{Central Equation} Assuming periodic boundary conditions, we can
  expand $\psi(\vect{r})$ in the set of plane waves as
  \( \psi(\vect{r}) = \sum_{\vect{k}} c_{\vect{k}} e^{i {\vect{k}} \cdot \vect{r}}. \)
  Similarly, we can expand $U(\vect{r})$ in a set of plane waves with the
  periodicity of the lattice as
  \( U(\vect{r}) = \sum_{\vect{K}} U_{\vect{K}} e^{i {\vect{K}} \cdot \vect{r}}, \)
  where
  \[ U_{\vect{K}} = \frac{1}{v} \int_{\R{cell}} U(\vect{r}) e^{i {\vect{K}} \cdot \vect{r}} d\vect{r}. \]
  Inserting these expansions into the Schrodinger equation, 
  $\op{H}\ket{\psi} = E\ket{\psi}$, we obtain for the kinetic
  energy term (FIX KETS HERE)
  \[ \frac{1}{2m} \op{p}^2 \psi = -\frac{\hbar^2}{2m} \del^2 \psi 
   = \sum_{\vect{k}} \frac{\hbar^2 k^2}{2m} c_{\vect{k}} e^{i {\vect{k}} \cdot \vect{r}}, \]
  and for the potential energy term,
  \[ \op{U} \psi = \sum_{\vect{K}, \vect{k}} U_{\vect{K}} c_{\vect{k}} e^{i {\vect{K} + \vect{k}} \cdot \vect{r}}
   = \sum_{\vect{K}, \vect{q}} U_{\vect{K}} c_{\vect{q} - \vect{K}} e^{i {\vect{q}} \cdot \vect{r}}. \]
  Here we have defined $\vect{q} = \vect{K} + \vect{k}$ and used the fact
  that summing over $\vect{q}$ is the same as summing over $\vect{k}$.
  Combining these results, we have
  \( \sum_{\vect{k}} e^{i {\vect{k}} \cdot \vect{r}} 
   \bracket{ \parenth{\frac{\hbar^2 k^2}{2m} - E}c_{\vect{k}} 
   + \sum_{\vect{K}} U_{\vect{K}} c_{\vect{k} - \vect{K}} } = 0. \)
  Since the set of plane waves satisfying periodic boundary conditions
  is orthogonal, each of the coefficents in brackets must individually be zero:
  \( \label{eq:CentralEq}
   \parenth{\frac{\hbar^2 k^2}{2m} - E}c_{\vect{k}} 
   + \sum_{\vect{K}} U_{\vect{K}} c_{\vect{k} - \vect{K}} = 0. \)
  This result, which is equivalent to Bloch's theorem, relates the momentum
  components of the wavefunction separated by reciprocal lattice vectors.
\end{comment}

\end{document}
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  