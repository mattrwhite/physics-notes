% ----------------------------------------------------------------
% Quantum Mechanics Reference ************************************
% ----------------------------------------------------------------
\documentclass[twocolumn]{article}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{booktabs}
\usepackage{txfonts}
\usepackage[letterpaper,margin=1in]{geometry}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
% Includes -----------------------------------------------------------
\input{../Include/Math.tex}
\input{../Include/Vectors.tex}
\input{../Include/Quantum.tex}
\input{../Include/Layout.tex}
% ----------------------------------------------------------------
\begin{document}
\title{Quantum Mechanics Reference}
\author{}
\date{}
%\maketitle
% ----------------------------------------------------------------
\paragraph{Basic Relations}
\begin{center}\begin{tabular}{ll}
\toprule
Wavevector & $k = p/\hbar$ \\
de Broglie wavelength & $\lambda = h/p$ \\
Probability & $\abs{\inner{\omega_i}{\psi}}^2$ \\
Expectation value & $\expval{\opA} = \bra{\psi}\opA\ket{\psi}$ \\
Uncertainty & $\Delta \opA = \sqrt{\expval{\opA^2} - \expval{\opA}^2}$ \\
 & $\Delta \opA \cdot \Delta \opB \geq \hbar/2$ \\
Heisenberg Equation & $i\hbar \D{}{t}\opA_H = [\opA_H,\op{H}]$ \\
Ehrenfest's Theorem & $i\hbar \D{}{t}\expval{\opA} = \expval{[\opA,\op{H}]}$ \\
Probability Current &  $\vect{J} = \frac{-i \hbar}{2m}(\psi^* \del \psi
 - \psi \del \psi^*)$ \\
Coordinate basis & $ \op{X} \To x, \eqnsep \op{P} \To -i\hbar \D{}{x}$ \\
Canonical Commutator & $[\op{X},\op{P}] = i\hbar \op{I}$ \\
BCH & $e^{\opA} \opB e^{-\opA} = \opB + [\opA,\opB]$ \\
& $\eqnsep +\frac{1}{2!}[\opA,[\opA,\opB]] + \ldots$ \\ \bottomrule
\end{tabular} \end{center}

\paragraph{Angular Momentum and Spin}
The eigenstates of orbital angular momentum have the following
properties:
\begin{align*}
\op{L}^2 \ket{l,m} = l(l+1)\hbar^2 \ket{l,m},
\eqnsep l = 0,1,2,\ldots, \\ \op{L}_z \ket{l,m} = m \hbar \ket{l,m},
 \eqnsep m = l, l-1, \ldots, -l+1, -l.
\end{align*}
In the coordinate basis: $\inner{\theta,\phi}{l,m} =
Y_l^m(\theta,\phi)$.

The eigenstates of a rotationally invariant Hamiltonian are
\[ \psi_{Elm}(r ,\theta, \phi) = R_{El}(r) Y_l^m (\theta, \phi) , \]
where $R_{El}(r)$ is given by the radial equation:
\[ -\frac{\hbar^2}{2m} \bracket{\PDD{}{r} -
 \frac{l(l+1)}{r^2}}U_{El} + V(r)U_{El} = E U_{El} , \]
with $U_{El} = r R_{El}$.

The spin angular momentum operator is $\vectop{S} = \hbar \vectop{\sigma}/2$
where $\vectop{\sigma}$ consists of the Pauli matrices:
\[ \op{\sigma}_x = \begin{bmatrix} 0 & 1 \\ 1 & 0 \end{bmatrix}, \eqnsep
\op{\sigma}_y = \begin{bmatrix} 0 & -i \\ i & 0 \end{bmatrix}, \eqnsep
\op{\sigma}_z = \begin{bmatrix} 1 & 0 \\ 0 & -1 \end{bmatrix} . \]
Properties: $[\op{\sigma}_i, \op{\sigma}_j] = 2i \eps_{ijk} \op{\sigma}_k$, i.e.,
$\op{\sigma}_x \op{\sigma}_y = i \op{\sigma}_z$ and
cyclic permutations, $[\op{\sigma}_i, \op{\sigma}_j]_+ = 2\op{I}\delta_{ij}$.
The total angular momentum operator is $\vectop{J} = \vectop{L}
+ \vectop{S}$. It satisfies $\vectop{J} \times \vectop{J}
= i\hbar \vectop{J}$.

\paragraph{Generators}
\begin{center}\begin{tabular}{ll} \toprule
Time Evolution & $\op{U}(t) = e^{-i\op{H}t/\hbar}$ \\
Translation & $\op{U}(\vect{r}) = e^{-i\vect{r} \cdot \vectop{P}/\hbar}$ \\
Rotation & $\op{U}(\vect{\theta}) = e^{-i\vect{\theta} \cdot
\vectop{J}/\hbar}$ \\ \bottomrule
\end{tabular} \end{center}

\paragraph{Simple Systems}
Piecewise potentials: $\psi'$ continuous only for finite
discontinuity. For $V(x) = V_0 \delta(x-\tilde{x})$, $\Delta
\psi'(\tilde{x}) = \frac{2m}{\hbar^2} V_0 \psi(\tilde{x})$.

\emph{Free particle}: $\psi = \inner{x}{p} = \sqrt{1/2 \pi \hbar}
e^{ikx}$ where $k=p/\hbar$. $E = \hbar^2 k^2/2m$.  For a free
particle confined to a large box of volume $V$, $\psi =
\sqrt{1/V}e^{ikx}$.

\emph{Infinite square well} ($0$ to $L$):
\[ \psi(x) = \sqrt{\frac{2}{L}} \sin \frac{n\pi x}{L}, \eqnsep
 E_n = \frac{\hbar^2 \pi^2 n^2}{2mL^2};
 \eqnsep n = 1,2,3,\ldots\]

\emph{SHO}: With $V(x) = \frac{1}{2}m\omega x^2$, $E = (n+
\frac{1}{2})\hbar\omega$, $n = 0,1,2,\ldots$. Ladder operators:
$\op{a}\ket{n} = \sqrt{n}\ket{n-1}$, $\op{a}^\dag\ket{n} =
\sqrt{n+1}\ket{n+1}$. Also note that $[\op{a},\op{a}^\dag] = 1$. In
terms of $\op{a}$ and $\op{a}^\dag$, $\op{H} = (\op{a}^\dag \op{a} +
\frac{1}{2})\hbar \omega$, $\op{X} =
\sqrt{\hbar/2m\omega}(\op{a}^\dag + \op{a})$, and $\op{P} =
i\sqrt{\hbar m\omega/2}(\op{a}^\dag - \op{a})$.

\paragraph{Time Independent Perturbation Theory}
For $\op{H} = \op{H}^0 + \op{H}^1$,
\[ \ket{n^1} = \sum_{m \neq n} \frac{\ket{m^0}
 \bra{m^0}\op{H}^1\ket{n^0}}{E_n^0 - E_m^0} \]
\[ E_n^1 = \bra{n^0}\op{H}^1\ket{n^0}, \eqnsep
 E_n^2 = \bra{n^0}\op{H}^1\ket{n^1} \]


\paragraph{Time Dependent Perturbation Theory} For $\op{H} = \op{H}^0
+ \op{H}^1(t)$ (in the Schrodinger picture), the first order
transition probability from EESs $\ket{i^0}$ to $\ket{f^0}$ is
$\abs{d_f(t)}^2$ with
\[ d_f(t) = \delta_{fi} - \frac{i}{\hbar} \int_{t_0}^t
 \bra{f^0}\op{H}^1(t')\ket{i^0} e^{i\omega_{fi}(t - t_0)} dt' , \]
where $\omega_{fi} = (E^0_f - E^0_i)/\hbar$.

\end{document}
