% ----------------------------------------------------------------
% Special Relativity *********************************************
% ----------------------------------------------------------------
\documentclass{article}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{booktabs}
%\usepackage{undertilde}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
\reversemarginpar
% MATH -----------------------------------------------------------
\input{../Include/Layout.tex}
\input{../Include/Math.tex}
\input{../Include/Vectors.tex}
\newcommand{\fv}[1]{\vec{#1}} % Four vector
\newcommand{\ff}[1]{\tilde{#1}} % four one-form
\newcommand{\bv}{\fv{e}} % basis vector
\renewcommand{\bf}{\ff{\omega}} % basis one-form
\newcommand{\tn}[1]{\boldsymbol{\mathrm{#1}}} % tensor
% ----------------------------------------------------------------
%\draftmode
\begin{document}
% \maketitle
% ----------------------------------------------------------------
\begin{enumerate}[I.]
 \item \textbf{Einstein's Postulates and the Lorentz Boost.}
 \begin{enumerate}[A.]
  \item \emph{Einstein's Postulates.}  The foundation of special
  relativity is the following postulate, due to Poincare:
  \begin{quote}
  \emph{The laws of physics are the same in all inertial reference
  frames.}
  \end{quote}
  This implies that the speed of light, determined by Maxwell's
  equations which make no reference to any particular frame, is
  the same for all observers.  Einstein formulated this as the second
  postulate of special relativity:
  \begin{quote}
  \emph{The speed of light in empty space is independent of the motion
   of the its source.}
  \end{quote}

  As shown below, no \emph{particle} can travel faster than (the
  speed of) light.  We postulate further that
  \begin{quote}
  \emph{Nothing - no information - can travel faster than light.}
  \end{quote}

  \item \emph{The Lorentz Transformation.}  Consider an inertial
  frame (henceforth a frame) $S'$ moving at speed $v$ in the $+x$
  direction relative to a frame $S$.  Without loss of generality,
  assume that the frames coincided at $t'=t=0$.  Then classically,
  the frames are related by the Galilean transformation:
  \begin{equation} \begin{split}
   x' &= x - vt, \\
   y' &= y, \\
   z' &= z, \\
   t' &= t.
  \end{split} \end{equation}
  By starting with the most general linear relation between the
  coordinates of the frames and requiring that something with
  speed $c$ in one frame have the same speed in any other frame,
  we arrive at the Lorentz boost (often called the Lorentz
  transformation although it is only a specific case of the actual
  Lorentz transformation, discussed below):
  \begin{subequations} \label{eq:LorentzBoost}
  \begin{namedalign}{\textbf{The Lorentz Boost}}
   x' &= \gamma(x - vt), \\
   y' &= y, \\
   z' &= z, \\
   t' &= \gamma(t-\frac{v}{c^2}x),
  \end{namedalign}
  \end{subequations}
  where
  \begin{equation}
   \gamma \defn \frac{1}{\sqrt{1-v^2/c^2}} .
  \end{equation}
  We often make the following definition as well:
  \( \beta \defn \frac{v}{c} \)
  The inverse transformation from $S'$ to $S$ can be obtained
  by taking $v \To -v$ in (\ref{eq:LorentzBoost}) (and swapping the
  primes).

 \end{enumerate}

 \item \textbf{Consequences of Special Relativity.}
 \begin{enumerate}[A.]

  \item \emph{Simultaneity and Time Dilation.} Two events (i.e., points
  in spacetime) which are
  simultaneous in one frame are in general not simultaneous in others.
  Let $A$ and $B$ be events given in $S$ by $x_A = 0$, $t_A = 0$ and
  $x_B = b$, $t_B = 0$.  Although these events are simultaneous in
  $S$, the Lorentz transformation shows that they are not
  simultaneous in $S'$: $x'_A = 0$, $t'_A = 0$ and $x'_B = \gamma b$,
  $t'_B = -\gamma (v/c^2) b$.  In $S'$, $B$ occurs before $A$.
  Note that if two events occur at the same place at the same
  time in one frame, they will appear as such in all frames.

  If we consider a time interval $\Delta t$ is $S$, we find that
  \( \label{eq:TimeDilation} \Delta t' = \frac{1}{\gamma} \Delta t. \)
  That is, an observer in $S$ will notice that the clocks in $S'$
  run slower by a factor of $1/\gamma$.  This effect is called
  time dilation.  The equivalence of inertial frames requires that to observers
  in $S'$, the clocks in $S$ appear to be running slower by exactly
  the same factor!

  \item \emph{The Lorentz Contraction.} Suppose an observer in $S$
  wishes to measure the length of a stick (at rest) in $S'$. In
  $S'$, the rest frame of the stick, its length is $\Delta x' =
  x'_r - x'_l$.  The observer in $S$ must take the positions of
  ends of the stick at the same instant in his frame. (Note
  that these measurements will not appear simultaneous to an
  observer in $S'$.)  Doing
  so, he finds that the length of the stick is shorter:
  \begin{equation}
   \Delta x = \frac{1}{\gamma} \Delta x'.
  \end{equation}
  This effect is called the Lorentz (length) contraction.  Only
  the direction along the direction of motion is contracted -
  perpendicular directions (in this case, $y$ and $z$) are
  unaffected.
  \item \emph{Transformation of Angles.}
  \begin{enumerate}[1.]
   \item Consider a stick of length $l_0$ at rest in $S'$ which makes an
   angle $\theta_0$ with the $x'$ axis.  Suppose $S'$ is moving at
   $v$ along $x$ in $S$.  Then the length of the stick as seen in
   $S$ is
   \( l = l_0 \sqrt{1-\beta^2 \cos^2 \theta_0} . \)
   The angle of the stick with the $x$ axis, $\theta$, is given by
   \( \tan \theta = \gamma \tan \theta_0 . \)
   \item If instead a light ray is fired off at an angle
   $\theta_0$ to the $x'$ axis by a source stationary in $S'$, the
   angle $\theta$ with the $x$ axis observed in $S$ is given by
   \( \cos \theta = \frac{\cos \theta_0 + \beta}{1 + \beta \cos \theta_0} . \)
  \end{enumerate}


  \item \emph{Addition of Velocities.}  Consider a particle moving
  at
  \[ u = \D{x}{t} \]
  in the $x$ direction in $S$.  For $S'$, the Lorentz
  transformation gives
  \[ dx' = \gamma (dx-v dt), \quad dt' = \gamma (dt - (v/c^2)dx). \]
  The velocity of the particle in $S'$ is then
  \[  u' = \D{x'}{t'} = \frac{u - v}{1 - uv/c^2}. \]

  The general transformation of velocity is
  \begin{subequations} \begin{namedalign}
  {\textbf{Einstein's Velocity Addition Rule}}
  u'_x &= \frac{u_x - v}{1 - v u_x/c^2}, \\
  u'_y &= \frac{u_y}{\gamma(1 - v u_x/c^2)}, \\
  u'_z &= \frac{u_z}{\gamma(1 - v u_x/c^2)}.
  \end{namedalign} \end{subequations}

  In more transparent notation, if $A$ is moving at $v_{AB}$ relative to $B$ and
  $B$ is moving at $v_{BC}$ relative to $C$, then the velocity of
  $A$ relative to $C$ is
  \begin{equation}
   v_{AC} = \frac{v_{AB} + v_{BC}}{1 +
   v_{AB}v_{BC}/c^2}.
  \end{equation}
  The most important implication of the velocity addition formula
  is that no particle can move faster than light.

  \item \emph{Relativistic Doppler Effect.}  It is easy to show
  (see Marion 14.5) that when a source of light at frequency $\omega_0$
  is moving relative
  to an observer at speed $v$ (positive if the source and observer are
  approaching each other, negative if they are receding), the
  frequency of light $\omega$ seen by observer is altered according to
  \begin{namedeqn}{\textbf{Relativistic Doppler Shift}}
   \omega = \sqrt{\frac{1+v/c}{1-v/c}}\omega_0 .
  \end{namedeqn}
  If the source and observer are receding, the frequency is
  decreased and the light is said to be redshifted.  If the two
  are approaching so the frequency is increased, the light is said
  to be blueshifted.

  Consider now the case of a stationary observer and a light
  source travelling in some arbitrary direction at velocity $\vect{v}$.
  If the angle between an observed light ray and $\vect{v}$ is
  $\theta$, then the frequency shift of the light is given by
  \( \omega = \frac{\sqrt{1-v^2/c^2}}{1-(v/c)\cos \theta} \omega_0 . \)

  \item \emph{Rapidity.} Many of the results of special relativity
  take an especially simple form when expressed in terms of the
  rapidity
  \begin{namedeqn}{\textbf{Rapidity}}
   \theta \defn \tanh^{-1} (v/c).
  \end{namedeqn}
  The following useful results follow easily:
  \( \tanh \theta = v/c, \eqnsep \cosh \theta = \gamma, \eqnsep \sinh
   \theta = \gamma (v/c) . \)
  Most notably, rapidities add:
  \( \theta_{AC} = \theta_{AB} + \theta_{BC} . \)
  The Lorentz boost expressed in terms of rapidity is
  \begin{equation} \begin{split}
   x' &= (-\sinh \theta)ct + (\cosh \theta)x, \\
   y' &= y, \\
   z' &= z, \\
   ct' &= (\cosh \theta)ct - (\sinh \theta)x.
  \end{split} \end{equation}
  From these, we can obtain the useful relations:
  \begin{equation} \begin{split}
   ct' + x' &= e^{-\theta} (ct + x), \\
   ct' - x' &= e^\theta (ct - x).
  \end{split} \end{equation}

  \item \emph{Famous Paradoxes of Special Relativity.}
  \begin{enumerate}[1.]
   \item \emph{The Twin Paradox.}

   \item \emph{The Barn and Ladder Paradox.}
  \end{enumerate}

 \end{enumerate}

 \item \textbf{The Structure of Spacetime.}
 \begin{enumerate}[A.]
  \item \emph{Lorentz Space.}
  \begin{enumerate}[1.]
   \item \emph{Vectors.} Spacetime is represented by a real,
   linear, four dimensional flat space.  In this space, vectors,
   denoted $\fv{v}$, exist as geometric objects, independent of any
   coordinate system.

   If we introduce a coordinate basis of vectors $\set{\bv_0, \bv_1,
   \bv_2, \bv_3}$, any vector can be written in terms of its components
   as
   \( \fv{v} = v^\mu \bv_\mu. \)
   Here, $v^\mu$ is the $\mu$th component of $\fv{v}$ and $\bv_\mu$ is
   the $\mu$th basis vector.  It follows from this relation that the
   components of our basis vectors are $\bv_0 = (1,0,0,0)$, $\bv_1 =
   (0,1,0,0)$, and so on. Note that we have used the Einstein
   summation notation, in which summation over repeated indexes is
   implied.  If the index is a greek letter, the summation is taken
   from $0$ to $3$; if the index is an arabic letter, the summation is
   taken from $1$ to $3$.  The manipulation of these indexes
   approaches a form of art and is called index gymnastics.  Some
   useful rules for index gymnastics are summarized below.
   \begin{enumerate}[a.]
    \item Every indexed quantity is simply a number (as opposed to a
    vector, etc.) and hence can moved around (commutativity), factored
    out (associativity), and so on.

    \item The scope of repeated indices is limited to the
    (multiplicative) term in which they appear, not the entire
    expression.  As an illustration:
    \[ a^\mu b_\mu - a^\nu c_\nu = a^\mu b_\mu - a^\mu c_\mu =
    a^\mu(b_\mu - c_\mu) .\]
   \end{enumerate}

   \item \emph{One-forms.} A one-form is a linear function
   $\ff{a}(\fv{v})$ that maps a vector to a real number; it is a
   member of the dual space to flat spacetime. Because of the
   linearity property,
   \[ \ff{a}(\alpha \fv{u} + \beta \fv{v})
    = \alpha \ff{a}(\fv{u}) + \beta \ff{a}(\fv{v}) \in \Real, \]
   the action of a one-form on the vector basis $\set{\bv_\mu}$ is
   sufficient to specify it uniquely.  We therefore define the
   components of a one-form as follows:
   \( a_\mu \defn \ff{a}(\bv_\mu) .\)
   Any component with a subscript (covariant) index is the component
   of a one-form while any component with a superscript
   (contravariant) index is the component of a vector. Given a vector
   $\fv{v} = v^\mu \bv_\mu$, we have
   \( \label{eq:Contract}
    \ff{a}(\fv{v}) = v^\mu \ff{a}(\bv_\mu) = v^\mu a_\mu. \)
   This operation is called contraction (of a one-form and a vector).

   Introducing a basis $\set{\bf^0, \bf^1, \bf^2, \bf^3}$ of the dual
   space, any one-form can be written as
   \( \ff{a} = a_\mu \bf^\mu . \)
   Applying this to $\fv{v} = v^\nu \bv_\nu$ yields $\ff{a}(\fv{v}) =
   v^\nu a_\mu \bf^\mu(\bv_\nu)$.  Comparing (\ref{eq:Contract})
   yields
   \( \bf^\mu(\bv_\nu) = \delta^\mu_\nu . \)
   Thus, the components of the basis one-forms are
   $\bf^0 = (1,0,0,0)$, $\bf^1 = (0,1,0,0)$, and so on.

   To bring one-forms and vectors onto equal footing, we can think of
   vectors as functions from one-forms to real numbers by defining
   their action as follows:
   \( \fv{v}(\ff{a}) \defn \ff{a}(\fv{v}) = v^\mu a_\mu .\)

   \item \emph{Tensors.} A $\binom{M}{N}$ tensor is a linear (with
   respect to every argument) function of $M$ one-forms and $N$
   vectors into the real numbers.  The basis of the $4^{(M+N)}$
   dimensional space of $\binom{M}{N}$ tensors is formed by taking
   the tensor product, denoted $\otimes$, of $M$ basis vectors and
   $N$ basis one-forms.  Note that a vector is a $\binom{1}{0}$
   tensor, a one-form is a $\binom{0}{1}$ tensor, and a scalar is a
   $\binom{0}{0}$ tensor.
   For simplicity, we will consider the case of
   $\binom{0}{2}$ tensors, the basis for which is the set
   $\set{\bf^\mu \otimes \bf^\nu}$.  Any $\binom{0}{2}$ tensor can be
   written as a linear combination of these 16 basis one-forms:
   \( \tn{f} = f_{\mu \nu} (\bf^\mu \otimes \bf^\nu) .\)
   As a function from vectors to reals,
   \begin{align}
    \tn{f}(\fv{u},\fv{v})
    &= f_{\mu \nu} \bf^\mu (\fv{u}) \otimes \bf^\nu (\fv{v}) \notag \\
    &= f_{\mu \nu} u^\alpha \bf^\mu (\bv_\alpha) \otimes v^\beta
     \bf^\nu (\bv_\beta) \notag \\
    &= f_{\mu \nu} u^\alpha \delta^\mu_\alpha v^\beta
     \delta^\nu_\beta \notag \\
    &= f_{\mu \nu} u^\mu  v^\nu .
   \end{align}
   Note that upon evaluation, the tensor product becomes an ordinary
   product; it should not appear at all in the above derivation but
   was retained for clarity.  Also note
   that $\tn{f}(\fv{u},\fv{v}) \neq \tn{f}(\fv{v},\fv{u})$ unless
   $f_{\mu \nu} = f_{\nu \mu}$, in which case $\tn{f}$ is said to
   be symmetric.  Also note that we have
   \( f_{\mu \nu} = \tn{f}(\bv_\mu, \bv_\nu). \)
   This is a general result for all tensors, i.e., the components are
   obtained by the action of the tensor on basis one-forms and/or
   vectors.  For a second example, $\set{\bv_\mu \otimes \bf^\nu}$ is
   the basis for $\binom{1}{1}$ tensors; the components of a
   $\binom{1}{1}$ tensor $\tn{f}$ are
   \( f_\nu^\mu = \tn{f}(\bf^\mu,\bv_\nu) . \)

   \item \emph{The Metric Tensor.}  The
   fundamental structure of flat space is contained in the metric
   tensor
   \( \tn{g} = g_{\mu \nu} \bf^\mu \otimes \bf^\nu, \)
   a $\binom{0}{2}$ symmetric (i.e., $g_{\mu \nu} = g_{\nu \mu}$)
   tensor.  If we apply $\tn{g}$ (or any other $\binom{0}{2}$ tensor)
   to a \emph{single} vector, we are left with a one-form:
   \begin{align}
    \tn{g}(\fv{v},-)
     &= g_{\mu \nu} \bf^\mu(\fv{v}) \otimes \bf^\nu \notag \\
     &= g_{\mu \nu} v^\alpha \bf^\mu(\bv_\alpha) \otimes \bf^\nu \notag \\
     &= g_{\mu \nu} v^\mu \bf^\nu.
   \end{align}
   Note that $\tn{g}(\fv{v},-) = \tn{g}(-,\fv{v})$ since $\tn{g}$ is
   symmetric.  The special property of the metric tensor is that we
   define $\tn{g}(\fv{v},-)$ as the one-form dual to $\fv{v}$, i.e.,
   \( \ff{v} \defn \tn{g}(\fv{v},-) = g_{\mu \nu} v^\mu \bf^\nu . \)
   Comparing $\ff{v} = v_\nu \bf^\nu$, we see that the components of
   $\ff{v}$ in terms of the components of $\fv{v}$ are
   \( v_\nu = g_{\mu \nu} v^\mu . \)

   Just as $\tn{g}$ converts vectors to their dual one-forms, its
   inverse $\tn{g}^{-1} = g^{\mu \nu} \bv_\mu \otimes \bv_\nu$ (which
   exists provided $\det (g_{\mu \nu}) \neq 0$) converts one-forms to
   their dual vectors:
   \( \tn{g}^{-1}(\ff{a},-) = \tn{g}^{-1}(-,\ff{a}) = g^{\mu \nu}
   a_\mu \bv_\nu = a^\nu \bv_\nu . \)
   By definition, $g^{\mu \nu}$ are the components of $\tn{g}^{-1}$,
   hence it must be true that
   \( g^{\mu \alpha} g_{\alpha \nu} = \delta^\mu_\nu. \)
   The metric tensor is the only tensor for which this is true, i.e.,
   for which its contravariant components are the elements of the
   matrix inverse to the matrix of its covariant components.

   The metric tensor and its inverse are used to lower and raise,
   respectively, the indexes of other quantities.  No other tensors
   can perform this function. Each factor of
   the metric tensor lowers one index and each factor of the inverse
   metric tensor raises one index.

   \item \emph{The Inner Product.} In flat spacetime, the components
   of metric tensor are
   \( (g_{\mu \nu}) = \begin{pmatrix}
     1 & 0 & 0 & 0 \\
     0 & -1 & 0 & 0 \\
     0 & 0 & -1 & 0 \\
     0 & 0 & 0 & -1
   \end{pmatrix} . \)
   Often times, $-g_{\mu \nu}$ is used instead; the sign convention
   here is chosen for convenience in advanced work.
   The inner (scalar) product between two four vectors $\fv{a}$ and
   $\fv{b}$ is a linear operation,
   \( \fv{a} \cdot \fv{b} = a^\mu b^\nu (\bv_\mu \cdot \bv_\nu), \)
   and hence can be completely specified by giving the 16 numbers
   $\bv_\mu \cdot \bv_\nu \defn g_{\mu \nu}$.
   Note that the inner product is commutative because the metric tensor
   is symmetric.
   Explicitly, the inner product of two four vectors is
   \( \fv{a} \cdot \fv{b} = a^0 b^0 - a^1 b^1 - a^2 b^2 - a^3 b^3. \)
   It can be shown that this quantity is a true scalar, i.e., it is
   frame independent, as it must be since $\tn{g}$ is a tensor.  The
   square root of the inner product of a four vector with itself is
   called its magnitude.
  \end{enumerate}

  \item \emph{The Lorentz Transformation.} In two dimensional
  Cartesian space, the transformation from one basis to another is
  accomplished via a simple rotation.  In four dimensional spacetime,
  the transformation from one basis to another is accomplished via
  the Lorentz transformation.  In order for a set of four quantities
  to qualify as a four vector, they must transform in the same way as
  the position four vector
  \( x^\mu = (ct,x,y,z) , \)
  that is, contravariantly under the Lorentz transformation.
  Representing the Lorentz transformation by a 4 by 4 matrix and
  denoting its components by $\Lambda^{\bar{\mu}}_\nu$, we have
  \( \label{eq:FVLT} v^{\bar{\mu}} = \Lambda^{\bar{\mu}}_\nu v^\nu . \)
  The bar indicates that $v^{\bar{\mu}}$ is a component of $\fv{v}$ in
  the primed basis.  This notation reduces clutter and serves to
  emphasize that the geometric object $\fv{v}$ remains unchanged;
  only the basis vectors (and hence the components of $\fv{v}$) change.

  By replacing $\vect{v}$ with $-\vect{v}$ everywhere, we obtain the
  inverse Lorentz transformation (note the bar on the lower index):
  \( \Lambda^\mu_{\bar{\nu}} (\vect{v})
   = \Lambda^{\bar{\mu}}_\nu (-\vect{v}). \)
  It is easy to show that
  \( \label{eq:LTprod} \Lambda^{\bar{\mu}}_\beta
   \Lambda^\alpha_{\bar{\mu}} = \delta^\alpha_\beta, \eqnsep
   \Lambda^\mu_{\bar{\beta}}
   \Lambda^{\bar{\alpha}}_\mu = \delta^{\bar{\alpha}}_{\bar{\beta}}, \)
  where $\delta^\alpha_\beta = \delta^{\bar{\alpha}}_{\bar{\beta}}$ is
  the Kronecker delta (technically a tensor).
  This is just to say that, as matrices, $(\Lambda^\mu_{\bar{\nu}})$ is
  the inverse of $(\Lambda^{\bar{\mu}}_\nu)$.

  By considering the expression $\fv{v} = \Lambda^{\bar{\mu}}_\nu v^\nu
  \bv_{\bar{\mu}} = v^\mu \bv_\mu$, it is easy to show that $\bv_\mu
  = \Lambda^{\bar{\nu}}_\mu \bv_{\bar{\nu}}$. Multiplying both sides by
  $\Lambda^\mu_{\bar{\alpha}}$ and using (\ref{eq:LTprod}) yields
  \( \bv_{\bar{\alpha}} = \Lambda^\beta_{\bar{\alpha}} \bv_\beta .\)
  As for the components of a one-form,
  \( a_{\bar{\mu}} = \ff{a}(\bv_{\bar{\mu}})
   = \ff{a}(\Lambda^\nu_{\bar{\mu}} \bv_\nu)
   = \Lambda^\nu_{\bar{\mu}} \ff{a}(\bv_\nu)
   = \Lambda^\nu_{\bar{\mu}} p_\nu . \)
  Note the use of the definition of the components of a one-form in the
  first and last equalities.  Evidently, the components of a one-form
  transform by the inverse Lorentz transformation.
  For basis one-forms we can show, in the manner that we did for basis
  vectors above, that
  \( \bf^{\bar{\mu}} = \Lambda^{\bar{\mu}}_\nu \bf^\nu .\)
  In general, each superscript index of a $\binom{M}{N}$, $M > 0$
  tensor transforms with one factor of the Lorentz matrix and each
  subscript index of a $\binom{M}{N}$, $N > 0$ tensor transforms with
  one factor of the inverse Lorentz matrix.  This does \emph{not}
  imply that the components of a $\binom{M}{M}$ tensor are invariant
  under the Lorentz transformation.

  We say that basis one-forms, the components of a vector, and any
  superscript index in general transform contravariantly (i.e., by
  the Lorentz transformation) while basis vectors, the components of
  a one-form, and any subscript index in general transform
  covariantly (i.e., by the inverse Lorentz transformation).  A true
  scalar is a quantity which is invariant under the Lorentz
  transformation.  It is an exercise in index gymnastics to show that
  any quantity (consisting of some collection of vector, one-form,
  and tensor components) with no free indices is a true scalar.

  \item \emph{The Lorentz Transformation Matrix.}
  Writing $x^{\bar{\mu}} = \Lambda^{\bar{\mu}}_\nu x^\nu$ and using
  (\ref{eq:LorentzBoost}) yields the components $\Lambda^{\bar{\mu}}_\nu$
  of the Lorentz transformation matrix for a boost from a frame $S$ to
  a frame $S'$ moving at speed $v$ in the $x$ direction relative to
  $S$:
  \begin{namedeqn}{\textbf{Lorentz Transformation Matrix}}
   (\Lambda^{\bar{\mu}}_\nu) = \begin{pmatrix}
    \gamma & -\gamma \beta & 0 & 0 \\
    -\gamma \beta & \gamma & 0 & 0 \\
    0 & 0 & 1 & 0 \\
    0 & 0 & 0 & 1
  \end{pmatrix} .
  \end{namedeqn}
  As usual, $\beta = v/c$ and $\gamma = (1-\beta^2)^{-1/2}$.  The
  inverse Lorentz transformation matrix is obtained by simply
  replacing $\beta$ with $-\beta$.

  With a little
  work (see CS 140 notes, p. 14), one can find the general Lorentz
  transformation matrix for a boost $\vect{v}$:
  \( \begin{split} (\Lambda^{\bar{\mu}}_\nu) &= \begin{pmatrix}
    \gamma & -\gamma v_x/c & -\gamma v_y/c & -\gamma v_z/c\\
    -\gamma v_x/c & 1+\frac{(\gamma-1)v_x^2}{v^2}
    & \frac{(\gamma-1)v_x v_y}{v^2} & \frac{(\gamma-1)v_x v_z}{v^2} \\
    -\gamma v_y/c & \frac{(\gamma-1)v_x v_y}{v^2}
    & 1+\frac{(\gamma-1)v_y^2}{v^2} & \frac{(\gamma-1)v_y v_z}{v^2} \\
    -\gamma v_z/c & \frac{(\gamma-1)v_x v_z}{v^2}
    & \frac{(\gamma-1)v_y v_z}{v^2} &
    1+\frac{(\gamma-1)v_z^2}{v^2}
  \end{pmatrix} \\ &= \begin{pmatrix}
   \gamma & -\gamma \vect{v}^T/c \\
   \gamma \vect{v}/c & I+\frac{\gamma - 1}{v^2} \vect{v} \vect{v}^T
  \end{pmatrix}. \end{split} \)
  Even more generally, a rotation of the three spatial axes can be
  added.  The resulting set of matrices form the Lorentz group.

  \item \emph{The Invariant Interval.}  A point in spacetime, i.e., a four vector, is
  called an \emph{event}. The trajectory (path) of a particle is called
  its world line. The displacement between two events
  $x^\mu_A$ and $x^\mu_B$ is simply $x^\mu_A - x^\mu_B$.  The
  magnitude of this displacement, that is, the length of the world line
  connecting the events, is the spacetime interval
  \( \label{eq:Interval} \Delta s^2 = (c \Delta t)^2 -
  \Delta x^2 - \Delta y^2 - \Delta x^2 .\)
  This is just the inner product of
  $x^\mu_A - x^\mu_B$ with itself.
  It is easy to show that the interval between any two events is
  invariant under Lorentz transformations.  Note that
  (\ref{eq:Interval}) is just the finite version of the differential
  distance (line element) for flat spacetime:
  \( ds^2 = dx_\mu dx^\mu = c^2 dt^2 - dx^2 - dy^2 - dz^2 . \)
  \begin{enumerate}[1.]
   \item If $\Delta s^2 < 0$, the interval is called spacelike.  In this
   case, there exists a frame in which the two events occur at the
   same time at different places.

   \item If $\Delta s^2 > 0$, the interval is called timelike.  In this
   case, there exists a frame in which the two events occur at the
   same place at different times. Particles with mass can only
   travel along timelike world lines.

   \item If $\Delta s^2 = 0$, the interval is called lightlike or null. In this
   case, there is no frame in which the two events occur at the
   same place and no frame in which they occur at the same time.
   In any frame, the events can be connected by a light signal
   because light always moves along lightlike world lines.
  \end{enumerate}

  \item \emph{Proper Time.} The time measured by a clock carried
  along a timelike world line is called the proper time $\tau$ (for that
  world line).  The proper time is related to the length of the
  world line by
  \( d\tau^2 = \frac{1}{c^2} ds^2 . \)
  To express the proper time in terms of the time measured for the
  world line in another frame $S$, we write
  \[ d\tau = \sqrt{ds^2}/c = \sqrt{dt^2 - (dx^2 + dy^2 +
  dz^2)/c^2} . \]
  Pulling $dt$ out of the root gives
  \( \label{eq:ProperTime} d\tau = \sqrt{1-u^2/c^2} dt , \)
  where $u$ is the velocity of the clock relative to $S$.  This
  expression can be integrated to give the proper time for a
  particle, given its trajectory $u(t)$ in some frame.  Note that
  (\ref{eq:ProperTime}) is just the differential version of the
  expression (\ref{eq:TimeDilation}) for time dilation.

  \item \emph{Differentiation in Flat Space.} We begin with the
  simplest possible derivative to introduce notation (note the
  subscript comma):
  \( \PD{x^\nu}{x^\mu} \defn \partial_\mu x^\nu \defn x^\nu_{,\mu} =
  \delta^\nu_\mu . \)
  This expression just says $\tPD{x}{x} = 1$, $\tPD{x}{y} = 0$, and
  so on.  Now, since $x^\nu = \Lambda^\nu_{\bar{\mu}} x^{\bar{\mu}}$,
  \( x^\nu_{,\bar{\mu}}
   = \PD{x^\nu}{x^{\bar{\mu}}} = \Lambda^\nu_{\bar{\mu}}. \)

  Consider a scalar field $\phi$ and the set of quantities
  \[ \phi_{,\mu} = \PD{\phi}{x^\mu} = \parenth{\PD{\phi}{(ct)},
   \PD{\phi}{x}, \PD{\phi}{y}, \PD{\phi}{z}}. \]
  We will show that $\phi_{,\mu}$ is a one-form, justifying the
  subscript comma notation.  By the chain rule,
  \[ \PD{\phi}{x'} = \PD{\phi}{(ct)}\PD{(ct)}{x'}
   + \PD{\phi}{x}\PD{x}{x'} + \PD{\phi}{y}\PD{y}{x'}
   + \PD{\phi}{z}\PD{z}{x'}. \]
  More generally,
  \( \PD{\phi}{x^{\bar{\mu}}} = \PD{\phi}{x^\nu}\PD{x^\nu}{x^{\bar{\mu}}}
  = \Lambda^\nu_{\bar{\mu}} \PD{\phi}{x^\nu}. \)
  So $\phi_{,\mu}$ transforms like a one-form, hence, it is a one-form.
  The operation $\partial_\mu$ is called covariant differentiation,
  since it converts a $\binom{M}{N}$ tensor to a $\binom{M}{N+1}$
  tensor.  We can similarly define contravariant differentiation:
  \( \PD{\phi}{x_\mu} \defn \partial^\mu \phi \defn \phi^{,\mu} \)
  which produces a $\binom{M+1}{N}$ tensor from a $\binom{M}{N}$
  tensor.
 \end{enumerate}

 \item \textbf{Relativistic Mechanics.}
 \begin{enumerate}[A.]
  \item \emph{Ordinary Velocity.} For a particle whose position (in $S$)
  is given by $\vect{r}$, the ordinary velocity
  (a three-vector) is defined as usual:
  \( \vect{u} \defn \D{\vect{r}}{t} . \)
  Note that $\vect{u}$ is not part of a four-vector - it
  transforms according to Einstein's velocity addition rule, not
  the Lorentz transformation.  We will always use $u$ for the velocity of
  a particle, reserving $v$ for the velocity of one frame relative
  to another.  Furthermore, we will use
  \( \gamma_u \defn (1-u^2/c^2)^{-1/2} \)
  to avoid confusion.

  \item \emph{Four-Velocity.} The proper four-velocity is given by
  \begin{namedeqn}{\textbf{Four-Velocity}}
   \eta^\mu \defn \D{x^\mu}{\tau} .
  \end{namedeqn}
  Of particular importance is the zeroth component:
  \( \label{eq:eta0} \eta^0 = c\D{t}{\tau} = c \gamma_u . \)
  The proper velocity three-vector (i.e., the three spatial parts
  of $\eta^\mu$) in terms of $\vect{u}$ is
  \( \vect{\eta} \defn (\eta^1, \eta^2, \eta^3) = \frac{1}{\sqrt{1-u^2/c^2}}
   \vect{u} = \gamma_u \vect{u} . \)
  In the rest frame of the particle, where $\vect{u} = 0$ and hence
  $\gamma_u = 1$, we see that
  \( \fv{\eta} = c \bv_0, \)
  or, in terms of components, $\fv{\eta} = (c,0,0,0)$.  It follows
  that the magnitude of $\fv{\eta}$ is just $c$ (in any frame).

  \item \emph{Momentum-Energy Four-Vector.}
  The momentum-energy four-vector (mom-energy or just momentum for
  short) for a particle of mass $m$ is given by
  \begin{namedeqn}{\textbf{Momentum Four-Vector}}
   p^\mu \defn m \eta^\mu .
  \end{namedeqn}
  The zeroth element of the momentum is interpreted as the
  relativistic energy:
  \begin{namedeqn}{\textbf{Relativistic Energy}}
   E \defn c p^0 = \gamma_u mc^2 .
  \end{namedeqn}
  At rest, $\gamma_u = 1$ so
  \begin{namedeqn}{\textbf{Rest Energy}}
   E = mc^2 .
  \end{namedeqn}
  This expression for the rest energy of a particle with mass $m$
  is the most famous equation in the world.  The difference between
  the relativistic energy and the
  rest energy is called the kinetic energy: $K = (\gamma_u - 1)mc^2$.
  This can be expanded as
  \( K = \frac{1}{2}mu^2 + \frac{3}{8} \frac{mu^4}{c^2} + \ldots . \)
  For $u \ll c$, we recover the classical expression for kinetic
  energy.

  The inner product of $\fv{p}$ with itself is equal to
  $m^2 c^2$.  The following useful relation follows immediately:
  \begin{namedeqn}{\textbf{Energy-Momentum Relation}}
  E^2 - p^2 c^2 = m^2 c^4 ,
  \end{namedeqn}
  where $p$ is the magnitude of the ordinary three-momentum.  For
  massless particles, such as the photon, this reduces to
  \( E = pc . \)

  We conclude with the following experimental result:
  \begin{quote}
  \emph{Relativistic energy and momentum (i.e., the net four-momentum)
  are conserved in every closed system.}
  \end{quote}

  \item \emph{Dynamics.}
  \begin{enumerate}[1.]
   \item \emph{Newton's Laws.} Newton's first law is built into
   relativity.  His second,
   \( \vect{F} = \D{\vect{p}}{t} , \)
   holds, provided we use the relativistic momentum $\vect{p} =
   m \vect{\eta} = \gamma_u m \vect{u}$.  The
   work-energy theorem holds as well:
   \[ W = \Delta E \]
   where
   \[ W \defn \int \dotpd{F}{dl} . \]  Newton's third law does not
   generally hold in relativity.

   \item \emph{How Force Transforms.} If a force $\vect{F}$ acts on a
   particle with velocity $\vect{u}$ in $S$, the force $\vect{F}'$ on
   the particle observed in $S'$ is given by
   \begin{subequations} \begin{namedalign}
   {\textbf{Transformation of Force}}
    F'_x &= \frac{F_x - \beta (\dotpd{u}{F})/c}{1-\beta u_x/c}, \\
    F'_y &= \frac{F_y}{\gamma (1-\beta u_x/c)}, \\
    F'_z &= \frac{F_z}{\gamma (1-\beta u_x/c)},
   \end{namedalign} \end{subequations}
   where $\beta = v/c$ and $\gamma = (1-\beta^2)^{-1/2}$.  In the
   case where the particle is instantaneously at rest in $S$ so
   $\vect{u} = 0$, these simplify to
   \( \vect{F}'_\bot = \frac{1}{\gamma} \vect{F}_\bot, \eqnsep
   F'_\| = F_\| . \)

   \item \emph{The Minkowski Force.}  The Minkowski force is
   defined as
   \begin{namedeqn}{\textbf{Minkowski Force}}
    K^\mu \defn \D{p^\mu}{\tau} .
   \end{namedeqn}
   The zeroth component gives the power delivered by the force:
   \( K^0 = \frac{1}{c} \D{E}{\tau} . \)

   Note that the normalization of the four-velocity requires that
   \( \label{eq:etaK} \eta_\mu K^\mu = 0 . \)
   Defining the four-acceleration as
   \( \alpha^\mu = \D{\eta^\mu}{\tau} , \)
   we have
   \( K^\mu = m \alpha^\mu . \)
   From (\ref{eq:etaK}), it is clear that $\eta_\mu \alpha^\mu = 0$.

   The spatial components of the Minkowski force are related to the
   ordinary force by
   \( \vect{K} = \frac{1}{\sqrt{1-u^2/c^2}} \vect{F} . \)
   As for the four-acceleration,
   \( \vect{\alpha} = \frac{1}{1-u^2/c^2} \bracket{\vect{a} +
    \frac{(\dotpd{u}{a})\vect{u}}{c^2 - u^2}}  \)
   and
   \( \alpha^0 = \frac{\dotpd{u}{a}}{c (1 - u^2/c^2)^2} . \)
  \end{enumerate}

 \end{enumerate}

\end{enumerate}

\end{document}
