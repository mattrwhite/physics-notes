% ----------------------------------------------------------------
% Special Relativity *********************************************
% ----------------------------------------------------------------
\documentclass{article}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{booktabs}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
\reversemarginpar
% MATH -----------------------------------------------------------
\input{D:/Physics/Notes/Include/Essentials.tex}
\input{D:/Physics/Notes/Include/Vectors.tex}
% ----------------------------------------------------------------
%\draftmode
\begin{document}
% \maketitle
% ----------------------------------------------------------------
\begin{enumerate}[I.]
 \item \textbf{Einstein's Postulates and the Lorentz Boost.}
 \begin{enumerate}[A.]
  \item \emph{Einstein's Postulates.}  The foundation of special
  relativity is the following postulate, due to Poincare:
  \begin{quote}
  \emph{The laws of physics are the same in all inertial reference
  frames.}
  \end{quote}
  This implies that the speed of light, determined by Maxwell's
  equations which make no reference to any particular frame, is
  the same for all observers.  Einstein formulated this as the second
  postulate of special relativity:
  \begin{quote}
  \emph{The speed of light in empty space is independent of the motion
   of the its source.}
  \end{quote}

  As shown below, no \emph{particle} can travel faster than (the
  speed of) light.  We postulate further that
  \begin{quote}
  \emph{Nothing - no information - can travel faster than light.}
  \end{quote}

  \item \emph{The Lorentz Transformation.}  Consider an inertial
  frame (henceforth a frame) $S'$ moving at speed $v$ in the $+x$
  direction relative to a frame $S$.  Without loss of generality,
  assume that the frames coincided at $t'=t=0$.  Then classically,
  the frames are related by the Galilean transformation:
  \begin{equation} \begin{split}
   x' &= x - vt, \\
   y' &= y, \\
   z' &= z, \\
   t' &= t.
  \end{split} \end{equation}
  By starting with the most general linear relation between the
  coordinates of the frames and requiring that something with
  speed $c$ in one frame have the same speed in any other frame,
  we arrive at the Lorentz boost (often called the Lorentz
  transformation although it is only a specific case of the actual
  Lorentz transformation, discussed below):
  \begin{subequations} \label{eq:LorentzBoost}
  \begin{namedalign}{\textbf{The Lorentz Boost}}
   x' &= \gamma(x - vt), \\
   y' &= y, \\
   z' &= z, \\
   t' &= \gamma(t-\frac{v}{c^2}x),
  \end{namedalign}
  \end{subequations}
  where
  \begin{equation}
   \gamma \defn \frac{1}{\sqrt{1-v^2/c^2}} .
  \end{equation}
  We often make the following definition as well:
  \( \beta \defn \frac{v}{c} \)
  The inverse transformation from $S'$ to $S$ can be obtained
  by taking $v \To -v$ in (\ref{eq:LorentzBoost}) (and swapping the
  primes).

 \end{enumerate}

 \item \textbf{Consequences of Special Relativity.}
 \begin{enumerate}[A.]

  \item \emph{Simultaneity and Time Dilation.} Two events (i.e., points
  in spacetime) which are
  simultaneous in one frame are in general not simultaneous in others.
  Let $A$ and $B$ be events given in $S$ by $x_A = 0$, $t_A = 0$ and
  $x_B = b$, $t_B = 0$.  Although these events are simultaneous in
  $S$, the Lorentz transformation shows that they are not
  simultaneous in $S'$: $x'_A = 0$, $t'_A = 0$ and $x'_B = \gamma b$,
  $t'_B = -\gamma (v/c^2) b$.  In $S'$, $B$ occurs before $A$.
  Note that if two events occur at the same place at the same
  time in one frame, they will appear as such in all frames.

  If we consider a time interval $\Delta t$ is $S$, we find that
  \( \label{eq:TimeDilation} \Delta t' = \frac{1}{\gamma} \Delta t. \)
  That is, an observer in $S$ will notice that the clocks in $S'$
  run slower by a factor of $1/\gamma$.  This effect is called
  time dilation.  The equivalence of inertial frames requires that to observers
  in $S'$, the clocks in $S$ appear to be running slower by exactly
  the same factor!

  \item \emph{The Lorentz Contraction.} Suppose an observer in $S$
  wishes to measure the length of a stick (at rest) in $S'$. In
  $S'$, the rest frame of the stick, its length is $\Delta x' =
  x'_r - x'_l$.  The observer in $S$ must take the positions of
  ends of the stick at the same instant in his frame. (Note
  that these measurements will not appear simultaneous to an
  observer in $S'$.)  Doing
  so, he finds that the length of the stick is shorter:
  \begin{equation}
   \Delta x = \frac{1}{\gamma} \Delta x'.
  \end{equation}
  This effect is called the Lorentz (length) contraction.  Only
  the direction along the direction of motion is contracted -
  perpendicular directions (in this case, $y$ and $z$) are
  unaffected.
  \item \emph{Transformation of Angles.}
  \begin{enumerate}[1.]
   \item Consider a stick of length $l_0$ at rest in $S'$ which makes an
   angle $\theta_0$ with the $x'$ axis.  Suppose $S'$ is moving at
   $v$ along $x$ in $S$.  Then the length of the stick as seen in
   $S$ is
   \( l = l_0 \sqrt{1-\beta^2 \cos^2 \theta_0} . \)
   The angle of the stick with the $x$ axis, $\theta$, is given by
   \( \tan \theta = \gamma \tan \theta_0 . \)
   \item If instead a light ray is fired off at an angle
   $\theta_0$ to the $x'$ axis by a source stationary in $S'$, the
   angle $\theta$ with the $x$ axis observed in $S$ is given by
   \( \cos \theta = \frac{\cos \theta_0 + \beta}{1 + \beta \cos \theta_0} . \)
  \end{enumerate}


  \item \emph{Addition of Velocities.}  Consider a particle moving
  at
  \[ u = \D{x}{t} \]
  in the $x$ direction in $S$.  For $S'$, the Lorentz
  transformation gives
  \[ dx' = \gamma (dx-v dt), \quad dt' = \gamma (dt - (v/c^2)dx). \]
  The velocity of the particle in $S'$ is then
  \[  u' = \D{x'}{t'} = \frac{u - v}{1 - uv/c^2}. \]

  The general transformation of velocity is
  \begin{subequations} \begin{namedalign}
  {\textbf{Einstein's Velocity Addition Rule}}
  u'_x &= \frac{u_x - v}{1 - v u_x/c^2}, \\
  u'_y &= \frac{u_y}{\gamma(1 - v u_x/c^2)}, \\
  u'_z &= \frac{u_z}{\gamma(1 - v u_x/c^2)}.
  \end{namedalign} \end{subequations}

  In more transparent notation, if $A$ is moving at $v_{AB}$ relative to $B$ and
  $B$ is moving at $v_{BC}$ relative to $C$, then the velocity of
  $A$ relative to $C$ is
  \begin{equation}
   v_{AC} = \frac{v_{AB} + v_{BC}}{1 +
   v_{AB}v_{BC}/c^2}.
  \end{equation}
  The most important implication of the velocity addition formula
  is that no particle can move faster than light.

  \item \emph{Relativistic Doppler Effect.}  It is easy to show
  (see Marion 14.5) that when a source of light at frequency $\omega_0$
  is moving relative
  to an observer at speed $v$ (positive if the source and observer are
  approaching each other, negative if they are receding), the
  frequency of light $\omega$ seen by observer is altered according to
  \begin{namedeqn}{\textbf{Relativistic Doppler Shift}}
   \omega = \sqrt{\frac{1+v/c}{1-v/c}}\omega_0 .
  \end{namedeqn}
  If the source and observer are receding, the frequency is
  decreased and the light is said to be redshifted.  If the two
  are approaching so the frequency is increased, the light is said
  to be blueshifted.

  Consider now the case of a stationary observer and a light
  source travelling in some arbitrary direction at velocity $\vect{v}$.
  If the angle between an observed light ray and $\vect{v}$ is
  $\theta$, then the frequency shift of the light is given by
  \( \omega = \frac{\sqrt{1-v^2/c^2}}{1-(v/c)\cos \theta} \omega_0 . \)

  \item \emph{Rapidity.} Many of the results of special relativity
  take an especially simple form when expressed in terms of the
  rapidity
  \begin{namedeqn}{\textbf{Rapidity}}
   \theta \defn \tanh^{-1} (v/c).
  \end{namedeqn}
  Most notably, rapidities add:
  \( \theta_{AC} = \theta_{AB} + \theta_{BC} . \)
  The Lorentz boost expressed in terms of rapidity is
  \begin{equation} \begin{split}
   x' &= (-\sinh \theta)ct + (\cosh \theta)x, \\
   y' &= y, \\
   z' &= z, \\
   ct' &= (\cosh \theta)ct - (\sinh \theta)x.
  \end{split} \end{equation}

  \item \emph{Famous Paradoxes of Special Relativity.}
  \begin{enumerate}[1.]
   \item \emph{The Twin Paradox.}

   \item \emph{The Barn and Ladder Paradox.}
  \end{enumerate}

 \end{enumerate}

 \item \textbf{The Structure of Spacetime.}
 \begin{enumerate}[A.]
  \item \emph{Lorentz Space.} Spacetime is represented by a real,
  linear, four dimensional Riemannian space.  Contravariant
  vectors in this space, position for example, are written as
  $a^\mu$.  The covariant (dual) vector corresponding to $a^\mu$ is
  given by
  \( a_\mu \defn g_{\mu \nu} a^\nu , \)
  where $g_{\mu \nu}$ is the (Riemannian) metric tensor of the
  space.  In the above relation, we have used the Einstein
  summation notation, in which summation over repeated indexes is
  implied.  If the index is a greek letter, the summation is
  taken from $0$ to $3$; if the index is an arabic letter, the
  summation is taken from $1$ to $3$.

  The inner product between two
  vectors $a^\mu$ and $b^\mu$ in our space is defined as
  \( \langle a^\mu , b^\mu \rangle \defn a_\mu b^\mu  = g_{\mu
  \nu} a^\nu b^\mu . \)
  The inner product is commutative because the metric tensor is
  symmetric, i.e., $g_{\mu \nu} = g_{\nu \mu}$.

  \item \emph{The Metric of Flat Spacetime.}
  The metric tensor of flat spacetime is
  \( g_{\mu \nu} = \begin{pmatrix}
    -1 & 0 & 0 & 0 \\
    0 & 1 & 0 & 0 \\
    0 & 0 & 1 & 0 \\
    0 & 0 & 0 & 1
  \end{pmatrix} . \)
  With this metric, we can switch between contravariant and covariant
  vectors by simply changing the sign of the zeroth component.
  This gives the following inner product:
  \( a_\mu b^\mu = -a^0 b^0 + a^1 b^1 + a^2 b^2 + a^3 b^3 . \)


  \item \emph{The Lorentz Transformation Matrix.} In four
  dimensional spacetime, the Lorentz transformation from a frame
  $S$ to a frame $S'$ moving at speed $v$ in the $x$ direction relative
  to $S$ is
  represented by the following matrix (a mixed tensor of rank two):
  \begin{namedeqn}{\textbf{Lorentz Transformation Matrix}}
   \Lambda^\mu_\nu = \begin{pmatrix}
    \gamma & -\gamma \beta & 0 & 0 \\
    -\gamma \beta & \gamma & 0 & 0 \\
    0 & 0 & 1 & 0 \\
    0 & 0 & 0 & 1
  \end{pmatrix} .
  \end{namedeqn}
  As usual, $\beta = v/c$ and $\gamma = (1-\beta^2)^{-1/2}$.  Note
  that it is hardly ever necessary to use the (most general) Lorentz
  transformation for motion of $S'$ in an arbitrary direction
  relative to $S$.

  We define the position four-vector in spacetime as
  \begin{namedeqn}{\textbf{Four-Position}} \label{eq:FourPos}
   x^\mu = (ct,x,y,z).
  \end{namedeqn}
  We then define a four-vector as any set of four quantities which
  transforms (experimentally) under $\Lambda^\mu_\nu$ in the same manner as the
  position four-vector.  Hence given the value of a four-vector
  $a^\mu$ in $S$, its value in $S'$ is
  \( a'^\mu = \Lambda^\mu_\nu a^\nu . \)
  Written out in components,
  \begin{subequations} \begin{align}
   a'^0 &= \gamma(a^0 - \beta a^1), \\
   a'^1 &= \gamma(a^1 - \beta a^0), \\
   a'^2 &= a^2, \\
   a'^3 &= a^3.
  \end{align} \end{subequations}
  A (contravariant rank two) tensor transforms according to
  \( T'^{\mu \nu} = \Lambda^\mu_\lambda \Lambda^\nu_\sigma
   T^{\lambda \sigma} . \)

  \item \emph{The Invariant Interval.}  A point in spacetime is
  called an \emph{event}. The trajectory (path) of a particle is called
  its \emph{world line}. The displacement between two events
  $x^\mu_A$ and $x^\mu_B$ is simply $x^\mu_A - x^\mu_B$.  The
  magnitude of this displacement, that is, the length of the world line
  connecting the events, is the spacetime interval
  \( \label{eq:Interval} \Delta s^2 = -(c \Delta t)^2 +
  \Delta x^2 + \Delta y^2 + \Delta x^2 .\)
  This is just the inner product of
  $x^\mu_A - x^\mu_B$ with itself.
  It is easy to show that the interval between any two events is
  invariant under Lorentz transformations.  Note that
  (\ref{eq:Interval}) is just the finite version of the differential
  distance (line element) for flat spacetime:
  \( ds^2 = dx_\mu dx^\mu = -c^2 dt^2 + dx^2 + dy^2 + dz^2 . \)
  \begin{enumerate}[1.]
   \item If $\Delta s^2 > 0$, the interval is called spacelike.  In this
   case, there exists a frame in which the two events occur at the
   same time at different places.

   \item If $\Delta s^2 < 0$, the interval is called timelike.  In this
   case, there exists a frame in which the two events occur at the
   same place at different times. Particles with mass can only
   travel along timelike world lines.

   \item If $\Delta s^2 = 0$, the interval is called lightlike or null. In this
   case, there is no frame in which the two events occur at the
   same place and no frame in which they occur at the same time.
   In any frame, the events can be connected by a light signal
   because light always moves along lightlike world lines.
  \end{enumerate}

  \item \emph{Proper Time.} The time measured by a clock carried
  along a timelike world line is called the proper time $\tau$ (for that
  world line).  The proper time is related to the length of the
  world line by
  \( d\tau^2 = -\frac{1}{c^2} ds^2 . \)
  To express the proper time in terms of the time measured for the
  world line in another frame $S$, we write
  \[ d\tau = \sqrt{-ds^2}/c = \sqrt{dt^2 - (dx^2 + dy^2 +
  dz^2)/c^2} . \]
  Pulling $dt$ out of the root gives
  \( \label{eq:ProperTime} d\tau = \sqrt{1-u^2/c^2} dt , \)
  where $u$ is the velocity of the clock relative to $S$.  This
  expression can be integrated to give the proper time for a
  particle, given its trajectory $u(t)$ in some frame.  Note that
  (\ref{eq:ProperTime}) is just the differential version of the
  expression (\ref{eq:TimeDilation}) for time dilation.
 \end{enumerate}

 \item \textbf{Relativistic Mechanics.}
 \begin{enumerate}[A.]
  \item \emph{Ordinary Velocity.} For a particle whose position (in $S$)
  is given by $\vect{r}$, the ordinary velocity
  (a three-vector) is defined as usual:
  \( \vect{u} \defn \D{\vect{r}}{t} . \)
  Note that $\vect{u}$ is not part of a four-vector - it
  transforms according to Einstein's velocity addition rule, not
  the Lorentz transformation.  We will always use $u$ for the velocity of
  a particle, reserving $v$ for the velocity of one frame relative
  to another.  Furthermore, we will use
  \( \gamma_u \defn (1-u^2/c^2)^{-1/2} \)
  to avoid confusion.

  \item \emph{Four-Velocity.} The proper four-velocity is given by
  \begin{namedeqn}{\textbf{Four-Velocity}}
   \eta^\mu \defn \D{x^\mu}{\tau} .
  \end{namedeqn}
  Of particular importance is the zeroth component:
  \( \label{eq:eta0} \eta^0 = c\D{t}{\tau} = c \gamma_u . \)
  The proper velocity three-vector (i.e., the three spatial parts
  of $\eta^\mu$) in terms of $\vect{u}$ is
  \( \vect{\eta} \defn (\eta^1, \eta^2, \eta^3) = \frac{1}{\sqrt{1-u^2/c^2}}
   \vect{u} = \gamma_u \vect{u} . \)
  The scalar product of the four-velocity with itself is always $-c^2$ (we say that the
  four-velocity is normalized), as we can easily show:
  \begin{align}
   \eta_\mu \eta^\mu &= -c^2 \parenth{\D{t}{\tau}}^2 +
   \norm{\vect{u}}^2 \parenth{\D{t}{\tau}}^2
   = -c^2 \parenth{\D{t}{\tau}}^2 \parenth{1-\frac{u^2}{c^2}} \notag \\
   &= -c^2 .
  \end{align}
  The last equality follows from (\ref{eq:eta0}) and the definition
  of $\gamma_u$.

  \item \emph{Momentum-Energy Four-Vector.}
  The momentum-energy four-vector (mom-energy or just momentum for
  short) for a particle of mass $m$ is given by
  \begin{namedeqn}{\textbf{Momentum Four-Vector}}
   p^\mu \defn m \eta^\mu .
  \end{namedeqn}
  The zeroth element of the momentum is interpreted as the
  relativistic energy:
  \begin{namedeqn}{\textbf{Relativistic Energy}}
   E \defn c p^0 = \gamma_u mc^2 .
  \end{namedeqn}
  At rest, $\gamma_u = 1$ so
  \begin{namedeqn}{\textbf{Rest Energy}}
   E = mc^2 .
  \end{namedeqn}
  This expression for the rest energy of a particle with mass $m$
  is the most famous equation in the world.  When $\gamma_u
  \neq 1$, the difference between the relativistic energy and the
  rest energy is called the kinetic energy: $K = (\gamma_u - 1)mc^2$.
  This can be expanded as
  \( K = \frac{1}{2}mu^2 + \frac{3}{8} \frac{mu^4}{c^2} + \ldots . \)
  For $u \ll c$, we recover the classical expression for kinetic
  energy.

  The scalar product of $p^\mu$ with itself is invariant and equal to
  $-m^2 c^2$.  The following useful relation follows immediately:
  \begin{namedeqn}{\textbf{Energy - Momentum Relation}}
  E^2 - p^2 c^2 = m^2 c^4 ,
  \end{namedeqn}
  where $p$ is the magnitude of the ordinary three-momentum.  For the
  massless photon, this reduces to
  \( E = pc . \)

  We conclude with the following experimental result:
  \begin{quote}
  \emph{Relativistic energy and momentum are conserved in every closed
  system.}
  \end{quote}

  \item \emph{Dynamics.}
  \begin{enumerate}[1.]
   \item \emph{Newton's Laws.} Newton's first law is built into
   relativity.  His second,
   \( \vect{F} = \D{\vect{p}}{t} , \)
   holds, provided we use the relativistic momentum $\vect{p} =
   m \vect{\eta} = \gamma_u m \vect{u}$.  The
   work-energy theorem holds as well:
   \[ W = \Delta E \]
   where
   \[ W \defn \int \dotpd{F}{dl} . \]  Newton's third law does not
   generally hold in relativity.

   \item \emph{How Force Transforms.} If a force $\vect{F}$ acts on a
   particle with velocity $\vect{u}$ in $S$, the force $\vect{F}'$ on
   the particle observed in $S'$ is given by
   \begin{subequations} \begin{namedalign}
   {\textbf{Transformation of Force}}
    F'_x &= \frac{F_x - \beta (\dotpd{u}{F})/c}{1-\beta u_x/c}, \\
    F'_y &= \frac{F_y}{\gamma (1-\beta u_x/c)}, \\
    F'_z &= \frac{F_z}{\gamma (1-\beta u_x/c)},
   \end{namedalign} \end{subequations}
   where $\beta = v/c$ and $\gamma = (1-\beta^2)^{-1/2}$.  In the
   case where the particle is instantaneously at rest in $S$ so
   $\vect{u} = 0$, these simplify to
   \( \vect{F}'_\bot = \frac{1}{\gamma} \vect{F}_\bot, \eqnsep
   F'_\| = F_\| . \)

   \item \emph{The Minkowski Force.}  The Minkowski force is
   defined as
   \begin{namedeqn}{\textbf{Minkowski Force}}
    K^\mu \defn \D{p^\mu}{\tau} .
   \end{namedeqn}
   The zeroth component gives the power delivered by the force:
   \( K^0 = \frac{1}{c} \D{E}{\tau} . \)

   Note that the normalization of the four-velocity requires that
   \( \label{eq:etaK} \eta_\mu K^\mu = 0 . \)
   Defining the four-acceleration as
   \( \alpha^\mu = \D{\eta^\mu}{\tau} , \)
   we have
   \( K^\mu = m \alpha^\mu . \)
   From (\ref{eq:etaK}), it is clear that $\eta_\mu \alpha^\mu = 0$.

   The spatial components of the Minkowski force are related to the
   ordinary force by
   \( \vect{K} = \frac{1}{\sqrt{1-u^2/c^2}} \vect{F} . \)
   As for the four-acceleration,
   \( \vect{\alpha} = \frac{1}{1-u^2/c^2} \bracket{\vect{a} +
    \frac{(\dotpd{u}{a})\vect{u}}{c^2 - u^2}}  \)
   and
   \( \alpha^0 = \frac{\dotpd{u}{a}}{c (1 - u^2/c^2)^2} . \)
  \end{enumerate}

 \end{enumerate}

\end{enumerate}

\end{document}
