% ----------------------------------------------------------------
% Chemical Thermodynamics ***************************************
% ----------------------------------------------------------------
\documentclass{article}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{booktabs}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
\reversemarginpar
% MATH -----------------------------------------------------------
\input{../Include/Math.tex}
\input{../Include/Layout.tex}
\input{../Include/Thermo.tex}
% ----------------------------------------------------------------
%\draftmode
\begin{document}
% \maketitle
% ----------------------------------------------------------------
\begin{enumerate}[I.]
 \item \textbf{Thermochemistry.}
 \begin{enumerate}[A.]
  \item \emph{Standard Enthalpy Change.}  At constant pressure,
  an exothermic (heat-releasing)
  process is one for which $\Delta H < 0$ while an endothermic
  (heat-absorbing) process is one for which $\Delta H > 0$.  The
  standard enthalpy change, $\Delta \Std{H}$ for a process is the
  change in enthalpy when the initial and final substances are in
  their standard states, i.e., in pure form at one bar.  Countless
  physical and chemical processes, such as phase changes, reactions,
  and ionization, have standard enthalpy changes associated with
  them; these are often tabulated in reference tables.

  The word standard and the symbol $\Std{}$ mean, if nothing else,
  that the value is given for a pressure of 1 bar.

  \item \emph{Enthalpy of Reaction.} The standard enthalpy change for
  a reaction, called the standard reaction enthalpy
  $\Delt{r}\Std{H}$, refers to the process which takes the reactants
  in their \emph{unmixed} standard state to the products in their
  \emph{unmixed} standard state.  Fortunately, the enthalpy changes
  associated with mixing and separation are usually insignificant
  compared to the contribution from the reaction itself.

  The standard reaction enthalpy is defined as the sum of the
  standard molar enthalpies $\Std{h}$ of each reactant and product,
  multiplied by the stoichiometric numbers $\nu_j$ (negative for
  reactants, positive for products):
  \( \Delt{r}\Std{H} \defn \sum_j \nu_j \Std{h_j} . \)

  A result of the first law of thermodynamics, Hess's law states
  that the standard
  enthalpy of an overall reaction is the sum of the standard
  enthalpies of the individual reactions into which it may be
  divided.  See Atkins example 2.5.

  \item \emph{Enthalpy of Formation.} The standard enthalpy for
  the formation of a compound from its elements in their reference
  states is denoted $\Delt{f}\Std{H}$.  The reference state of an
  element at a specified temperature is its most stable state at one
  bar and that temperature.  Hess's law allows to write the standard
  enthalpy of a
  reaction in terms of the enthalpies of formation of the reactants
  and products (which can be found in reference tables):
  \( \label{eq:HForm} \Delt{r}\Std{H} = \sum_j \nu_j \Delt{f}\Std{H_j} . \)

  \item \emph{Reaction Enthalpy vs.\ Temperature.}  The dependence of
  $\Delt{r}\Std{H}$ on temperature is given by Kirchhoff's law:
  \( \Delt{r}\Std{H}(T_2) = \Delt{r}\Std{H}(T_1) + \int_{T_1}^{T_2}
  \Delt{r}\Std{C_p} dT , \)
  where
  \( \Delt{r}\Std{C_p} = \sum_j \nu_j \Std{c_{p,j}} .\)
  Here, $\Std{c_{p,j}}$ is just the standard molar heat capacity of
  substance $j$.

 \end{enumerate}

 \item \textbf{Mixtures.}
 \begin{enumerate}[A.]
  \item \emph{Partial Molar Quantities.}  When dealing with mixtures,
  we use $n$ to denote the total number of moles of all substances in
  the mixture, $n_j$ to denote the number of moles of substance $j$,
  and $n'$ to denote the amount of all the other components the mixture
  besides $n_j$.

  The mole fraction of a substance $j$ in a mixture is
  \( x_j \defn n_j/n . \)

  The molar concentration, moles per unit volume, also called molarity, is denoted with brackets:
  \begin{namedeqn}{\textbf{Molarity}}
    [j] \defn n_j/V .
  \end{namedeqn}
  The usual unit for molarity is moles per liter, called molar: 1 mol/L $\defn$ 1 molar $\defn$ 1M.  As molarity measures the number density of a substance, it is the quantity usually found in expressions for reaction rates. Pure water at STP is about 55M.  One molecule per cubic nanometer corresponds to 1.66M.

  Moles of solute per unit mass of solvent is called molality: $n_j/m_\textrm{solvent}$.

  The partial pressure of substance $j$ in a mixture (of gases) is
  \( p_j \defn x_j p . \)
  Dalton's law states that the pressure exerted by a mixture of ideal gases
  is equal to the sum of the partial pressures of the individual
  gases.

  We define the partial molar volume of a substance $j$ in a mixture
  as
  \( v_j \defn \TD{V}{n_j}{p,T,n'} . \)
  Like all partial molar quantities, this is defined for constant $T$
  and $p$.

  \item \emph{The Chemical Potential.}
  The partial molar Gibbs energy of a substance in a mixture is
  called its chemical potential:
  \begin{namedeqn}{\textbf{Chemical Potential}}
   \mu_j \defn \TD{G}{n_j}{p,T,n'} .
  \end{namedeqn}
  The name ``chemical potential'' is fitting because the Gibbs free
  energy is the maximum non-expansion work a system can do, so $\mu$
  is a measure of a system's ``potential'' to do work.

  A rigorous treatment of the chemical potential and its relation to
  $E$, $S$, $H$, and $A$ can found in \farref{sec:Gibbs.ThermoMulti}.
  It is shown there that
  the total Gibbs energy of a mixture is $G = n_\R{A} \mu_\R{A}
  + n_\R{B} \mu_\R{B} + \ldots$.  By comparing the differential of
  this expression to the fundamental equation of chemical
  thermodynamics (also derived in \farref{sec:Gibbs.ThermoMulti}):
  \( \label{eq:FECH} dG = Vdp - S dT + \mu_\R{A} dn_\R{A}
   + \mu_\R{B} dn_\R{B} + \ldots , \)
  we obtain the Gibbs--Duhem equation, which tells us
  that the chemical potential of one component of a mixture cannot
  change independently of the others:
  \begin{namedeqn}{\textbf{Gibbs--Duhem Equation}}
   \sum_j n_j d\mu_j = V dp - S dT .
  \end{namedeqn}
  Of course, this must be equal to zero at constant pressure and
  temperature.

  \item \emph{Thermodynamics of Gas Mixtures.}  The Gibbs energy of
  mixing for two perfect gases $\R{A}$ and $\R{B}$ is (see Atkins  7.2(a)):
  \begin{align}
  \Delt{mix}G &= n_\R{A} RT \ln \frac{p_\R{A}}{p} + n_\R{B} RT \ln
   \frac{p_\R{B}}{p} \notag \\
  &= nRT (x_\R{A} \ln x_\R{A} + x_\R{B} \ln x_\R{B}) ,
  \end{align}
  where the second equality follows from Dalton's law.
  For non-perfect gases, we need only replace $p_\R{A}$ and $p_\R{B}$ with the
  fugacities $f_\R{A}$ and $f_\R{B}$.  Since mole fractions are of course less than one, the $ln(x)$ factors are negative and $\Delt{mix}G < 0$, so perfect gases always mix spontaneously.

  Using $(\partial G / \partial T)_{p,n}
  = -S$ yields the entropy of mixing for perfect gases:
  \( \Delt{mix}S = -n R (x_\R{A} \ln x_\R{A} + x_\R{B} \ln x_\R{B}) . \)
  For perfect gases, the enthalpy of mixing $\Delt{mix}H$ is zero.

  \item \emph{The Activity.}   When discussing mixtures, we often
  address deviations from ideality
  by replacing $x_j$, or some other measure of concentration such as
  molarity, with the activity $a_j$.  By definition, the chemical
  potential of a substance $j$ in a mixture is related to the chemical
  potential $\Pure{\mu}_j$ of $j$ in pure form by
  \begin{namedeqn}{\textbf{Activity}} \label{eq:Activity}
   \mu_j = \Pure{\mu}_j + RT \ln a_j .
  \end{namedeqn}
  In general, $a_j \To x_j$ as $x_j \To 1$, so the activity of a pure
  substance is one. We often write the activity as $a_j = \gamma_j x_j$,
  where $\gamma_j = \gamma_j(x_j)$ is called the activity coefficient
  (which depends on the measure of concentration used).  The activity
  is usually considered a purely empirical quantity.  For a gas, $a_j =
  f_j/\Std{p}$.

  \item \emph{Liquids.}
  \begin{enumerate}[1.]
   \item \emph{Ideal Solutions.}  An ideal solution is one which
   obeys Raoult's law:
   \( p_\R{A} = x_\R{A} \Pure{p_\R{A}}, \)
   where $p_\R{A}$ is the vapor pressure of component $\R{A}$ for the
   mixture, $x_\R{A}$ is the mole fraction of $\R{A}$ in the mixture, and
   $\Pure{p_\R{A}}$ is the vapor pressure of pure $\R{A}$.  At
   equilibrium, the chemical potential of $\R{A}$ in solution must be
   equal to the chemical potential of $\R{A}$ in vapor form, so (see
   Atkins 7.3):
   \( \label{eq:IdealSoln} \mu_\R{A} = \Pure{\mu_\R{A}}
    + RT \ln x_\R{A} \)
   relates the chemical potential of $\R{A}$ in an ideal solution to the
   chemical potential of pure $\R{A}$, $\Pure{\mu_\R{A}}$.

   For dilute solutions, the solute often does not obey Raoult's law.
   In this case we can replace $\Pure{p_\R{A}}$ with an empirical
   constant $K_\R{A}$ to yield Henry's law.

   \item \emph{Liquid Mixtures.}  The Gibbs energy, entropy, and
   enthalpy of mixing for ideal solutions is the same as for ideal
   gases since we assume that the vapors are ideal.

   \item \emph{Activities.}  In order to account for deviations of
   solutions from ideality, we can replace $x_\R{A}$ in
   (\ref{eq:IdealSoln}) by an activity. The activity $a_\R{A}$ of a
   solvent $\R{A}$ is defined by the relation
   \( \mu_\R{A} = \Pure{\mu_\R{A}} + RT \ln a_\R{A} . \)
   Here, the activity is simply the ratio
   \( a_\R{A} = p_\R{A} / \Pure{p_\R{A}} \)
   of the vapor pressures for the solvent with and without any
   solute.

   For a solute $\R{B}$ which obeys Henry's law, the chemical potential
   is given by
   \( \mu_\R{B} = \Pure{\mu_\R{B}}
   + RT \ln \parenth{\frac{K_\R{B}}{\Pure{p_\R{B}}}}
   + RT \ln x_\R{B} . \)
   Deviations from ideality are addressed by replacing $x_\R{B}$ with the
   solute activity
   \( a_\R{B} = p_\R{B} / K_\R{B} . \)
  \end{enumerate}

  \item \emph{Colligative Properties.}  Colligative properties refer
  to the properties of (dilute) solutions due to the present of
  a solute, regardless of its identity.  These properties arise from
  the reduction in the chemical potential of the solvent ($\R{A}$) by $RT \ln
  x_\R{A}$.

  The presence of a solute $\R{B}$ in mole fraction $x_\R{B}$ increases the
  boiling point of the solution by
  \( \Delta T_b = \frac{RT_b^2}{\Delt{vap}H} x_\R{B} , \)
  where $T_b$ is the normal boiling point and $\Delt{vap}H$ is the
  enthalpy (heat) of vaporization (see Atkins Justification 7.2).
  Furthermore, the freezing point of the solution is lowered by
  \( \Delta T_f = \frac{RT_f^2}{\Delt{fus}H} x_\R{B} , \)
  where $T_f$ is the normal freezing point and $\Delt{fus}H$ is the
  enthalpy (heat) of fusion.  The solubility at temperature $T$ of a
  substance $\R{B}$ with freezing (melting) point $T_f$ and enthalpy of
  fusion $\Delt{fus}H$ is given by
  \( \ln x_\R{B} = -\frac{\Delt{fus}H}{R} \parenth{\frac{1}{T} -
  \frac{1}{T_f}} . \)
  The fact that this expression is independent of the solvent should
  make it clear that it is only an approximate result.

  \item \emph{Osmosis.}  Osmosis, a colligative property, is the
  spontaneous passage of a pure solvent through a semipermeable
  membrane into a solution.  The osmotic pressure, the pressure that
  must be applied to the solution to stop the influx of the solvent,
  is given by (see Atkins Justification 7.3):
  \begin{namedeqn}{\textbf{Osmotic Pressure}}
   \Pi = [\R{B}]RT,
  \end{namedeqn}
  where $[\R{B}] = n_\R{B}/V$ is the molarity of the solute.
  Deviations from ideality are addressed by expanding this expression
  in powers of $[\R{B}]$.
 \end{enumerate}

 \item \textbf{Chemical Equilibrium.}
 \begin{enumerate}[A.]
  \item \emph{Extent of Reaction.}  The extent of reaction
  $\Extent$, which has units of moles, is defined such that for a
  change $d\Extent$, the change in the amount of substance $j$ in
  a reaction mixture is $dn_j = \nu_j d\Extent$. Here, $\nu_j$ is just
  the stoichiometric number for substance $j$ (positive for products,
  negative for reactants).

  For example, when
  $\Extent$ changes by $\Delta \Extent$ for a reaction such as
  \( \label{eq:Reaction} \R{A} + 2\R{B}
   \longleftrightarrow 3\R{C} + \R{D} ,\)
  the amount of $\R{A}$ changes by $-\Delta \Extent$, the amount of
  $\R{B}$ changes by $-2\Delta \Extent$, the amount of $\R{C}$ changes by
  $3 \Delta \Extent$, and the amount of $\R{D}$ changes by $\Delta
  \Extent$.

  \item \emph{Reaction Gibbs Energy.} The reaction Gibbs energy is
  defined as
  \begin{namedeqn}{\textbf{Reaction Gibbs Energy}}
   \Delt{r}G \defn \TD{G}{\Extent}{p,T} .
  \end{namedeqn}
  For the reaction (\ref{eq:Reaction}), we have (at constant pressure
  and temperature):
  \begin{align*}
   dG &= \mu_\R{A} dn_\R{A} + \mu_\R{B} dn_\R{B}
    + \mu_\R{C} dn_\R{C} + \mu_\R{D} dn_\R{D} \\
   &= -\mu_\R{A} d\Extent - 2 \mu_\R{B} d\Extent
    + 3 \mu_\R{C} d\Extent + \mu_\R{D} d\Extent .
  \end{align*}
  so
  \[ \Delt{r}G = -\mu_\R{A} - 2 \mu_\R{B} + 3 \mu_\R{C} + \mu_\R{D} . \]
  In general, we have
  \( \label{eq:RGE} \Delt{r}G = \sum_j \nu_j \mu_j . \)
  $\Delt{r}G$ is the difference in chemical potential between the
  reactants and products.  When $\Delt{r}G < 0$, the reaction is
  spontaneous to the right (exergonic); when $\Delt{r}G > 0$, the
  reaction is spontaneous to the left (endergonic).  It follows that
  the condition for chemical equilibrium is
  \( \Delt{r}G = 0 . \)
  Note that this is simply another way of expressing the general
  condition for equilibrium, $G = \textrm{minimum}$.

  \item \emph{General Reactions.} Using (\ref{eq:Activity}) and
  (\ref{eq:RGE}), we obtain for a general reaction (see Atkins 9.1(d)):
  \( \label{eq:Gibbs1} \Delt{r}G = \Delt{r}\Std{G} + RT \ln Q , \)
  where $Q$, the reaction quotient, is the ratio of the activities of
  the products to the activities of the reactants:
  \( Q \defn a_\R{A}^{\nu_\R{A}} a_\R{B}^{\nu_\R{B}} \cdots
   = \prod_j a_j^{\nu_j} .\)
  In analogy with (\ref{eq:HForm}), the standard reaction Gibbs
  energy $\Delt{r}\Std{G}$ can be written in terms of the standard
  Gibbs energies of formation:
  \( \Delt{r}\Std{G} = \sum_j \nu_j \Delt{f}\Std{G_j} .\)
  Note that the Gibbs energy of formation, using the definition of
  $G$, can be written
  \( \Delt{f}\Std{G_j} = \Delt{f}\Std{H_j} - T \Delt{f}\Std{S_j} . \)

  By definition, the equilibrium coefficient of a reaction $K$ is equal
  to reaction quotient at equilibrium (i.e., using the equilibrium
  activities):
  \( K \defn \prod_j a_{j,\textrm{eq}}^{\nu_j} .\)
  We can write this in terms of the conventional equilibrium constant
  $K_c$ (which involves the product of the equilibrium concentrations,
  rather than of the equilibrium activities):
  \( K = K_\gamma K_c = \prod_j \gamma_j^{\nu_j} \times \prod_j
  x_{j,\textrm{eq}}^{\nu_j} , \)
  where the $\gamma$s are the activity coefficients.  Instead of
  $x_j$, we could use, for example, the molarity at equilibrium, $[j]$. This produces the form of $K_c$ most
  often encountered in general chemistry, where we usually assume that
  $K \approx K_c$. Note that $K$ is independent of the
  initial concentrations.

  Setting $\Delt{r}G = 0$ in (\ref{eq:Gibbs1})---this is just the
  condition for equilibrium---yields the following very important
  relation:
  \( RT \ln K = - \Delt{r}\Std{G} . \)
  Solving for $K$ yields $K = e^{- \Delt{r}\Std{G}/RT}$.

  \item \emph{Effect of Temperature and Pressure on Equilibria.}  The
  value of $\Delt{r}\Std{G}$ and hence $K$ is independent of the pressure
  at which equilibrium is established:
  \( \TD{K}{p}{T} = 0 . \)
  The variation of $K$ with temperature is described by the van't
  Hoff equation (see Atkins Justification 9.3):
  \( \D{}{T} \ln K = \frac{\Delt{r}\Std{H}}{RT^2} , \)
  where $\Delt{r}\Std{H}$ is the standard reaction enthalpy at
  temperature $T$.  See Metiu 18.7 for more.

 \end{enumerate}

 \item \textbf{Electrochemical Thermodynamics.}
 \begin{enumerate}[A.]
  \item \emph{Ions in Solution.}  Electrolyte solutions deviate
  greatly from ideal behavior because of the strong electrostatic
  interaction between anions (negative charged ions) and cations
  (positively charged ions).  In order to allow for
  discussion of the thermodynamics of ions in solution, we define the
  standard enthalpy of formation, the Gibbs energy of formation, and
  the standard entropy for
  the hydrogen ion $\R{H}^+$ in water to be zero at all temperatures.
  Values for other ions can be found by considering $\Delt{r}G$ for
  reactions involving $\R{H}^+$ and the other ion.

  \item \emph{Debye--Huckel Law.}  For the following discussion, we
  will choose as our measure of concentration the molality, $b$,
  expressed in moles of solute per kilograms of solution.  The
  activity of a species $j$ is then
  \( a_j = \gamma b_j / \Std{b}_j \)
  where $\Std{b}$ equals 1 mole per kilogram. As usual, $\gamma \To 1$
  as $b_j \To 0$.

  Let us consider the dissolution of a compound $\R{M}_p \R{X}_q$ to
  give $p$ cations (each with charge $z_+$) and $q$ anions (each
  with charge $z_-$). The molar Gibbs energy of the ions is
  \[ g = p \mu_+ + q \mu_- = p(\Std{\mu} + RT \ln \gamma_+ \frac{b_+}
  {\Std{b}}) + q(\Std{\mu} + RT \ln \gamma_- \frac{b_-}{\Std{b}}) \]
  In this expression, we can replace the activity coefficients
  $\gamma_+$ and $\gamma_-$ with their geometric mean, called the
  mean activity coefficient:
  \[ \gamma_{\pm} \defn (\gamma_-^p \gamma_+^q)^{1/(p+q)} . \]
  This has the effect of distributing the responsibility for
  nonideality equally between the two ions.  The chemical potential
  of an ion in solution is then
  \( \mu_j = \Std{\mu}_j + RT \ln \gamma_\pm \frac{b_j}{\Std{b}} . \)

  The Debye--Huckel law furnishes the value of $\gamma_\pm$ for a given
  cation/anion pair (see Atkins 10.2):
  \( \ln \gamma_\pm = \frac{-\abs{z_+ z_-}A\sqrt{I}}{1+B\sqrt{I}}, \)
  where $I$ is the ionic strength of the solution:
  \( I = \frac{1}{2} \sum_j z_j^2 \frac{b_j}{\Std{b}} . \)
  Here, $z_j$ is the (signed) charge on ion $j$.  Note that $I$ includes
  all ions in the solution, not just the pair we are calculating
  $\gamma_\pm$ for.
  The coefficients $A$ and $B$ can be regarded as empirical
  quantities.  If $B = 0$, we obtain the Debye--Huckel limiting law,
  for which we can show
  \( A = \frac{e^3 N_A^2}{4 \pi}
   \sqrt{\frac{\rho \Std{b}}{2 \epsilon^3 R^3 T^3}}, \)
  where $\epsilon$ is the electrical permittivity of the solution and
  $\rho$ is the (mass) density of the solvent.

  \item \emph{Electrochemical Cells.}  An electrochemical cell is a
  chemical system in which pairs of ``redox'' half reactions of the form
  \( \label{eq:redox} \R{O} + ne^- \longleftrightarrow \R{R} \)
  occur at electrodes (named according to the below table), possibly
  accompanied by other reactions.  The cell reaction is the net reaction
  for the cell, i.e., the result of the combined electrode reactions.
  The forward reaction in (\ref{eq:redox})
  corresponds to reduction, the gain of electrons by a species; the
  reverse reaction corresponds to oxidation, the loss of electrons.
  An electrolytic
  cell is one in which reactions are driven by the
  application of an external potential, while in a galvanic cell
  spontaneous reactions drive a current between the electrodes.
  \begin{center} \begin{tabular}{lccc}
  \toprule  & & \multicolumn{2}{c}{Relative Potential} \\
  \cmidrule(r){3-4}
  Electrode &
  Direction of (\ref{eq:redox}) % Reaction
  & Electrolytic
  Cell & Galvanic Cell \\ \midrule
  Cathode &
  $\rightarrow$ (reduction) % $\R{O} + ne^- \longrightarrow \R{R}$
  & $-$ & $+$ \\
  Anode &
  $\leftarrow$ (oxidation) % $\R{R} \longrightarrow \R{O} + ne^- $
  & $+$ & $-$ \\ \bottomrule
  \end{tabular} \end{center}
  The flow of electrons from an electrode to species in solution
  is called a cathodic current.  The flow of electrons in the
  opposite direction is called an anodic current.  Remember that the
  direction of current is opposite the direction of electron flow.

  \item \emph{Cell Thermodynamics.}  The potential difference across
  an electrochemical cell is a very real manifestation of the Gibbs
  energy available in the chemical system.  Recalling the definition
  of $\Delt{r}G$, the free energy available when the cell reaction(\ref{eq:redox})
  proceeds by $d\Extent$ is $dG = \Delt{r}G d\Extent$.  Recalling
  further that $\Extent$ has units of moles, the charge driven
  between the electrodes when it changes by $d\Extent$ is $-nF$, where
  \( F = N_Ae = 96,485 \textrm{ C/mol} \)
  is called Faraday's constant.  If the potential difference between
  the electrodes is $\CellV$, the work associated with this process
  is $-nF\CellV d\Extent$.
  Assuming that the reaction proceeds \emph{quasi-statically} so that
  the system does it maximum work, we have $\Delt{r}G d\Extent = -nF\CellV
  d\Extent$, so
  \( \Delt{r}G = -nF\CellV . \)
  Combining this with (\ref{eq:Gibbs1}) yields the Nerst equation:
  \begin{namedeqn}{\textbf{The Nerst Equation}} \label{eq:Nerst}
   \CellV = \Std{\CellV} - \frac{RT}{nF} \ln Q
  \end{namedeqn}

  \item \emph{Standard Potentials.} Rather than tabulate the
  potentials of all possible electrochemical cells, the potentials
  for half cells are given versus the normal hydrogen electrode (NHE
  or SHE) for which the half reaction is
  \[ 2\R{H}^+ + 2e^- \longleftrightarrow \R{H}_2 . \]
  For the NHE, we assume that $\R{H}_2$ and $\R{H}^+$ have unit
  activity.  Combining this with (\ref{eq:redox}) and using
  (\ref{eq:Nerst}) yields
  \( \CellV = \Std{\CellV} -  \frac{RT}{nF} \ln
   \frac{a_R^{\nu_R}}{a_O^{\nu_O}} .\)
  Often, we absorb the activity coefficients into the formal
  potential:
  \( {\Std{\CellV}}' \defn \Std{\CellV} - \frac{RT}{nF} \ln
   \frac{\gamma_R^{\nu_R}}{\gamma_O^{\nu_O}} \)
  and write
  \( \CellV = {\Std{\CellV}}' -  \frac{RT}{nF} \ln
   \frac{[\R{R}]^{\nu_R}}{[\R{O}]^{\nu_O}} .\)

  \item \emph{Working with Cells at Equilibrium.}  To determine the
  potential of a cell at equilibrium, we write the half reactions for
  both electrodes as reduction reactions along with their standard
  potentials.  The reaction with the higher (more positive)
  potential, call it $\CellV_\R{red}$, proceeds forward, while the
  other, with potential $\CellV_\R{ox}$, proceeds in reverse.  The
  $\CellV_\R{red}$ electrode is at a higher potential, by
  $\CellV_\R{red} - \CellV_\R{ox}$.

  \item \emph{Electrochemical Potential.}  Let us define the
  electrochemical free energy (a generalized Gibbs energy):
  \( \ecG \defn E - TS + pV + q\phi, \)
  where $q$ is the charge of the system and $\phi$ its electrostatic
  potential.  With
  \[ dE = TdS - pdV - q d\phi + \sum_j \mu_j dn_j , \]
  we have
  \[ d\ecG = -SdT + Vdp + \phi dq + \sum_j \mu_j dn_j . \]
  We assume constant temperature and pressure, so the first two terms
  drop off.  Now, let $z_j$ be the charge carried by species $j$, in
  $e$ per molecule. Then we can write
  \[ dq = \sum_j z_j F dn_j \]
  to yield
  \[ d\ecG = \sum_j (\mu_j + z_j F \phi) dn_j . \]
  The quantity in parentheses plays the same role as $\mu_j$ does in
  the chemical Gibbs energy $G$, motivating the definition
  \( \ecPot_j \defn \mu_j + z_j F \phi \)
  of the electrochemical potential.

  In a system where all external parameters except the volume and
  potential are held constant, the condition for equilibrium is $\ecG
  = \textrm{minimum}$.  For further discussion of the electrochemical
  potential, see Bard and Faulkner 2.2.4.

 \end{enumerate}


\end{enumerate}

\end{document}
