% ----------------------------------------------------------------
% Thermodynamics Material from Mazenko Text **********************
% ----------------------------------------------------------------
\documentclass{article}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{booktabs}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
\reversemarginpar
% MATH -----------------------------------------------------------
\input{../Include/Layout.tex}
\input{../Include/Math.tex}
\input{../Include/Thermo.tex}
% ----------------------------------------------------------------
%\draftmode
\begin{document}
% \maketitle
% ----------------------------------------------------------------
\begin{enumerate}[I.]
 \itemI{General Formulation of Thermodynamics.}
 \begin{enumerate}[A.]
  \itemA{Macrovariables.} The definition of a macrovariable in
  precise terms is quite tricky (see Mazenko 1.5).  In the most
  elementary sense, we can say that the macrovariables of a system are
  the globally conserved quantities associated with the Hamiltonian,
  plus those arising from external constraints and broken symmetries.

  In practice, the macrovariables of a system are those which
  uniquely specify its macroscopic state.  The homogenity condition below
  implies that macrovariables are extensive quantities.

  \itemA{Postulates.} The laws of thermodynamics, as they are
  typically presented, do not form a good basis for building a
  general formulation of thermodynamics.  Instead, we will use the
  following set of postulates:
  \begin{enumerate}[1.]
   \item The equilibrium state of a system is specified by a set of
   macrovariables $\set{X_0,X_1,\ldots,X_t}$.

   \item The entropy is continuous, differentiable function of the
   macrovariables of the system
   \( S = S(X_0,X_1,\ldots,X_t) = S(X) \)
   and is furthermore a homogeneous first order function of the
   macrovariables so that
   \( S(\lambda X) = \lambda S(X). \)
   Finally, $S$ can be inverted uniquely to give
   \( E \defn X_0 = E(S,X_1,\ldots,X_t). \)

   \item The relation $S = S(E,X_1,\ldots,X_t)$ contains all possible
   thermodynamic information about the system.
  \end{enumerate}

  \itemA{Conjugate Forces.} In light of the above postulates, the
  differential of the entropy can be written as
  \( \label{eq:GenS1}
   dS = \sum_{i=0}^t \TD{S}{X_i}{X'} dX_i = \kB \sum_i F_i dX_i ,\)
  where we have defined the generalized force conjugate to $X_i$:
  \( \label{eq:SRepForces} F_i \defn \frac{1}{\kB} \TD{S}{X_i}{X'}. \)
  Note that the partial derivatives are taken with all other $X_i$s
  fixed; henceforth this will be implicit unless stated otherwise.
  With $X_0 = E$, we have
  \( \label{eq:GenS2}
   dS = \frac{dE}{T} + \kB \sum_{i=1}^t F_i dX_i. \)

  We can exchange the roles of $S$ and $E$ so that our independent
  variables are $Y_i = X_i$, $i \neq 0$, $Y_0 = S$. Then rearranging
  (\ref{eq:GenS2}),
  \( dE = T dS - \kB T \sum_{i=1}^t F_i dX_i = \sum_{i=0}^t G_i dY_i, \)
  where the conjugate forces in this so called energy representation
  are
  \( \label{eq:ERepForces} G_i \defn \TD{E}{Y_i}{Y'} .\)
  In terms of the $X_i$,
  \( G_i = -\kB T F_i \)
  for $i \neq 0$ and $G_0 = T$.

  \itemA{The Euler Equation.} Using the homogeneity of $S$, we can
  write
  \begin{align*}
   S(X)&= \At{\PD{}{\lambda} (\lambda S(X))}{\lambda=1}
    = \At{\PD{}{\lambda} S(\lambda X))}{\lambda=1} \\
   &= \At{\sum_i \PD{S(\lambda X)}{(\lambda X_i)}
    \PD{(\lambda X_i)}{\lambda}}{\lambda=1}
     = \sum_i \PD{S(X)}{X_i} X_i.
   \end{align*}
   Using the definition of $F_i$, we see that
   \( S(X) = \kB \sum_{i=0}^t F_i X_i. \)
   Taking the differential of this expression and subtracting
   (\ref{eq:GenS1}) yields the Gibbs-Duhem relation:
   \( \label{eq:GibbsDuhemS} \sum_{i=0}^t X_i dF_i = 0 .\)
   A similar procedure for $E$ (the homogeneity of which follows from
   that of $S$) yields
   \( E(Y) = \sum_{i=0}^t G_i Y_i \)
   and
   \( \label{eq:GibbsDuhemE} \sum_{i=0}^t Y_i dG_i = 0 . \)

  \itemA{Legendre Transformations: Conjugate Forces as Independent
  Variables.}
  \begin{enumerate}[1.]
   \itemi{Legendre Transformations.} Consider a dependent variable
   $y = y(x)$, $x$ being the
   independent variable.  We would like to treat $x$ as a dependent
   variable, not of $y$, but of $p = \tD{y}{x}$, so $x = x(p)$.
   Defining $\psi(p) = y(x(p)) - p x(p)$ and differentiating, we find
   \[ x(p) = -\D{\psi(p)}{p} .\]

   \itemi{Example.}  Let us change dependent variable from $S$ to $T$
   in the energy representation, ignoring all other variables.  Since
   $E = E(S)$ and $T = \tPD{E(S)}{S}$, we can make the following
   identifications with the above section: $x \To S$, $y \To E$, $p
   \To T$.  Then we identify $\psi$ with $F[T] = E - TS$, which
   replaces $E$ as the fundamental potential, and have $S =
   -\tD{F}{T}$.
   An identical procedure for the entropy representation, $S = S(E)$,
   yields $E = -T^2 \tD{(F/T)}{T}$, nothing more than the expression
   for energy in the canonical ensemble.

   \itemi{General Case.}  A partial Legendre transformation which
   replaces $X_0,\ldots,X_s$ by $F_0,\ldots,F_s$ (we need not assume
   that $X_0 = E$ here.) gives the potential
   \( \label{eq:S_s}
    S_s[F_0,\ldots,F_s] \defn S/\kB - \sum_{i=0}^s F_i X_i .\)
   The variables $X_0,\ldots,X_s$ are now dependent variables:
   \( X_i = X_i(F_0,\ldots,F_s,X_{s+1},\ldots,X_t),
    \eqnsep i \leq s. \)
   It is easy to show that
   \begin{subequations} \label{eq:LT1} \begin{gather}
    \PD{S_s}{F_i} = -X_i \eqnsep i=0,\ldots,s, \\
    \PD{S_s}{X_i} = F_i, \eqnsep i=s+1,\ldots,t.
   \end{gather} \end{subequations}
   Thus,
   \( \label{eq:dS_s} dS_s[F_0,\ldots,F_s] = -\sum_{i=0}^s X_i dF_i
    + \sum_{i=s+1}^t F_i dX_i .\)

   In the energy representation, we have similarly (replacing
   $Y_0,\ldots,Y_s$ by $G_0,\ldots,G_s$):
   \( \label{eq:dE_s} dE_s[G_0,\ldots,G_s]
    = -\sum_{i=0}^s Y_i dG_i + \sum_{i=s+1}^t G_i dY_i ,\)
   where the potential $E_s$ is
   \( \label{eq:E_s}
    E_s[G_0,\ldots,G_s] \defn E - \sum_{i=0}^s G_i Y_i .\)
   From (\ref{eq:dE_s}), we can see that (\ref{eq:LT1}) hold in the
   energy representation with the obvious replacements.

   As the example of the previous section demonstrates, changing
   thermodynamic variables via Legendre transformations amounts to
   changing the statistical mechanical ensemble we are working in.
   For example, if $E$ is the independent variable, we have the
   microcanonical ensemble, while if $T$ is the independent variable,
   we have the canonical ensemble.
  \end{enumerate}

  \itemA{Second Derivatives of Macrovariables.} Differentiating and
  applying the
  equality of mixed partials to (\ref{eq:LT1}) yields generalized
  Maxwell relations:
  \begin{subequations} \begin{gather}
   \PD{X_j}{F_k} = \PD{X_k}{F_j}, \eqnsep j,k \leq s, \\
   \PD{X_j}{X_k} = -\PD{F_k}{F_j}, \eqnsep j \leq s, k > s, \\
   \PD{F_j}{X_k} = \PD{F_k}{X_j}, \eqnsep j,k > s.
  \end{gather} \end{subequations}

  In order to relate second derivatives (of macrovariables) from
  different ensembles, we can make use of the following relation
  \( \TD{u}{x}{y,\ldots,z} = \PD{(u,y,\ldots,z)}{(x,y,\ldots,z)}, \)
  as well as some more familiar properties of the Jacobian:
  \begin{gather*}
   \PD{(u,v,\ldots,w)}{(x,y,\ldots,z)}
    = -\PD{(v,u,\ldots,w)}{(x,y,\ldots,z)}, \\
   \PD{(u,v,\ldots,w)}{(x,y,\ldots,z)} =
    \PD{(u,v,\ldots,w)}{(r,s,\ldots,t)}
    \PD{(r,s,\ldots,t)}{(x,y,\ldots,z)}, \\
   \PD{(u,v,\ldots,w)}{(x,y,\ldots,z)}
    = \bracket{\PD{(x,y,\ldots,z)}{(u,v,\ldots,w)}}^{-1}.
  \end{gather*}

 \end{enumerate}

 \itemI{Common Systems.}
 \begin{enumerate}[A.]
  \itemA{Energy Representation.} For the macrovariables $S$,$V$,$N$,
  (\ref{eq:ERepForces}) gives the following conjugate forces:
  \( G_S = \PD{E}{S} \defn T, \eqnsep G_V = \PD{E}{V} \defn -p,
   \eqnsep G_N = \PD{E}{N} \defn \mu . \)
  The Gibbs-Duhem relation is, from (\ref{eq:GibbsDuhemE}),
  \( SdT - Vdp + N d\mu = 0 .\)
  For various choices of independent variables, (\ref{eq:dE_s}) and
  (\ref{eq:E_s}) give the following:
  \begin{center}\hspace*{-3cm}\begin{tabular}{llll} \toprule Indep. Vars
   & Name & Potential & Differential \\ \midrule
   $S$,$V$,$N$ & Energy & $E = TS - pV + \mu N$
    & $dE = TdS - pdV + \mu dN$ \\
   $T$,$V$,$N$ & Helmholtz F.E.& $F=E-TS=-pV+\mu N$ & $dF = -SdT-pdV+\mu dN$ \\
   $T$,$p$,$N$ & Gibbs F.E.& $G = E-TS+pV = \mu N$
    & $dG = -SdT + Vdp + \mu dN$ \\
   $S$,$p$,$N$ & Enthalpy & $H = E+pV=TS+\mu N$ & $dH = TdS + Vdp + \mu dN$  \\
   $T$,$V$,$\mu$ & Landau Pot.& $\Omega = E-TS-\mu N = -pV$
    & $d\Omega = -SdT - pdV - Nd\mu$ \\  \bottomrule
  \end{tabular} \end{center}
  We can read off derivatives of the form (\ref{eq:LT1}) from the
  last column.

  \itemA{Entropy Representation.} For the macrovariables $E$,$V$,$N$,
  (\ref{eq:SRepForces}) gives the following conjugate forces:
  \( \kB F_S = \PD{S}{E} \defn \frac{1}{T},
   \eqnsep \kB F_V = \PD{S}{V} \defn \frac{p}{T},
   \eqnsep \kB F_N = \PD{S}{N} \defn -\frac{\mu}{T} . \)
  For various choices of independent variables, (\ref{eq:S_s}) and
  (\ref{eq:dS_s}) give the following:
  \begin{center}\begin{tabular}{llll} \toprule Indep. Vars
   & Name & Potential & Differential \\ \midrule
   $E$,$V$,$N$ & Entropy & $S = (E+pV-\mu N)/T$
    & $dS = (dE+pdV-\mu N)/T$ \\ \bottomrule
  \end{tabular} \end{center}
  We can read off derivatives of the form (\ref{eq:LT1}) from the
  last column.


 \end{enumerate}

  \itemI{Equilibrium.}
 \begin{enumerate}[A.]
  \itemA{Extrema Principle.}  The following two statements are equivalent:
  \begin{itemize}
    \item The equilibrium value of an uncontrained macrovariable is that which maximizes $S$ at fixed $E$.
    \item The equilibrium value of an uncontrained macrovariable is that which minimizes $E$ at fixed $S$.
  \end{itemize}
  This follows from $G_i = -\kB T F_i$, i.e.,
  \[ \TD{E}{Y_i}{Y'} = -T \TD{S}{X_i}{X'} = -T \TD{S}{Y_i}{Y'}, \]
  so a maxima of $S$ implies a minima in $E$ and visa versa.  Notice that the equilibrium value of the conjugate force for an unconstrained macrovariable is zero.  See Callen, Ch. 5.

  % Ref is Callen, Thermodynamics
  \itemA{Utility of Thermodynamic Potentials.}  We consider a subsystem coupled to a much larger reservior through $Y_0,\ldots, Y_s$.  All macrovariables of the combined system are fixed by isolation (or constraints), i.e., $dY^{0}_i = 0$, as are macrovariables $Y_{s+1},\ldots, Y_t$ of the subsystem, i.e., $dY_i = 0$, $i > s$.  From $Y^{0}_i = Y_i + Y_i'$, we then have $dY_i = -dY_i'$, $i \leq s$ and $dY_i' = 0$, $i > s$.  The equilibrium conditions for the combined system are $dE = 0$ and $dY_{i_S} = dS = 0$.

  For $i \leq s$, and using $dE^0 = dE + dE' = 0$,
  \begin{align*}
   dY^0_i &= dY_i + dY_i' \\
   &= \PD{Y_i}{E} dE + \PD{Y_i'}{E'} dE' \\
   &= \bracket{\PD{Y_i}{E} - \PD{Y_i'}{E'}} dE \\
   &= \bracket{\frac{1}{G_i} - \frac{1}{G_i'}} dE .
  \end{align*}
  so $G_i = G_i'$, $i \leq s$ in equilibrium.  This does not hold in general for the remaining conjugate forces since $dY_i = 0$ and $dY_i' = 0$ individually.  Because of the size of the reservior, we assume that $dG_i' = 0$ for all $i$, which implies $dG_i = 0$, $i \leq s$.

  Using the above results and assumptions, we can show that $dE^0 = dE_s$:
  \begin{align*}
   dE^0 &= dE + dE' \\
   &= dE + \sum_{i=0}^s G_i' dY_i' + \sum_{i=s+1}^t G_i' dY_i' \\
   &= dE - \sum_{i=0}^s G_i dY_i \\
   &= d\parenth{E - \sum_{i=0}^s G_i Y_i} \\
   &= dE_s .
  \end{align*}
  In equilibrium, the energy of the combined system is minimized; the above result shows that this is equivalent to minimizing the appropriate thermodynamic potential $E_s$ of the subsystem.  Since the direction of spontaneous change is always toward equilibrium, this can be restated as: the direction of spontaneous change of a system coupled to a reservior is that which satisfies $dE_s < 0$ (when conjugate forces $G_i$, $i \leq s$ are fixed by coupling to the reservior and macrovariables $Y_i$, $i > s$ are fixed by constraints or isolation).

  \itemA{Example.}  For a subsystem where $T$ and $p$ are fixed by contact with the reservior, while $N$ is fixed by forbidding particle exchange with the reservior, the relevant thermodynamic potential is the Gibbs free energy; the energy of the combined system is minimized when the Gibbs free energy of the subsystem is minimized.

 \end{enumerate}

\end{enumerate}

\end{document}
