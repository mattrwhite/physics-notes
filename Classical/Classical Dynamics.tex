% ----------------------------------------------------------------
% Classical Dynamics *********************************************
% ----------------------------------------------------------------
\documentclass{article}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{booktabs}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
\reversemarginpar
% MATH -----------------------------------------------------------
\input{../Include/Essentials.tex}
\input{../Include/Vectors.tex}
\renewcommand{\r}{\vect{r}}
\newcommand{\Rcm}{\vect{R}}
\newcommand{\p}{\vect{p}}
\newcommand{\Pcm}{\vect{P}}
\renewcommand{\L}{\vect{L}}
% ----------------------------------------------------------------
% \draftmode
\begin{document}
% \maketitle
% ----------------------------------------------------------------
\begin{enumerate}[I.]
 \item \textbf{Kinematics.}
 \begin{enumerate}[A.]
  \item \emph{Position, Velocity, and Acceleration.}  Position is a
  true attribute (or rather, displacement). It is often denoted by
  $x$ in one dimension and $\vect{r}$ in three dimensions.  Velocity
  is the rate of change of position with time: $\vect{v} \defn
  \dot{\vect{r}}$.  The magnitude of velocity $v = \norm{\vect{v}}$ is
  called speed.  Acceleration is the rate of change of velocity
  with time: $\vect{a} \defn \dot{\vect{v}} = \ddot{\vect{r}}$.

  \item \emph{Galilean Relativity.}  Let $\vect{r}_{AB}$ denote the position
  of $B$ \wrt $A$ (i.e., $\vect{r}_{AB}$ is the vector from $A$ to $B$) and
  let $\vect{r}_{BC}$ denote the position of $C$ \wrt $B$.  Then the position
  of $C$ relative to $A$ is just
  \( \vect{r}_{AC} = \vect{r}_{AB} + \vect{r}_{BC} . \)
  Differentiating this \wrt time yields
  \( \vect{v}_{AC} = \vect{v}_{AB} + \vect{v}_{BC} . \)
  Here, $\vect{v}_{AC}$ denotes the velocity of $C$ relative to $A$
  and so on.  Differentiating again gives
  \( \vect{a}_{AC} = \vect{a}_{AB} + \vect{a}_{BC} . \)

  \item \emph{Average Velocity and Acceleration.} The average
  velocity is just distance travelled divided by time:
  \[ v_{\textrm{avg}} \defn \frac{\Delta x}{\Delta t} . \]
  The average acceleration is just the change in velocity divided by
  time:
  \[ a_{\textrm{avg}} \defn \frac{\Delta v}{\Delta t} . \]

  \item \emph{Equations of Motion.}  The general kinematical
  equations of motion in three dimensions are
  \( \vect{v}(t) = \vect{v}(t_0) + \int_{t_0}^t \vect{a} dt \)
  and
  \( \vect{r}(t) = \vect{r}(t_0) + \int_{t_0}^t \vect{v} dt .\)
  Each of these integrals actually represents three integrals, one
  for each component.

  \item \emph{Constant Acceleration.}  If the acceleration in
  some direction $a$ is constant, then the equations of motion for that
  direction become (taking $v(t_0) = v_0$, $x(t_0) = x_0$, and $t_0 = 0$):
  \( v(t) = v_0 + a t ,\)
  \( x(t) = x_0 + v_0 t + \frac{1}{2} a t^2 .\)
  Eliminating time, we get the relation
  \( v^2 = v_0^2 + 2 a (x - x_0) .\)

  \item \emph{Solving the Equations of Motion.}  Given some
  expression, say $a(v)$, describing the motion of a system, we
  simply need to solve the appropriate differential equation to find
  $x(t)$, or whatever else we are interested in.  For this process,
  the following relation is often very useful:
  \( \label{eq:Accel} a = \D{v}{t} = \D{v}{x} \D{x}{t} = v \D{v}{x} .\)
  For example, given $a(v)$, we get
  \[ x - x_0 = \int_{v_0}^{v(x)} \frac{v}{a(v)} dv .\]
  Then with $v(x)$,
  \[ t - t_0 = \int_{x_0}^{x(t)} \frac{dx}{v(x)} \]
  gives us $x(t)$.

  \item \emph{Perpendicular and Parallel Components of Acceleration.}
  Let us write the acceleration $\vect{a}$ as
  \( \vect{a} = \D{\vect{v}}{t} = \D{}{t} v \unitvect{v} =
  \D{v}{t}\unitvect{v} + v \D{\unitvect{v}}{t} . \)
  We call
  \( \vect{a}_\| \defn \D{v}{t} \unitvect{v} \)
  the parallel component of acceleration (parallel to the velocity,
  that is).  It does not change the direction of motion, only the
  speed.  We call
  \( \vect{a}_\bot \defn v \D{\unitvect{v}}{t} \)
  the perpendicular component of acceleration (perpendicular to the
  velocity - the derivative of a unit vector is always perpendicular to
  the unit vector).  It changes the direction of motion, but not the
  magnitude of the velocity (i.e., the speed).

  We can also write $\vect{a}_\| = (\vect{a} \cdot \unitvect{v})
  \unitvect{v}$ and $\vect{a}_\bot = \vect{a} - (\vect{a} \cdot
  \unitvect{v}) \unitvect{v}$.

  \item \emph{Plane Polar Coordinates.} In plane polar coordinates,
  the position of an object is specified by $r$, its distance from
  the origin, and $\theta$, the angle between $\vect{r}$ and the $x$
  axis.  The unit vectors $\unitvect{r}$ and $\unitvect{\theta}$
  (perpendicular to $\unitvect{r}$, pointing in the counterclockwise
  direction) are not fixed, and as a result, we cannot break up the
  kinematical equations into components.  Note that the position of
  an object is still $\vect{r} = r \unitvect{r}$.

  Writing
  \begin{subequations}
  \begin{align}
   \unitvect{r} &= \unitvect{i} \cos \theta + \unitvect{j} \sin
   \theta , \\ \unitvect{\theta} &= -\unitvect{i} \sin \theta
   + \unitvect{j} \cos \theta ,
  \end{align}
  \end{subequations}
  we can show that
  \begin{subequations}
  \begin{align}
  \D{}{t} \unitvect{r} &= \dot{\theta} \unitvect{\theta}, \\
  \D{}{t} \unitvect{\theta} &= -\dot{\theta} \unitvect{r},
  \end{align}
  \end{subequations}
  Note that $\dot{\theta} = \omega$ is the usual angular velocity.
  In plane polar coordinates, the velocity $\vect{v}$ can be written as
  \( \vect{v} = \dot{r} \unitvect{r} + r \dot{\theta}
  \unitvect{\theta} . \)
  Here, $\dot{r} \unitvect{r}$ is the radial velocity---the motion of
  the particle away from or towards the origin, while $r \dot{\theta}
  \unitvect{\theta}$ is the rotational velocity.

  Differentiating again, we find that the acceleration is
  \( \vect{a} = (\ddot{r} - r \dot{\theta}^2) \unitvect{r} + (r
  \ddot{\theta} + 2 \dot{r} \dot{\theta}) \unitvect{\theta} . \)
  \begin{itemize}
   \item $\ddot{r} \unitvect{r}$ is the linear acceleration in the
    $\unitvect{r}$ direction.
   \item $r \ddot{\theta} \unitvect{\theta}$  is the linear
   acceleration in the $\unitvect{\theta}$ direction caused by change
    in angular velocity.
   \item $- r \dot{\theta}^2 \unitvect{r}$  is the centripetal
   acceleration directed toward the origin (i.e., $-\unitvect{r}$ direction).
   This is the acceleration necessary to keep $r$ from changing.
   \item $2 \dot{r} \dot{\theta} \unitvect{\theta}$ is the Coriolis
   acceleration, which occurs whenever $r$ and $\theta$ are changing
   simultaneously (i.e., all non-circular motion). See CS1A lecture
    11/23/99 for more.
  \end{itemize}

  For uniform motion in a straight line, $\vect{a} = 0$ of course,
  but this does not imply that $\dot{r}$, $\ddot{r}$, $\dot{\theta}$,
  and $\ddot{\theta}$ are zero individually.

  \item \emph{Circular Motion.}  For circular motion, $\dot{r} =
  \ddot{r} = 0$, hence $v = r \dot{\theta}$, in the
  $\unitvect{\theta}$ direction.
  For uniform circular motion, we have
  the additional requirement that $\ddot{\theta} = 0$, i.e., that
  $\dot{\theta}$ is constant.  In this case, the only acceleration
  present is the centripetal acceleration $ a = r \dot{\theta}^2 = r
  \omega^2$ (in the $- \unitvect{r}$ direction).  We can also
  write this as $a = v^2 / r$.
 \end{enumerate}

 \item \textbf{Dynamics.}
 \begin{enumerate}[A.]
  \item \emph{Inertial Frames.}  By definition, an inertial frame is
  one in which Newton's laws, given below, hold.  It follows that the
  relative velocity of any two inertial frames is constant; inertial
  frames cannot be accelerating.  In solving dynamics problems, it is
  often useful to change to the inertial frame where the greatest
  number of objects under consideration are stationary.

  \item \emph{Newton's First Law.} A body moves with constant (or no)
  velocity (constant speed in a straight line) unless acted upon by
  a net external force.

  \item \emph{Newton's Second Law.} The acceleration experienced by an
  object is proportional to the net external force $\sum
  \vect{F}_\textrm{ext}$ acting on it and is in the same direction as
  that force.  We can ignore internal forces because they cancel
  each other completely, thanks to Newton's third law.
  \begin{namedeqn}{\textbf{Newton's Second Law}}
   \sum \vect{F}_\textrm{ext} = m \vect{a}.
  \end{namedeqn}
  The constant of
  proportionality $m$ is called the (inertial) mass of the object.  Mass
  is a measure of the amount of matter in an object, and its
  corresponding resistance to change in motion, called inertia.

  The SI unit of force is the Newton: 1 N = 1 kg m/$\textrm{s}^2$.  So a Newton
  is the force required to accelerate a kilogram at one meter per
  second per second.

  \item \emph{Newton's Third Law.}  If a body $A$ exerts a force
  $\vect{F}_{AB}$ on $B$, then $B$ must exert a force $\vect{F}_{BA}
  = -\vect{F}_{AB}$ on $A$.  In words, for every action, there is an
  equal (in magnitude) and opposite (in direction) reaction.
  Newton's third law contains the real physics---the first two just
  define inertial frames and forces.

  \item \emph{Applying Newton's Laws.} See CCS 1A Assignments 6 and 7
  for examples.
  \begin{enumerate}[1.]
   \item Identify all forces and the bodies on which they act.
   This is usually accomplished with free-body diagrams.
   \begin{itemize}
    \item A separate diagram should be drawn for each body.
    \item Forces on free-body diagrams are labeled with magnitudes
    ($F$), not as vectors ($\vect{F}$).
    \item It is useful to indicate the direction of the body's
    acceleration with an arrow that is not attached to the body.
   \end{itemize}

   \item Choose a coordinate system
   \begin{itemize}
    \item Each body can have a different coordinate system.

    \item It is usually best to choose the coordinate system for each
    body so that the acceleration of the body is solely along one axis
    and the greatest number of forces possible are in the direction of
    the axes.
   \end{itemize}

   \item Break up forces into components according to the coordinate
   system (that is, those forces which do not lie along one of the axes)
   and write equations of the form
   \[ \sum F = ma \]
   for each object, one equation for each direction. Note that these
   are \emph{not} vector equations.
   \begin{itemize}
    \item We can replace $ma$s with $0$ at this stage if we know that the
    acceleration of the body in a given direction will be zero.

    \item Also write the applicable equations for the common forces of
    nature.

    \item Since these are not vector equations, direction must be
    indicated by sign: positive if the force (component) is in the positive
    direction along the axis, negative if it is in the negative direction.
   \end{itemize}

   \item Find equations of constraint for acceleration, that is, relate
   the accelerations of the bodies in the system.
   \begin{itemize}
    \item For example, if bodies $A$ and $B$ are attached, then
    $a_A = a_B$, assuming both bodies have compatible coordinate
    systems.

    \item In difficult cases, first relate the positions of the objects,
    then derive acceleration ($a = \ddot{x}$).

    \item Given the system of force and acceleration equations, solve
    for unknowns in terms of known variables, substituting numeric
    values in only at the very end.
   \end{itemize}
  \end{enumerate}

  \item \emph{Common Forces.}
  \begin{enumerate}[1.]
   \item \emph{Gravity.} On Earth, gravity acts toward the center of the
   Earth with magnitude $W=mg$ at the surface, where $m$ is the mass of the
   body it acts on and $g = 9.8 \textrm{ m}/\textrm{s}^2$.  The
   weight of an object is just the magnitude of the gravitational
   force on it.

   \item \emph{Normal Force.} The normal force is the contact force
   between surfaces.  It always acts perpendicular (normal) to the
   surface.  Its magnitude is only great enough to prevent motion into
   surface (i.e., it can't push something off the surface); symbolized
   by $\eta$.

   \item \emph{Friction.} Friction is the force parallel to a surface
   which resists motion along (over) the surface; proportional to the
   normal force (that is, how hard the surfaces are pressed
   together).  The constant of proportionality is called the
   coefficient of friction and depends on the surfaces in contact.
   \begin{enumerate}[a.]
    \item Static friction prevents a body from starting to move relative
    to a surface.  Its magnitude is equal to the anti-parallel force
    trying to cause motion relative to the surface, but not greater than
    the maximum value.  When the maximum value is exceeded, the body
    begins to move and kinetic friction takes over.  Mathematically,
    \( f_s \leq \mu_s \eta , \)
    where $f_s$ is the static friction force and $\mu_s$ is the
    coefficient of static friction.

    \item   Kinetic friction acts in the direction opposite the
    velocity of a body across a surface.  Usually weaker than static
    friction and generally velocity independent.
    \( f_k = \mu_k \eta , \)
    where $f_k$ is the kinetic friction force and $\mu_k$ is the
    coefficient of kinetic friction.

    \item Tension is the internal force in a rope.  Tension is the
    force applied by the rope at its ends.  It is uniform only for a
    massless rope.
   \end{enumerate}
  \end{enumerate}
 \end{enumerate}

 \item \textbf{Statics.}
 \begin{enumerate}[A.]
  \item \emph{Equilibrium.}  In statics, we wish to compute the
  forces on an object at equilibrium, that is, a stationary object.
  The conditions for equilibrium are
  \begin{enumerate}[1.]
   \item The net (external) force on the body must be zero:
   \( \sum \vect{F} = 0 . \)
   This ensures that the object does not translate.

   \item The net (external) torque on the body must be zero:
   \( \sum \vect{\tau} = 0 .\)
   This ensures that the object does not rotate.
  \end{enumerate}
  Of course, we are assuming that the object is not translating or
  rotating initially.

  \item \emph{A Note About Torques.} When doing statics problems, we
  can choose \emph{whatever point we please} to calculate the torques
  about, as long as we remain consistent in using this origin throughout
  the problem.  We need not use the center of mass or even a point
  that is on the body.

  \item \emph{Center of Gravity.}  In a uniform gravitational field,
  a body moves as if all its mass were concentrated at its center of
  mass.  In a nonuniform gravitational field, the point with this
  property is called the center of gravity.
 \end{enumerate}


 \item \textbf{Work and Energy.}
 \begin{enumerate}[A.]
  \item \emph{The Work-Energy Theorem.} In three dimensions,
  \begin{align}
  \int_{\vect{r}_0}^{\vect{r}} \dotpd{F}{dr} &= m
  \int_{\vect{r}_0}^{\vect{r}} \parenth{\D{\vect{v}}{t}} \cdot
  \vect{dr} \notag \\ & = m \int_{\vect{r}_0}^{\vect{r}}
  \parenth{\D{\vect{v}}{t}} \cdot \vect{v}dt \notag \\
  &= m \int_{\vect{r}_0}^{\vect{r}} \frac{1}{2} \D{}{t} (v^2) dt \notag \\
  &= \frac{1}{2} m \int_{\vect{r}_0}^{\vect{r}} dv^2 \notag \\
  \label{eq:WorkEnergy}
  &= \left. \frac{1}{2} m v^2 \right|_{\vect{r}_0}^{\vect{r}} .
  \end{align}
  Note the use of $\vect{dr} = \vect{v}dt$ and the result $\vect{v} \cdot
  \vect{v}' = \frac{1}{2} (v^2)'$. The derivation is one dimension is simpler,
  and makes use of (\ref{eq:Accel}).  For an alternate derivation in
  three dimensions, see CS1A lecture 12/2/1999.


  We call
  \begin{namedeqn}{\textbf{Kinetic Energy}}
   K \defn \frac{1}{2} m v^2
  \end{namedeqn}
  the kinetic energy (sometimes denoted by $T$ instead of $K$).
  The line integral
  \begin{namedeqn}{\textbf{Work}} \label{eq:Work}
   W \defn \int \dotpd{F}{dr}
  \end{namedeqn}
  is called the work.  Note that only the component of force along the
  direction of motion contributes to the work.
  If the force is constant and acts through a
  distance $d$, then the work is just $W = Fd$, where $F$ is the
  component of force along the direction of motion.  Note that work can
  be negative, when the force opposes the motion.  The work-energy
  theorem (\ref{eq:WorkEnergy}) states that the change in the kinetic
  energy of an object is equal to the work done on it:
  \begin{namedeqn}{\textbf{Work-Energy Theorem}}
   W = \Delta K .
  \end{namedeqn}

  The kinetic energy of an object is equal to the amount of work that
  was done to accelerate it from rest.  Conversely, the kinetic
  energy of an object is the amount of work it can do in the process
  of begin brought to rest.

  Because the work-energy is just a restatement of Newton's second
  law, Newton's third law applies: If object $A$ does work $W$ on
  object $B$, object $B$ does work $-W$ on $A$.

  \item \emph{Conservative Forces and Potential Energy.} A conservative
  force is one which does the same amount of work no matter what path is
  taken between two points (it depends only on the displacement between
  the points).  This means that a conservative force does no net work on
  a round trip (as there is no displacement).

  A conservative force (field) $\vect{F}$ depends only on position and
  satisfies $\curl{F} = 0$.  For any such force it is possible to find
  a scalar function $U$ such that
  \( \vect{F} = - \del U .\)

  For a conservative force, the line integral for work (\ref{eq:Work})
  is simply a function of the initial and final points.  We can then define
  a scalar field $U(\vect{r})$ such that
  \( \int_{\vect{r}_0}^{\vect{r}} \dotpd{F}{dr} = -(U(\vect{r} -
  U(\vect{r}_0)) . \)
  It follows that
  \begin{namedeqn}{\textbf{Potential Energy}}
   U \defn - \int \dotpd{F}{dr} .
  \end{namedeqn}
  This is called the potential energy - it is defined only to within an
  arbitrary constant (which we are completely free to choose) since only
  changes in potential energy matter. The minus sign is simply a matter
  of convention and convenience.

  Clearly,
  \( W = \Delta K = - \Delta U . \)
  Note that while we can speak of the potential energy for each force
  that is acting independently, this expression only holds when $U$
  is the total potential energy.
  If only conservative forces do work, it follows that the total
  mechanical energy
  \( E \defn K + U , \)
  remains constant in any closed system.

  If non-conservative forces do work, we have
  \( \label{eq:Wother} \Delta K = -\Delta U + W_{\textrm{other}} , \)
  where $W_{\textrm{other}}$ is the work done by non-conservative forces,
  such as friction.

  \item \emph{Understanding Potential Energy.}
  \begin{enumerate}[1.]
   \item Saying that $A$ does work $W$ on $B$, increasing its kinetic
   energy by an
   amount $W$ also means the potential energy of $A$ \wrt $B$ has been
   decreased by an amount $W$.  Potential energy is the ``potential''
   that an object has to do work.  A decrease in potential energy and
   an object doing work are the same thing, stated in two different ways.

   For example, consider two masses, $A$ and $B$.  Say $A$ does work
   on (gravitationally attracts) $B$, accelerating $B$ towards it, i.e.,
   increasing its kinetic energy.  But now $A$ is closer to $B$, so its
   potential energy has decreases, by the same amount that the kinetic
   energy increased!

   \item If we ignore the fact that wind resistance depends on speed,
   we can think of a steady wind as a conservative force field.  Now,
   let us imagine a young man riding his bicycle to the beach
   from his house, against the wind.  He must pedal hard to fight
   against the wind but his efforts are not in vain, for he is
   increasing his potential energy as he does work $W_{\textrm{other}}$
   against the wind.  We could say that he is storing up potential
   energy for the trip home. In (\ref{eq:Wother}), $\Delta K = 0$, so
   $W_{\textrm{other}} = \Delta U > 0$.  When the young man wishes to
   return to his house, he will not have to do much work at all
   ($W_{\textrm{other}} = 0$) - the wind will push him there.
   He is converting his stored potential energy to kinetic energy.

   From his example, it should be clear that potential
   energy is not real in the same sense as mass or force.  It is just
   a mathematical tool for working with a special class of forces.
   When we say that the young man is storing up potential energy, he
   obviously isn't filling up a bag with it!  Rather, he has changed
   his position is a force field relative to where he wants to go
   (back home).

   \item \emph{Constraint Forces.} Forces of constraint act to hold on
   object on a given trajectory. These forces (the normal force, for
   example) are
   always perpendicular to the object's line of motion and therefore do
   no work.  They do not change the object's speed and do not come into
   play in the work-energy theorem.

   \item \emph{Potential Energy and Force.} In one dimension,
   \[ F = -\D{U}{x} .\]
   Force is the negative of the slope of the potential energy graph, so
   a minimum on the graph of $U$ indicates a stable equilibrium - a point
   where there is no net force acting and the net force on either side
   is directed toward the equilibrium (think a ball at the bottom of a
   valley).  A maximum indicates an unstable equilibrium - a point where
   no net force acts, and the net force on either side of the
   equilibrium acts to accelerate the object away from it (think a ball on
   top of a mountain).
  \end{enumerate}

  \item \emph{Power.} Power is the rate of doing work (delivering energy),
  i.e., the rate of change of energy:
  \begin{namedeqn}{\textbf{Power}}
   P = \D{W}{t} = \D{E}{t} .
  \end{namedeqn}
  If the force is constant, we have
  \( P = \dotpd{F}{v} ,\)
  where $\vect{v}$ is the velocity of the object being acted on.
 \end{enumerate}


 \item \textbf{Momentum.}
 \begin{enumerate}[A.]
  \item \emph{Definition of Momentum.} The momentum of an object with
  mass $m$ and velocity $\vect{v}$ is
  \begin{namedeqn}{\textbf{Momentum}}
   \vect{p} \defn m \vect{v} .
  \end{namedeqn}
  Newton's second law is then (assuming constant mass):
  \( \vect{F} = \D{\vect{p}}{t} . \)
  The primary application of momentum is in dealing with systems of
  particles.  Note that because momentum and force are vectors, they
  can be broken
  into components such that the net force in a given direction is equal
  to the time rate of change of momentum in that direction.

  \item For a given particle $i$ in a system, we have $\vect{F}_i
  = \D{}{t}\vect{p}_i$, where $\vect{F}_i$ is the net force on the
  particle and $\vect{p}_i$ is the momentum of the particle.  Summing
  over all particles in a given system, we get
  \( \label{eq:Mom1} \vect{F}_{\textrm{ext}} = \D{}{t} \vect{P} , \)
  where $\vect{F}_{\textrm{ext}}$ is the net external force on the
  system and $\vect{P}$ is the vector sum of the momentums of the
  particles in the system, i.e., it is the total momentum of the system.
  Note that by Newton's Third Law, any internal forces in the system
  will not affect the motion of the system as a whole (although they
  will affect the internal motion of the system).
  Altogether, this says that the net external force on a
  system is equal to the change in momentum of the system.

  \item \emph{Conservation of Momentum.}  From (\ref{eq:Mom1}), it is
  clear that:
  \begin{quote}
  \emph{If no external forces act on
  a system, then its momentum does not change.}
  \end{quote}

  \item \emph{Center of Mass.}  Consider of system of particles, with
  mass $m_i$ and position $\vect{r}_i$.  Let us define the center of
  mass of the system as
  \begin{namedeqn}{\textbf{Center of Mass}}
   \vect{R} \defn \frac{\sum_i m_i \vect{r}_i}{M} ,
  \end{namedeqn}
  where $M = \sum_i m_i$ is the total mass of the system. It is easy
  to show that
  \( \vect{F}_{\textrm{ext}} = \D{}{t} \vect{P}
  = M \DD{}{t} \vect{R}, \)
  which is to say that the center of mass of a system moves (i.e., in
  response to external forces) as if all the mass of the system were
  concentrated there. Therefore, the velocity of the center of mass
  does not change unless external forces are applied to the system.

  The center of mass of a continuous distribution of matter is just
  \( \vect{R} = \frac{1}{M} \int \vect{r} dm . \)
  For a solid mass, $dm = \rho d\Vol$.

  To find the coordinate of the center of mass along some direction,
  say $x$, we can take (infinitesimal) slices of a body normal to that
  direction and
  treat each slice as a point mass on (in this case) the $x$ axis.

  Note that the center of mass of a complex body can be found by
  finding the center of mass of its pieces, then finding the center
  of mass of these.

  \item \emph{Center of Mass Coordinates.}  As long as no net external force
  acts on a system, the center of mass experiences no acceleration,
  and we can set it as the origin of an inertial coordinate system,
  called the center of mass frame.  We sometimes
  call this the center of momentum frame instead.

  In the center of mass frame, $\vect{R} \defn 0$, so $\vect{P}
  = M \D{}{t} \vect{R} = 0$.  That is, the total momentum of a
  system in the center of mass frame is always zero.

  If the position of particle in some frame (called the lab frame) is
  $\vect{r}_i$, its position in the center of mass frame is just
  $\vect{r}_{i,\textrm{CM}}= \vect{r}_i - \vect{R}$, where
  $\vect{R}$ is the position of the center of mass as measured in the
  lab frame.  It follows that $\vect{v}_{i,\textrm{CM}}= \vect{v}_i
  - \vect{V}$, and so on.

  \item \emph{Reduced Mass.}  Let us consider a system of two
  particles.  The relative position vector (from particle 1 to
  particle 2) is:
  \[ \vect{r} \defn \vect{r}_2 - \vect{r}_1 . \]
  It follows that $\ddot{\vect{r}} = \ddot{\vect{r}}_2
  - \ddot{\vect{r}}_1$.  Now let us call the force on particle 2 due
  to particle 1 $\vect{F}_\textrm{1 on 2}$.  From Newton's third law,
  we have $\vect{F}_\textrm{2 on 1} = - \vect{F}_\textrm{1 on 2}$.
  Newton's second law gives
  \[ \vect{F}_\textrm{1 on 2} = m_2 \ddot{\vect{r}}_2 , \eqnsep
   \vect{F}_\textrm{2 on 1} = m_1 \ddot{\vect{r}}_1 . \]
  Combining all this and simplifing, we find that
  \[ \vect{F}_\textrm{1 on 2} = \mu \ddot{\vect{r}} , \]
  where
  \begin{namedeqn}{\textbf{Reduced Mass}}
   \mu = \frac{m_1 m_2}{m_1 + m_2}
  \end{namedeqn}
  is called the reduced mass.

  The significance of the reduced mass is that without changing the
  reference frame
  or any of the forces in a two particle system, we can
  fix the position of one particle (effectively give it
  infinite mass so that no matter what forces act upon
  it or what collides with it, it will not move) and
  assign to the other particle the reduced mass .  This
  particle will then move normally (note that the origin of
  the coordinate system is usually placed at the fixed particle)
  according to $\vect{F} = \mu \vect{a}$.  It reduces a two body
  problem to a single body problem.

  Note that the reduced mass is always less than the masses
  of either or (hence the name) because as the internal forces
  between and act on both bodies, their relative position changes
  in magnitude more than that of either particle \wrt
  the fixed center of mass.

  \item \emph{Systems with Variable Mass.}  Newton's second law for a
  system with variable mass is not quite as straightforward as one
  might guess.  The correct expression is
  \begin{namedeqn}{\textbf{System with Variable Mass}}
   m \D{\vect{v}}{t} = \vect{F}_{\textrm{ext}} + \vect{u} \D{m}{t}.
  \end{namedeqn}
  As usual, $\vect{F}_{\textrm{ext}}$ is the net force on the system.
  Clearly, $dm/dt$ is the change in mass of the system. Finally,
  $\vect{u}$ is the velocity of the mass being added or removed
  \emph{relative to the system}.

  Consider, for example, a rocket with initial mass $m_0$ and exhaust
  speed $u$ (relative to the rocket of course).  Then we have
  \[ \D{v}{t} = \frac{u}{m} \D{m}{t}. \]
  Integrating,
  \[ v(t) = u \ln \frac{m_0}{m(t)} . \]

  As another example, say we want to find the force, $F$, necessary
  to keep an object from changing velocity as mass is added. We would
  solve
  \( F + u \D{m}{t} = 0 , \)
  where $dm/dt$, the rate at which mass is added might be equal to
  $dm/dx$, the mass per unit length of whatever is being added, times
  $u = dx/dt$, the rate at which it is being added.  See CCS 2A problem
  solving 1/26/2000 for the solution to such a problem, K\&K 4.11.

  \item \emph{Momentum Transport.} Momentum transport
  involves the transfer of momentum by a stream of matter, as opposed
  to a single moving particle or object.  The matter stream might be
  a jet of water from a hose, a beam of light from a laser, or a number
  of discrete particles (e.g.
  baseballs), in which case we compute average values for the necessary
  quantities.

  Assuming the velocity of the stream is constant (so $d^2 x/dt^2 = 0$),
  $\dot{p}$ is interpreted in the context of momentum transport as:
  \( \D{p}{t} = \D{p}{x} \D{x}{t} = \D{x}{t} \D{m}{x} \D{x}{t} , \)
  the rate at which the stream ``transports'' momentum. Examining the
  units of the rightmost expression confirms that it is a force, as
  it must be. In this important expression, $dm/dx$ is the mass per
  unit length of the matter stream and $dx/dt$ is the velocity of
  the matter stream.
  Its contribution is two fold:  first to the momentum of the stream
  and second as the rate at which that momentum is transported.  The
  generalization to three dimensions, again with the assumption that
  the velocity of the stream is constant, is simply
  \begin{namedeqn}{\textbf{Momentum Transport}}
   \D{\vect{p}}{t} = \D{m}{x} \parenth{\D{\vect{x}}{t}}^2 =
  \D{m}{x} \vect{v}^2 .
  \end{namedeqn}

  Noting our above interpretation of $\dot{\vect{p}}$, we say that a
  matter stream strikes an object, transporting momentum
  at a rate of $\dot{\vect{p}}_i$ and rebounds, carrying away momentum
  at a rate of $\dot{\vect{p}}_f$. The resulting force on the object is
  \( \vect{F} = \dot{\vect{p}}_f - \dot{\vect{p}}_i . \)
  By Newton's third law, we have $\vect{F}
  = - \dot{\vect{p}}_\textrm{stream}$ where
  $\dot{\vect{p}}_\textrm{stream}$ is the net change in momentum of
  the stream due to the object.  See K\&K 3.6 for more.

 \end{enumerate}

 \item \textbf{Angular Momentum.}
 \begin{enumerate}[A.]
  \item \emph{Definition of Angular Momentum.} The angular momentum
  of an object with momentum $\vect{p}$ is
  \begin{namedeqn}{\textbf{Angular Momentum}}
   \vect{L} \defn \crosspd{r}{p},
  \end{namedeqn}
  where $\vect{r}$ is the position of the object.  Let us now
  consider
  \[ \D{}{t} \vect{L} = \D{}{t} \crosspd{r}{p} = (\dot{\vect{r}}
  \times \vect{p}) + (\vect{r} \times \dot{\vect{p}}). \]
  Since $p = m \dot{\vect{r}}$, $\dot{\vect{r}} \times \vect{p} = 0$.
  Defining the torque
  \begin{namedeqn}{\textbf{Torque}}
   \vect{\tau} \defn \crosspd{r}{F},
  \end{namedeqn}
  we can write
  \( \dot{\vect{L}} = \vect{r} \times \dot{\vect{p}}
   = \vect{\tau}.  \)
  It follows that the angular momentum of an object is conserved if
  no external torques act on it.

  \item \emph{Systems of Particles.} The angular momentum of a
  system of particles is (using the notation of the above section)
  \( \L = \sum_i \L_i = \sum_i (\r_i \times \p_i). \)
  If we write the position of a particle as
  \( \r_i = \Rcm + \r_i' , \)
  where $\r_i'$ is just the position of the particle in the CM frame,
  we find after some algebra (see Marion 9.4)
  \( \L = \Rcm \times \Pcm + \sum_i \r_i' \times \p_i' . \)
  Here, of course, $\p_i' = m \dot{\r}_i'$.  In words, the total
  angular momentum of a system is the sum of the angular momentum of
  its center of mass about the origin and the angular momentum of the
  system about its center of mass.

  If we assume that all internal forces are central, then it can
  shown that (again, see Marion 9.4)
  \( \dot{\L} = \vect{\tau}_{\textrm{ext}}, \)
  i.e., the change in the total angular momentum is equal to the net
  external torque.

 \end{enumerate}

\end{enumerate}

\end{document}
