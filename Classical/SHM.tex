% ----------------------------------------------------------------
% Simple Harmonic Motion *****************************************
% ----------------------------------------------------------------
\documentclass{article}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{booktabs}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
\reversemarginpar
% MATH -----------------------------------------------------------
\input{../Include/Essentials.tex}
% ----------------------------------------------------------------
% \draftmode
\begin{document}
% \maketitle
% ----------------------------------------------------------------
\begin{enumerate}[I.]
 \item \textbf{The Differential Equation (DE) of SHM.}
 \begin{enumerate}[A.]
  \item We consider the following forces acting on a body:
  \( F = -k x - b v + f(t), \)
  where $-k x$ is the restoring force (note that it does not act at the
  equilibrium, $x = 0$), $-b v = -b \dot{x}$ is a velocity dependent friction
  force, and $f(t)$ is an external, time varying driving force.

  \item Combining this with Newton's second law, we get the DE of SHM:
  \( \label{eq:SHMDE} m \ddot{x} + b \dot{x} + k x = f(t), \)
  where $m$ is the mass of the body being acted upon.  This can also be written
  as
  \( \ddot{x} + 2 \beta \dot{x} + \omega_0^2 x = f(t) , \)
  where
  \( \omega_0 = \sqrt{\frac{k}{m}} \)
  and $\beta = b/2m$.  We sometimes replace $\beta$ with $2\beta =
  \gamma$. Also, we often use $m\omega_0^2$ in place of $k$.

  \item For the SHO,
  \( U = -\int F dx = \frac{1}{2} k x^2 \)
  This fact is
  useful in applying SHM to cases where, for small oscillations, the
  potential is close to a parabolic function.  Namely, given an
  arbitrary potential $U(x)$, we can analyze oscillations of a mass
  about some minimum $x_0$ (i.e. stable equilibrium) of the potential
  via a Taylor expansion:
  \[ U(x) = U(x_0) + \At{\D{U}{x}}{x_0} (x - x_0)
   + \frac{1}{2} \At{\DD{U}{x}}{x_0} (x- x_0)^2 + \ldots .\]
  We can discard the first term because it is a constant.  Furthermore,
  the second term is zero because we are expanding about a minimum of U.
  This leaves
  \( U(x) \approx \frac{1}{2} \At{\DD{U}{x}}{x_0} (x- x_0)^2 .\)
  Noticing that this is identical to the potential for a simple harmonic
  oscillator, we can let
  \( k = \At{\DD{U}{x}}{x_0} (x- x_0)^2 \)
  and use all of our results for SHM, assuming the oscillations remain
  small so that our approximation of neglecting higher order terms remains
  valid.
 %\end{enumerate}

 \item \emph{Physical Cases.}
 \begin{enumerate}[1.]
  \item \emph{Mass on a Spring.} This is the most obvious example of SHM.

  \item \emph{Pendulum.} Using the small angle approximation $\sin \theta
  \approx \theta$, we can treat
  the pendulum as a SHO.  Here the restoring force is $F = -m g \sin \theta$,
  which becomes $F \approx - m g \theta = m g x/L$.
  We can then solve for the motion easily with $k = mg / L$.

  \item \emph{LRC Circuit.} The flow of charge in a simple LRC (inductor
  - resistor - capacitor) circuit is described by the DE of SHM.  The
  analogy is as follows:
  \begin{center}
  \begin{tabular}{llll}
  \toprule \multicolumn{2}{l}{Mechanical} &
  \multicolumn{2}{l}{Electrical} \\
  \midrule
  $x$ & Displacement & $q$ & Charge \\
  $\dot{x}$ & Velocity & $\dot{q}=I$ & Current \\
  $m$ & Mass & $L$ & Inductance \\
  $b$ & Damping & $R$ & Resistance \\
  $1/k$ & Mechanical Compliance & $C$ & Capacitance \\
  $F$ & Forcing & $\mathcal{E}$ & EMF \\
  \bottomrule
  \end{tabular}
  \end{center}
  For a circuit with inductance $L$, resistance $R$, capacitance $C$,
  and a time varying EMF $\mathcal{E}_0 \cos \omega t$, we have
  \( L \ddot{q} + R \dot{q} + \frac{q}{C}
  = \mathcal{E}_0 \cos \omega t . \)
  Hence, $\omega_0 = 1/\sqrt{LC}$ and $\beta = R/2L$.  We are usually
  interested in $\dot{q} = I$, so we solve for $q(t)$, then
  differentiate.
 \end{enumerate}

 \item \emph{Aspects of Harmonic Motion.}
 \begin{itemize}
  \item Amplitude $A$: the maximum magnitude of displacement from equilibrium.
  \item Period $T$: the time required for one complete cycle of oscillation.
  \item Frequency $f$: the number of oscillations per unit time, so $f = 1/T$.
  \item Angular frequency $\omega$: the number of radians of oscillation per
  unit of time: $\omega = 2 \pi f$.
 \end{itemize}
\end{enumerate}

 \item \textbf{Solving the DE.} The basic method for solving $m \ddot{x}
 + b \dot{x} + k x = f(t)$ is as follows.  See K\&K Ch.10 and CS 2A lectures
 2/24/2000, 2/29/2000, and 3/2/2000 for the theory of solutions.
 \begin{enumerate}[A.]
  \item Solve the homogeneous equation $m \ddot{x} + b \dot{x} + k x = 0$.
  Call the solution $x_h (t)$.
  \begin{enumerate}[1.]
   \item Solve the characteristic equation (CE) $m \lambda^2 + b \lambda
   + k = 0$ for $\lambda$.

   \item If the CE has two real roots $\lambda_1$ and $\lambda_2$,
    \( x_h (t) = k_1 e^{\lambda_1 t} + k_2 e^{\lambda_2 t} , \)
   where $k_1$ and $k_2$ are undetermined coefficients which depend on the
   initial conditions.

   \item If the CE has two complex roots $\lambda = \alpha \pm i \beta$,
    \( x_h (t) = k_1 e^{\alpha t} \cos \beta t+ k_2 e^{\alpha t
    \sin \beta t }. \)
   This solution is often written in phase-amplitude form:
    \( x_h (t) = A e^{\alpha t} \cos (\beta t - \phi), \)
   where $A = \sqrt{k_1^2 + k_2^2}$ and $\phi = \arctan (k_1 / k_2)$.

    \item If the CE has a double real root $\lambda$,
     \( x_h (t) = k_1 e^{\lambda t} + k_2 t e^{\lambda t} .\)
   \end{enumerate}

   \item If $f(t) \neq 0$, find a particular solution $x_p (t)$ to the
   DE.  This is a matter of plugging in a guess and seeing if it
   works:
   \begin{center}
   \begin{tabular}{ll}
   \toprule
   If $f(t)$ is this: & Try this for $x_p$: \\
   \midrule
   $n$th order polynomial & same order polynomial \\
   & increase order by one if $0$ is a \\
   & \hspace{2pt} root of the CE \\
   & increase order by two if $0$ is a \\
   & \hspace{2pt} double root of the CE \\
   $e^{r t}$ & $C e^{rt}$, for some $C$ \\
   & $C t e^{rt}$, if $r$ is a root of the CE \\
   & $C t^2 e^{rt}$, if $r$ is a double root of the CE \\
   $a e^{\mu t} \cos \nu t + b e^{\mu t} \sin \nu t$ & the same thing
   with different constants \\
   & \hspace{2pt} multiply by $t$ if $\mu + i \nu$ is a root of the CE \\
   \bottomrule
   \end{tabular}
   \end{center}

   \item The general solution of the DE is $x(t) = x_h(t) + x_p(t)$.
   We then use our given initial conditions (usually $x(0)$ and $\dot{x}(0)$)
   to get the final solutions (i.e., to determine $k_1$ and $k_2$).

   \item \emph{Phase Equations and Diagrams.}  In some cases, it is much
   easier to find $\dot{x} (x)$ (i.e., velocity as a function of position)
   than $x(t)$.   The plot of this relationship is called a phase diagram
   (the $\dot{x}, x$ plane is called phase space).  For an undamped oscillator,
   (\ref{eq:SHMDE}) becomes
   \( \D{\dot{x}}{x} = -\omega_0^2 \frac{x}{\dot{x}}, \)
   the solution of which is
   \( \frac{x^2}{A^2} + \frac{\dot{x}^2}{\omega_0^2 A^2} = 1 .\)
   See Marion p. 114 for more.
  \end{enumerate}

 \item \textbf{Nature of Solutions: Unforced Oscillators.}  The most
 general solution for the unforced case is
 \( x(t) = e^{-\beta t} \bracket{k_1 e^{\parenth{\sqrt{\beta^2
  - \omega_0^2}}t} + k_2 e^{-\parenth{\sqrt{\beta^2
  - \omega_0^2}}t}} .\)
  However, we usually write this in a more convenient form,
  depending on the particular case.
 \begin{enumerate}[A.]
  \item \emph{Undamped Case: $\beta = 0$.} In this case, we have pure SHM.  The
  equation of motion is just
  \( x(t) = A \cos (\omega_0 t + \phi) . \)
  $A$ and $\phi$ are determined by the initial conditions.  For $x(0) = x_0$
  and $\dot{x}(0) = v_0$,
  \( A = \sqrt{x_0^2 + \parenth{\frac{v_0}{\omega_0}}^2},
  \eqnsep \phi = \arctan \parenth{\frac{-v_0}{x_0 \omega_0}}. \)

  \item \emph{Underdamped Case: $\beta < \omega_0$.} In this case,
  the solution is:
  \( x(t) = A e^{-\beta t} \cos(\omega_1 t + \phi) , \)
  where
  \( \omega_1 \defn \sqrt{\omega_0^2 - \beta^2} . \)
  Again, $A$ and $\phi$ are determined by initial conditions.  This results in
  oscillations which decrease in amplitude with time according to $e^{-\beta t}$
  as friction dissipates energy.  Notice that the oscillation
  frequency is decreased, i.e., $\omega_1 < \omega_0$.
  \begin{enumerate}[1.]
   \item This energy dissipation shifts the
   maximum amplitude reached during each cycle to before the halfway
   point between equilibrium crossings.  The phase angle of the shift
   $\phi \approx \beta / \omega_1 = 1 / 2 Q$, where $Q$ is the quality
   factor of the oscillator, to be discussed later (note that the "phase
   angle" for a full cycle is just $ 2 \pi$). See CS2A Assignment 6, problem
   2 for more.

   \item The decrement of the motion of an unforced, damped
   oscillator is defined as the ratio of successive maximum
   displacements and is equal to $e^{2 \pi \beta / \omega_1}$.  The
   logarithmic decrement is simply the natural log of this quantity,
   $\delta = 2 \pi \beta / \omega_1 = \pi / Q$, where $Q$ is the quality
   factor of the oscillator.
  \end{enumerate}

  \item \emph{Critically Damped: $\beta = \omega_0$.} The solution in this
  case is
  \( x(t) = k_1 e^{-\beta t} + k_2 t e^{-\beta t} .\)
  There is no oscillation and the oscillator returns to equilibrium as
  quickly as is possible without oscillation. This makes critical damping
  the ideal case for vibration elimination.

  In order to prevent overshoot (i.e., a change in sign of $x(t)$ for $t > 0$),
  we must ensure that $k_1$ and $k_2$ have the same sign.

  \item \emph{Overdamped: $\beta > \omega_0$.} The solution in this case is
  \( x (t) = e^{-\beta t} ( k_1 e^{\omega_2 t} + k_2 e^{-\omega_2 t}) ,
  \)
  where $\omega_2 = \sqrt{\beta^2 - \omega_0^2}$.  There is no
  oscillation and the system returns to equilibrium slowly because the
  friction is excessive.  For an in depth discussion of overdamped
  oscillators, particularly of their phase diagrams, see Marion p. 122.

  By setting  $x(t) = 0$, we find that an overdamped oscillator will pass
  through the equilibrium point, i.e., overshoot, if $k_2 / k_1 < -1$.
 \end{enumerate}

 \item \textbf{Nature of Solutions: Forced Oscillators.}
 \begin{enumerate}[A.]
  \item \emph{Simple Forced, Undamped Case.} Here,
  \( \ddot{x} + \omega_0^2 x = \frac{F_0}{m} \cos \omega t , \)
  where $\omega$ is the angular frequency of the driving force.  The
  solution is
  \( x(t) = \frac{F_0}{m} \frac{1}{\omega_0^2 - \omega^2}\cos \omega t + B
  \cos (\omega_0 t + \phi) , \)
  where $B$ and $\phi$ are determined by initial conditions.
  Notice that the first term is the particular solution and the second
  is the homogeneous solution.

  \item \emph{Simple Forced, Damped Case.}  Here,
  \( \ddot{x} + 2 \beta \dot{x} + \omega_0^2 x
  = \frac{F_0}{m} \cos \omega t . \)
  In this case, the damping quickly eliminates any initial motion
  the oscillator may have had, leaving only the motion resulting
  from the driving force. So we will look only at this part - the
  steady state part - of the solution:
  \(x(t) = A \cos (\omega t + \phi), \)
  with
  \( \label{eq:lineshape} A = \frac{F_0/m}{ \sqrt{(\omega_0^2 - \omega^2)^2
   + 4 \omega^2 \beta^2}} \)
  and
  \( \phi = \arctan \parenth{\frac{2 \omega \beta}{\omega_0^2 -
  \omega^2}} . \)
  Note that $A$ and $\phi$ are given (unlike in the undamped cases)
  because the motion in the forced damped case does not depend on
  initial conditions, only the nature of the oscillator and the driving
  force.

  It is worth noting that this result is the real part of the
  solution
  \[ z(t) = \frac{F_0/m}{(\omega_0^2 - \omega^2) + i 2 \beta \omega}
   e^{i \omega t} \]
  to $\ddot{z} + 2 \beta \dot{z} + \omega_0^2 z
   = (F_0/m) e^{i \omega t}$.

  \item \emph{Resonance.}  When the frequency of the driving force being
  applied to a system is near its natural frequency $\omega_0 =
  \sqrt{k/m}$, resonance occurs as the oscillator stores up energy from
  the driving force over time, resulting in very large amplitude
  oscillations.
  \begin{enumerate}[1.]
   \item In the undamped case, this occurs when the driving frequency
   $\omega$ is near $\omega_0$.  As $\omega \To \omega_0$, the amplitude
   of oscillation approaches infinity. Physically, the amplitude quickly
   increases until the system is destroyed. When $\omega = \omega_0$, a
   different solution of the DE is needed.  Also note that
   when $\omega < \omega_0$, the oscillations are in phase with the
   forcing while they are out of phase when $\omega > \omega_0$.

   \item In the damped case, friction prevents the oscillations from reaching
   infinite amplitude. Because friction alters the natural frequency of
   the system, the resonance frequency (for maximum amplitude) is $\omega_R =
   \sqrt{\omega_0^2 - 2 \beta^2}$.  Note that this less than the natural
   frequency of the system, which is $\omega_1 = \sqrt{\omega_0^2 - \beta^2}$.

   \item \emph{Lorentzian Approximation.} Near resonance,
   $\abs{\omega_0 - \omega}/\omega_0 \ll 1$ and (\ref{eq:lineshape})
   can be expanded in powers of $(\omega_0 - \omega)/\omega_0$, keeping
   only the lowest order term (zeroth order in this case).  The
   result is a Lorentzian curve:
   \( \label{eq:Lorentzian} A = \frac{F_0/m}{2 \omega_0
    \sqrt{(\omega_0 - \omega)^2 + \beta^2}}. \)
   We shall make use of this result when we discuss energy below.
  \end{enumerate}
 \end{enumerate}

 \item \textbf{Green's Function Method for Solving Inhomogeneous DEs.}
 The Green's function method allows us to find the solution to a very
 general inhomogeneous DE if we can find the solution to the related
 homogenous DE.  This is extremely useful for harmonic oscillators
 with complicated forcing functions.

 \begin{enumerate}[A.]
 \item \emph{Motivation.} Let $L_t$ be a linear, self-adjoint
 differential operator in the variable $t$ and consider the
 differential equation $L_t f(t) = F(t)$.  The solution to this
 equation is
 \begin{align}
   f(t) &= L_t^{-1} F(t) \notag \\
    &= L_t^{-1} \int F(t') \delta(t-t') dt' \notag \\
    &= \int F(t') L_t^{-1} \delta(t-t') dt' \notag \\
    &= \int F(t') G(t,t') dt',
 \end{align}
 where
 \( G(t,t') = L_t^{-1} \delta(t-t') \)
 is called the Green's function (or, in engineering fields, the impulse
 response function).  Thus, if we can find the
 solution $G(t,t')$ to $L_t G(t,t') = \delta(t-t')$, we need only
 perform an integral to obtain the solution to $L_t f(t) = F(t)$. The
 basic procedure for applying this method is as follows.

 \item \emph{Procedure.}
 \begin{enumerate}[1.]
  \item Determine the related homogenous DE and set it equal to
  $\delta(t-t')$ (the Dirac delta function); that is, make it equal to
  the delta function instead
  of zero.  Solve.  This just gives two homogenous solutions, one for
  $t<t'$ and another for $t>t'$ (as the delta function is just zero for
  $t \neq t'$). We write the undetermined coefficients in the solutions
  as functions of $t'$, e.g. $A(t')$.  Our Green's function now looks
  like this:
  \[ G(t,t') = \begin{cases} f_1(t,t') & t < t' , \\
    f_2(t,t') & t > t' . \end{cases} \]
   Note that the functions $f$ themselves depend on $t$,
   while their coefficients are functions of $t'$, as stated above.

  \item Next, we apply the initial conditions for our problem to our Green's
  function $G(t,t')$.  In doing this, we usually assume that $t'>0$ and
  therefore apply the initial conditions to $f_1(t,t')$. This gives us
  the coefficients for $f_1$.

  \item Now, we enforce continuity
  at $t = t'$ by setting $f_1(t=t') = f_2(t=t')$ (which is made very easy
  if $f_1(t=t')=0$.  Then we enforce differentiability by setting
  \[ \At{\D{f_1}{t}}{t=t'} - \At{\D{f_2}{t}}{t=t'} = 1 .\]
  This gives us two equations and allows us to find all the coefficients.

  \item Given our Green's function and the inhomogeneous part
  of the DE, which we will call $F(t)$, our steady state solution
  is
  \[ x(t) = \int_{-\infty}^t G(t,t') F(t') dt' .\]
  Note that the limits should be adjusted so that we only integrate
  over the range of times when both $G(t,t')$ and $f(t)$ are nonzero.
  Also note that even if the upper limit is not equal to $t$ (i.e., $f(t)$
  has gone to zero at some $\tau < t$ ), we will still get a $t$
  dependance because $G(t,t')$ depends on $t$.
 \end{enumerate}
 \end{enumerate}

 \item \textbf{Energy, $Q$, and Resonance.}
 \begin{enumerate}[A.]
  \item \emph{Undamped, Unforced Case.} The energy
  \( E = E_0 = K+U = \frac{1}{2}mv^2 + \frac{1}{2} kx^2 \)
  is constant and equal to the potential energy at the extremes of motion
  where $K=0$, so $E_0 = \frac{1}{2} k A^2$.

  Note that the time averages of the K.E. and P.E. over a cycle are the same:
  \( \langle K \rangle = \langle U \rangle = \frac{1}{4} k A^2 . \)
  On the other hand, the space averages of kinetic and potential energy
  over a cycle are not the same:
  \( \bar{U} = \frac{1}{6} k A^2; \eqnsep \bar{K}
  = \frac{1}{3} k A^2 .\)

  \item \emph{Damped, Unforced Case.}  In this case, the
  energy decreases exponentially in time according to
  \( \label{eq:EnergyDecay} E(t) = E_0 e^{-2\beta t} , \)
  where $E_0$ is the initial energy of the system.  We refer to
  $1/2\beta$ as the damping time.

  \item \emph{Damped, Forced Case.}  For this case,
  the time averages of energy are
  \( \langle U \rangle = \frac{1}{4} k A^2 ; \eqnsep
   \langle K \rangle = \frac{1}{4} m \omega^2 A^2 .\)
  Notice that here $\omega_0$ is replaced by $\omega$. This gives
  for the total energy:
  \(  \label{eq:EnergyFull} \langle E \rangle
   = \frac{1}{4} m A^2 (\omega^2 + \omega_0^2) . \)
  It is worth noting that the kinetic energy resonance (i.e.
  the maximum average kinetic energy) of the system occurs when $\omega
  = \omega_0$, the natural frequency of the system without damping,
  while the potential energy resonance occurs at $\omega =
  \sqrt{\omega_0^2 - 2 \beta^2}$, the same as the amplitude resonance
  (as the potential energy is proportional to the square of the amplitude).
  See Marion p.~130 and K\&K p.~426 for more.

  Near resonance, we can use the Lorentzian lineshape
  (\ref{eq:Lorentzian}) to obtain for the total energy
  \( \label{eq:EnergyLorentzian}
   \langle E \rangle = \frac{1}{8} \frac{F_0^2}{m}
   \frac{1}{(\omega_0 - \omega)^2 + \beta^2}. \)
  Here, we have used $(\omega^2 + \omega_0^2) \approx 2 \omega_0^2$
  in (\ref{eq:EnergyFull}).

  \item \emph{The Quality Factor $Q$.}  For any oscillatory system,
  $Q$ is defined as:
  \( Q = \frac{\textrm{energy stored in system}}{\textrm{energy dissipated
  per radian}} .\)
  The energy dissipated per radian is simply the energy
  dissipated per cycle divided by $2 \pi$. For a harmonic oscillator
  at frequency $\omega_1$ we have, using (\ref{eq:EnergyDecay}),
  \[ \frac{\Delta E}{\textrm{rad}} = E (1 - e^{-2\beta/\omega_1})
   \approx \frac{2 \beta E}{\omega_1} , \]
  so
  \( Q = \frac{\omega_1}{2 \beta} .\)
  There are a multitude of expressions for $Q$, most of which involve
  approximations for light damping (i.e., small $\beta$ ).
  \begin{enumerate}[1.]
   \item Because $\omega_1 \approx \omega_0 \approx \omega_R$ for light
   damping,
   \( Q = \frac{\omega_1}{2 \beta} \approx
   \frac{\omega_0}{2 \beta} \approx \frac{\omega_R}{2 \beta} . \)

   \item We can also express $Q$ in terms of the width of the resonance
   curves:
   \( \label{eq:Q2} Q \approx \frac{\omega_0}{\Delta \omega}, \)
   where $\Delta \omega$ is the full width of the energy resonance
   curve at half its maximum height (equivalently, the full width
   of the amplitude resonance curve at $1/\sqrt{2}$ of its maximum
   height).  Using the Lorentzian approximation
   (\ref{eq:EnergyLorentzian}),
   \[ \Delta \omega = 2 \beta, \]
   justifying our claim (\ref{eq:Q2}). See See K\&K 10.3, p.~427 for
   more discussion.
  \end{enumerate}

  \item \emph{Resonance and Q.} The expression $Q = \omega_0
  / \Delta \omega$ implies that the greater the damping in a system,
  the wider the resonance curve. This means the system will respond to
  a greater range of frequencies with resonance (but with less amplitude
  for resonance).  As such, $Q$ characterizes the frequency response of
  a system.

  Recall that for an unforced, damped oscillator, $E(t) = E_0 e^{-2\beta
  t}$.  The damping ($e$-folding) time $\tau$ is defined such that
  $E(\tau) = E_0 / e$, so $\tau = 1/2\beta$. With $\Delta \omega
  = 2 \beta$, we have $\tau \Delta \omega = 1$.  This is the statement
  that the resonance width and the damping time for an oscillator cannot
  be determined independently.  That is, a system which is very selective
  in its frequency response for resonance (small $\Delta \omega$) will
  necessarily oscillate for a long time when disturbed, while a system
  whose oscillations die out quickly will respond to a wider range of
  driving frequencies. This is the classical equivalent of the
  time-energy uncertainty relation in quantum mechanics.
 \end{enumerate}
\end{enumerate}

\end{document}
