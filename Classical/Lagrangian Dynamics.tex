% ----------------------------------------------------------------
% First Try ***********************************************
% **** -----------------------------------------------------------
\documentclass{article}
\usepackage{enumerate}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
% MATH -----------------------------------------------------------
\newcommand{\norm}[1]{\left\Vert#1\right\Vert}
\newcommand{\abs}[1]{\left\vert#1\right\vert}
\newcommand{\set}[1]{\left\{#1\right\}}
\newcommand{\Real}{\mathbb R}
\newcommand{\eps}{\varepsilon}
\newcommand{\To}{\longrightarrow}
\newcommand{\BX}{\mathbf{B}(X)}
\newcommand{\A}{\mathcal{A}}
\newcommand{\PD}[2]{\frac{\partial #1}{\partial #2}}
% ----------------------------------------------------------------
\begin{document}
% \maketitle
% ----------------------------------------------------------------
\begin{enumerate}[I.]
  \item \textbf{The Calculus of Variations}
   \begin{enumerate}[A.]
     \item The basic problem of the calculus of variations is to
     find the function $f$ which minimizes the value of the integral
     \begin{equation}\label{JInt}
      J=\int_{x_1}^{x_2} f(y(x),y'(x);x)dx
     \end{equation}
     where $y'(x)=\frac{dy}{dx}$
     and $y(x)$ is called the functional (it is a path from $x_1$ to
     $x_2$).  This is varied until an extremum of the integral is found.
     \item The Derivation of Euler's equation.
       \begin{enumerate}[1.]
        \item We first write $y(\alpha,x)=y(0,x)+\alpha\eta(x)$,
        where $y(0,x)$ is defined as the function which yields the
        extremum of $J$, and $\eta(x)$ is some deviation, which
        disappears at $x_1$ and $x_2$.  This yields
        \[J=\int_{x_1}^{x_2} f(y(\alpha,x),y'(\alpha,x);x)dx\] and we
        have that $\frac{\partial J}{\partial \alpha}|_{\alpha=0} = 0$
        is a necessary condition for an extremum of $J$ to exist.

        \item We preform the above differentiation of $J$ and pull the
        derivative inside the integral (as the limits are constants
        and hence unaffected by it).  This gives: \[\PD{J}{\alpha}=
        \int_{x_1}^{x_2} \PD{f}{y} \PD{y}{\alpha} + \PD{f}{y'} \PD{y'}
        {\alpha}dx\]
        Next we plug in $\PD{y}{\alpha}=\eta(x)$ and $\PD{y'}{\alpha}
        =\PD{\eta}{x}$ from the first equation in a above. This gives:
        \[\PD{J}{\alpha}= \int_{x_1}^{x_2} \PD{f}{y} \eta(x)
        + \PD{f}{y'} \PD{\eta}{x}dx\]

        \item Integrating the second term in the above result by
        parts gives: \[\int_{x_1}^{x_2} \PD{f}{y'} \PD{\eta}{x}dx =
        \PD{f}{y'} \eta(x)\Big|_{x_1}^{x_2}-\int_{x_1}^{x_2}\frac{d}{dx}
        \Big(\PD{f}{y'}\Big) \eta(x)dx\]  Note that the first term
        disappears because $\eta(x)=0$ at $x_1$ and $x_2$.
        Combining this with the rest of the integral, we have:
        \[\PD{J}{\alpha}= \int_{x_1}^{x_2} \left(\PD{f}{y}
        - \frac{d}{dx}\PD{f}{y'}\right) \eta(x)dx\]

        \item Now, because this integral must vanish at $\alpha=0$
        for any arbitrary $\eta(x)$, the rest of the integrand must
        vanish, giving us \emph{Euler's Equation}:
        \begin{equation}
         \PD{f}{y} - \frac{d}{dx}\PD{f}{y'}=0
        \end{equation}
        Solving this differential equation gives us the function
        $f$ which minimizes the integral (\ref{JInt}).
       \end{enumerate}
     \item \emph{Second form of Euler's equation.}  We can get another,
     equivalent form of Euler's equation by writing $\frac{df}{dx}$
     and comparing terms to the first form of Euler's equation.  This
     gives us (see Marion p. 223):
     \begin{equation}
         \PD{f}{x} - \frac{d}{dx}\left(f-y'\PD{f}{y'}\right)=0
     \end{equation}
     This is most useful if $\PD{f}{x}=0$ in which case
     $f-y'\PD{f}{y'}=constant$.

    \item \emph{Euler's Equation with Multiple Dependent
    Variables.} For some function
    $f=f(y_1(x),y'_1(x),y_2(x),y'_2(x),\ldots;x)$, we write, as
    above, $y_i(\alpha,x)=y_i(0,x)+\alpha \eta_i(x)$.  Following
    the above derivation, we obtain: \[\PD{J}{\alpha}=
    \int_{x_1}^{x_2} \sum_i \left(\PD{f}{y_i}
        - \frac{d}{dx}\PD{f}{y'_i}\right) \eta_i(x)dx\]
    Euler's Equations for multiple variables follows immediately:
    \begin{equation}
         \PD{f}{y_i} -
         \frac{d}{dx}\PD{f}{y'_i}=0\qquad i=1,2,\ldots,n
    \end{equation}

    \item \emph{Euler's Equation with Auxiliary Conditions
    (Constraints).} In the most general case of $m$ dependent
    variables, and $n$ constraints, given by $g_j(y_i;x)=0$,
    we have the following system of equations (see Marion 6.6 for details):
    \begin{equation}
         \PD{f}{y_i} -
         \frac{d}{dx}\PD{f}{y'_i} + \sum_{j=1}^n \lambda_j(x)
         \PD{g_j}{y_i}=0\qquad i=1,2,\ldots,m
    \end{equation}
    \begin{equation}
         g_j(y_i;x)=0\qquad j=1,2,\ldots,n
    \end{equation}
    Where the $\lambda_i(x)$ are called Lagrange undetermined
    multipliers.  Note that this system contains $n+m$ unknowns,
    $n$ of which are undetermined multipliers, and $n+m$
    equations, $n$ of which are constraint equations.

    \item \emph{Using Euler's Equation.}  The basic procedure for
    utilizing Euler's equation is to determine the integral for
    the quantity we with to maximize or minimize, then determine
    the functional and apply Euler's equation.  EXAMPLE!
   \end{enumerate}
  \item \textbf{Lagrangian Dynamics}
   \begin{enumerate}[A.]
    \item \emph{Hamilton's Principle and Lagrange's Equations of motion.}
    The cornerstone of Lagrangian dynamics, in fact of most of classical
    physics as we know it today, is Hamilton's Principle, which can be
    stated as:
    \begin{quote}
    A dynamical system, in moving from one state to another in a
    specified time (consistent with any constraints), follows the path
    which minimizes the time integral of the difference between kinetic
    and potential energy.
    \end{quote}
    \begin{enumerate}[1.]
     \item Mathematically, this means our task is to find the minimum of
     \[ \int_{t_1}^{t_2} (K-U)dt \]
     where the $K = K(\dot{x}_1,\dot{x}_2,\ldots,\dot{x}_n)$ is the kinetic
     energy and $U = U(x_1,x_2,\ldots,x_n)$ is the potential
     energy. Note the $x_1,x_2,\ldots,x_n$ are our spatial
     coordinates.

     \item Applying Euler's equation to this problem, we obtain
     Lagrange's equations of motion:
     \begin{equation}
      \PD{L}{x_i} -
       \frac{d}{dt}\PD{L}{\dot{x}_i}=0\qquad i=1,2,\ldots,n
     \end{equation}
     where $L=K-U$ is called the Lagrangian. Solving this system
     of differential equations gives us the path of the system as
     $x_1(t),x_2(t),\ldots,x_n(t)$.
    \end{enumerate}
    \item \emph{Lagrange's Equations in Generalized Coordinates.}
    \begin{enumerate}[1.]
     \item \emph{Degrees of Freedom.} Consider a system composed
     of $n$ point particles, each described by three spatial
     coordinates, and bound by $m$ equations of constraint.  In
     this system $3n-m$ coordinates are independent and $s=3n-m$
     is said to be the number of degrees of freedom in the system.

     \item \emph{Generalized Coordinates.} Any set of parameters
     (with any dimensions) which completely describes the state of
     a system is called a set of generalized coordinates.  If the
     set consists of $s$ unconstrained parameters, it is called a
     proper set of generalized coordinates.

     \item Let us write the Lagrangian in some set
     $q_1,q_2,\ldots,q_s$ of generalized coordinates. Now the
     Lagrangian is a scalar quantity and is therefore invariant
     under a coordinate transformation, giving us:
     \begin{equation}
     L=K(q_i,\dot{q}_i,t)-U(q_i,t)\qquad i=1,2,\ldots,s
     \end{equation}
     The problem is again to minimize the time integral of the
     function.  Applying Euler's equation gives us Lagrange's
     equations of motion in their most general form:
     \begin{equation}
      \PD{L}{q_i} -
       \frac{d}{dt}\PD{L}{\dot{q}_i}=0\qquad i=1,2,\ldots,s
     \end{equation}
     This set of $s$ second order differential equations
     (requiring $2s$ initial conditions) together with our $m$
     equations of constraint, which must be of the form
     $f_j(q_i,t)=0$, completely specify the motion of the system.

     \item In the case that the $m$ equations of constraint cannot be
     reduced to the form given above (e.g., they depend on
     $\dot{q}_i$), or we wish to use a non-proper set of $n$
     generalized coordinates, we use undetermined multipliers and
     obtain the following equations of motion:
     \begin{equation}
         \PD{L}{q_i} -
         \frac{d}{dx}\PD{L}{\dot{q}_i} + \sum_{j=1}^m \lambda_j(t)
         \PD{f_j}{q_i}=0\qquad i=1,2,\ldots,n
     \end{equation}
     The most important aspect of this equation is the physical
     significance of the $\lambda_j(t)$.  They are in fact the
     forces of constraint.  The generalized forces of constraint
     $Q_i$ are given by:
     \begin{equation}
      Q_i=\sum_{j=1}^m \lambda_j(t) \PD{f_j}{q_i}
     \end{equation}
    \end{enumerate}

    \item \emph{Theorems Concerning Kinetic Energy.} In the
    special case that the equations linking the rectangular
    coordinates for a system with the generalized coordinates
    $x_k=x_k(q_i,t)$ are independent of time, we have (see Marion
    7.8):
    \begin{equation}
      K=\sum_{i,j} a_{ij} \dot{q}_i \dot{q}_j
    \end{equation}
    Where \[ a_{ij} = \frac{1}{2}m \sum_k \PD{x_k}{q_i}
    \PD{x_k}{q_j} \]
    Proceeding further, we obtain:
    \begin{equation}
      K=\frac{1}{2}\sum_i \dot{q}_i \PD{K}{\dot{q}_i}
    \end{equation}
   \end{enumerate}
  \item \textbf{Hamiltonian Dynamics}
   \begin{enumerate} [A.]
    \item \emph{Generalized Momentum.} It can be shown (see Marion
    7.9) that if the potential energy is independent of velocity,
    then the components of linear momentum in rectangular
    coordinates are given by
    \begin{equation}
     p_i=\PD{L}{\dot{x}_i}
    \end{equation}
    In analogy, we can define a set of generalized momenta in
    generalized coordinates as
    \begin{equation}
     p_i=\PD{L}{\dot{q}_i}
    \end{equation}
    Lagrange's equations of motion can then be simply expressed
    as:
    \begin{equation}
     \dot{p}_i=\PD{L}{q_i}
    \end{equation}

    \item \emph{The Hamiltonian.}  Let us define the following
    quantity, called the Hamiltonian of a system:
    \begin{equation}
     H(q_i,p_i,t)=\sum_j p_j \dot{q}_j - L(q_i,\dot(q)_i,t)
    \end{equation}

    \item \emph{Hamilton's Equations of Motion.}  By writing the
    total differential $dH$, we can arrive at Hamilton's equations
    of motion:
    \begin{equation}
     \dot{q}_i=\PD{H}{p_i}
    \end{equation}
    \begin{equation}
     -p_i=\PD{H}{q_i}
    \end{equation}
    We also find that
    \begin{equation}
     -\PD{L}{t}=\PD{H}{t}=\frac{dH}{dt}
    \end{equation}
    These equations form a set of $2s$ first order differential
    equations, replacing the $s$ second order differential
    equations produced by the Lagrangian.

    \item \emph{Physical Meaning of the Hamiltonian.} If the
    potential energy of a system is independent of velocity and
    time (i.e., all forces acting on the system are conservative)
    and the equations linking the generalized coordinates to the
    rectangular coordinates are independent of time, we find
    (Marion 7.9) that:
    \begin{equation}
     H=K+U=E
    \end{equation}
    In other words, the Hamiltonian is just the total energy of
    the system!
   \end{enumerate}
\end{enumerate}

\end{document}
% ----------------------------------------------------------------
