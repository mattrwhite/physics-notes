% ----------------------------------------------------------------
% Central Force Motion and Orbits ********************************
% ----------------------------------------------------------------
\documentclass{article}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{booktabs}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
\reversemarginpar
% MATH -----------------------------------------------------------
\input{../Include/Essentials.tex}
\input{../Include/Vectors.tex}
\newcommand{\Ueff}{U_\textrm{eff}}
\newcommand{\rmin}{r_\textrm{min}}
\newcommand{\rmax}{r_\textrm{max}}
% ----------------------------------------------------------------
% \draftmode
\begin{document}
% \maketitle
% ----------------------------------------------------------------
\begin{enumerate}[I.]
 \item \textbf{The General Two Body Central Force Problem.}
 \begin{enumerate}[A.]
  \item \emph{Reduction to a Single Body Problem.} If the positions
  of the two bodies are given by $\vect{r}_1$ and $\vect{r}_2$,
  then the central force acts along the relative position vector
  $\vect{r} = \vect{r}_2 - \vect{r}_1$ (from $\vect{r}_1$ to
  $\vect{r}_2$). When we work out the details (see K\&K Ch. 9, or
  Marion 8.2) we find that the motion of the bodies can be described by
  \( \label{eq:CFM1} \mu \ddot{\vect{r}} = F(r) \unitvect{r} ,\)
  where
  \( \mu = \frac{m_1 m_2}{m_1 + m_2} \)
  is the reduced mass and $F(r)$ is the magnitude of the central force
  (acting on one of the bodies) at distance $r$ from its source.  This
  effectively reduces the problem to one of a single body of mass $\mu$
  moving under the influence of a central force emanating from the
  origin.

  It is immediately apparent that for any central force, the motion
  must be confined to a plane.  The trajectory of an object moving
  under the influence of a central force is called an orbit.

  \item \emph{Equations of Motion.}  Using polar coordinates in the
  plane of motion, (\ref{eq:CFM1}) becomes
  \begin{subequations}
  \begin{gather}
   \mu (\ddot{r} - r \dot{\theta}^2) = F(r) , \\
   \mu (r \ddot{\theta} + 2 \dot{r} \dot{\theta}) = 0 .
  \end{gather}
  \end{subequations}
  The first equation is radial component of acceleration, the second
  is the tangential component.

  \item \emph{Angular Momentum.}  For any central force, angular momentum
  must be conserved. We can see this by fixing the source of the central
  force at the origin; then $\vect{\tau} = \dot{\vect{L}} = 0$ because $\vect{r} \|
  \vect{F}$ everywhere.  More explicitly,
  \begin{align}
   \vect{L} &= \crosspd{r}{p} \notag \\
   &= \mu \bracket{\vect{r} \times \parenth{\dot{r}\unitvect{r}
    + r \dot{\theta} \unitvect{\theta}}} \notag \\
   &= \mu r^2 \dot{\theta} \parenth{\unitvect{r} \times
   \unitvect{\theta}}.
  \end{align}
  The (constant) magnitude of the angular momentum is
  \( l = \mu r^2 \dot{\theta} . \)
  Recall that $\dot{\theta} = \omega$ is just the angular velocity.
  We can also write this as $l = \mu r v_\theta$, where $v_\theta$ is the
  tangential velocity $r \dot{\theta}$.  That is, $l$ is just the
  radius times the component of velocity perpendicular to the radius.

  \item \emph{Areal Velocity - Kepler's Second Law.} In a short time
  interval $dt$, the radius vector $\vect{r}$ sweeps out a small triangle
  with area $da = \frac{1}{2} r^2 d\theta$.  Dividing by $dt$ gives us
  \( \label{eq:ArealV} \D{a}{t} = \frac{1}{2} r^2 \D{\theta}{t} = \frac{l}{2 \mu} =
  \const . \)
  The radial vector always sweeps out equal areas in equal times for
  any central force.

  \item \emph{Energy.}  As we are considering closed systems and
  conservative forces, energy is constant. Generally, $E = \frac{1}{2}
  \mu v^2 + U(r)$, where $U(r)$ is the usual potential energy.
  For the case of central force motion, we can use $l = \mu r^2
  \dot{\theta}$ to eliminate $\dot{\theta}$, giving
  \begin{namedeqn}{\textbf{Energy for Central Force Motion.}}
   E = \frac{1}{2} \mu \dot{r}^2 + \frac{l}{2 \mu r^2} + U(r) .
  \end{namedeqn}
  The first term is the kinetic energy due to radial motion.  The
  second term is often referred to as the centrifugal potential
  (the fictional centrifugal force associated with it is $\mu r
  \dot{\theta}^2$). This is often grouped with the true potential
  to give the effective potential:
  \( \Ueff(r) = \frac{l}{2 \mu r^2} + U(r) . \)
  Then $E = \frac{1}{2} \mu \dot{r}^2 + \Ueff(r)$.

  \item \emph{General Equations of Motion.} By solving the
  above expression for energy for $\dot{r}$, we obtain
  \( \dot{r} = \pm \sqrt{\frac{2}{\mu} \bracket{E - \Ueff(r)}} \)
  which can be integrated to find $r(t)$.

  Writing $d\theta = (\dot{\theta}/\dot{r})dr$ and using
  $\dot{\theta} = l/\mu r^2$ and the above expression for $\dot{r}$,
  we get
  \( \label{eq:ThetaR} \theta(r) = \int \frac{\pm l}{r^2 \sqrt{2 \mu (E-\Ueff)}} dr . \)

  Lagrangian dynamics can give us a very powerful differential
  equation relating $r$ and $\theta$ describing motion in a central
  force (see Marion 8.4).  It is:
  \( \DD{u}{\theta} + u = -\frac{\mu}{l^2} \frac{1}{u^2}
  F\parenth{\frac{1}{u}} , \)
  where $u = 1/r$.  This equation is more easily solved for $u$
  rather than $r$.

  From the expression $E = \frac{1}{2} \mu v^2 - U(r)$ (note that we
  are not using $\Ueff$ here), we get
  \( v = \sqrt{\frac{2}{\mu} [E - U(r)]} . \)
  Conservation of angular momentum gives us $r v_\theta = \const$, which
  allows us determine $v$ whenever $\dot{r} = 0$ .

  \item \emph{Stability of Circular Orbits.} (see Marion 8.10)
  The conditions for stability for any circular orbit of radius $r_0$
  are
  \[ \At{\PD{\Ueff}{r}}{r = r_0} = 0 , \eqnsep \At{\PDD{\Ueff}{r}}{r
  = r_0} > 0 .\]
  For a central force of the form $F(r) = -C / r^n$, these give us the
  requirement
  \[ (3-n) \frac{l^2}{\mu} > 0 .\]
  The more general case can be approached by considering the frequency
  of small oscillations about a circular orbit.  This gives us (using
  a prime to denote differentiation \wrt $r$):
  \[ \frac{F'(r_0)}{F(r_0)} + \frac{3}{r_0} > 0 \]
  as the stability condition. Using $x$ to represent the amplitude of
  oscillations about a circular orbit, i.e., about $r = r_0$, we have $\ddot{x}
  + \omega_0^2 x = 0$, where
  \[ \omega_0^2 = \frac{3 g(r_0)}{r_0} + g'(r_0) , \]
  with $g(r) = - F(r) / \mu$.

 \end{enumerate}

 \item \textbf{Inverse Square Force.}
 \begin{enumerate}[A.]
  \item \emph{Effective Potential.} For a force $F(r) = - C/r^2$, the
  potential is $U(r) = - C /r$, so the effective potential is
  \( \Ueff(r) = \frac{l^2}{2 \mu r} - \frac{C}{r} .\)
  For gravity, $C = G m_1 m_2$ and for the electrostatic force $C = -q_1
  q_2 / 4 \pi \epsilon_0$.

  \item \emph{Equation of Motion.} Plugging the
  effective potential into the expression (\ref{eq:ThetaR})
  for $\theta(r)$ and evaluating gives us
  \( \label{eq:GenOrbit} r(\theta) = \frac{r_0}{1 + \epsilon \cos \theta} ,\)
  where
  \( r_0 = \frac{l^2}{\mu C} \)
  is the average radius of the orbit and
  \( \epsilon = \pm \sqrt{1 + \frac{2 E l^2}{\mu C^2}} \)
  is the called the eccentricity of the orbit.  Unless otherwise noted,
  we take $\epsilon > 0$.

  \item \emph{Types of Orbits.} The expression (\ref{eq:GenOrbit})
  is the equation of a conic section in polar coordinates.  We can best
  analyze it in conjunction with an energy diagram.  For a given $l$,
  we can have any level of total energy greater than or equal to $\Ueff$.
  There are four general cases:
  \begin{enumerate}[1.]
   \item \emph{Circle.}  The minimum possible energy for a given $l$
   \[ E_\textrm{min} = -\frac{\mu C^2}{2 l^2} \]
   corresponds to a circular orbit: $\epsilon = 0$.

   \item \emph{Ellipse.}  For energies in the range
   \[ -\frac{\mu C^2}{2 l^2} < E_1 < 0 , \]
   so $0 < \epsilon < 1$.  Any orbital radius between $\rmin$
   and $\rmax$ (see below) is allowed, due to the
   additional kinetic energy.

   \item \emph{Parabola.}  In this case,
   \[ E_0 = 0 \]
   so $ \epsilon = 1$ and $r_\textrm{max} \To \infty$.  That is, $E_0$
   is the minimum energy needed to escape to infinity.

   \item \emph{Hyperbola.}  Here, we have more than enough energy to escape
   to infinity:
   \[ E_2 > 0 ,\]
   so $\epsilon > 1$.
  \end{enumerate}

  \item \emph{Elliptical Orbits.} For the following discussion, we
  place one of the focuses of the ellipse at the origin.  From
  (\ref{eq:GenOrbit}), it is immediately apparent that the minimum
  radius of the orbit (called the periastron or pericenter), is
  \[ \rmin = \frac{r_0}{1+\epsilon} .\]
  The maximum radius of the orbit (called the apiastron or apocenter)
  is
  \[ \rmax = \frac{r_0}{1-\epsilon} .\]
  Conservation of energy implies that the body should have the least
  kinetic energy at $\rmax$, i.e., be moving the slowest there, and have
  the most at $\rmin$.  As we would expect, $\dot{r} = 0$ at $\rmin$ and
  $\rmax$; they are the turnaround points.

  \item \emph{Axes of an Elliptical Orbit.} The semimajor axis of an
  elliptical orbit,
  \( a = \frac{r_0}{1 - \epsilon^2} = \frac{C}{2 \abs{E}} ,\)
  is the distance from the center of the ellipse to
  the two points furthest from the center, namely the apsides $\rmin$
  and $\rmax$ (which are measured from a focus, not the center). The major
  axis $A = 2a$ is just the distance between the apsides .

  Because the distance between the center of an ellipse and a focus is
  $a \epsilon$, we find that $\rmin = a(1-\epsilon)$ and $\rmax
  = a(1+\epsilon)$.  Note that this gives us another expression for
  the eccentricity of the orbit:
  \[ \epsilon = \frac{\rmax - \rmin}{A} .\]
  We can use this together with the other expression for $\epsilon$ to
  further solve the orbit.

  The semiminor axis,
  \( b = \frac{r_0}{\sqrt{1 - \epsilon^2}} = \sqrt{a r_0}
   = \frac{l}{\sqrt{2 \mu \abs{E}}} ,\)
  is the distance from the center of the ellipse to the points closest
  to the center.  The minor axis is $B = 2b$.

  \item \emph{Orbital Period: Kepler's Third Law.} The period of the
  orbit $T$ (the time it takes to complete one orbit)
  can be found by integrating the expression for areal velocity
  (\ref{eq:ArealV}) to give
  \[ T = \frac{2 \mu}{l} \mathcal{A} , \]
  where $\mathcal{A}$ the area inside the orbital path (this is
  actually a general result for any closed orbit in a central force).
  Then using the expressions for the axes $a$ and $b$ (as the area
  of an ellipse is $\mathcal{A} = \pi a b$), we find that the period is
  \[ T = \pi C \sqrt{\frac{\mu}{2}} \abs{E}^(-3/2) , \]
  or in a more familiar form,
  \begin{namedeqn}{\textbf{Kepler's Third Law}}
   T^2 = \frac{4 \pi^2 \mu}{C} a^3
  \end{namedeqn}

  \item \emph{Orbital Maneuvers.}  We will consider changes in orbits due
  to forces acting only in the plane of the orbit.  See Marion 8.8 for an
  excellent example.

  Changes in energy without changes in angular momentum are accomplished by
  forces acting along the radial line of the object's orbit, resulting
  in zero torque, as $\vect{r} \| \vect{F}$.  This will increase the
  energy of the object's orbit and therefore its eccentricity.  It is
  equivalent to rasing the energy line on the plot of $\Ueff$.

  The component of any force in the tangential direction of the orbit
  results in a change in both angular momentum (as there is a net
  torque) and energy. We need only to remember that $\vect{\tau} =
  \dot{\vect{L}}$ and that we can split any force in the orbital plane
  into radial and tangential components, so
  \( \tau = r F_\bot = \D{l}{t} . \)

  Finally, remember that $W = \Delta K$ (we usually ignore changes in
  gravitational potential energy over the time that the force is
  applied).
 \end{enumerate}
\end{enumerate}

\end{document}
