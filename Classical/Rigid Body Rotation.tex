% ----------------------------------------------------------------
% First Try ***********************************************
% **** -----------------------------------------------------------
\documentclass{article}
\usepackage{enumerate}
\usepackage{amsmath}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
% MATH -----------------------------------------------------------
\newcommand{\norm}[1]{\left\Vert#1\right\Vert}
\newcommand{\abs}[1]{\left\vert#1\right\vert}
\newcommand{\set}[1]{\left\{#1\right\}}
\newcommand{\Real}{\mathbb R}
\newcommand{\eps}{\varepsilon}
\newcommand{\To}{\longrightarrow}
\newcommand{\BX}{\mathbf{B}(X)}
\newcommand{\A}{\mathcal{A}}
\newcommand{\PD}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\vect}[1]{\boldsymbol{#1}}
% ----------------------------------------------------------------
\begin{document}
% \maketitle
% ----------------------------------------------------------------
\begin{enumerate}[I.]
  \item \textbf{Rotational Kinematics}
   \begin{itemize}
    \item Angular Displacement: $\theta$ - angle through which the
    object has rotated.  $\theta=2\pi$ represents one full
    rotation.  As a vector $\vect{\theta}$ points along the axis
    of rotation in the direction given by the right hand rule.

    \item Angular velocity:
    $\vect{\omega}=\frac{d\vect{\theta}}{dt}$

    \item Angular acceleration:
    $\alpha=\frac{d\omega}{dt}=\frac{d^2\theta}{dt^2}$
   \end{itemize}
   \begin{enumerate}[A.]
    \item These quantities are the equivalents of the linear
    displacement, velocity, and acceleration, and many of the same
    kinematical equations apply, e.g. $\Delta\theta=\int_{t_0}^t
    \omega dt$

    \item Consider a rigid body consisting of $n$ particles, with
    masses $m_i$ and positions $\vect{r}_i$ relative to a fixed
    point in the body's coordinate system moving with velocity
    $\vect{V}$ relative to some fixed coordinate system.  Then
    the velocity of the particle (in the fixed system) is given
    by:
    \begin{equation}
     \vect{v}_i=\vect{V}+\vect{\omega}\times\vect{r}_i
    \end{equation}

    \item The angular acceleration
   \end{enumerate}
  \item \textbf{Elements of Rotational Dynamics.}
   \begin{enumerate}[A.]
    \item \emph{Kinetic Energy.}  The kinetic energy of the system
    described above is
    \[K=\frac{1}{2}\sum_\alpha m_\alpha {v_i}^2 = \frac{1}{2}\sum_\alpha m_\alpha
    (\vect{V}+\vect{\omega}\times\vect{r}_\alpha)^2 \]
    Expanding this and using the fact that the origin of the body
    coordinate system coincides with the center of mass of the
    object, we obtain:
    \begin{equation}
     K=K_{trans}+K_{rot}
    \end{equation}
    Where $K_{trans}$, the translational kinetic energy, is just
    \begin{equation}
     K_{trans}=\frac{1}{2}\sum_\alpha m_\alpha V^2 = \frac{1}{2} M V^2
    \end{equation}
    and $K_{rot}$, the rotational kinetic energy is
    \begin{equation}
     K_{rot}=\frac{1}{2}\sum_\alpha m_\alpha
     (\vect{\omega}\times\vect{r}_\alpha)^2
    \end{equation}
    Using the identity $(\vect{A}\times\vect{B})^2 = A^2 B^2 -
    (A\cdot B)^2$ we obtain
    \begin{equation}
     K_{rot}=\frac{1}{2}\sum_\alpha m_\alpha[\omega^2 {r_\alpha}^2 -
     (\vect{\omega}\cdot\vect{r}_\alpha)^2]
    \end{equation}
    Expanding into components,
    $\vect{r_\alpha}=(x_{\alpha,1},x_{\alpha,2},x_{\alpha,3})$ and
    $\vect{\omega}=(\omega_1,\omega_2,\omega_3)$, and then
    writing $\omega_i=\sum_j \delta_{ij} \omega_j$, we obtain the
    rotational kinetic energy for a rigid body:
    \begin{equation}
     K_{rot}=\frac{1}{2}\sum_{i,j} I_{ij} \omega_i \omega_j
    \end{equation}
    where
    \begin{equation}
     I_{ij}=\sum_\alpha m_\alpha \left( \delta_{ij} \sum_k
     {x_{\alpha,k}}^2 - x_{\alpha,i} x_{\alpha,j} \right)
    \end{equation}
    are the components of the tensor of inertia.

    \item \emph{The Tensor of Inertia.} Using $x_1 = x,
    x_2 = y, x_3 = z$, the tensor of inertia can be written:
    \begin{equation}
     \textbf{I} = \left[
  \begin{array}{ccc}
    \sum_\alpha m_\alpha ({y_\alpha}^2 + {z_\alpha}^2) &
    -\sum m_\alpha x_\alpha y_\alpha &
    -\sum m_\alpha x_\alpha z_\alpha \\
    -\sum m_\alpha y_\alpha x_\alpha &
    \sum_\alpha m_\alpha ({x_\alpha}^2 + {z_\alpha}^2) &
    -\sum m_\alpha y_\alpha z_\alpha \\
    -\sum m_\alpha z_\alpha x_\alpha &
    -\sum m_\alpha z_\alpha y_\alpha &
    \sum_\alpha m_\alpha ({x_\alpha}^2 + {y_\alpha}^2)
  \end{array}
  \right]
    \end{equation}
    The elements on the diagonal are called the moments of inertia.
    Note that ${y_\alpha}^2 + {z_\alpha}^2 = {r_\alpha}^2 -
    {z_\alpha}^2$ is just the distance of $m_\alpha$ from the $x$
    axis, and so on. The off diagonal elements are called the
    products of inertia.  Note that the tensor is symmetric, i.e.,
    $I_{ij}=I_{ji}$.  In most cases, we deal with solid rigid
    bodies, in which case the sums become integrals:
    \begin{equation}
     I_{ij} = \int_V \rho(\vect{r}) \left( \delta_{ij} \sum_k
     {x_k}^2 - x_i x_j \right) dV
    \end{equation}

    \item \emph{Steiner's Parallel Axis Theorem.} Given the tensor
    of inertia of a body about its center of mass, $\textbf{I}$, the
    tensor about some other point separated from the center of
    mass by $\vect{a}=(a_1,a_2,a_3)$ is given by:
    \begin{equation}
     J_{ij} = I_{ij} + M (a^2 \delta_{ij} - a_i a_j)
    \end{equation}
    where $M$ is the total mass of the body.

    \item \emph{Angular Momentum.} We know that
    \[ \vect{L} = \sum_\alpha \vect{r}_\alpha \times
    \vect{p}_\alpha \] is the angular momentum of the body.
    Using $\vect{p}_\alpha = m_\alpha v_\alpha = m_\alpha
    \vect{\omega}_\alpha \times \vect{r}_\alpha$ and the
    vector identity $\vect{A} \times (\vect{B} \times
    \vect{A}) = A^2\vect{B} - (\vect{A} \cdot \vect{B})
    \vect{A}$, we find
    \begin{equation}
     \vect{L} = \sum_\alpha [ {r_\alpha}^2 \vect{\omega} -
      (\vect{r}_\alpha \cdot \vect{\omega}) \vect{r}_\alpha]
    \end{equation}
    We expand this into components as we did for $K$ and find that
    the angular momentum vector is given by:
    \begin{equation}\label{Lcomp}
     L_i = \sum_j I_{ij} \omega_j
    \end{equation}
    We can write this as
    \begin{equation}
     \vect{L} = \textbf{I} \cdot \vect{\omega}
    \end{equation}
    By multiplying \ref{Lcomp} by $\frac{1}{2} \omega_i$ and
    summing over i, we obtain:
    \begin{equation}
      K_{rot} = \frac{1}{2} \vect{\omega} \cdot \vect{L}
    \end{equation}
   \end{enumerate}

  \item \textbf{Principle Axes of Inertia and Euler's Equations of Motion}
   \begin{enumerate}[A.]
    \item \emph{Diagonalization of the Inertia Tensor.} We would
    like to write $I_{ij} = I_i \delta_{ij}$ so that we have the
    simple relations
    \begin{equation}
     K_{rot} = \frac{1}{2} \sum_i I_i \omega_i^2
    \end{equation}
    and
    \begin{equation}
     L_i = I_i \omega_i
    \end{equation}
    In order to do this, we simply need find the eigenvalues and
    eigenvectors of the inertia tensor.  The three eigenvalues so
    obtained are the moments of inertia about the object's three
    principle axes, the direction of which are specified by the
    corresponding eigenvectors.

    \item \emph{Eulerian Angles.} We wish to define a transformation
    from the body coordinate system, ${x'}_i$ to the fixed system,
    $x_i$.  This is most commonly using the Eulerian angles defined
    as follows:
    \begin{enumerate}[1.]
     \item Rotate counterclockwise about the $x'_3$ axis through
     an angle $\phi$, yielding the $x''_i$ axes.  The transformation
     matrix is:
     \begin{equation}
     \boldsymbol{\lambda}_\phi = \left(
      \begin{array}{ccc}
       \cos \phi & \sin \phi & 0 \\
       -\sin \phi & \cos \phi & 0 \\
       0 & 0 & 1
      \end{array} \right)
     \end{equation}

     \item Rotate counterclockwise about the $x''_1$ axis through an
     angle $\theta$, yielding the $x'''_i$ axes.  The transformation
     matrix is:
     \begin{equation}
     \boldsymbol{\lambda}_\theta = \left(
      \begin{array}{ccc}
       1 & 0 & 0 \\
       0 & \cos \theta & \sin \theta \\
       0 & -\sin \theta & \cos \theta
      \end{array} \right)
     \end{equation}

     \item Rotate counterclockwise about the $x'''_3$ axis through
     an angle $\psi$, yielding the $x_i$ axes.  The transformation
     matrix is:
     \begin{equation}
     \boldsymbol{\lambda}_\psi = \left(
      \begin{array}{ccc}
       \cos \psi & \sin \psi & 0 \\
       -\sin \psi & \cos \psi & 0 \\
       0 & 0 & 1
      \end{array} \right)
     \end{equation}
    \end{enumerate}
    The fixed system is now related to the body system by
    \begin{equation}
     \vect{x} = \boldsymbol{\lambda}_\phi \boldsymbol{\lambda}_\theta
      \boldsymbol{\lambda}_\psi \vect{x'} =
      \boldsymbol{\lambda} \vect{x'}
    \end{equation}

    \item Next, we wish to relate the components of the angular
    velocities in $x'_i$ and $x_i$.  In $x_i$, we have
    $\vect{\omega}=(\omega_1,\omega_2,\omega_3)$.  Now consider
    the three vectorial angular velocities:
    \begin{itemize}
     \item $\vect{\dot{\phi}}$, directed along the $x'_3$ (fixed) axis
     \item $\vect{\dot{\theta}}$, directed along the line of nodes
     \item $\vect{\dot{\psi}}$, directed along the $x_3$ (body) axis
    \end{itemize}
    The line of nodes is the line common to the $x_1 - x_2$ and
    $x'_1 - x'_2$ planes.  Now the angular velocities are related
    by:

    \[ \omega_1 = \dot{\phi}_1 + \dot{\theta}_1 + \dot{\psi}_1 =
    \dot{\phi} \sin \theta \sin \psi + \dot{\theta} \cos \psi \]

    \begin{equation}\label{Euler1}
     \omega_2 = \dot{\phi}_2 + \dot{\theta}_2 + \dot{\psi}_2 =
      \dot{\phi} \sin \theta \cos \psi - \dot{\theta} \sin \psi
    \end{equation}

    \[ \omega_3 = \dot{\phi}_3 + \dot{\theta}_3 + \dot{\psi}_3 =
    \dot{\phi} \cos \theta + \dot{\psi} \]

   \item \emph{Euler's Equations of Motion for Force Free Motion.}
   Let us choose the $x_i$ axes to correspond to the principle
   axes of the body so that
   \[ K = \frac{1}{2} \sum_i I_i \omega^2_i \]
   Now, let the Eulerian angles be the generalized coordinates,
   then Lagrange's equation for $\psi$ is
   \[ \PD{K}{\psi} - \frac{d}{dt} \PD{K}{\dot{\psi}} = 0 \]
   This can be written
   \[ \sum_i \PD{K}{\omega_i} \PD{\omega_i}{\psi} - \frac{d}{dt}
   \sum_i \PD{K}{\omega_i} \PD{\omega_i}{\dot{\psi}_i} = 0 \]

   Differentiating equations (\ref{Euler1}), we find
   $\PD{\omega_1}{\psi} = \omega_2$, $\PD{\omega_2}{\psi} =
   -\omega_1$, and $\PD{\omega_2}{\psi} = 0$.  Now, we also have
   $\PD{K}{\omega_i}=I_i \omega_i$.  This gives us
   \[ (I_1 - I_2) \omega_1 \omega_2 - I_3 \dot{\omega}_3 = 0 \]
   Because the choice of a particular principle axis as $x_3$ is
   completely arbitrary, we can permute this equation to obtain
   Euler's equations for force free motion:


   \end{enumerate}

  \item has
\end{enumerate}

\end{document}
