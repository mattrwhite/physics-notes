% ----------------------------------------------------------------
% Electromagnetic Field Theory ***********************************
% ----------------------------------------------------------------
\documentclass{article}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{booktabs}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
\reversemarginpar
% MATH -----------------------------------------------------------
\input{../Include/Essentials.tex}
\input{../Include/Vectors.tex}
\input{../Include/EM.tex}
\newcommand{\MST}{\tilde{\vect{T}}} %The Maxwell Stress Tensor
\newcommand{\momdensity}{\mathrm{p}}
\newcommand{\Umech}{U_\textrm{mech}}
\newcommand{\Pmech}{\vect{P}_\textrm{mech}}
\newcommand{\trzero}{t_{r0}}
% ----------------------------------------------------------------
% \draftmode
\begin{document}
% \maketitle
% ----------------------------------------------------------------
\begin{enumerate}[I.]
 \item \textbf{General Potentials and Fields.}
 \begin{enumerate}[A.]
  \item \emph{Relation of Fields to Potentials.}  Because the
  magnetic field $\B$ has no divergence, we can write it as the
  curl of vector potential:
  \( \label{eq:B} \B = \curl{A} .\)
  In general, the electric field $\E$ has nonzero divergence and
  curl.  However, if we write
  \[ \curl{E} = -\PD{}{t} \B = -\PD{}{t} (\curl{A}), \]
  we can see that the quantity $\E + \PD{}{t} \A$ always
  has zero curl and hence can be written as the gradient of a scalar
  field $V$.  We then have (using the conventional minus sign in
  front of $\del V$):
  \( \label{eq:E} \E = -\del V - \PD{\A}{t} . \)

  Combining (\ref{eq:B}) and (\ref{eq:E}) with Maxwell's equations we
  obtain
  \( \label{eq:Vwave} \nabla^2 V + \PD{}{t} (\diverge{A})
   = -\frac{\rho}{\epsilon_0} \)
  and
  \( \label{eq:Awave} \parenth{\del^2 V - \mu_0 \epsilon_0 \PDD{\A}{t}}
  - \del \parenth{\diverge{A} + \mu_0 \epsilon_0 \PD{V}{t}} = -\mu_0
  \vect{J}. \)
  These two equations contain all the information of Maxwell's
  equations.

  \item \emph{Gauges.}  It is easy to show that given any scalar
  field $\lambda$, the transformation
  \( \label{eq:gauge} \A \To \A + \del \lambda, \eqnsep V \To V -
  \PD{\lambda}{t} \)
  leaves the fields $\E$ and $\B$ unchanged.  Because the fields are
  the only physical entity - the potentials are just helpful mathematical
  tools - we have complete freedom in choosing $\lambda$ (this is
  equivalent to choosing $\diverge{A}$).  This freedom is called
  gauge freedom; equations (\ref{eq:gauge}) are called gauge
  transformations.
  \begin{enumerate}[1.]
   \item \emph{The Coulomb Gauge.} In the Coulomb gauge, we choose
   $\diverge{A} = 0$.  This is the best choice for electrostatics and
   magnetostatics,  but it makes (\ref{eq:Awave}) particularly ugly.
   Although
   $V$ is easy to calculate in the Coulomb gauge, it is calculated
   from the charge distribution at the present moment, regardless of
   its distance.  Special relativity, however, dictates that the fields cannot
   change instantaneously in response to changes in the distribution.  This
   means that the vector potential must also change so as to keep the
   fields from changing until the appropriate time.  Clearly, the
   Coulomb gauge is not appropriate for non-static situations.

   \item \emph{The Lorentz Gauge.}  In the Lorentz (or Lorenz) gauge,
   we choose
   \begin{namedeqn}{\textbf{Lorentz Gauge Condition.}}
   \diverge{A} = -\mu_0 \epsilon_0 \PD{V}{t} .
   \end{namedeqn}
   With this choice, equations (\ref{eq:Vwave}) and (\ref{eq:Awave})
   become
   \begin{subequations}\label{eq:LorentzWave}
   \begin{namedalign}{\textbf{Field Equations in the Lorentz Gauge}}
    \square^2 V &= -\frac{\rho}{\epsilon_0}, \\
    \square^2 \A &= -\mu_0 \vect{J},
   \end{namedalign}
   \end{subequations}
   where
   \( \square^2 \defn \nabla^2 - \mu_0 \epsilon_0 \PDD{}{t} \)
   is called the d'Alembertian.  These equations treat $V$ and $\A$
   as waves propagating at the speed of light, with $\rho$ and
   $\vect{J}$ as the sources.

   From now on, we will work in the Lorentz gauge unless stated
   otherwise.
  \end{enumerate}

  \item \emph{Potentials and Fields of an Arbitrary Distribution.}
  \begin{enumerate}[1.]
   \item \emph{Retarded Time; The Potentials.} Special relativity
   states that no information can travel faster than light.  It
   follows that if a charge distribution changes, the the fields it
   generates cannot change instantaneously everywhere.  Rather
   (assuming the fields change as soon as they can), it is reasonable
   to conclude that the field at a point $\vect{r}$ at a time $t$ is
   determined by the charge distribution at a time
   \( t_r = t - \frac{\Disp}{c}. \)
   This is called the retarded time.  Note that it is different for
   each point of the source distribution.

   The potentials for an arbitrary charge and current distribution
   are then
   \begin{subequations}\label{eq:retPot}
   \begin{namedalign}{\textbf{Retarded Potentials}}
    V(\vect{r},t) &= \kay \int \frac{\rho(\vect{r}',t_r)}{\Disp} d\Vol' , \\
    \A (\vect{r},t) &= \frac{\mu_0}{4 \pi} \int
    \frac{\vect{J}(\vect{r}',t_r)}{\Disp} d\Vol',
   \end{namedalign}
   \end{subequations}
   as can be verified by substitution into (\ref{eq:LorentzWave}).

   \item \emph{The Fields: Jefimenko's Equations.} Plugging
   equations (\ref{eq:retPot}) into (\ref{eq:B}) and (\ref{eq:E}), we
   obtain Jefimenko's equations, which give $\E$ and $\B$ for an
   arbitrary distribution:
   \begin{subequations}
   \( \E (\vect{r},t) = \kay \int \bracket{
    \frac{\rho(\vect{r}',t_r)}{\Disp^2} \DispHat
    + \frac{\dot{\rho}(\vect{r}',t_r)}{c \Disp} \DispHat
    - \frac{\dot{\vect{J}}(\vect{r}',t_r)}{c^2 \Disp} } d\Vol' \)
   and
   \( \B (\vect{r},t) = \frac{\mu_0}{4 \pi} \int \bracket{
   \frac{\vect{J}(\vect{r},t_r)}{\Disp^2}
   +\frac{\dot{\vect{J}}(\vect{r}',t_r)}{c\Disp}}\times \DispHat
   d\Vol' .\)
   \end{subequations}
   It is almost always easier to calculate the potentials and then
   obtain the fields using (\ref{eq:B}) and (\ref{eq:E}).

  \end{enumerate}
 \end{enumerate}

 \item \textbf{Relativistic Electromagnetism.}
 \begin{enumerate}[A.]
  \item \emph{How the Fields Transform.}  Let the fields in a frame
  $S$ be given by $\E$ and $\B$.  Then the fields in a frame $S'$
  moving at speed $v$ in the $x$ direction relative to $S$ are (see
  Griffiths 12.3.2):
  \begin{namedeqn}{\textbf{Transformation of $\E$ and $\B$}}
  \label{eq:EBtransform}
  \begin{aligned}
   E'_x &= E_x, & B'_x &= B_x, \\
   E'_y &= \gamma (E_y - v B_z), & B'_y &= \gamma (B_y + \frac{v}{c^2}
   E_z), \\
   E'_z &= \gamma (E_z + v B_y), & B'_z &= \gamma (B_z - \frac{v}{c^2}
   E_y).
  \end{aligned}
  \end{namedeqn}
  As usual $\gamma = (1-\beta^2)^{-1/2}$, $\beta = v/c$.
  If $\B = 0$ in $S$,
  \( \B' = -\frac{1}{c^2} (\vect{v} \times \E'). \)
  If $\E = 0$ in $S$,
  \( \E' = \vect{v} \times \B' . \)

  In light of these results, we can think of the magnetic field as
  nothing more than the electric field viewed through the lens of
  relativity, rather than a distinct phenomenon.  Then it comes as no
  surprise that no magnetic monopoles have ever been found.
  See Griffiths 12.3.1 and Purcell 5.9 for more.

  \item \emph{The Field Tensor.} The above transformations can be
  expressed more compactly if we introduce the electromagnetic field
  tensor (an antisymmetric tensor of rank two):
  \begin{namedeqn}{\textbf{The EM Field Tensor}}
   F^{\mu \nu} \defn
    \begin{pmatrix}
    0 & E_x/c & E_y/c & E_z/c \\
    -E_x/c & 0 & B_z & -B_y \\
    -E_y/c & -B_z & 0 & B_x \\
    -E_z/c & B_y & -B_x & 0
  \end{pmatrix} .
  \end{namedeqn}
  We also introduce the dual matrix of $F^{\mu \nu}$:
  \begin{equation}
   G^{\mu \nu} \defn
    \begin{pmatrix}
    0 & B_x & B_y & B_z \\
    -B_x & 0 & -E_z/c & E_y/c \\
    -B_y & E_z/c & 0 & -E_x/c \\
    -B_z & -E_y/c & E_x/c & 0
  \end{pmatrix} .
  \end{equation}
  These tensors transform normally under the Lorentz transformation,
  that is, the transformations in (\ref{eq:EBtransform}) can be
  written as (using the Einstein summation notation):
  \( F'^{\mu \nu} = \Lambda^\mu_\lambda \Lambda^\nu_\sigma F^{\lambda
  \sigma}, \)
  where
 \begin{equation}
   \Lambda^\mu_\nu = \begin{pmatrix}
    \gamma & -\gamma \beta & 0 & 0 \\
    -\gamma \beta & \gamma & 0 & 0 \\
    0 & 0 & 1 & 0 \\
    0 & 0 & 0 & 1
  \end{pmatrix} .
  \end{equation}
  is the Lorentz transformation matrix.

  The following products are invariant quantities (i.e., the same in
  all frames):
  \begin{subequations}
  \begin{align}
   F^{\mu \nu} F_{\mu \nu} &= -G^{\mu \nu} G_{\mu \nu} = -2(E^2/c^2 - B^2), \\
   F^{\mu \nu} G_{\mu \nu} &= -\frac{4}{c} (\dotpd{E}{B}).
  \end{align}
  \end{subequations}
  Note the implied double summation over $\mu$ and $\nu$.

  \item \emph{The Current Density Four-Vector and Maxwell's
  Equations.} Let us define the current density four-vector:
  \begin{namedeqn}{\textbf{Current Density Four-Vector}}
   J^\mu \defn (c\rho, J_x, J_y, J_z).
  \end{namedeqn}
  Local conservation of charge, i.e., the continuity equation
  requires that
  \( \PD{J^\mu}{x^\mu} = 0 , \)
  where summation over $\mu$ is implied.
  Meanwhile, Maxwell's equations can be written as
  \begin{subequations}
  \begin{namedalign}{\textbf{Maxwell's Equations: Tensor Notation}}
   \label{eq:MaxTensor1} \PD{F^{\mu \nu}}{x^\nu} &= \mu_0 J^\mu , \\
   \label{eq:MaxTensor2} \PD{G^{\mu \nu}}{x^\nu} &= 0.
  \end{namedalign}
  \end{subequations}
  Note that summation over $\nu$ is implied.  Finally, the Lorentz
  force law can be expressed in terms of the Minkowski force on a
  charge $q$:
  \( K^\mu = q \eta_\nu F^{\mu \nu} . \)

  \item \emph{The Four-Potential.}  We define the electromagnetic
  potential four-vector as
  \begin{namedeqn}{\textbf{Four-Potential}}
   A^\mu \defn (V/c, A_x, A_y, A_z).
  \end{namedeqn}
  We can then write
  \( F^{\mu \nu} = \PD{A^\nu}{x_\mu} - \PD{A^\mu}{x_\nu} . \)
  Note the covariant (as opposed to contravariant) derivatives. This
  expression automatically satisfies (\ref{eq:MaxTensor2}), while
  (\ref{eq:MaxTensor1}) becomes
  \( \label{eq:MaxPot} \PD{}{x_\mu} \parenth{\PD{A^\nu}{x^\nu}}
   - \PD{}{x_\nu} \parenth{\PD{A^\mu}{x^\nu}} = \mu_0 J^\mu .\)
  In the Lorentz gauge we have (note the contravariant derivative)
  \( \PD{A^\mu}{x^\nu} = 0 , \)
  so (\ref{eq:MaxPot}) becomes
  \begin{namedeqn}{\textbf{EM in One Equation}}
   \square^2 A^\mu = - \mu_0 J^\mu ,
  \end{namedeqn}
  where
  \( \square^2 \defn \PD{}{x_\nu} \PD{}{x^\nu}
   = \nabla^2 - \frac{1}{c^2} \PDD{}{t} \)
  is the usual d'Alembertian.
 \end{enumerate}

 \item \textbf{Energy and Momentum.}
  \begin{enumerate}[A.]
  \item \emph{Energy: Poynting's Theorem.}  Using the Lorentz Force
  Law, we find that the work done on a charge $q$ is given by
  \[ dW = \dotpd{F}{dl} = q(\E + \crosspd{v}{B})\cdot \vect{v} = q
  \dotpd{E}{v} dt . \]
  If we consider all the charges in a volume, we find that the work
  done per unit time, i.e., the power delivered, is
  \( \D{W}{t} = \int_\Vol (\dotpd{E}{J}) d\Vol .\)
  We can use Maxwell's laws to eliminate $\vect{J}$, arriving at
  \begin{namedeqn}{\textbf{Poynting's Theorem}}
   \D{W}{t} = -\D{}{t} \int_\Vol (U_e + U_m) d\Vol
    - \oint_\Surf \dotpd{S}{da} ,
  \end{namedeqn}
  where
  \( U_e = \frac{\epsilon_0}{2} E^2 \)
  is the usual energy density of the electric field,
  \( U_m = \frac{1}{2 \mu_0} B^2 \)
  is the usual energy density of the magnetic field, and
  \begin{namedeqn}{\textbf{The Poynting Vector}}
   \vect{S} \defn \frac{1}{\mu_0} (\crosspd{E}{B})
  \end{namedeqn}
  is called the Poynting vector.  The Poynting vector represents the
  flux of energy per unit area per unit time carried by the fields.

  If we let $\Umech$ represent the total non-electromagnetic energy
  density of a system, we have
  \( \D{W}{t} = \D{}{t} \int_\Vol \Umech d\Vol . \)
  Note that the integral on the right is just the total energy of the system.
  We can then write the differential version of Poynting's theorem as
  \( \PD{}{t} (\Umech + U_e + U_m) = - \diverge{S} .\)

  \item \emph{The Maxwell Stress Tensor.} Now let us consider the
  electromagnetic force per unit volume
  \( \vect{f} = \rho \E + \crosspd{J}{B} . \)
  Again using Maxwell's equations to eliminate $\vect{J}$ and $\rho$,
  we obtain
  \( \vect{f} = \del \cdot \MST - \epsilon_0 \mu_0 \PD{\vect{S}}{t} , \)
  where $\MST$ is the Maxwell stress tensor, given by
  \begin{namedeqn}{\textbf{The Maxwell Stress Tensor}}
   \MST_{ij} \defn \epsilon_0 \parenth{E_i E_j - \frac{1}{2}
   \delta_{ij} E^2} + \frac{1}{\mu_0} \parenth{B_i B_j - \frac{1}{2}
   \delta_{ij} B^2} .
  \end{namedeqn}
  Note that $i$ and $j$ range from one to three.  The dot product of a
  vector with a tensor such as $\MST$ is given by
  \( (\vect{a} \cdot \MST)_j = \sum_i a_i T_{ij}. \)
  In integral form, the total electromagnetic force on a volume
  of charges is
  \( \label{eq:EMForce} \vect{F} = \oint_\Surf \MST \cdot \vect{da}
   - \epsilon_0 \mu_0 \D{}{t} \int_\Vol \vect{S} d\Vol . \)
  Physically, $\MST$ is the force per unit area (stress) acting on
  the surface $\Surf$; the diagonal elements represent pressures
  while the off-diagonal elements represent shears.

  \item \emph{Momentum and Angular Momentum.}  Newton's second law
  states that
  \[ \vect{F} = \D{}{t} \Pmech . \]
  Combining this with (\ref{eq:EMForce}), we obtain
  \( \label{eq:Mom1} \D{}{t} \Pmech = -\D{}{t} \int_\Vol \momdensity_{em}
  d\Vol + \oint_\Surf \MST \cdot \vect{da}, \)
  where
  \( \momdensity_{em} = \mu_0 \epsilon_0 \vect{S} \)
  is the density of momentum \emph{in the electromagnetic fields}.
  Note that the fields can only carry momentum when both $\E$ and
  $\B$ are present.
  The first integral in (\ref{eq:Mom1}) represents the momentum stored
  in the fields while the second integral represents the momentum per
  unit time flowing through the surface $\Surf$.  If we introduce the
  mechanical momentum density $\momdensity_{\textrm{mech}}$, we can write
  (\ref{eq:Mom1}) in differential form:
  \( \PD{}{t} (\momdensity_{\textrm{mech}} + \momdensity_{em}) = \del \cdot
  \MST . \)
  From this expression, we can see that $-\MST$ is the momentum flux
  density, i.e., $-T_{ij}$ is the momentum in the $i$ direction
  crossing the surface oriented in the $j$ direction, per unit area,
  per unit time.

 \end{enumerate}

 \item \textbf{Radiation.}
  \begin{enumerate}[A.]
  \item \emph{Radiation.}
  When charges are accelerated, i.e., when currents change, they
  produce electromagnetic radiation which carries energy irreversibly
  away from the source.  The power carried by the fields into or out
  of a volume is given by
  \[ \oint_\Surf \dotpd{S}{da} , \]
  where $\Surf$ is the boundary of the volume.  Because we are
  interested in energy that is radiated to "infinity", we only need to
  consider the terms of the Poynting vector $\vect{S} =
  \frac{1}{\mu_0}(\crosspd{E}{B})$ which fall off no faster and $1/r^2$.
  This means that we need only consider the terms of $\E$ and $\B$
  which fall off no faster than $1/r$.  These components of the fields
  are called the radiation fields.

  \item \emph{Electric Dipole Radiation from an Arbitrary Source.}
  Let us consider the potential of an arbitrary charge distribution
  (localized near the origin), in light of the comments above:
  \[ V(\vect{r},t) = \kay \int \frac{\rho(\vect{r}',t - \Disp /
  c)}{\Disp} d\Vol' .\]
  Our first approximation is to assume that we are far from the
  charge distribution (i.e., at ``infinity''), so $r' \ll r$.  With
  this, $\Disp = \sqrt{r^2 + r'^2 - 2\dotpd{r}{r}'}$ becomes
  \[ \Disp \approx r \parenth{1-\frac{\dotpd{r}{r}'}{r^2}} , \]
  so
  \[ \frac{1}{\Disp} \approx \frac{1}{r}
  \parenth{1+\frac{\dotpd{r}{r}'}{r^2}} . \]
  Now we expand $\rho$ in a Taylor series about the retarded time at
  the origin $\trzero = t - r/c$, yielding
  \begin{multline} \rho(\vect{r}',t - \Disp / c) \approx \rho(\vect{r}',\trzero)
   \\ + \dot{\rho}(\vect{r}',\trzero) \parenth{\frac{\unitvect{r} \cdot
   \vect{r}'}{c}} + \frac{1}{2} \ddot{\rho}(\vect{r}',\trzero)
   \parenth{\frac{\unitvect{r} \cdot \vect{r}'}{c}}^2 + \ldots .
  \end{multline}
  We drop all but the first order terms in $r'$ by assuming that
  \[ r' \ll \frac{c}{\abs{\ddot{\rho}/\dot{\rho}}} ,
   \frac{c}{\abs{\dddot{\rho}/\dot{\rho}}^{1/2}} , \ldots . \]
  Plugging this back into the expression for $V$, we get
  \[ V(\vect{r},t) \approx \kay \bracket{\frac{Q}{r}
   + \frac{\unitvect{r} \cdot \vect{p}(\trzero)}{r^2}
   + \frac{\unitvect{r} \cdot \dot{\vect{p}}(\trzero)}{cr}}, \]
  where $Q$ is the net charge of the distribution and
  \[ \vect{p} = \int \vect{r}' \rho(\vect{r}',\trzero) d\Vol' \]
  is the dipole moment of the distribution (at $\trzero$).  It follows
  that we will be calculating the fields for electric dipole
  radiation from the distribution.

  Similarly, we find that
  \[ \A (\vect{r}, t) \approx \frac{\mu_0}{4 \pi} \frac{1}{r} \int
  \vect{J} (\vect{r}' , \trzero) d\Vol'
   = \frac{\mu_0}{4 \pi} \frac{\dot{\vect{p}}(\trzero)}{r}. \]

  Now, we calculate the fields using (\ref{eq:B}) and (\ref{eq:E}),
  discarding terms of order $1/r^2$ (or higher).  Finally, we arrive
  at the electric dipole radiation fields for an arbitrary charge
  distribution:
  \begin{subequations}
  \( \E (\vect{r} ,t) = \frac{\mu_0}{4 \pi r} [\unitvect{r} \times
  (\unitvect{r} \times \ddot{\vect{p}})] , \)
  \( \B (\vect{r} ,t) = \frac{\mu_0}{4 \pi c r}(\unitvect{r} \times
   \ddot{\vect{p}}) . \)
  \end{subequations}
  In spherical coordinates with $\ddot{\vect{p}}$ along $z$, we have
  \begin{subequations}
  \( \E (r,\theta,t) = \frac{\mu_0 \ddot{p}(\trzero)}{4 \pi}
  \parenth{\frac{\sin \theta}{r}} \unitvect{\theta} , \)
  \( \B (r,\theta,t) = \frac{\mu_0 \ddot{p}(\trzero)}{4 \pi c}
  \parenth{\frac{\sin \theta}{r}} \unitvect{\phi} . \)
  \end{subequations}
  Note that the fields are perpendicular to each other and to the
  direction of propagation ($\unitvect{r}$). The Poynting vector is
  \( \vect{S} = \frac{\mu_0}{16 \pi^2 c} [\ddot{p}(\trzero)]^2
  \parenth{\frac{\sin^2 \theta}{r^2}} \unitvect{r} , \)
  and the total power radiated is
  \( P = \oint \dotpd{S}{da} = \frac{\mu_0 \ddot{p}^2}{6 \pi c} . \)

  \item \emph{Electric Dipole Radiation from an Oscillating Perfect
  Dipole.} Let us take
  \( \vect{p}(t) = p_0 \cos (\omega t) \unitvect{z} , \)
  with $p_0 = q_0 d$.  In this case, the approximations above become
  $r \gg c/\omega \gg d$.  These
  amount to saying that we are far from the source and that the source
  is much smaller than the wavelength of radiation it produces.  The
  radiation fields are
  \begin{subequations}
  \( \E (r,\theta,t) = -\frac{\mu_0 p_0 \omega^2}{4 \pi}
  \parenth{\frac{\sin \theta}{r}} \cos [\omega(t-r/c)] \unitvect{\theta} , \)
  \( \B (r,\theta,t) = -\frac{\mu_0 p_0 \omega^2}{4 \pi c}
  \parenth{\frac{\sin \theta}{r}} \cos [\omega(t-r/c)] \unitvect{\phi} . \)
  \end{subequations}
  The average of the Poynting vector over a complete cycle is
  \( \langle \vect{S} \rangle = \frac{\mu_0 p_0^2 \omega^4}{32 \pi^2 c}
  \parenth{\frac{\sin^2 \theta}{r^2}} \unitvect{r} , \)
  and the average total power radiated is
  \( P = \frac{\mu_0 p_0^2 \omega^4}{12 \pi c} . \)

  \item \emph{Magnetic Dipole Radiation.}  By considering an
  oscillating magnetic dipole
  \( \vect{m} (t) = m_0 \cos (\omega t) \unitvect{z} \)
  and making a similar series of approximations (we need only consider
  $\A$ as we can take $V = 0$ since no net charge is present), we
  obtain the fields for magnetic dipole radiation (see Griffiths
  11.1.3):
  \begin{subequations}
  \( \E (r,\theta,t) = \frac{\mu_0 m_0 \omega^2}{4 \pi c}
  \parenth{\frac{\sin \theta}{r}} \cos [\omega(t-r/c)] \unitvect{\phi} , \)
  \( \B (r,\theta,t) = -\frac{\mu_0 m_0 \omega^2}{4 \pi c^2}
  \parenth{\frac{\sin \theta}{r}} \cos [\omega(t-r/c)] \unitvect{\theta} . \)
  The average of the Poynting vector over a complete cycle is
  \( \langle \vect{S} \rangle = \frac{\mu_0 m_0^2 \omega^4}{32 \pi^2 c^3}
  \parenth{\frac{\sin^2 \theta}{r^2}} \unitvect{r} , \)
  \end{subequations}
  and the average total power radiated is
  \( \langle P \rangle = \frac{\mu_0 m_0^2 \omega^4}{12 \pi c^3} . \)
  Noting the additional factor of $1/c$ in the fields, we can conclude
  that magnetic dipole radiation is significantly weaker than
  electric dipole radiation for comparable distributions.  In fact,
  magnetic dipole radiation is of the same order as electric
  quadrupole radiation.

 \end{enumerate}

 \item \textbf{Point Charges.}
  \begin{enumerate}[A.]
  \item \emph{The Lienard-Wiechert Potentials.}  With a little bit of
  work, we can calculate the potentials for a free charge $q$ moving
  at speed $\vect{v}$:
  \begin{subequations}
  \begin{namedeqn}{\textbf{The Lienard-Wiechert Potentials}}
   V (\vect{r},t) = \kay \frac{q c}{c\Disp - \DispV \cdot \vect{v}},
  \end{namedeqn}
  \( \A (\vect{r},t) = \frac{\mu_0}{4 \pi} \frac{qc\vect{v}}{c\Disp
  - \DispV \cdot \vect{v}} = \frac{\vect{v}}{c^2} V (\vect{r},t). \)
  \end{subequations}
  Here, $\DispV = \vect{r} - \vect{w}(t_r)$ and $\vect{v} = \vect{v}(t_r) =
  \dot{\vect{w}}(t_r)$, where $\vect{w}(t_r)$ is
  the position of the charge at the retarded time $t_r$.

  \item \emph{Fields of Point Charges; The Lorentz Force Law.}  Using
  (\ref{eq:B}) and (\ref{eq:E}), we can calculate the fields for a
  moving point charge.  They are:
  \begin{subequations}
  \begin{namedeqn}{\textbf{Fields of a Moving Charge}}
   \label{eq:MovingE}
   \E (\vect{r},t) = \frac{q}{4 \pi \epsilon_0} \frac{\Disp}{(\DispV
    \cdot \vect{u})^3} \bracket{(c^2 - v^2)\vect{u} + \DispV \times
    (\crosspd{u}{a})} ,
  \end{namedeqn}
  \( \label{eq:MovingB}
   \B (\vect{r},t) = \frac{\DispHat}{c} \times \E (\vect{r},t) . \)
  \end{subequations}
  Here $\DispV$ and $\vect{v}$ are as above, $\vect{a} = \dot{\vect{v}}(t_r)$
  and $\vect{u} \defn c \DispHat- \vect{v}$.

  For a point charge moving at constant velocity,
  \( \E (\vect{r},t) = \frac{q}{4 \pi \epsilon_0}
  \frac{1-\beta^2}{(1-\beta^2 \sin^2 \theta)^{3/2}}
  \frac{\unitvect{R}}{R^2} , \)
  where $\vect{R} \defn \vect{r} - \vect{v} t$ is the vector from the
  present (as opposed to the retarded) position of the particle to
  $\vect{r}$.  From (\ref{eq:MovingB}), we have
  \( \B (\vect{r},t) = \frac{1}{c^2} (\crosspd{v}{E}) .\)

  Using the above results, we obtain the exact form of the Lorentz
  force law:
  \begin{multline}
   \vect{F} = \frac{q Q}{4 \pi \epsilon_0} \frac{\Disp}{(\DispV
    \cdot \vect{u})^3} \left[ \bracket{(c^2 - v^2)\vect{u} + \DispV \times
    (\crosspd{u}{a})} \right. \\ + \frac{\vect{V}}{c} \left. \parenth{\DispHat \times
    \bracket{(c^2 - v^2)\vect{u} + \DispV \times (\crosspd{u}{a})}} \right] ,
  \end{multline}
  where $\vect{V}$ is the velocity of the second charge $Q$.

  \item \emph{Radiation from Point Charges.}  The radiation fields of
  a point charge are those terms of (\ref{eq:MovingE}) and
  (\ref{eq:MovingB}) which fall off no faster than $1/r$ (called the
  acceleration fields).  Namely,
  \( \E_{\textrm{rad}} = \frac{q}{4 \pi \epsilon_0} \frac{\Disp}{(\DispV \cdot
  \vect{u})^2} [\DispV \times (\crosspd{u}{a})] . \)
  $\B_{\textrm{rad}}$ follows directly from (\ref{eq:MovingB}). The Poynting
  vector is
  \( \vect{S}_{\textrm{rad}} = \frac{1}{\mu_0 c} E_{\textrm{rad}}^2 \DispHat . \)
  If the charge is instantaneously at rest (at $t_r$), then $\vect{u}
  = c \DispHat$ and we find
  \( \vect{S}_{\textrm{rad}} = \frac{\mu_0 q^2 a^2}{16 \pi^2 c}
  \parenth{\frac{\sin^2 \theta}{\Disp^2}} \DispHat , \)
  where $\theta$ is the angle between $\DispHat$ and $\vect{a}$.  In
  this case, the total power radiated is
  \begin{namedeqn}{\textbf{The Larmor Formula}}
   P = \frac{\mu_0 q^2 a^2}{6 \pi c} .
  \end{namedeqn}
  This is called the Larmor formula.  It holds well for $v \ll c$.
  Special relativity considerations allow us to generalize
  to any velocity; the result is called Lienard's formula:
  \begin{namedeqn}{\textbf{Lienard's Formula}}
   P = \frac{\mu_0 q^2 \gamma^6}{6 \pi c} \bracket{a^2
    - \abs{\frac{\crosspd{v}{a}}{c}}^2}.
  \end{namedeqn}
  Because of the $\gamma^6$ term, the radiated power increases
  tremendously with velocity.  The power radiated in a solid angle
  $d\Omega$ is given by
  \( \D{P}{\Omega} = \frac{q^2}{16 \pi^2 \epsilon_0} \frac{\abs{\DispHat
   \times (\crosspd{u}{a})}^2}{(\DispHat \cdot \vect{u})^5} \)
  If $\vect{v}$ and $\vect{a}$ are collinear, we have
  \( \D{P}{\Omega} = \frac{\mu_0 q^2 a^2}{16 \pi^2 \epsilon_0}
  \frac{\sin^2 \theta}{(1-\beta \cos \theta)^2} . \)
  Although no radiation is emitted precisely in the forward direction,
  at high speeds most the radiation is concentrated in an
  increasingly narrow cone about the forward direction.

  \item \emph{The Radiation Reaction.} Conservation of energy
  requires that a radiating charge slow down; its kinetic energy is
  converted to electromagnetic radiation.  Physically, the fields
  from one part of a charge exert force on another and these internal
  forces do not quite balance out, yielding a self-force!  This
  force, called the radiation reaction is (see Griffiths 11.2.2):
  \( \vect{F}_{rad} = \frac{\mu_0 q^2}{6 \pi c} \dot{\vect{a}} . \)
  This formula holds well for speeds far less than $c$.

 \end{enumerate}

\end{enumerate}

\end{document}
