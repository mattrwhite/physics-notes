% ----------------------------------------------------------------
% E&M Reference **************************************************
% ----------------------------------------------------------------
\documentclass[twocolumn]{article}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{booktabs}
\usepackage{txfonts}
\usepackage[letterpaper,margin=1in]{geometry}
% ----------------------------------------------------------------
\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
% Includes -----------------------------------------------------------
\input{../Include/Math.tex}
\input{../Include/Vectors.tex}
\input{../Include/EM.tex}
\input{../Include/Layout.tex}
%\renewcommand{\DispV}{(\vect{r} - \vect{r}')}
%\renewcommand{\Disp}{\norm{\vect{r} - \vect{r}'}}
%\renewcommand{\DispHat}{\frac{\DispV}{\Disp}}
% ----------------------------------------------------------------
\begin{document}
\title{Electromagnetism Reference}
\author{}
\date{}
%\maketitle
% ----------------------------------------------------------------
\paragraph{Fields and Charges}
The force on a charge $q$ due to an electric field $\vect{E}$ and a
magnetic field $\vect{B}$ is $\vect{F} = q(\E + \crosspd{v}{B})$,
where $\vect{v}$ is the velocity of the charge.  The conservation of
charge is expressed mathematically as $\diverge{J} = - (\tPD{\rho}{t}).$

The electric field due to an arbitrary charge distribution is
\[ \E(\vect{r}) = \kay \int \frac{\DispHat}{\Disp^2} dq , \]
where $\DispV = \vect{r} - \vect{r}'$.
For a point charge, $\E = \kay \frac{q}{\Disp^2} \DispHat$.

The magnetic field due to an arbitrary current distribution is
\[ \vect{B}(\vect{r}) = \frac{\mu_0}{4\pi} \int
 \frac{[\vect{J}(\vect{r}')d\Vol'] \times \DispV}{\Disp^2} . \]

\paragraph{Maxwell's Equations}
\begin{center}\begin{tabular}{ll} \toprule
SI units & cgs units \\ \midrule
$\diverge{E} = \rho/\epsilon_0$ & $\diverge{E} = 4 \pi \rho$ \\
$\curl{E} = -\PD{\vect{B}}{t}$
 & $\curl{E} = -\frac{1}{c}\PD{\vect{B}}{t}$ \\
$\diverge{B} = 0$ & $\diverge{B} = 0$ \\
$\curl{B} = \mu_0\vect{J} + \mu_0 \epsilon_0 \PD{\vect{E}}{t}$
 & $\curl{B} = \frac{4 \pi}{c}\vect{J} + \frac{1}{c} \PD{\vect{E}}{t}$ \\
\bottomrule
\end{tabular} \end{center}
In SI units, the Maxwell's equations in integral form are:
\begin{align*}
&\oint_\Surf \dotpd{E}{da} = \frac{Q_{encl}}{\epsilon_0}, \eqnsep
\oint_\Curve \dotpd{E}{dl} = -\PD{}{t} \int_\Surf \dotpd{B}{da}, \\
&\oint_\Surf \dotpd{B}{da} = 0, \eqnsep
\oint_\Curve \dotpd{B}{dl} = \mu_0
I_{\textrm{encl}} + \mu_0
 \epsilon_0 \PD{}{t} \int_\Surf \dotpd{E}{da} .
\end{align*}
The energy densities of the fields are $U_e = \epsilon_0 E^2 /2$ and
$U_m = B^2/2 \mu_0$. The momentum contained in the fields is
$\vect{P} = \epsilon_0 \mu_0 \int \vect{S} d\Vol$ where $\vect{S} =
(\crosspd{E}{B})/\mu_0$ is the Poynting vector.

\paragraph{Fields in Matter}  With
$\vect{D} = \epsilon_0 \E + \vect{P}$ and $\vect{H} = \vect{B}/\mu_0
- \vect{M}$, Maxwell's equations in matter are
\begin{center}\begin{tabular}{ll} \toprule
General & Linear media \\ \midrule
$\diverge{D} = \rho_f$ & $\diverge{E} = \rho/\epsilon$ \\
$\curl{E} = -\PD{\vect{B}}{t}$
 & $\curl{E} = -\PD{\vect{B}}{t}$ \\
$\diverge{B} = 0$ & $\diverge{B} = 0$ \\
$\curl{H} = \vect{J}_f + \PD{\vect{D}}{t}$
 & $\curl{B} = \mu \vect{J} + \mu \epsilon \PD{\vect{E}}{t}$ \\
\bottomrule
\end{tabular} \end{center}
In linear media, $\vect{D} = \epsilon \E$ and
$\vect{H} = \frac{1}{\mu} \vect{B}$.

\paragraph{Potentials} In electrostatics, the electric
potential is $V = -\int \dotpd{E}{dl}$, so $\E = -\del V$. Combining
this with Gauss's Law yields ${\nabla}^2 V = - \rho / \epsilon_0$.
For an arbitrary charge distribution,
\[V(\vect{r}) = \kay \int \frac{dq}{\Disp}. \]
For a point charge, $V(\vect{r}) = \kay \frac{q}{r}$.  Note that in
electrodynamics, $\E = -\del V - (\tPD{\vect{A}}{t})$.

The magnetic field is related to the magnetic vector potential
$\vect{A}$ by $\vect{B} = \curl{A}$. For an arbitrary current
distribution,
\[ \vect{A}(\vect{r}) = \frac{\mu_0}{4 \pi} \int
 \frac{\vect{J}(\vect{r}')}{\Disp} d\Vol' . \]

\paragraph{Boundary Conditions}
\begin{center}\begin{tabular}{ll} \toprule
General & Linear media \\ \midrule
$D_1^\perp - D_2^\perp = \sigma_f$
 &  $\epsilon_1 E_1^\perp - \epsilon_2 E_2^\perp = \sigma_f$\\
$\E_1^\parallel - \E_2^\parallel = 0$
 &  $\E_1^\parallel - \E_2^\parallel = 0$\\
$B_1^\perp - B_2^\perp = 0$ & $B_1^\perp - B_2^\perp = 0$ \\
$\vect{H}_1^\parallel- \vect{H}_2^\parallel
 = \vect{K}_f \times \unitvect{n}$
 &  $\frac{1}{\mu_1}\vect{B}_1^\parallel- \frac{1}{\mu_2}\vect{B}_2^\parallel
 = \vect{K}_f \times \unitvect{n}$\\
\bottomrule
\end{tabular} \end{center}

\paragraph{Relativistic EM}
If frame $S'$ is moving at speed $v$ in the $x$ direction relative to
frame $S$, the fields in $S$ and $S'$ are related by:
\begin{align*}
 E'_x &= E_x, & E'_y &= \gamma (E_y - v B_z), &
 E'_z &= \gamma (E_z + v B_y), \\
 B'_x &= B_x & B'_y &= \gamma (B_y + \frac{v}{c^2}
 E_z), & B'_z &= \gamma (B_z - \frac{v}{c^2}
 E_y).
\end{align*}
As usual, $\gamma = (1-\beta^2)^{-1/2}$, $\beta = v/c$.
If $\B = 0$ in $S$, $\B' = -\frac{1}{c^2} (\vect{v} \times \E').$
If $\E = 0$ in $S$, $\E' = \vect{v} \times \B'.$

\paragraph{Dipoles} The potential due to a dipole is
$V(\vect{r}) = \kay (\vect{p} \cdot \vect{\hat{r}}) / r^2$ where
$\vect{p} = \int \vect{r'} \rho(\vect{r'}) d \Vol'$ is the electric
dipole moment.  For point charges $q$ and $-q$ separated by
$\vect{d}$, $\vect{p} = q\vect{d}$.  The potential energy of a dipole
in a field $\E$ is $-\dotpd{p}{E}$.

The multipole expansion is
\[ V(\vect{r}) = \kay \sum_{n=0}^\infty \frac{1}{r^{n+1}} \int
 (r')^n P_n(\cos \theta') \rho(\vect{r'}) d \Vol' . \]

The magnetic dipole moment of a current loop carrying current $I$ is
$\vect{m} = I\vect{a}$, where $\vect{a}$ is the vector area of the loop.
The potential energy of a magnetic dipole is $-\dotpd{m}{B}$.


\end{document}
